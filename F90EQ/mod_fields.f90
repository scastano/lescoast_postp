module mod_fields
  use mod_typeKinds
  use mod_basicTypes
  use mod_grid
  implicit none 
  private

  character(len=30), parameter :: newres = 'new_res_forms.h5'
  type, public :: instantField
    type(vector) :: velocity
    type(scalar) :: pressure
    type(scalar) :: re
    type(tensor) :: velocityGradient
    type(vector) :: vorticity
    type(grid), pointer :: cGrid ! use target attribute on pointed-to grid
    ! active/passive scalars not implemented
  contains
    !procedure :: readInstantField => debugReadField
    procedure :: readInstantField => H5ReadField
    procedure :: printField => printInstantToVTK
  end type instantField

contains
  subroutine H5ReadField(this,gri,folder)
    use hdf5
    class(instantField), intent(in out) :: this
    class(grid), intent(in),target      :: gri
    character(*), intent(in)            :: folder

    integer :: sizx, sizy, sizz 
    integer :: si, sj, sk, ni, nj, nk

    real(kind=d_kind), dimension(:,:,:), allocatable :: x, y, z, v, re

    integer :: error
    integer(HID_T) :: file_id, dset_id 
    integer(HSIZE_T) :: data_dims(7)
    character(len=50) :: dsetname

    this%cGrid => gri
    sizx = this%cGrid%centroids%jx - this%cGrid%centroids%ix + 1
    sizy = this%cGrid%centroids%jy - this%cGrid%centroids%iy + 1
    sizz = this%cGrid%centroids%jz - this%cGrid%centroids%iz + 1
    si = this%cGrid%centroids%ix
    sj = this%cGrid%centroids%iy
    sk = this%cGrid%centroids%iz
    ni = this%cGrid%centroids%jx
    nj = this%cGrid%centroids%jy
    nk = this%cGrid%centroids%jz

    call this%velocity%alloc(si,sj,sk,ni,nj,nk)   
    call this%pressure%alloc(si,sj,sk,ni,nj,nk)  
    call this%re%alloc(si,sj,sk,ni,nj,nk)  

    allocate(x(si:ni,sj:nj,sk:nk)) 
    allocate(y(si:ni,sj:nj,sk:nk)) 
    allocate(z(si:ni,sj:nj,sk:nk)) 
    allocate(v(si:ni,sj:nj,sk:nk)) 
    allocate(re(si:ni,sj:nj,sk:nk)) 
    ! open H5 file 
    call h5open_f(error)

    call h5fopen_f(newres,H5F_ACC_RDWR_F,file_id,error)
    ! ***************************************************************************
    ! U velocity
    dsetname = trim(folder)//"/u"
    call h5dopen_f(file_id, dsetname, dset_id, error)

    data_dims = (/sizx,sizy,sizz,0,0,0,0/)
    call h5dread_f(dset_id, H5T_NATIVE_DOUBLE, x, data_dims, error)

    this%velocity%x = x
    call h5dclose_f(dset_id, error)
    ! ***************************************************************************
    ! V velocity
    dsetname = trim(folder)//"/v"
    call h5dopen_f(file_id, dsetname, dset_id, error)

    data_dims = (/sizx,sizy,sizz,0,0,0,0/)

    call h5dread_f(dset_id, H5T_NATIVE_DOUBLE, y, data_dims, error)

    this%velocity%y = y
    call h5dclose_f(dset_id, error)
    ! ***************************************************************************
    ! W velocity
    dsetname = trim(folder)//"/w"
    call h5dopen_f(file_id, dsetname, dset_id, error)

    data_dims = (/sizx,sizy,sizz,0,0,0,0/)

    call h5dread_f(dset_id, H5T_NATIVE_DOUBLE, z, data_dims, error)

    this%velocity%z = z
    call h5dclose_f(dset_id, error)
    ! ***************************************************************************
    ! FI 
    dsetname = trim(folder)//"/fi"
    call h5dopen_f(file_id, dsetname, dset_id, error)

    data_dims = (/sizx,sizy,sizz,0,0,0,0/)

    call h5dread_f(dset_id, H5T_NATIVE_DOUBLE, v, data_dims, error)

    this%pressure%v = v
    call h5dclose_f(dset_id, error)
    ! ***************************************************************************
    ! ***************************************************************************
    ! RE
    dsetname = trim(folder)//"/nuT"
    call h5dopen_f(file_id, dsetname, dset_id, error)

    data_dims = (/sizx,sizy,sizz,0,0,0,0/)

    call h5dread_f(dset_id, H5T_NATIVE_DOUBLE, re, data_dims, error)

    this%re%v = re
    call h5dclose_f(dset_id, error)
    ! ***************************************************************************

    call h5fclose_f(file_id, error)

    call gradVel(this)
    deallocate(x) 
    deallocate(y) 
    deallocate(z) 
    deallocate(v) 
    deallocate(re) 
  end subroutine H5ReadField
  subroutine debugReadField(this,gri)
    class(instantField) :: this
    class(grid), target :: gri
    
    integer :: i, j, k, si, sj, sk, ni, nj, nk, nscal
    real(kind=d_kind) :: ti, press, rho

    open(11,file='old_res_form',status='old')
    read(11,*)nscal
    read(11,*)ti
    read(11,*)press

    this%cGrid => gri

    si = this%cGrid%centroids%ix
    sj = this%cGrid%centroids%iy
    sk = this%cGrid%centroids%iz
    ni = this%cGrid%centroids%jx
    nj = this%cGrid%centroids%jy
    nk = this%cGrid%centroids%jz

    call this%velocity%alloc(si,sj,sk,ni,nj,nk)   
    call this%pressure%alloc(si,sj,sk,ni,nj,nk)   


    do k = sk, nk
      do j = sj, nj
        do i = si, ni
          read(11,*) this%velocity%x(i,j,k), this%velocity%y(i,j,k), &
                     this%velocity%z(i,j,k), this%pressure%v(i,j,k), rho
        end do
      end do
    end do
    close(11)
    call gradVel(this)
  end subroutine debugReadField
  subroutine gradVel(field)
    class(instantField), intent(in out) :: field

    integer :: i, j, k, si, sj, sk, ni, nj, nk

    real(kind=d_kind) :: ja11, ja12, ja13, ja21, ja22, ja23, &
                         ja31, ja32, ja33, ddcsi, ddeta, ddzit
    
    if((.not.associated(field%cGrid)).and.&
       (.not.field%cGrid%centroids%isInitialized)) &
      stop "ERROR: No grid associated to the instantaneous field"
    
    si = field%cGrid%centroids%ix
    sj = field%cGrid%centroids%iy
    sk = field%cGrid%centroids%iz
    ni = field%cGrid%centroids%jx
    nj = field%cGrid%centroids%jy
    nk = field%cGrid%centroids%jz

    call field%velocityGradient%alloc(si,sj,sk,ni,nj,nk) 
    call field%vorticity%alloc(si,sj,sk,ni,nj,nk) 
    ! into the field
    cfield: do k = sk+1, nk-1
      do j = sj+1, nj-1
        do i = si+1, ni-1
          ja11 = field%cGrid%jacobian%a11(i,j,k)
          ja12 = field%cGrid%jacobian%a12(i,j,k)
          ja13 = field%cGrid%jacobian%a13(i,j,k)
          ja21 = field%cGrid%jacobian%a21(i,j,k)
          ja22 = field%cGrid%jacobian%a22(i,j,k)
          ja23 = field%cGrid%jacobian%a23(i,j,k)
          ja31 = field%cGrid%jacobian%a31(i,j,k)
          ja32 = field%cGrid%jacobian%a32(i,j,k)
          ja33 = field%cGrid%jacobian%a33(i,j,k)

          ! du/dx du/dy du/dz
          field%velocityGradient%a11(i,j,k) = &
                                ja11*0.5*(field%velocity%x(i+1,j,k)-field%velocity%x(i-1,j,k)) + &
                                ja21*0.5*(field%velocity%x(i,j+1,k)-field%velocity%x(i,j-1,k)) + &
                                ja31*0.5*(field%velocity%x(i,j,k+1)-field%velocity%x(i,j,k-1)) 
          field%velocityGradient%a12(i,j,k) = &
                                ja12*0.5*(field%velocity%x(i+1,j,k)-field%velocity%x(i-1,j,k)) + &
                                ja22*0.5*(field%velocity%x(i,j+1,k)-field%velocity%x(i,j-1,k)) + &
                                ja32*0.5*(field%velocity%x(i,j,k+1)-field%velocity%x(i,j,k-1)) 
          field%velocityGradient%a13(i,j,k) = &
                                ja13*0.5*(field%velocity%x(i+1,j,k)-field%velocity%x(i-1,j,k)) + &
                                ja23*0.5*(field%velocity%x(i,j+1,k)-field%velocity%x(i,j-1,k)) + &
                                ja33*0.5*(field%velocity%x(i,j,k+1)-field%velocity%x(i,j,k-1)) 
          ! dv/dx dv/dy dv/dz
          field%velocityGradient%a21(i,j,k) = &
                                ja11*0.5*(field%velocity%y(i+1,j,k)-field%velocity%y(i-1,j,k)) + &
                                ja21*0.5*(field%velocity%y(i,j+1,k)-field%velocity%y(i,j-1,k)) + &
                                ja31*0.5*(field%velocity%y(i,j,k+1)-field%velocity%y(i,j,k-1)) 
          field%velocityGradient%a22(i,j,k) = &                                     
                                ja12*0.5*(field%velocity%y(i+1,j,k)-field%velocity%y(i-1,j,k)) + &
                                ja22*0.5*(field%velocity%y(i,j+1,k)-field%velocity%y(i,j-1,k)) + &
                                ja32*0.5*(field%velocity%y(i,j,k+1)-field%velocity%y(i,j,k-1)) 
          field%velocityGradient%a23(i,j,k) = &                                     
                                ja13*0.5*(field%velocity%y(i+1,j,k)-field%velocity%y(i-1,j,k)) + &
                                ja23*0.5*(field%velocity%y(i,j+1,k)-field%velocity%y(i,j-1,k)) + &
                                ja33*0.5*(field%velocity%y(i,j,k+1)-field%velocity%y(i,j,k-1)) 
          ! dw/dx dw/dy dw/dz
          field%velocityGradient%a31(i,j,k) = &
                                ja11*0.5*(field%velocity%z(i+1,j,k)-field%velocity%z(i-1,j,k)) + &
                                ja21*0.5*(field%velocity%z(i,j+1,k)-field%velocity%z(i,j-1,k)) + &
                                ja31*0.5*(field%velocity%z(i,j,k+1)-field%velocity%z(i,j,k-1)) 
          field%velocityGradient%a32(i,j,k) = &                                     
                                ja12*0.5*(field%velocity%z(i+1,j,k)-field%velocity%z(i-1,j,k)) + &
                                ja22*0.5*(field%velocity%z(i,j+1,k)-field%velocity%z(i,j-1,k)) + &
                                ja32*0.5*(field%velocity%z(i,j,k+1)-field%velocity%z(i,j,k-1)) 
          field%velocityGradient%a33(i,j,k) = &                                     
                                ja13*0.5*(field%velocity%z(i+1,j,k)-field%velocity%z(i-1,j,k)) + &
                                ja23*0.5*(field%velocity%z(i,j+1,k)-field%velocity%z(i,j-1,k)) + &
                                ja33*0.5*(field%velocity%z(i,j,k+1)-field%velocity%z(i,j,k-1)) 
        end do
      end do
    end do cfield
    ! faces
    faces56: do j = sj+1, nj-1
      do i = si+1, ni-1

        k = sk
        ja11 = field%cGrid%jacobian%a11(i,j,k)
        ja12 = field%cGrid%jacobian%a12(i,j,k)
        ja13 = field%cGrid%jacobian%a13(i,j,k)
        ja21 = field%cGrid%jacobian%a21(i,j,k)
        ja22 = field%cGrid%jacobian%a22(i,j,k)
        ja23 = field%cGrid%jacobian%a23(i,j,k)
        ja31 = field%cGrid%jacobian%a31(i,j,k)
        ja32 = field%cGrid%jacobian%a32(i,j,k)
        ja33 = field%cGrid%jacobian%a33(i,j,k)
        ! du/dx du/dy du/dz
        ddcsi = field%velocity%x(i+1,j,k)-field%velocity%x(i-1,j,k)
        ddeta = field%velocity%x(i,j+1,k)-field%velocity%x(i,j-1,k)
        ddzit =(field%velocity%x(i,j,k+1)-field%velocity%x(i,j,k))*2.
        field%velocityGradient%a11(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a12(i,j,k) =    &
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a13(i,j,k) =    &
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
        ! dv/dx dv/dy dv/dz
        ddcsi = field%velocity%y(i+1,j,k)-field%velocity%y(i-1,j,k)
        ddeta = field%velocity%y(i,j+1,k)-field%velocity%y(i,j-1,k)
        ddzit =(field%velocity%y(i,j,k+1)-field%velocity%y(i,j,k))*2.
        field%velocityGradient%a21(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a22(i,j,k) =    &                                     
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a23(i,j,k) =    &                                     
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
        ! dw/dx dw/dy dw/dz
        ddcsi = field%velocity%z(i+1,j,k)-field%velocity%z(i-1,j,k)
        ddeta = field%velocity%z(i,j+1,k)-field%velocity%z(i,j-1,k)
        ddzit =(field%velocity%z(i,j,k+1)-field%velocity%z(i,j,k))*2.
        field%velocityGradient%a31(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a32(i,j,k) =    &                                     
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a33(i,j,k) =    &                                     
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
        k = nk
        ja11 = field%cGrid%jacobian%a11(i,j,k)
        ja12 = field%cGrid%jacobian%a12(i,j,k)
        ja13 = field%cGrid%jacobian%a13(i,j,k)
        ja21 = field%cGrid%jacobian%a21(i,j,k)
        ja22 = field%cGrid%jacobian%a22(i,j,k)
        ja23 = field%cGrid%jacobian%a23(i,j,k)
        ja31 = field%cGrid%jacobian%a31(i,j,k)
        ja32 = field%cGrid%jacobian%a32(i,j,k)
        ja33 = field%cGrid%jacobian%a33(i,j,k)
        ! du/dx du/dy du/dz
        ddcsi = field%velocity%x(i+1,j,k)-field%velocity%x(i-1,j,k)
        ddeta = field%velocity%x(i,j+1,k)-field%velocity%x(i,j-1,k)
        ddzit =(field%velocity%x(i,j,k)-field%velocity%x(i,j,k-1))*2.
        field%velocityGradient%a11(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a12(i,j,k) =    &
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a13(i,j,k) =    &
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
        ! dv/dx dv/dy dv/dz
        ddcsi = field%velocity%y(i+1,j,k)-field%velocity%y(i-1,j,k)
        ddeta = field%velocity%y(i,j+1,k)-field%velocity%y(i,j-1,k)
        ddzit =(field%velocity%y(i,j,k)-field%velocity%y(i,j,k-1))*2.
        field%velocityGradient%a21(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a22(i,j,k) =    &                                     
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a23(i,j,k) =    &                                     
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
        ! dw/dx dw/dy dw/dz
        ddcsi = field%velocity%z(i+1,j,k)-field%velocity%z(i-1,j,k)
        ddeta = field%velocity%z(i,j+1,k)-field%velocity%z(i,j-1,k)
        ddzit =(field%velocity%z(i,j,k)-field%velocity%z(i,j,k-1))*2.
        field%velocityGradient%a31(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a32(i,j,k) =    &                                     
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a33(i,j,k) =    &                                     
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
      end do
    end do faces56
    faces34: do k = sk+1, nk-1
      do i = si+1, ni-1

        j = sj
        ja11 = field%cGrid%jacobian%a11(i,j,k)
        ja12 = field%cGrid%jacobian%a12(i,j,k)
        ja13 = field%cGrid%jacobian%a13(i,j,k)
        ja21 = field%cGrid%jacobian%a21(i,j,k)
        ja22 = field%cGrid%jacobian%a22(i,j,k)
        ja23 = field%cGrid%jacobian%a23(i,j,k)
        ja31 = field%cGrid%jacobian%a31(i,j,k)
        ja32 = field%cGrid%jacobian%a32(i,j,k)
        ja33 = field%cGrid%jacobian%a33(i,j,k)
        ! du/dx du/dy du/dz
        ddcsi = field%velocity%x(i+1,j,k)-field%velocity%x(i-1,j,k)
        ddeta =(field%velocity%x(i,j+1,k)-field%velocity%x(i,j,k))*2.
        ddzit = field%velocity%x(i,j,k+1)-field%velocity%x(i,j,k-1)
        field%velocityGradient%a11(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a12(i,j,k) =    &
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a13(i,j,k) =    &
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
        ! dv/dx dv/dy dv/dz
        ddcsi = field%velocity%y(i+1,j,k)-field%velocity%y(i-1,j,k)
        ddeta =(field%velocity%y(i,j+1,k)-field%velocity%y(i,j,k))*2.
        ddzit = field%velocity%y(i,j,k+1)-field%velocity%y(i,j,k-1)
        field%velocityGradient%a21(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a22(i,j,k) =    &                                     
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a23(i,j,k) =    &                                     
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
        ! dw/dx dw/dy dw/dz
        ddcsi = field%velocity%z(i+1,j,k)-field%velocity%z(i-1,j,k)
        ddeta =(field%velocity%z(i,j+1,k)-field%velocity%z(i,j,k))*2.
        ddzit = field%velocity%z(i,j,k+1)-field%velocity%z(i,j,k-1)
        field%velocityGradient%a31(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a32(i,j,k) =    &                                     
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a33(i,j,k) =    &                                     
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
        j = nj
        ja11 = field%cGrid%jacobian%a11(i,j,k)
        ja12 = field%cGrid%jacobian%a12(i,j,k)
        ja13 = field%cGrid%jacobian%a13(i,j,k)
        ja21 = field%cGrid%jacobian%a21(i,j,k)
        ja22 = field%cGrid%jacobian%a22(i,j,k)
        ja23 = field%cGrid%jacobian%a23(i,j,k)
        ja31 = field%cGrid%jacobian%a31(i,j,k)
        ja32 = field%cGrid%jacobian%a32(i,j,k)
        ja33 = field%cGrid%jacobian%a33(i,j,k)
        ! du/dx du/dy du/dz
        ddcsi = field%velocity%x(i+1,j,k)-field%velocity%x(i-1,j,k)
        ddeta =(field%velocity%x(i,j,k)-field%velocity%x(i,j-1,k))*2.
        ddzit = field%velocity%x(i,j,k+1)-field%velocity%x(i,j,k-1)
        field%velocityGradient%a11(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a12(i,j,k) =    &
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a13(i,j,k) =    &
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
        ! dv/dx dv/dy dv/dz
        ddcsi = field%velocity%y(i+1,j,k)-field%velocity%y(i-1,j,k)
        ddeta =(field%velocity%y(i,j,k)-field%velocity%y(i,j-1,k))*2.
        ddzit = field%velocity%y(i,j,k+1)-field%velocity%y(i,j,k-1)
        field%velocityGradient%a21(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a22(i,j,k) =    &                                     
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a23(i,j,k) =    &                                     
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
        ! dw/dx dw/dy dw/dz
        ddcsi = field%velocity%z(i+1,j,k)-field%velocity%z(i-1,j,k)
        ddeta =(field%velocity%z(i,j,k)-field%velocity%z(i,j-1,k))*2.
        ddzit = field%velocity%z(i,j,k+1)-field%velocity%z(i,j,k-1)
        field%velocityGradient%a31(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a32(i,j,k) =    &                                     
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a33(i,j,k) =    &                                     
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
      end do
    end do faces34
    faces12: do k = sk+1, nk-1
      do j = sj+1, nj-1

        i = si
        ja11 = field%cGrid%jacobian%a11(i,j,k)
        ja12 = field%cGrid%jacobian%a12(i,j,k)
        ja13 = field%cGrid%jacobian%a13(i,j,k)
        ja21 = field%cGrid%jacobian%a21(i,j,k)
        ja22 = field%cGrid%jacobian%a22(i,j,k)
        ja23 = field%cGrid%jacobian%a23(i,j,k)
        ja31 = field%cGrid%jacobian%a31(i,j,k)
        ja32 = field%cGrid%jacobian%a32(i,j,k)
        ja33 = field%cGrid%jacobian%a33(i,j,k)
        ! du/dx du/dy du/dz
        ddcsi =(field%velocity%x(i+1,j,k)-field%velocity%x(i,j,k))*2.
        ddeta = field%velocity%x(i,j+1,k)-field%velocity%x(i,j-1,k)
        ddzit = field%velocity%x(i,j,k+1)-field%velocity%x(i,j,k-1)
        field%velocityGradient%a11(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a12(i,j,k) =    &
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a13(i,j,k) =    &
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
        ! dv/dx dv/dy dv/dz
        ddcsi =(field%velocity%y(i+1,j,k)-field%velocity%y(i,j,k))*2.
        ddeta = field%velocity%y(i,j+1,k)-field%velocity%y(i,j-1,k)
        ddzit = field%velocity%y(i,j,k+1)-field%velocity%y(i,j,k-1)
        field%velocityGradient%a21(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a22(i,j,k) =    &                                     
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a23(i,j,k) =    &                                     
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
        ! dw/dx dw/dy dw/dz
        ddcsi =(field%velocity%z(i+1,j,k)-field%velocity%z(i,j,k))*2.
        ddeta = field%velocity%z(i,j+1,k)-field%velocity%z(i,j-1,k)
        ddzit = field%velocity%z(i,j,k+1)-field%velocity%z(i,j,k-1)
        field%velocityGradient%a31(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a32(i,j,k) =    &                                     
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a33(i,j,k) =    &                                     
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
        i = ni
        ja11 = field%cGrid%jacobian%a11(i,j,k)
        ja12 = field%cGrid%jacobian%a12(i,j,k)
        ja13 = field%cGrid%jacobian%a13(i,j,k)
        ja21 = field%cGrid%jacobian%a21(i,j,k)
        ja22 = field%cGrid%jacobian%a22(i,j,k)
        ja23 = field%cGrid%jacobian%a23(i,j,k)
        ja31 = field%cGrid%jacobian%a31(i,j,k)
        ja32 = field%cGrid%jacobian%a32(i,j,k)
        ja33 = field%cGrid%jacobian%a33(i,j,k)
        ! du/dx du/dy du/dz
        ddcsi =(field%velocity%x(i,j,k)-field%velocity%x(i-1,j,k))*2.
        ddeta = field%velocity%x(i,j+1,k)-field%velocity%x(i,j-1,k)
        ddzit = field%velocity%x(i,j,k+1)-field%velocity%x(i,j,k-1)
        field%velocityGradient%a11(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a12(i,j,k) =    &
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a13(i,j,k) =    &
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
        ! dv/dx dv/dy dv/dz
        ddcsi =(field%velocity%y(i,j,k)-field%velocity%y(i-1,j,k))*2.
        ddeta = field%velocity%y(i,j+1,k)-field%velocity%y(i,j-1,k)
        ddzit = field%velocity%y(i,j,k+1)-field%velocity%y(i,j,k-1)
        field%velocityGradient%a21(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a22(i,j,k) =    &                                     
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a23(i,j,k) =    &                                     
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
        ! dw/dx dw/dy dw/dz
        ddcsi =(field%velocity%z(i,j,k)-field%velocity%z(i-1,j,k))*2.
        ddeta = field%velocity%z(i,j+1,k)-field%velocity%z(i,j-1,k)
        ddzit = field%velocity%z(i,j,k+1)-field%velocity%z(i,j,k-1)
        field%velocityGradient%a31(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a32(i,j,k) =    &                                     
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a33(i,j,k) =    &                                     
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
      end do
    end do faces12
    ! edges
    edges34: do j = sj+1, nj-1

        k = sk
        i = si
        ja11 = field%cGrid%jacobian%a11(i,j,k)
        ja12 = field%cGrid%jacobian%a12(i,j,k)
        ja13 = field%cGrid%jacobian%a13(i,j,k)
        ja21 = field%cGrid%jacobian%a21(i,j,k)
        ja22 = field%cGrid%jacobian%a22(i,j,k)
        ja23 = field%cGrid%jacobian%a23(i,j,k)
        ja31 = field%cGrid%jacobian%a31(i,j,k)
        ja32 = field%cGrid%jacobian%a32(i,j,k)
        ja33 = field%cGrid%jacobian%a33(i,j,k)
        ! du/dx du/dy du/dz
        ddcsi =(field%velocity%x(i+1,j,k)-field%velocity%x(i,j,k))*2.
        ddeta = field%velocity%x(i,j+1,k)-field%velocity%x(i,j-1,k)
        ddzit =(field%velocity%x(i,j,k+1)-field%velocity%x(i,j,k))*2.
        field%velocityGradient%a11(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a12(i,j,k) =    &
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a13(i,j,k) =    &
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
        ! dv/dx dv/dy dv/dz
        ddcsi =(field%velocity%y(i+1,j,k)-field%velocity%y(i,j,k))*2.
        ddeta = field%velocity%y(i,j+1,k)-field%velocity%y(i,j-1,k)
        ddzit =(field%velocity%y(i,j,k+1)-field%velocity%y(i,j,k))*2.
        field%velocityGradient%a21(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a22(i,j,k) =    &                                     
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a23(i,j,k) =    &                                     
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
        ! dw/dx dw/dy dw/dz
        ddcsi =(field%velocity%z(i+1,j,k)-field%velocity%z(i,j,k))*2.
        ddeta = field%velocity%z(i,j+1,k)-field%velocity%z(i,j-1,k)
        ddzit =(field%velocity%z(i,j,k+1)-field%velocity%z(i,j,k))*2.
        field%velocityGradient%a31(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a32(i,j,k) =    &                                     
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a33(i,j,k) =    &                                     
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
        k = sk
        i = ni
        ja11 = field%cGrid%jacobian%a11(i,j,k)
        ja12 = field%cGrid%jacobian%a12(i,j,k)
        ja13 = field%cGrid%jacobian%a13(i,j,k)
        ja21 = field%cGrid%jacobian%a21(i,j,k)
        ja22 = field%cGrid%jacobian%a22(i,j,k)
        ja23 = field%cGrid%jacobian%a23(i,j,k)
        ja31 = field%cGrid%jacobian%a31(i,j,k)
        ja32 = field%cGrid%jacobian%a32(i,j,k)
        ja33 = field%cGrid%jacobian%a33(i,j,k)
        ! du/dx du/dy du/dz
        ddcsi =(field%velocity%x(i,j,k)-field%velocity%x(i-1,j,k))*2.
        ddeta = field%velocity%x(i,j+1,k)-field%velocity%x(i,j-1,k)
        ddzit =(field%velocity%x(i,j,k+1)-field%velocity%x(i,j,k))*2.
        field%velocityGradient%a11(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a12(i,j,k) =    &
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a13(i,j,k) =    &
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
        ! dv/dx dv/dy dv/dz
        ddcsi =(field%velocity%y(i,j,k)-field%velocity%y(i-1,j,k))*2.
        ddeta = field%velocity%y(i,j+1,k)-field%velocity%y(i,j-1,k)
        ddzit =(field%velocity%y(i,j,k+1)-field%velocity%y(i,j,k))*2.
        field%velocityGradient%a21(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a22(i,j,k) =    &                                     
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a23(i,j,k) =    &                                     
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
        ! dw/dx dw/dy dw/dz
        ddcsi =(field%velocity%z(i,j,k)-field%velocity%z(i-1,j,k))*2.
        ddeta = field%velocity%z(i,j+1,k)-field%velocity%z(i,j-1,k)
        ddzit =(field%velocity%z(i,j,k+1)-field%velocity%z(i,j,k))*2.
        field%velocityGradient%a31(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a32(i,j,k) =    &                                     
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a33(i,j,k) =    &                                     
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
        k = nk
        i = si
        ja11 = field%cGrid%jacobian%a11(i,j,k)
        ja12 = field%cGrid%jacobian%a12(i,j,k)
        ja13 = field%cGrid%jacobian%a13(i,j,k)
        ja21 = field%cGrid%jacobian%a21(i,j,k)
        ja22 = field%cGrid%jacobian%a22(i,j,k)
        ja23 = field%cGrid%jacobian%a23(i,j,k)
        ja31 = field%cGrid%jacobian%a31(i,j,k)
        ja32 = field%cGrid%jacobian%a32(i,j,k)
        ja33 = field%cGrid%jacobian%a33(i,j,k)
        ! du/dx du/dy du/dz
        ddcsi =(field%velocity%x(i+1,j,k)-field%velocity%x(i,j,k))*2.
        ddeta = field%velocity%x(i,j+1,k)-field%velocity%x(i,j-1,k)
        ddzit =(field%velocity%x(i,j,k)-field%velocity%x(i,j,k-1))*2.
        field%velocityGradient%a11(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a12(i,j,k) =    &
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a13(i,j,k) =    &
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
        ! dv/dx dv/dy dv/dz
        ddcsi =(field%velocity%y(i+1,j,k)-field%velocity%y(i,j,k))*2.
        ddeta = field%velocity%y(i,j+1,k)-field%velocity%y(i,j-1,k)
        ddzit =(field%velocity%y(i,j,k)-field%velocity%y(i,j,k-1))*2.
        field%velocityGradient%a21(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a22(i,j,k) =    &                                     
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a23(i,j,k) =    &                                     
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
        ! dw/dx dw/dy dw/dz
        ddcsi =(field%velocity%z(i+1,j,k)-field%velocity%z(i,j,k))*2.
        ddeta = field%velocity%z(i,j+1,k)-field%velocity%z(i,j-1,k)
        ddzit =(field%velocity%z(i,j,k)-field%velocity%z(i,j,k-1))*2.
        field%velocityGradient%a31(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a32(i,j,k) =    &                                     
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a33(i,j,k) =    &                                     
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
        k = nk
        i = ni
        ja11 = field%cGrid%jacobian%a11(i,j,k)
        ja12 = field%cGrid%jacobian%a12(i,j,k)
        ja13 = field%cGrid%jacobian%a13(i,j,k)
        ja21 = field%cGrid%jacobian%a21(i,j,k)
        ja22 = field%cGrid%jacobian%a22(i,j,k)
        ja23 = field%cGrid%jacobian%a23(i,j,k)
        ja31 = field%cGrid%jacobian%a31(i,j,k)
        ja32 = field%cGrid%jacobian%a32(i,j,k)
        ja33 = field%cGrid%jacobian%a33(i,j,k)
        ! du/dx du/dy du/dz
        ddcsi =(field%velocity%x(i,j,k)-field%velocity%x(i-1,j,k))*2.
        ddeta = field%velocity%x(i,j+1,k)-field%velocity%x(i,j-1,k)
        ddzit =(field%velocity%x(i,j,k)-field%velocity%x(i,j,k-1))*2.
        field%velocityGradient%a11(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a12(i,j,k) =    &
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a13(i,j,k) =    &
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
        ! dv/dx dv/dy dv/dz
        ddcsi =(field%velocity%y(i,j,k)-field%velocity%y(i-1,j,k))*2.
        ddeta = field%velocity%y(i,j+1,k)-field%velocity%y(i,j-1,k)
        ddzit =(field%velocity%y(i,j,k)-field%velocity%y(i,j,k-1))*2.
        field%velocityGradient%a21(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a22(i,j,k) =    &                                     
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a23(i,j,k) =    &                                     
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
        ! dw/dx dw/dy dw/dz
        ddcsi =(field%velocity%z(i,j,k)-field%velocity%z(i-1,j,k))*2.
        ddeta = field%velocity%z(i,j+1,k)-field%velocity%z(i,j-1,k)
        ddzit =(field%velocity%z(i,j,k)-field%velocity%z(i,j,k-1))*2.
        field%velocityGradient%a31(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a32(i,j,k) =    &                                     
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a33(i,j,k) =    &                                     
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
    end do edges34
    edges12: do k = sk+1, nk-1

        j = sj
        i = si
        ja11 = field%cGrid%jacobian%a11(i,j,k)
        ja12 = field%cGrid%jacobian%a12(i,j,k)
        ja13 = field%cGrid%jacobian%a13(i,j,k)
        ja21 = field%cGrid%jacobian%a21(i,j,k)
        ja22 = field%cGrid%jacobian%a22(i,j,k)
        ja23 = field%cGrid%jacobian%a23(i,j,k)
        ja31 = field%cGrid%jacobian%a31(i,j,k)
        ja32 = field%cGrid%jacobian%a32(i,j,k)
        ja33 = field%cGrid%jacobian%a33(i,j,k)
        ! du/dx du/dy du/dz
        ddcsi =(field%velocity%x(i+1,j,k)-field%velocity%x(i,j,k))*2.
        ddeta =(field%velocity%x(i,j+1,k)-field%velocity%x(i,j,k))*2
        ddzit = field%velocity%x(i,j,k+1)-field%velocity%x(i,j,k-1)
        field%velocityGradient%a11(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a12(i,j,k) =    &
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a13(i,j,k) =    &
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
        ! dv/dx dv/dy dv/dz
        ddcsi =(field%velocity%y(i+1,j,k)-field%velocity%y(i,j,k))*2.
        ddeta =(field%velocity%y(i,j+1,k)-field%velocity%y(i,j,k))*2
        ddzit = field%velocity%y(i,j,k+1)-field%velocity%y(i,j,k-1)
        field%velocityGradient%a21(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a22(i,j,k) =    &                                     
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a23(i,j,k) =    &                                     
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
        ! dw/dx dw/dy dw/dz
        ddcsi =(field%velocity%z(i+1,j,k)-field%velocity%z(i,j,k))*2.
        ddeta =(field%velocity%z(i,j+1,k)-field%velocity%z(i,j,k))*2
        ddzit = field%velocity%z(i,j,k+1)-field%velocity%z(i,j,k-1)
        field%velocityGradient%a31(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a32(i,j,k) =    &                                     
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a33(i,j,k) =    &                                     
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
        j = sj
        i = ni
        ja11 = field%cGrid%jacobian%a11(i,j,k)
        ja12 = field%cGrid%jacobian%a12(i,j,k)
        ja13 = field%cGrid%jacobian%a13(i,j,k)
        ja21 = field%cGrid%jacobian%a21(i,j,k)
        ja22 = field%cGrid%jacobian%a22(i,j,k)
        ja23 = field%cGrid%jacobian%a23(i,j,k)
        ja31 = field%cGrid%jacobian%a31(i,j,k)
        ja32 = field%cGrid%jacobian%a32(i,j,k)
        ja33 = field%cGrid%jacobian%a33(i,j,k)
        ! du/dx du/dy du/dz
        ddcsi =(field%velocity%x(i,j,k)-field%velocity%x(i-1,j,k))*2.
        ddeta =(field%velocity%x(i,j+1,k)-field%velocity%x(i,j,k))*2
        ddzit = field%velocity%x(i,j,k+1)-field%velocity%x(i,j,k-1)
        field%velocityGradient%a11(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a12(i,j,k) =    &
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a13(i,j,k) =    &
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
        ! dv/dx dv/dy dv/dz
        ddcsi =(field%velocity%y(i,j,k)-field%velocity%y(i-1,j,k))*2.
        ddeta =(field%velocity%y(i,j+1,k)-field%velocity%y(i,j,k))*2
        ddzit = field%velocity%y(i,j,k+1)-field%velocity%y(i,j,k-1)
        field%velocityGradient%a21(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a22(i,j,k) =    &                                     
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a23(i,j,k) =    &                                     
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
        ! dw/dx dw/dy dw/dz
        ddcsi =(field%velocity%z(i,j,k)-field%velocity%z(i-1,j,k))*2.
        ddeta =(field%velocity%z(i,j+1,k)-field%velocity%z(i,j,k))*2
        ddzit = field%velocity%z(i,j,k+1)-field%velocity%z(i,j,k-1)
        field%velocityGradient%a31(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a32(i,j,k) =    &                                     
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a33(i,j,k) =    &                                     
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
        i = si
        j = nj
        ja11 = field%cGrid%jacobian%a11(i,j,k)
        ja12 = field%cGrid%jacobian%a12(i,j,k)
        ja13 = field%cGrid%jacobian%a13(i,j,k)
        ja21 = field%cGrid%jacobian%a21(i,j,k)
        ja22 = field%cGrid%jacobian%a22(i,j,k)
        ja23 = field%cGrid%jacobian%a23(i,j,k)
        ja31 = field%cGrid%jacobian%a31(i,j,k)
        ja32 = field%cGrid%jacobian%a32(i,j,k)
        ja33 = field%cGrid%jacobian%a33(i,j,k)
        ! du/dx du/dy du/dz
        ddcsi =(field%velocity%x(i+1,j,k)-field%velocity%x(i,j,k))*2.
        ddeta =(field%velocity%x(i,j,k)-field%velocity%x(i,j-1,k))*2
        ddzit = field%velocity%x(i,j,k+1)-field%velocity%x(i,j,k-1)
        field%velocityGradient%a11(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a12(i,j,k) =    &
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a13(i,j,k) =    &
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
        ! dv/dx dv/dy dv/dz
        ddcsi =(field%velocity%y(i+1,j,k)-field%velocity%y(i,j,k))*2.
        ddeta =(field%velocity%y(i,j,k)-field%velocity%y(i,j-1,k))*2
        ddzit = field%velocity%y(i,j,k+1)-field%velocity%y(i,j,k-1)
        field%velocityGradient%a21(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a22(i,j,k) =    &                                     
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a23(i,j,k) =    &                                     
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
        ! dw/dx dw/dy dw/dz
        ddcsi =(field%velocity%z(i+1,j,k)-field%velocity%z(i,j,k))*2.
        ddeta =(field%velocity%z(i,j,k)-field%velocity%z(i,j-1,k))*2
        ddzit = field%velocity%z(i,j,k+1)-field%velocity%z(i,j,k-1)
        field%velocityGradient%a31(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a32(i,j,k) =    &                                     
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a33(i,j,k) =    &                                     
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
        j = nj
        i = ni
        ja11 = field%cGrid%jacobian%a11(i,j,k)
        ja12 = field%cGrid%jacobian%a12(i,j,k)
        ja13 = field%cGrid%jacobian%a13(i,j,k)
        ja21 = field%cGrid%jacobian%a21(i,j,k)
        ja22 = field%cGrid%jacobian%a22(i,j,k)
        ja23 = field%cGrid%jacobian%a23(i,j,k)
        ja31 = field%cGrid%jacobian%a31(i,j,k)
        ja32 = field%cGrid%jacobian%a32(i,j,k)
        ja33 = field%cGrid%jacobian%a33(i,j,k)
        ! du/dx du/dy du/dz
        ddcsi =(field%velocity%x(i,j,k)-field%velocity%x(i-1,j,k))*2.
        ddeta =(field%velocity%x(i,j,k)-field%velocity%x(i,j-1,k))*2
        ddzit = field%velocity%x(i,j,k+1)-field%velocity%x(i,j,k-1)
        field%velocityGradient%a11(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a12(i,j,k) =    &
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a13(i,j,k) =    &
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
        ! dv/dx dv/dy dv/dz
        ddcsi =(field%velocity%y(i,j,k)-field%velocity%y(i-1,j,k))*2.
        ddeta =(field%velocity%y(i,j,k)-field%velocity%y(i,j-1,k))*2
        ddzit = field%velocity%y(i,j,k+1)-field%velocity%y(i,j,k-1)
        field%velocityGradient%a21(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a22(i,j,k) =    &                                     
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a23(i,j,k) =    &                                     
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
        ! dw/dx dw/dy dw/dz
        ddcsi =(field%velocity%z(i,j,k)-field%velocity%z(i-1,j,k))*2.
        ddeta =(field%velocity%z(i,j,k)-field%velocity%z(i,j-1,k))*2
        ddzit = field%velocity%z(i,j,k+1)-field%velocity%z(i,j,k-1)
        field%velocityGradient%a31(i,j,k) =    &
                              ja11*0.5*ddcsi + &
                              ja21*0.5*ddeta + &
                              ja31*0.5*ddzit 
        field%velocityGradient%a32(i,j,k) =    &                                     
                              ja12*0.5*ddcsi + &
                              ja22*0.5*ddeta + &
                              ja32*0.5*ddzit 
        field%velocityGradient%a33(i,j,k) =    &                                     
                              ja13*0.5*ddcsi + &
                              ja23*0.5*ddeta + &
                              ja33*0.5*ddzit 
    end do edges12
    ! vertices
    ! 000
    i = si
    j = sj
    k = sk
    ja11 = field%cGrid%jacobian%a11(i,j,k)
    ja12 = field%cGrid%jacobian%a12(i,j,k)
    ja13 = field%cGrid%jacobian%a13(i,j,k)
    ja21 = field%cGrid%jacobian%a21(i,j,k)
    ja22 = field%cGrid%jacobian%a22(i,j,k)
    ja23 = field%cGrid%jacobian%a23(i,j,k)
    ja31 = field%cGrid%jacobian%a31(i,j,k)
    ja32 = field%cGrid%jacobian%a32(i,j,k)
    ja33 = field%cGrid%jacobian%a33(i,j,k)
    ! du/dx du/dy du/dz
    ddcsi =(field%velocity%x(i+1,j,k)-field%velocity%x(i,j,k))*2.
    ddeta =(field%velocity%x(i,j+1,k)-field%velocity%x(i,j,k))*2.
    ddzit =(field%velocity%x(i,j,k+1)-field%velocity%x(i,j,k))*2.
    field%velocityGradient%a11(i,j,k) =    &
                          ja11*0.5*ddcsi + &
                          ja21*0.5*ddeta + &
                          ja31*0.5*ddzit 
    field%velocityGradient%a12(i,j,k) =    &
                          ja12*0.5*ddcsi + &
                          ja22*0.5*ddeta + &
                          ja32*0.5*ddzit 
    field%velocityGradient%a13(i,j,k) =    &
                          ja13*0.5*ddcsi + &
                          ja23*0.5*ddeta + &
                          ja33*0.5*ddzit 
    ! dv/dx dv/dy dv/dz
    ddcsi =(field%velocity%y(i+1,j,k)-field%velocity%y(i,j,k))*2.
    ddeta =(field%velocity%y(i,j+1,k)-field%velocity%y(i,j,k))*2.
    ddzit =(field%velocity%y(i,j,k+1)-field%velocity%y(i,j,k))*2.
    field%velocityGradient%a21(i,j,k) =    &
                          ja11*0.5*ddcsi + &
                          ja21*0.5*ddeta + &
                          ja31*0.5*ddzit 
    field%velocityGradient%a22(i,j,k) =    &                                     
                          ja12*0.5*ddcsi + &
                          ja22*0.5*ddeta + &
                          ja32*0.5*ddzit 
    field%velocityGradient%a23(i,j,k) =    &                                     
                          ja13*0.5*ddcsi + &
                          ja23*0.5*ddeta + &
                          ja33*0.5*ddzit 
    ! dw/dx dw/dy dw/dz
    ddcsi =(field%velocity%z(i+1,j,k)-field%velocity%z(i,j,k))*2.
    ddeta =(field%velocity%z(i,j+1,k)-field%velocity%z(i,j,k))*2.
    ddzit =(field%velocity%z(i,j,k+1)-field%velocity%z(i,j,k))*2.
    field%velocityGradient%a31(i,j,k) =    &
                          ja11*0.5*ddcsi + &
                          ja21*0.5*ddeta + &
                          ja31*0.5*ddzit 
    field%velocityGradient%a32(i,j,k) =    &                                     
                          ja12*0.5*ddcsi + &
                          ja22*0.5*ddeta + &
                          ja32*0.5*ddzit 
    field%velocityGradient%a33(i,j,k) =    &                                     
                          ja13*0.5*ddcsi + &
                          ja23*0.5*ddeta + &
                          ja33*0.5*ddzit 
    ! 100
    i = ni
    j = sj
    k = sk
    ja11 = field%cGrid%jacobian%a11(i,j,k)
    ja12 = field%cGrid%jacobian%a12(i,j,k)
    ja13 = field%cGrid%jacobian%a13(i,j,k)
    ja21 = field%cGrid%jacobian%a21(i,j,k)
    ja22 = field%cGrid%jacobian%a22(i,j,k)
    ja23 = field%cGrid%jacobian%a23(i,j,k)
    ja31 = field%cGrid%jacobian%a31(i,j,k)
    ja32 = field%cGrid%jacobian%a32(i,j,k)
    ja33 = field%cGrid%jacobian%a33(i,j,k)
    ! du/dx du/dy du/dz
    ddcsi =(field%velocity%x(i,j,k)-field%velocity%x(i-1,j,k))*2.
    ddeta =(field%velocity%x(i,j+1,k)-field%velocity%x(i,j,k))*2.
    ddzit =(field%velocity%x(i,j,k+1)-field%velocity%x(i,j,k))*2.
    field%velocityGradient%a11(i,j,k) =    &
                          ja11*0.5*ddcsi + &
                          ja21*0.5*ddeta + &
                          ja31*0.5*ddzit 
    field%velocityGradient%a12(i,j,k) =    &
                          ja12*0.5*ddcsi + &
                          ja22*0.5*ddeta + &
                          ja32*0.5*ddzit 
    field%velocityGradient%a13(i,j,k) =    &
                          ja13*0.5*ddcsi + &
                          ja23*0.5*ddeta + &
                          ja33*0.5*ddzit 
    ! dv/dx dv/dy dv/dz
    ddcsi =(field%velocity%y(i,j,k)-field%velocity%y(i-1,j,k))*2.
    ddeta =(field%velocity%y(i,j+1,k)-field%velocity%y(i,j,k))*2.
    ddzit =(field%velocity%y(i,j,k+1)-field%velocity%y(i,j,k))*2.
    field%velocityGradient%a21(i,j,k) =    &
                          ja11*0.5*ddcsi + &
                          ja21*0.5*ddeta + &
                          ja31*0.5*ddzit 
    field%velocityGradient%a22(i,j,k) =    &                                     
                          ja12*0.5*ddcsi + &
                          ja22*0.5*ddeta + &
                          ja32*0.5*ddzit 
    field%velocityGradient%a23(i,j,k) =    &                                     
                          ja13*0.5*ddcsi + &
                          ja23*0.5*ddeta + &
                          ja33*0.5*ddzit 
    ! dw/dx dw/dy dw/dz
    ddcsi =(field%velocity%z(i,j,k)-field%velocity%z(i-1,j,k))*2.
    ddeta =(field%velocity%z(i,j+1,k)-field%velocity%z(i,j,k))*2.
    ddzit =(field%velocity%z(i,j,k+1)-field%velocity%z(i,j,k))*2.
    field%velocityGradient%a31(i,j,k) =    &
                          ja11*0.5*ddcsi + &
                          ja21*0.5*ddeta + &
                          ja31*0.5*ddzit 
    field%velocityGradient%a32(i,j,k) =    &                                     
                          ja12*0.5*ddcsi + &
                          ja22*0.5*ddeta + &
                          ja32*0.5*ddzit 
    field%velocityGradient%a33(i,j,k) =    &                                     
                          ja13*0.5*ddcsi + &
                          ja23*0.5*ddeta + &
                          ja33*0.5*ddzit 
    ! 110
    i = ni
    j = nj
    k = sk
    ja11 = field%cGrid%jacobian%a11(i,j,k)
    ja12 = field%cGrid%jacobian%a12(i,j,k)
    ja13 = field%cGrid%jacobian%a13(i,j,k)
    ja21 = field%cGrid%jacobian%a21(i,j,k)
    ja22 = field%cGrid%jacobian%a22(i,j,k)
    ja23 = field%cGrid%jacobian%a23(i,j,k)
    ja31 = field%cGrid%jacobian%a31(i,j,k)
    ja32 = field%cGrid%jacobian%a32(i,j,k)
    ja33 = field%cGrid%jacobian%a33(i,j,k)
    ! du/dx du/dy du/dz
    ddcsi =(field%velocity%x(i,j,k)-field%velocity%x(i-1,j,k))*2.
    ddeta =(field%velocity%x(i,j,k)-field%velocity%x(i,j-1,k))*2.
    ddzit =(field%velocity%x(i,j,k+1)-field%velocity%x(i,j,k))*2.
    field%velocityGradient%a11(i,j,k) =    &
                          ja11*0.5*ddcsi + &
                          ja21*0.5*ddeta + &
                          ja31*0.5*ddzit 
    field%velocityGradient%a12(i,j,k) =    &
                          ja12*0.5*ddcsi + &
                          ja22*0.5*ddeta + &
                          ja32*0.5*ddzit 
    field%velocityGradient%a13(i,j,k) =    &
                          ja13*0.5*ddcsi + &
                          ja23*0.5*ddeta + &
                          ja33*0.5*ddzit 
    ! dv/dx dv/dy dv/dz
    ddcsi =(field%velocity%y(i,j,k)-field%velocity%y(i-1,j,k))*2.
    ddeta =(field%velocity%y(i,j,k)-field%velocity%y(i,j-1,k))*2.
    ddzit =(field%velocity%y(i,j,k+1)-field%velocity%y(i,j,k))*2.
    field%velocityGradient%a21(i,j,k) =    &
                          ja11*0.5*ddcsi + &
                          ja21*0.5*ddeta + &
                          ja31*0.5*ddzit 
    field%velocityGradient%a22(i,j,k) =    &                                     
                          ja12*0.5*ddcsi + &
                          ja22*0.5*ddeta + &
                          ja32*0.5*ddzit 
    field%velocityGradient%a23(i,j,k) =    &                                     
                          ja13*0.5*ddcsi + &
                          ja23*0.5*ddeta + &
                          ja33*0.5*ddzit 
    ! dw/dx dw/dy dw/dz
    ddcsi =(field%velocity%z(i,j,k)-field%velocity%z(i-1,j,k))*2.
    ddeta =(field%velocity%z(i,j,k)-field%velocity%z(i,j-1,k))*2.
    ddzit =(field%velocity%z(i,j,k+1)-field%velocity%z(i,j,k))*2.
    field%velocityGradient%a31(i,j,k) =    &
                          ja11*0.5*ddcsi + &
                          ja21*0.5*ddeta + &
                          ja31*0.5*ddzit 
    field%velocityGradient%a32(i,j,k) =    &                                     
                          ja12*0.5*ddcsi + &
                          ja22*0.5*ddeta + &
                          ja32*0.5*ddzit 
    field%velocityGradient%a33(i,j,k) =    &                                     
                          ja13*0.5*ddcsi + &
                          ja23*0.5*ddeta + &
                          ja33*0.5*ddzit 
    ! 111
    i = ni
    j = nj
    k = nk
    ja11 = field%cGrid%jacobian%a11(i,j,k)
    ja12 = field%cGrid%jacobian%a12(i,j,k)
    ja13 = field%cGrid%jacobian%a13(i,j,k)
    ja21 = field%cGrid%jacobian%a21(i,j,k)
    ja22 = field%cGrid%jacobian%a22(i,j,k)
    ja23 = field%cGrid%jacobian%a23(i,j,k)
    ja31 = field%cGrid%jacobian%a31(i,j,k)
    ja32 = field%cGrid%jacobian%a32(i,j,k)
    ja33 = field%cGrid%jacobian%a33(i,j,k)
    ! du/dx du/dy du/dz
    ddcsi =(field%velocity%x(i,j,k)-field%velocity%x(i-1,j,k))*2.
    ddeta =(field%velocity%x(i,j,k)-field%velocity%x(i,j-1,k))*2.
    ddzit =(field%velocity%x(i,j,k)-field%velocity%x(i,j,k-1))*2.
    field%velocityGradient%a11(i,j,k) =    &
                          ja11*0.5*ddcsi + &
                          ja21*0.5*ddeta + &
                          ja31*0.5*ddzit 
    field%velocityGradient%a12(i,j,k) =    &
                          ja12*0.5*ddcsi + &
                          ja22*0.5*ddeta + &
                          ja32*0.5*ddzit 
    field%velocityGradient%a13(i,j,k) =    &
                          ja13*0.5*ddcsi + &
                          ja23*0.5*ddeta + &
                          ja33*0.5*ddzit 
    ! dv/dx dv/dy dv/dz
    ddcsi =(field%velocity%y(i,j,k)-field%velocity%y(i-1,j,k))*2.
    ddeta =(field%velocity%y(i,j,k)-field%velocity%y(i,j-1,k))*2.
    ddzit =(field%velocity%y(i,j,k)-field%velocity%y(i,j,k-1))*2.
    field%velocityGradient%a21(i,j,k) =    &
                          ja11*0.5*ddcsi + &
                          ja21*0.5*ddeta + &
                          ja31*0.5*ddzit 
    field%velocityGradient%a22(i,j,k) =    &                                     
                          ja12*0.5*ddcsi + &
                          ja22*0.5*ddeta + &
                          ja32*0.5*ddzit 
    field%velocityGradient%a23(i,j,k) =    &                                     
                          ja13*0.5*ddcsi + &
                          ja23*0.5*ddeta + &
                          ja33*0.5*ddzit 
    ! dw/dx dw/dy dw/dz
    ddcsi =(field%velocity%z(i,j,k)-field%velocity%z(i-1,j,k))*2.
    ddeta =(field%velocity%z(i,j,k)-field%velocity%z(i,j-1,k))*2.
    ddzit =(field%velocity%z(i,j,k)-field%velocity%z(i,j,k-1))*2.
    field%velocityGradient%a31(i,j,k) =    &
                          ja11*0.5*ddcsi + &
                          ja21*0.5*ddeta + &
                          ja31*0.5*ddzit 
    field%velocityGradient%a32(i,j,k) =    &                                     
                          ja12*0.5*ddcsi + &
                          ja22*0.5*ddeta + &
                          ja32*0.5*ddzit 
    field%velocityGradient%a33(i,j,k) =    &                                     
                          ja13*0.5*ddcsi + &
                          ja23*0.5*ddeta + &
                          ja33*0.5*ddzit 
    ! 011
    i = si
    j = nj
    k = nk
    ja11 = field%cGrid%jacobian%a11(i,j,k)
    ja12 = field%cGrid%jacobian%a12(i,j,k)
    ja13 = field%cGrid%jacobian%a13(i,j,k)
    ja21 = field%cGrid%jacobian%a21(i,j,k)
    ja22 = field%cGrid%jacobian%a22(i,j,k)
    ja23 = field%cGrid%jacobian%a23(i,j,k)
    ja31 = field%cGrid%jacobian%a31(i,j,k)
    ja32 = field%cGrid%jacobian%a32(i,j,k)
    ja33 = field%cGrid%jacobian%a33(i,j,k)
    ! du/dx du/dy du/dz
    ddcsi =(field%velocity%x(i+1,j,k)-field%velocity%x(i,j,k))*2.
    ddeta =(field%velocity%x(i,j,k)-field%velocity%x(i,j-1,k))*2.
    ddzit =(field%velocity%x(i,j,k)-field%velocity%x(i,j,k-1))*2.
    field%velocityGradient%a11(i,j,k) =    &
                          ja11*0.5*ddcsi + &
                          ja21*0.5*ddeta + &
                          ja31*0.5*ddzit 
    field%velocityGradient%a12(i,j,k) =    &
                          ja12*0.5*ddcsi + &
                          ja22*0.5*ddeta + &
                          ja32*0.5*ddzit 
    field%velocityGradient%a13(i,j,k) =    &
                          ja13*0.5*ddcsi + &
                          ja23*0.5*ddeta + &
                          ja33*0.5*ddzit 
    ! dv/dx dv/dy dv/dz
    ddcsi =(field%velocity%y(i+1,j,k)-field%velocity%y(i,j,k))*2.
    ddeta =(field%velocity%y(i,j,k)-field%velocity%y(i,j-1,k))*2.
    ddzit =(field%velocity%y(i,j,k)-field%velocity%y(i,j,k-1))*2.
    field%velocityGradient%a21(i,j,k) =    &
                          ja11*0.5*ddcsi + &
                          ja21*0.5*ddeta + &
                          ja31*0.5*ddzit 
    field%velocityGradient%a22(i,j,k) =    &                                     
                          ja12*0.5*ddcsi + &
                          ja22*0.5*ddeta + &
                          ja32*0.5*ddzit 
    field%velocityGradient%a23(i,j,k) =    &                                     
                          ja13*0.5*ddcsi + &
                          ja23*0.5*ddeta + &
                          ja33*0.5*ddzit 
    ! dw/dx dw/dy dw/dz
    ddcsi =(field%velocity%z(i+1,j,k)-field%velocity%z(i,j,k))*2.
    ddeta =(field%velocity%z(i,j,k)-field%velocity%z(i,j-1,k))*2.
    ddzit =(field%velocity%z(i,j,k)-field%velocity%z(i,j,k-1))*2.
    field%velocityGradient%a31(i,j,k) =    &
                          ja11*0.5*ddcsi + &
                          ja21*0.5*ddeta + &
                          ja31*0.5*ddzit 
    field%velocityGradient%a32(i,j,k) =    &                                     
                          ja12*0.5*ddcsi + &
                          ja22*0.5*ddeta + &
                          ja32*0.5*ddzit 
    field%velocityGradient%a33(i,j,k) =    &                                     
                          ja13*0.5*ddcsi + &
                          ja23*0.5*ddeta + &
                          ja33*0.5*ddzit 
    ! 101
    i = ni
    j = sj
    k = nk
    ja11 = field%cGrid%jacobian%a11(i,j,k)
    ja12 = field%cGrid%jacobian%a12(i,j,k)
    ja13 = field%cGrid%jacobian%a13(i,j,k)
    ja21 = field%cGrid%jacobian%a21(i,j,k)
    ja22 = field%cGrid%jacobian%a22(i,j,k)
    ja23 = field%cGrid%jacobian%a23(i,j,k)
    ja31 = field%cGrid%jacobian%a31(i,j,k)
    ja32 = field%cGrid%jacobian%a32(i,j,k)
    ja33 = field%cGrid%jacobian%a33(i,j,k)
    ! du/dx du/dy du/dz
    ddcsi =(field%velocity%x(i,j,k)-field%velocity%x(i-1,j,k))*2.
    ddeta =(field%velocity%x(i,j+1,k)-field%velocity%x(i,j,k))*2.
    ddzit =(field%velocity%x(i,j,k)-field%velocity%x(i,j,k-1))*2.
    field%velocityGradient%a11(i,j,k) =    &
                          ja11*0.5*ddcsi + &
                          ja21*0.5*ddeta + &
                          ja31*0.5*ddzit 
    field%velocityGradient%a12(i,j,k) =    &
                          ja12*0.5*ddcsi + &
                          ja22*0.5*ddeta + &
                          ja32*0.5*ddzit 
    field%velocityGradient%a13(i,j,k) =    &
                          ja13*0.5*ddcsi + &
                          ja23*0.5*ddeta + &
                          ja33*0.5*ddzit 
    ! dv/dx dv/dy dv/dz
    ddcsi =(field%velocity%y(i,j,k)-field%velocity%y(i-1,j,k))*2.
    ddeta =(field%velocity%y(i,j+1,k)-field%velocity%y(i,j,k))*2.
    ddzit =(field%velocity%y(i,j,k)-field%velocity%y(i,j,k-1))*2.
    field%velocityGradient%a21(i,j,k) =    &
                          ja11*0.5*ddcsi + &
                          ja21*0.5*ddeta + &
                          ja31*0.5*ddzit 
    field%velocityGradient%a22(i,j,k) =    &                                     
                          ja12*0.5*ddcsi + &
                          ja22*0.5*ddeta + &
                          ja32*0.5*ddzit 
    field%velocityGradient%a23(i,j,k) =    &                                     
                          ja13*0.5*ddcsi + &
                          ja23*0.5*ddeta + &
                          ja33*0.5*ddzit 
    ! dw/dx dw/dy dw/dz
    ddcsi =(field%velocity%z(i,j,k)-field%velocity%z(i-1,j,k))*2.
    ddeta =(field%velocity%z(i,j+1,k)-field%velocity%z(i,j,k))*2.
    ddzit =(field%velocity%z(i,j,k)-field%velocity%z(i,j,k-1))*2.
    field%velocityGradient%a31(i,j,k) =    &
                          ja11*0.5*ddcsi + &
                          ja21*0.5*ddeta + &
                          ja31*0.5*ddzit 
    field%velocityGradient%a32(i,j,k) =    &                                     
                          ja12*0.5*ddcsi + &
                          ja22*0.5*ddeta + &
                          ja32*0.5*ddzit 
    field%velocityGradient%a33(i,j,k) =    &                                     
                          ja13*0.5*ddcsi + &
                          ja23*0.5*ddeta + &
                          ja33*0.5*ddzit 
    ! 001
    i = si
    j = sj
    k = nk
    ja11 = field%cGrid%jacobian%a11(i,j,k)
    ja12 = field%cGrid%jacobian%a12(i,j,k)
    ja13 = field%cGrid%jacobian%a13(i,j,k)
    ja21 = field%cGrid%jacobian%a21(i,j,k)
    ja22 = field%cGrid%jacobian%a22(i,j,k)
    ja23 = field%cGrid%jacobian%a23(i,j,k)
    ja31 = field%cGrid%jacobian%a31(i,j,k)
    ja32 = field%cGrid%jacobian%a32(i,j,k)
    ja33 = field%cGrid%jacobian%a33(i,j,k)
    ! du/dx du/dy du/dz
    ddcsi =(field%velocity%x(i+1,j,k)-field%velocity%x(i,j,k))*2.
    ddeta =(field%velocity%x(i,j+1,k)-field%velocity%x(i,j,k))*2.
    ddzit =(field%velocity%x(i,j,k)-field%velocity%x(i,j,k-1))*2.
    field%velocityGradient%a11(i,j,k) =    &
                          ja11*0.5*ddcsi + &
                          ja21*0.5*ddeta + &
                          ja31*0.5*ddzit 
    field%velocityGradient%a12(i,j,k) =    &
                          ja12*0.5*ddcsi + &
                          ja22*0.5*ddeta + &
                          ja32*0.5*ddzit 
    field%velocityGradient%a13(i,j,k) =    &
                          ja13*0.5*ddcsi + &
                          ja23*0.5*ddeta + &
                          ja33*0.5*ddzit 
    ! dv/dx dv/dy dv/dz
    ddcsi =(field%velocity%y(i+1,j,k)-field%velocity%y(i,j,k))*2.
    ddeta =(field%velocity%y(i,j+1,k)-field%velocity%y(i,j,k))*2.
    ddzit =(field%velocity%y(i,j,k)-field%velocity%y(i,j,k-1))*2.
    field%velocityGradient%a21(i,j,k) =    &
                          ja11*0.5*ddcsi + &
                          ja21*0.5*ddeta + &
                          ja31*0.5*ddzit 
    field%velocityGradient%a22(i,j,k) =    &                                     
                          ja12*0.5*ddcsi + &
                          ja22*0.5*ddeta + &
                          ja32*0.5*ddzit 
    field%velocityGradient%a23(i,j,k) =    &                                     
                          ja13*0.5*ddcsi + &
                          ja23*0.5*ddeta + &
                          ja33*0.5*ddzit 
    ! dw/dx dw/dy dw/dz
    ddcsi =(field%velocity%z(i+1,j,k)-field%velocity%z(i,j,k))*2.
    ddeta =(field%velocity%z(i,j+1,k)-field%velocity%z(i,j,k))*2.
    ddzit =(field%velocity%z(i,j,k)-field%velocity%z(i,j,k-1))*2.
    field%velocityGradient%a31(i,j,k) =    &
                          ja11*0.5*ddcsi + &
                          ja21*0.5*ddeta + &
                          ja31*0.5*ddzit 
    field%velocityGradient%a32(i,j,k) =    &                                     
                          ja12*0.5*ddcsi + &
                          ja22*0.5*ddeta + &
                          ja32*0.5*ddzit 
    field%velocityGradient%a33(i,j,k) =    &                                     
                          ja13*0.5*ddcsi + &
                          ja23*0.5*ddeta + &
                          ja33*0.5*ddzit 
    ! 010
    i = si
    j = nj
    k = sk
    ja11 = field%cGrid%jacobian%a11(i,j,k)
    ja12 = field%cGrid%jacobian%a12(i,j,k)
    ja13 = field%cGrid%jacobian%a13(i,j,k)
    ja21 = field%cGrid%jacobian%a21(i,j,k)
    ja22 = field%cGrid%jacobian%a22(i,j,k)
    ja23 = field%cGrid%jacobian%a23(i,j,k)
    ja31 = field%cGrid%jacobian%a31(i,j,k)
    ja32 = field%cGrid%jacobian%a32(i,j,k)
    ja33 = field%cGrid%jacobian%a33(i,j,k)
    ! du/dx du/dy du/dz
    ddcsi =(field%velocity%x(i+1,j,k)-field%velocity%x(i,j,k))*2.
    ddeta =(field%velocity%x(i,j,k)-field%velocity%x(i,j-1,k))*2.
    ddzit =(field%velocity%x(i,j,k+1)-field%velocity%x(i,j,k))*2.
    field%velocityGradient%a11(i,j,k) =    &
                          ja11*0.5*ddcsi + &
                          ja21*0.5*ddeta + &
                          ja31*0.5*ddzit 
    field%velocityGradient%a12(i,j,k) =    &
                          ja12*0.5*ddcsi + &
                          ja22*0.5*ddeta + &
                          ja32*0.5*ddzit 
    field%velocityGradient%a13(i,j,k) =    &
                          ja13*0.5*ddcsi + &
                          ja23*0.5*ddeta + &
                          ja33*0.5*ddzit 
    ! dv/dx dv/dy dv/dz
    ddcsi =(field%velocity%y(i+1,j,k)-field%velocity%y(i,j,k))*2.
    ddeta =(field%velocity%y(i,j,k)-field%velocity%y(i,j-1,k))*2.
    ddzit =(field%velocity%y(i,j,k+1)-field%velocity%y(i,j,k))*2.
    field%velocityGradient%a21(i,j,k) =    &
                          ja11*0.5*ddcsi + &
                          ja21*0.5*ddeta + &
                          ja31*0.5*ddzit 
    field%velocityGradient%a22(i,j,k) =    &                                     
                          ja12*0.5*ddcsi + &
                          ja22*0.5*ddeta + &
                          ja32*0.5*ddzit 
    field%velocityGradient%a23(i,j,k) =    &                                     
                          ja13*0.5*ddcsi + &
                          ja23*0.5*ddeta + &
                          ja33*0.5*ddzit 
    ! dw/dx dw/dy dw/dz
    ddcsi =(field%velocity%z(i+1,j,k)-field%velocity%z(i,j,k))*2.
    ddeta =(field%velocity%z(i,j,k)-field%velocity%z(i,j-1,k))*2.
    ddzit =(field%velocity%z(i,j,k+1)-field%velocity%z(i,j,k))*2.
    field%velocityGradient%a31(i,j,k) =    &
                          ja11*0.5*ddcsi + &
                          ja21*0.5*ddeta + &
                          ja31*0.5*ddzit 
    field%velocityGradient%a32(i,j,k) =    &                                     
                          ja12*0.5*ddcsi + &
                          ja22*0.5*ddeta + &
                          ja32*0.5*ddzit 
    field%velocityGradient%a33(i,j,k) =    &                                     
                          ja13*0.5*ddcsi + &
                          ja23*0.5*ddeta + &
                          ja33*0.5*ddzit

    field%vorticity%x = field%velocityGradient%a23 - field%velocityGradient%a32 
    field%vorticity%y = field%velocityGradient%a12 - field%velocityGradient%a21 
    field%vorticity%z = field%velocityGradient%a31 - field%velocityGradient%a13
  end subroutine gradVel
  subroutine printInstantToVTK(this,filename)
    class(instantField), intent(in) :: this
    character(*), intent(in)        :: filename

    integer :: i,j,k,si,sj,sk,ni,nj,nk,sizx,sizy,sizz

    si = this%cGrid%centroids%ix
    sj = this%cGrid%centroids%iy
    sk = this%cGrid%centroids%iz
    ni = this%cGrid%centroids%jx
    nj = this%cGrid%centroids%jy
    nk = this%cGrid%centroids%jz

    sizx = (ni-si+1)
    sizy = (nj-sj+1)
    sizz = (nk-sk+1)

    open(unit=11,file=filename,status='replace')

    write(11,'(A)') "# vtk DataFile Version 2.0"
    write(11,'(A)') "LESCOAST OUTPUT FIELD"
    write(11,'(A)') "ASCII"
    write(11,'(A)') "DATASET STRUCTURED_GRID"
    write(11,'(A,3I10)') "DIMENSIONS ", sizx, sizy, sizz
    write(11,'(A,I10,A)') "POINTS ", sizx*sizy*sizz, " double"

    cents: do k = sk, nk
      do j = sj, nj
        do i = si, ni
          write(11, '(3F18.10)') this%cGrid%centroids%x(i,j,k), &
            this%cGrid%centroids%y(i,j,k),  this%cGrid%centroids%z(i,j,k)
        end do
      end do
    end do cents
    
    write(11,'(A,I10)') "POINT_DATA ", sizx*sizy*sizz
    write(11,'(A)') "SCALARS fi double 1"
    write(11,'(A)') "LOOKUP_TABLE default"
    press: do k = sk, nk
      do j = sj, nj
        do i = si, ni
          write(11, '(F18.10)') this%pressure%v(i,j,k)
        end do
      end do
    end do press
    write(11,'(A)') "SCALARS re double 1"
    write(11,'(A)') "LOOKUP_TABLE default"
    re: do k = sk, nk
      do j = sj, nj
        do i = si, ni
          write(11, '(F18.10)') this%re%v(i,j,k)
        end do
      end do
    end do re
    write(11,'(A)') "VECTORS velocity double"
    vel: do k = sk, nk
      do j = sj, nj
        do i = si, ni
          write(11, '(3F18.10)') this%velocity%x(i,j,k), &
            this%velocity%y(i,j,k),  this%velocity%z(i,j,k)
        end do
      end do
    end do vel
    write(11,'(A)') "VECTORS vorticity double"
    vor: do k = sk, nk
      do j = sj, nj
        do i = si, ni
          write(11, '(3F18.10)') this%vorticity%x(i,j,k), &
            this%vorticity%y(i,j,k),  this%vorticity%z(i,j,k)
        end do
      end do
    end do vor
    write(11,'(A)') "TENSORS gradU double"
    gradu: do k = sk, nk
      do j = sj, nj
        do i = si, ni
          write(11, '(3F18.10)') this%velocityGradient%a11(i,j,k), &
            this%velocityGradient%a12(i,j,k),  this%velocityGradient%a13(i,j,k)
          write(11, '(3F18.10)') this%velocityGradient%a21(i,j,k), &
            this%velocityGradient%a22(i,j,k),  this%velocityGradient%a23(i,j,k)
          write(11, '(3F18.10)') this%velocityGradient%a31(i,j,k), &
            this%velocityGradient%a32(i,j,k),  this%velocityGradient%a33(i,j,k)
          write(11,*)
        end do
      end do
    end do gradu
    close(11)
  end subroutine printInstantToVTK
end module mod_fields
