program TEST
  use mod_typeKinds
  use mod_basicTypes
  use mod_grid
  use mod_fields
  implicit none

  type(grid), target :: lesGrid
  type(instantField) :: old
  type(vector) :: x
  real(kind=d_kind) :: r = 3.0

  call lesGrid%readGrid()
  !call old%readInstantField(lesGrid)
  call old%readInstantField(lesGrid,'2000')
  call old%printField('instantField.vtk')

  call x%alloc(lesGrid%centroids%ix,lesGrid%centroids%iy,lesGrid%centroids%iz, &
               lesGrid%centroids%jx,lesGrid%centroids%jy,lesGrid%centroids%jz)

  !x = lesGrid%centroids - r*lesGrid%centroids
  !print '(F18.10)', x%x 
end program TEST
