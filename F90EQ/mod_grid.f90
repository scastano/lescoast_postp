module mod_grid
  use mod_typeKinds
  use mod_basicTypes
  implicit none 
  private

  type, public :: grid
    type(vector) :: centroids
    type(vector) :: nodes
    type(tensor) :: jacobian
  contains
    procedure :: readGrid
  end type grid

contains
  subroutine readGrid(this)
    class(grid), intent(in out) :: this

    integer :: i, j, k, nx, ny, nz
    real(kind=d_kind) :: alx, aly, alz

    open(11,file='gri3dp_in.dat',status='old')
    read(11,*)alx,aly,alz
    read(11,*)nx,ny,nz

    call this%nodes%alloc(0,0,0,nx,ny,nz)

    do k=this%nodes%iz,this%nodes%jz
      do j=this%nodes%iy,this%nodes%jy
        do i=this%nodes%ix,this%nodes%jx
         read(11,*)this%nodes%x(i,j,k)
         read(11,*)this%nodes%y(i,j,k)
         read(11,*)this%nodes%z(i,j,k)
        end do
      end do
    end do
    close(11)
   
    call calculateCentroids (this%nodes,this%centroids)
    call calculateMetrics (this%centroids,this%jacobian) 
  end subroutine readGrid
  subroutine calculateCentroids(points, this)
    class(vector), intent(in    ) :: points
    class(vector), intent(in out) :: this

    integer :: i, j, k, nx, ny, nz

    nx = points%jx -  points%ix + 1 
    ny = points%jy -  points%iy + 1
    nz = points%jz -  points%iz + 1

    call this%alloc(points%ix,points%iy,points%iz, &
          nx,ny,nz)

    do k = this%iz + 1, this%jz - 1
      do j = this%iy + 1, this%jy - 1
        do i = this%ix + 1, this%jx - 1
          this%x(i,j,k) = 0.125 * (points%x(i,j,k)+points%x(i,j,k-1)+ &
            points%x(i-1,j,k-1)+points%x(i-1,j,k)+points%x(i,j-1,k) + &
            points%x(i,j-1,k-1)+points%x(i-1,j-1,k-1)+points%x(i-1,j-1,k))
          this%y(i,j,k) = 0.125 * (points%y(i,j,k)+points%y(i,j,k-1)+ &
            points%y(i-1,j,k-1)+points%y(i-1,j,k)+points%y(i,j-1,k) + &
            points%y(i,j-1,k-1)+points%y(i-1,j-1,k-1)+points%y(i-1,j-1,k))
          this%z(i,j,k) = 0.125 * (points%z(i,j,k)+points%z(i,j,k-1)+ &
            points%z(i-1,j,k-1)+points%z(i-1,j,k)+points%z(i,j-1,k) + &
            points%z(i,j-1,k-1)+points%z(i-1,j-1,k-1)+points%z(i-1,j-1,k))
        end do
      end do
    end do
! walls 1 & 2
    do k = this%iz + 1, this%jz - 1
      do j = this%iy + 1, this%jy - 1
        i = this%ix
        this%x(i,j,k) = 0.25 * (points%x(i,j,k)+points%x(i,j,k-1)+      &
                                points%x(i,j-1,k-1)+points%x(i,j-1,k))
        this%y(i,j,k) = 0.25 * (points%y(i,j,k)+points%y(i,j,k-1)+      &
                                points%y(i,j-1,k-1)+points%y(i,j-1,k))
        this%z(i,j,k) = 0.25 * (points%z(i,j,k)+points%z(i,j,k-1)+      &
                                points%z(i,j-1,k-1)+points%z(i,j-1,k))
        i = this%jx-1
        this%x(i+1,j,k) = 0.25 * (points%x(i,j,k)+points%x(i,j,k-1)+      &
                                points%x(i,j-1,k-1)+points%x(i,j-1,k))
        this%y(i+1,j,k) = 0.25 * (points%y(i,j,k)+points%y(i,j,k-1)+      &
                                points%y(i,j-1,k-1)+points%y(i,j-1,k))
        this%z(i+1,j,k) = 0.25 * (points%z(i,j,k)+points%z(i,j,k-1)+      &
                                points%z(i,j-1,k-1)+points%z(i,j-1,k))
      end do
    end do
    do i = this%ix+1, this%jx - 1
      j = this%iy
      k = this%iz
      this%x(i,j,k) = 0.5 * (points%x(i,j,k)+points%x(i-1,j,k))
      this%y(i,j,k) = 0.5 * (points%y(i,j,k)+points%y(i-1,j,k))
      this%z(i,j,k) = 0.5 * (points%z(i,j,k)+points%z(i-1,j,k))
      j = this%iy
      k = this%jz-1
      this%x(i,j,k+1) = 0.5 * (points%x(i,j,k)+points%x(i-1,j,k))
      this%y(i,j,k+1) = 0.5 * (points%y(i,j,k)+points%y(i-1,j,k))
      this%z(i,j,k+1) = 0.5 * (points%z(i,j,k)+points%z(i-1,j,k))
      j = this%jy-1
      k = this%jz-1
      this%x(i,j+1,k+1) = 0.5 * (points%x(i,j,k)+points%x(i-1,j,k))
      this%y(i,j+1,k+1) = 0.5 * (points%y(i,j,k)+points%y(i-1,j,k))
      this%z(i,j+1,k+1) = 0.5 * (points%z(i,j,k)+points%z(i-1,j,k))
      j = this%jy-1
      k = this%iz
      this%x(i,j+1,k) = 0.5 * (points%x(i,j,k)+points%x(i-1,j,k))
      this%y(i,j+1,k) = 0.5 * (points%y(i,j,k)+points%y(i-1,j,k))
      this%z(i,j+1,k) = 0.5 * (points%z(i,j,k)+points%z(i-1,j,k))
    end do
! walls 3 & 4
    do k = this%iz + 1, this%jz - 1
      do i = this%ix + 1, this%jx - 1
        j = this%iy
        this%x(i,j,k) = 0.25 * (points%x(i,j,k)+points%x(i,j,k-1)+      &
                                points%x(i-1,j,k-1)+points%x(i-1,j,k))
        this%y(i,j,k) = 0.25 * (points%y(i,j,k)+points%y(i,j,k-1)+      &
                                points%y(i-1,j,k-1)+points%y(i-1,j,k))
        this%z(i,j,k) = 0.25 * (points%z(i,j,k)+points%z(i,j,k-1)+      &
                                points%z(i-1,j,k-1)+points%z(i-1,j,k))
        j = this%jy-1
        this%x(i,j+1,k) = 0.25 * (points%x(i,j,k)+points%x(i,j,k-1)+      &
                                points%x(i-1,j,k-1)+points%x(i-1,j,k))
        this%y(i,j+1,k) = 0.25 * (points%y(i,j,k)+points%y(i,j,k-1)+      &
                                points%y(i-1,j,k-1)+points%y(i-1,j,k))
        this%z(i,j+1,k) = 0.25 * (points%z(i,j,k)+points%z(i,j,k-1)+      &
                                points%z(i-1,j,k-1)+points%z(i-1,j,k))
      end do
    end do
    do j = this%iy + 1, this%jy - 1
      i = this%ix
      k = this%iz
      this%x(i,j,k) = 0.5 * (points%x(i,j,k)+points%x(i,j-1,k))
      this%y(i,j,k) = 0.5 * (points%y(i,j,k)+points%y(i,j-1,k))
      this%z(i,j,k) = 0.5 * (points%z(i,j,k)+points%z(i,j-1,k))
      i = this%ix
      k = this%jz-1
      this%x(i,j,k+1) = 0.5 * (points%x(i,j,k)+points%x(i,j-1,k))
      this%y(i,j,k+1) = 0.5 * (points%y(i,j,k)+points%y(i,j-1,k))
      this%z(i,j,k+1) = 0.5 * (points%z(i,j,k)+points%z(i,j-1,k))
      i = this%jx-1
      k = this%jz-1
      this%x(i+1,j,k+1) = 0.5 * (points%x(i,j,k)+points%x(i,j-1,k))
      this%y(i+1,j,k+1) = 0.5 * (points%y(i,j,k)+points%y(i,j-1,k))
      this%z(i+1,j,k+1) = 0.5 * (points%z(i,j,k)+points%z(i,j-1,k))
      i = this%jx-1
      k = this%iz
      this%x(i+1,j,k) = 0.5 * (points%x(i,j,k)+points%x(i,j-1,k))
      this%y(i+1,j,k) = 0.5 * (points%y(i,j,k)+points%y(i,j-1,k))
      this%z(i+1,j,k) = 0.5 * (points%z(i,j,k)+points%z(i,j-1,k))
    end do
! walls 1 & 2
    do i = this%ix + 1, this%jx - 1
      do j = this%iy + 1, this%jy - 1
        k = this%iz 
        this%x(i,j,k) = 0.25 * (points%x(i,j,k)+points%x(i,j-1,k)+      &
                                points%x(i-1,j-1,k)+points%x(i-1,j,k))
        this%y(i,j,k) = 0.25 * (points%y(i,j,k)+points%y(i,j-1,k)+      &
                                points%y(i-1,j-1,k)+points%y(i-1,j,k))
        this%z(i,j,k) = 0.25 * (points%z(i,j,k)+points%z(i,j-1,k)+      &
                                points%z(i-1,j-1,k)+points%z(i-1,j,k))
        k = this%jz-1
        this%x(i,j,k+1) = 0.25 * (points%x(i,j,k)+points%x(i,j-1,k)+      &
                                  points%x(i-1,j-1,k)+points%x(i-1,j,k))
        this%y(i,j,k+1) = 0.25 * (points%y(i,j,k)+points%y(i,j-1,k)+      &
                                  points%y(i-1,j-1,k)+points%y(i-1,j,k))
        this%z(i,j,k+1) = 0.25 * (points%z(i,j,k)+points%z(i,j-1,k)+      &
                                  points%z(i-1,j-1,k)+points%z(i-1,j,k))
      end do
    end do
    do k = this%iz + 1, this%jz - 1
      j = this%iy 
      i = this%ix
      this%x(i,j,k) = 0.5 * (points%x(i,j,k)+points%x(i,j,k-1))
      this%y(i,j,k) = 0.5 * (points%y(i,j,k)+points%y(i,j,k-1))
      this%z(i,j,k) = 0.5 * (points%z(i,j,k)+points%z(i,j,k-1))
      j = this%iy 
      i = this%jx-1
      this%x(i+1,j,k) = 0.5 * (points%x(i,j,k)+points%x(i,j,k-1))
      this%y(i+1,j,k) = 0.5 * (points%y(i,j,k)+points%y(i,j,k-1))
      this%z(i+1,j,k) = 0.5 * (points%z(i,j,k)+points%z(i,j,k-1))
      j = this%jy-1
      i = this%jx-1
      this%x(i+1,j+1,k) = 0.5 * (points%x(i,j,k)+points%x(i,j,k-1))
      this%y(i+1,j+1,k) = 0.5 * (points%y(i,j,k)+points%y(i,j,k-1))
      this%z(i+1,j+1,k) = 0.5 * (points%z(i,j,k)+points%z(i,j,k-1))
      j = this%jy-1
      i = this%ix
      this%x(i,j+1,k) = 0.5 * (points%x(i,j,k)+points%x(i,j,k-1))
      this%y(i,j+1,k) = 0.5 * (points%y(i,j,k)+points%y(i,j,k-1))
      this%z(i,j+1,k) = 0.5 * (points%z(i,j,k)+points%z(i,j,k-1))
    end do

    i = this%ix
    j = this%iy 
    k = this%iz 
    this%x(i,j,k) = points%x(i,j,k)
    this%y(i,j,k) = points%y(i,j,k)
    this%z(i,j,k) = points%z(i,j,k)
    i = this%jx-1
    j = this%iy 
    k = this%iz 
    this%x(i+1,j,k) = points%x(i,j,k)
    this%y(i+1,j,k) = points%y(i,j,k)
    this%z(i+1,j,k) = points%z(i,j,k)
    i = this%jx-1
    j = this%jy-1
    k = this%iz 
    this%x(i+1,j+1,k) = points%x(i,j,k)
    this%y(i+1,j+1,k) = points%y(i,j,k)
    this%z(i+1,j+1,k) = points%z(i,j,k)
    i = this%jx-1
    j = this%iy 
    k = this%jz-1
    this%x(i+1,j,k+1) = points%x(i,j,k)
    this%y(i+1,j,k+1) = points%y(i,j,k)
    this%z(i+1,j,k+1) = points%z(i,j,k)
    i = this%jx-1
    j = this%jy-1
    k = this%jz-1
    this%x(i+1,j+1,k+1) = points%x(i,j,k)
    this%y(i+1,j+1,k+1) = points%y(i,j,k)
    this%z(i+1,j+1,k+1) = points%z(i,j,k)
    i = this%ix
    j = this%jy-1
    k = this%jz-1
    this%x(i,j+1,k+1) = points%x(i,j,k)
    this%y(i,j+1,k+1) = points%y(i,j,k)
    this%z(i,j+1,k+1) = points%z(i,j,k)
    i = this%ix
    j = this%iy 
    k = this%jz-1
    this%x(i,j,k+1) = points%x(i,j,k)
    this%y(i,j,k+1) = points%y(i,j,k)
    this%z(i,j,k+1) = points%z(i,j,k)
    i = this%ix
    j = this%jy-1
    k = this%iz 
    this%x(i,j+1,k) = points%x(i,j,k)
    this%y(i,j+1,k) = points%y(i,j,k)
    this%z(i,j+1,k) = points%z(i,j,k)
  end subroutine calculateCentroids

  subroutine calculateMetrics(cent,this)
    class(vector), intent(in   ) :: cent ! input are the centroids
    class(tensor), intent(inout) :: this

    type(tensor) :: ijac

    integer :: i, j, k, nx, ny, nz

    if (.not.cent%isInitialized) &
      stop "Class is not initialized. STOP."

    nx = cent%jx - cent%ix 
    ny = cent%jy - cent%iy 
    nz = cent%jz - cent%iz 

    call this%alloc(cent%ix,cent%iy,cent%iz, &
          nx,ny,nz)

    call ijac%alloc(cent%ix,cent%iy,cent%iz, &
          nx,ny,nz)

    do k = ijac%iz + 1, ijac%jz -1
      do j = ijac%iy + 1, ijac%jy -1
        do i = ijac%ix + 1, ijac%jx -1
         ! dx/dcsi, dy/dcsi, dz/dcsi
         ijac%a11(i,j,k) = 0.5*(cent%x(i+1,j,k)-cent%x(i-1,j,k)) 
         ijac%a21(i,j,k) = 0.5*(cent%y(i+1,j,k)-cent%y(i-1,j,k)) 
         ijac%a31(i,j,k) = 0.5*(cent%z(i+1,j,k)-cent%z(i-1,j,k)) 
         ! dx/deta, dy/deta, dz/deta
         ijac%a12(i,j,k) = 0.5*(cent%x(i,j+1,k)-cent%x(i,j-1,k)) 
         ijac%a22(i,j,k) = 0.5*(cent%y(i,j+1,k)-cent%y(i,j-1,k)) 
         ijac%a32(i,j,k) = 0.5*(cent%z(i,j+1,k)-cent%z(i,j-1,k)) 
         ! dx/dzita, dy/dzita, dz/dzita
         ijac%a13(i,j,k) = 0.5*(cent%x(i,j,k+1)-cent%x(i,j,k-1)) 
         ijac%a23(i,j,k) = 0.5*(cent%y(i,j,k+1)-cent%y(i,j,k-1)) 
         ijac%a33(i,j,k) = 0.5*(cent%z(i,j,k+1)-cent%z(i,j,k-1)) 
        end do
      end do
    end do
    !faces 1 & 2
    do k = ijac%iz + 1, ijac%jz -1
      do j = ijac%iy + 1, ijac%jy -1
        i = ijac%ix
        ! first order
        ! dx/dcsi, dy/dcsi, dz/dcsi
        ijac%a11(i,j,k) = (cent%x(i+1,j,k)-cent%x(i,j,k))
        ijac%a21(i,j,k) = (cent%y(i+1,j,k)-cent%y(i,j,k))
        ijac%a31(i,j,k) = (cent%z(i+1,j,k)-cent%z(i,j,k))
        ! dx/deta, dy/deta, dz/deta
        ijac%a12(i,j,k) = 0.5*(cent%x(i,j+1,k)-cent%x(i,j-1,k)) 
        ijac%a22(i,j,k) = 0.5*(cent%y(i,j+1,k)-cent%y(i,j-1,k)) 
        ijac%a32(i,j,k) = 0.5*(cent%z(i,j+1,k)-cent%z(i,j-1,k)) 
        ! dx/dzita, dy/dzita, dz/dzita
        ijac%a13(i,j,k) = 0.5*(cent%x(i,j,k+1)-cent%x(i,j,k-1)) 
        ijac%a23(i,j,k) = 0.5*(cent%y(i,j,k+1)-cent%y(i,j,k-1)) 
        ijac%a33(i,j,k) = 0.5*(cent%z(i,j,k+1)-cent%z(i,j,k-1)) 
        i = ijac%jx
        ! first order
        ! dx/dcsi, dy/dcsi, dz/dcsi
        ijac%a11(i,j,k) = (cent%x(i,j,k)-cent%x(i-1,j,k))
        ijac%a21(i,j,k) = (cent%y(i,j,k)-cent%y(i-1,j,k))
        ijac%a31(i,j,k) = (cent%z(i,j,k)-cent%z(i-1,j,k))
        ! dx/deta, dy/deta, dz/deta
        ijac%a12(i,j,k) = 0.5*(cent%x(i,j+1,k)-cent%x(i,j-1,k)) 
        ijac%a22(i,j,k) = 0.5*(cent%y(i,j+1,k)-cent%y(i,j-1,k)) 
        ijac%a32(i,j,k) = 0.5*(cent%z(i,j+1,k)-cent%z(i,j-1,k)) 
        ! dx/dzita, dy/dzita, dz/dzita
        ijac%a13(i,j,k) = 0.5*(cent%x(i,j,k+1)-cent%x(i,j,k-1)) 
        ijac%a23(i,j,k) = 0.5*(cent%y(i,j,k+1)-cent%y(i,j,k-1)) 
        ijac%a33(i,j,k) = 0.5*(cent%z(i,j,k+1)-cent%z(i,j,k-1)) 
      end do
    end do
    !faces 3 & 4
    do k = ijac%iz + 1, ijac%jz -1
      do i = ijac%ix + 1, ijac%jx -1
        j = ijac%iy
        ! dx/dcsi, dy/dcsi, dz/dcsi
        ijac%a11(i,j,k) = 0.5*(cent%x(i+1,j,k)-cent%x(i-1,j,k)) 
        ijac%a21(i,j,k) = 0.5*(cent%y(i+1,j,k)-cent%y(i-1,j,k)) 
        ijac%a31(i,j,k) = 0.5*(cent%z(i+1,j,k)-cent%z(i-1,j,k)) 
        ! first order
        ! dx/deta, dy/deta, dz/deta
        ijac%a12(i,j,k) = (cent%x(i,j+1,k)-cent%x(i,j,k)) 
        ijac%a22(i,j,k) = (cent%y(i,j+1,k)-cent%y(i,j,k)) 
        ijac%a32(i,j,k) = (cent%z(i,j+1,k)-cent%z(i,j,k)) 
        ! dx/dzita, dy/dzita, dz/dzita
        ijac%a13(i,j,k) = 0.5*(cent%x(i,j,k+1)-cent%x(i,j,k-1)) 
        ijac%a23(i,j,k) = 0.5*(cent%y(i,j,k+1)-cent%y(i,j,k-1)) 
        ijac%a33(i,j,k) = 0.5*(cent%z(i,j,k+1)-cent%z(i,j,k-1)) 
        j = ijac%jy
        ! dx/dcsi, dy/dcsi, dz/dcsi
        ijac%a11(i,j,k) = 0.5*(cent%x(i+1,j,k)-cent%x(i-1,j,k)) 
        ijac%a21(i,j,k) = 0.5*(cent%y(i+1,j,k)-cent%y(i-1,j,k)) 
        ijac%a31(i,j,k) = 0.5*(cent%z(i+1,j,k)-cent%z(i-1,j,k)) 
        ! first order
        ! dx/deta, dy/deta, dz/deta
        ijac%a12(i,j,k) = (cent%x(i,j,k)-cent%x(i,j-1,k)) 
        ijac%a22(i,j,k) = (cent%y(i,j,k)-cent%y(i,j-1,k)) 
        ijac%a32(i,j,k) = (cent%z(i,j,k)-cent%z(i,j-1,k)) 
        ! dx/dzita, dy/dzita, dz/dzita
        ijac%a13(i,j,k) = 0.5*(cent%x(i,j,k+1)-cent%x(i,j,k-1)) 
        ijac%a23(i,j,k) = 0.5*(cent%y(i,j,k+1)-cent%y(i,j,k-1)) 
        ijac%a33(i,j,k) = 0.5*(cent%z(i,j,k+1)-cent%z(i,j,k-1)) 
      end do
    end do
    !faces 5 & 6
    do j = ijac%iy + 1, ijac%jy -1
      do i = ijac%ix + 1, ijac%jx -1
        k = ijac%iz
        ! dx/dcsi, dy/dcsi, dz/dcsi
        ijac%a11(i,j,k) = 0.5*(cent%x(i+1,j,k)-cent%x(i-1,j,k)) 
        ijac%a21(i,j,k) = 0.5*(cent%y(i+1,j,k)-cent%y(i-1,j,k)) 
        ijac%a31(i,j,k) = 0.5*(cent%z(i+1,j,k)-cent%z(i-1,j,k)) 
        ! dx/deta, dy/deta, dz/deta
        ijac%a12(i,j,k) = 0.5*(cent%x(i,j+1,k)-cent%x(i,j-1,k)) 
        ijac%a22(i,j,k) = 0.5*(cent%y(i,j+1,k)-cent%y(i,j-1,k)) 
        ijac%a32(i,j,k) = 0.5*(cent%z(i,j+1,k)-cent%z(i,j-1,k)) 
        ! first order
        ! dx/dzita, dy/dzita, dz/dzita
        ijac%a13(i,j,k) = (cent%x(i,j,k+1)-cent%x(i,j,k))
        ijac%a23(i,j,k) = (cent%y(i,j,k+1)-cent%y(i,j,k))
        ijac%a33(i,j,k) = (cent%z(i,j,k+1)-cent%z(i,j,k))
        k = ijac%jz
        ! dx/dcsi, dy/dcsi, dz/dcsi
        ijac%a11(i,j,k) = 0.5*(cent%x(i+1,j,k)-cent%x(i-1,j,k)) 
        ijac%a21(i,j,k) = 0.5*(cent%y(i+1,j,k)-cent%y(i-1,j,k)) 
        ijac%a31(i,j,k) = 0.5*(cent%z(i+1,j,k)-cent%z(i-1,j,k)) 
        ! dx/deta, dy/deta, dz/deta
        ijac%a12(i,j,k) = 0.5*(cent%x(i,j+1,k)-cent%x(i,j-1,k)) 
        ijac%a22(i,j,k) = 0.5*(cent%y(i,j+1,k)-cent%y(i,j-1,k)) 
        ijac%a32(i,j,k) = 0.5*(cent%z(i,j+1,k)-cent%z(i,j-1,k)) 
        ! first order
        ! dx/dzita, dy/dzita, dz/dzita
        ijac%a13(i,j,k) = (cent%x(i,j,k)-cent%x(i,j,k-1))
        ijac%a23(i,j,k) = (cent%y(i,j,k)-cent%y(i,j,k-1))
        ijac%a33(i,j,k) = (cent%z(i,j,k)-cent%z(i,j,k-1))
      end do
    end do
    ! edges
    do k = ijac%iz + 1, ijac%jz -1
      i = ijac%ix
      j = ijac%iy
      ! first order
      ! dx/dcsi, dy/dcsi, dz/dcsi
      ijac%a11(i,j,k) = (cent%x(i+1,j,k)-cent%x(i,j,k))
      ijac%a21(i,j,k) = (cent%y(i+1,j,k)-cent%y(i,j,k))
      ijac%a31(i,j,k) = (cent%z(i+1,j,k)-cent%z(i,j,k))
      ! first order
      ! dx/deta, dy/deta, dz/deta
      ijac%a12(i,j,k) = (cent%x(i,j+1,k)-cent%x(i,j,k)) 
      ijac%a22(i,j,k) = (cent%y(i,j+1,k)-cent%y(i,j,k)) 
      ijac%a32(i,j,k) = (cent%z(i,j+1,k)-cent%z(i,j,k)) 
      ! dx/dzita, dy/dzita, dz/dzita
      ijac%a13(i,j,k) = 0.5*(cent%x(i,j,k+1)-cent%x(i,j,k-1)) 
      ijac%a23(i,j,k) = 0.5*(cent%y(i,j,k+1)-cent%y(i,j,k-1)) 
      ijac%a33(i,j,k) = 0.5*(cent%z(i,j,k+1)-cent%z(i,j,k-1))
 
      i = ijac%ix
      j = ijac%jy
      ! first order
      ! dx/dcsi, dy/dcsi, dz/dcsi
      ijac%a11(i,j,k) = (cent%x(i+1,j,k)-cent%x(i,j,k))
      ijac%a21(i,j,k) = (cent%y(i+1,j,k)-cent%y(i,j,k))
      ijac%a31(i,j,k) = (cent%z(i+1,j,k)-cent%z(i,j,k))
      ! first order
      ! dx/deta, dy/deta, dz/deta
      ijac%a12(i,j,k) = (cent%x(i,j,k)-cent%x(i,j-1,k)) 
      ijac%a22(i,j,k) = (cent%y(i,j,k)-cent%y(i,j-1,k)) 
      ijac%a32(i,j,k) = (cent%z(i,j,k)-cent%z(i,j-1,k)) 
      ! dx/dzita, dy/dzita, dz/dzita
      ijac%a13(i,j,k) = 0.5*(cent%x(i,j,k+1)-cent%x(i,j,k-1)) 
      ijac%a23(i,j,k) = 0.5*(cent%y(i,j,k+1)-cent%y(i,j,k-1)) 
      ijac%a33(i,j,k) = 0.5*(cent%z(i,j,k+1)-cent%z(i,j,k-1))
 
      i = ijac%jx
      j = ijac%iy
      ! first order
      ! dx/dcsi, dy/dcsi, dz/dcsi
      ijac%a11(i,j,k) = (cent%x(i,j,k)-cent%x(i-1,j,k))
      ijac%a21(i,j,k) = (cent%y(i,j,k)-cent%y(i-1,j,k))
      ijac%a31(i,j,k) = (cent%z(i,j,k)-cent%z(i-1,j,k))
      ! first order
      ! dx/deta, dy/deta, dz/deta
      ijac%a12(i,j,k) = (cent%x(i,j+1,k)-cent%x(i,j,k)) 
      ijac%a22(i,j,k) = (cent%y(i,j+1,k)-cent%y(i,j,k)) 
      ijac%a32(i,j,k) = (cent%z(i,j+1,k)-cent%z(i,j,k)) 
      ! dx/dzita, dy/dzita, dz/dzita
      ijac%a13(i,j,k) = 0.5*(cent%x(i,j,k+1)-cent%x(i,j,k-1)) 
      ijac%a23(i,j,k) = 0.5*(cent%y(i,j,k+1)-cent%y(i,j,k-1)) 
      ijac%a33(i,j,k) = 0.5*(cent%z(i,j,k+1)-cent%z(i,j,k-1)) 
 
      i = ijac%jx
      j = ijac%jy
      ! first order
      ! dx/dcsi, dy/dcsi, dz/dcsi
      ijac%a11(i,j,k) = (cent%x(i,j,k)-cent%x(i-1,j,k))
      ijac%a21(i,j,k) = (cent%y(i,j,k)-cent%y(i-1,j,k))
      ijac%a31(i,j,k) = (cent%z(i,j,k)-cent%z(i-1,j,k))
      ! first order
      ! dx/deta, dy/deta, dz/deta
      ijac%a12(i,j,k) = (cent%x(i,j,k)-cent%x(i,j-1,k)) 
      ijac%a22(i,j,k) = (cent%y(i,j,k)-cent%y(i,j-1,k)) 
      ijac%a32(i,j,k) = (cent%z(i,j,k)-cent%z(i,j-1,k)) 
      ! dx/dzita, dy/dzita, dz/dzita
      ijac%a13(i,j,k) = 0.5*(cent%x(i,j,k+1)-cent%x(i,j,k-1)) 
      ijac%a23(i,j,k) = 0.5*(cent%y(i,j,k+1)-cent%y(i,j,k-1)) 
      ijac%a33(i,j,k) = 0.5*(cent%z(i,j,k+1)-cent%z(i,j,k-1)) 
    end do
    do j = ijac%iy + 1, ijac%jy -1
      i = ijac%ix
      k = ijac%iz
      ! first order
      ! dx/dcsi, dy/dcsi, dz/dcsi
      ijac%a11(i,j,k) = (cent%x(i+1,j,k)-cent%x(i,j,k))
      ijac%a21(i,j,k) = (cent%y(i+1,j,k)-cent%y(i,j,k))
      ijac%a31(i,j,k) = (cent%z(i+1,j,k)-cent%z(i,j,k))
      ! dx/deta, dy/deta, dz/deta
      ijac%a12(i,j,k) = 0.5*(cent%x(i,j+1,k)-cent%x(i,j-1,k)) 
      ijac%a22(i,j,k) = 0.5*(cent%y(i,j+1,k)-cent%y(i,j-1,k)) 
      ijac%a32(i,j,k) = 0.5*(cent%z(i,j+1,k)-cent%z(i,j-1,k)) 
      ! first order
      ! dx/dzita, dy/dzita, dz/dzita
      ijac%a13(i,j,k) = (cent%x(i,j,k+1)-cent%x(i,j,k))
      ijac%a23(i,j,k) = (cent%y(i,j,k+1)-cent%y(i,j,k))
      ijac%a33(i,j,k) = (cent%z(i,j,k+1)-cent%z(i,j,k))

      i = ijac%jx
      k = ijac%iz
      ! first order
      ! dx/dcsi, dy/dcsi, dz/dcsi
      ijac%a11(i,j,k) = (cent%x(i,j,k)-cent%x(i-1,j,k))
      ijac%a21(i,j,k) = (cent%y(i,j,k)-cent%y(i-1,j,k))
      ijac%a31(i,j,k) = (cent%z(i,j,k)-cent%z(i-1,j,k))
      ! dx/deta, dy/deta, dz/deta
      ijac%a12(i,j,k) = 0.5*(cent%x(i,j+1,k)-cent%x(i,j-1,k)) 
      ijac%a22(i,j,k) = 0.5*(cent%y(i,j+1,k)-cent%y(i,j-1,k)) 
      ijac%a32(i,j,k) = 0.5*(cent%z(i,j+1,k)-cent%z(i,j-1,k)) 
      ! first order
      ! dx/dzita, dy/dzita, dz/dzita
      ijac%a13(i,j,k) = (cent%x(i,j,k+1)-cent%x(i,j,k))
      ijac%a23(i,j,k) = (cent%y(i,j,k+1)-cent%y(i,j,k))
      ijac%a33(i,j,k) = (cent%z(i,j,k+1)-cent%z(i,j,k))

      i = ijac%ix
      k = ijac%jz
      ! first order
      ! dx/dcsi, dy/dcsi, dz/dcsi
      ijac%a11(i,j,k) = (cent%x(i+1,j,k)-cent%x(i,j,k))
      ijac%a21(i,j,k) = (cent%y(i+1,j,k)-cent%y(i,j,k))
      ijac%a31(i,j,k) = (cent%z(i+1,j,k)-cent%z(i,j,k))
      ! dx/deta, dy/deta, dz/deta
      ijac%a12(i,j,k) = 0.5*(cent%x(i,j+1,k)-cent%x(i,j-1,k)) 
      ijac%a22(i,j,k) = 0.5*(cent%y(i,j+1,k)-cent%y(i,j-1,k)) 
      ijac%a32(i,j,k) = 0.5*(cent%z(i,j+1,k)-cent%z(i,j-1,k)) 
      ! first order
      ! dx/dzita, dy/dzita, dz/dzita
      ijac%a13(i,j,k) = (cent%x(i,j,k)-cent%x(i,j,k-1))
      ijac%a23(i,j,k) = (cent%y(i,j,k)-cent%y(i,j,k-1))
      ijac%a33(i,j,k) = (cent%z(i,j,k)-cent%z(i,j,k-1))

      i = ijac%jx
      k = ijac%jz
      ! first order
      ! dx/dcsi, dy/dcsi, dz/dcsi
      ijac%a11(i,j,k) = (cent%x(i,j,k)-cent%x(i-1,j,k))
      ijac%a21(i,j,k) = (cent%y(i,j,k)-cent%y(i-1,j,k))
      ijac%a31(i,j,k) = (cent%z(i,j,k)-cent%z(i-1,j,k))
      ! dx/deta, dy/deta, dz/deta
      ijac%a12(i,j,k) = 0.5*(cent%x(i,j+1,k)-cent%x(i,j-1,k)) 
      ijac%a22(i,j,k) = 0.5*(cent%y(i,j+1,k)-cent%y(i,j-1,k)) 
      ijac%a32(i,j,k) = 0.5*(cent%z(i,j+1,k)-cent%z(i,j-1,k)) 
      ! first order
      ! dx/dzita, dy/dzita, dz/dzita
      ijac%a13(i,j,k) = (cent%x(i,j,k)-cent%x(i,j,k-1))
      ijac%a23(i,j,k) = (cent%y(i,j,k)-cent%y(i,j,k-1))
      ijac%a33(i,j,k) = (cent%z(i,j,k)-cent%z(i,j,k-1))
    end do
    do i = ijac%ix + 1, ijac%jx -1
      j = ijac%iy
      k = ijac%iz
      ! dx/dcsi, dy/dcsi, dz/dcsi
      ijac%a11(i,j,k) = 0.5*(cent%x(i+1,j,k)-cent%x(i-1,j,k)) 
      ijac%a21(i,j,k) = 0.5*(cent%y(i+1,j,k)-cent%y(i-1,j,k)) 
      ijac%a31(i,j,k) = 0.5*(cent%z(i+1,j,k)-cent%z(i-1,j,k)) 
      ! first order
      ! dx/deta, dy/deta, dz/deta
      ijac%a12(i,j,k) = (cent%x(i,j+1,k)-cent%x(i,j,k)) 
      ijac%a22(i,j,k) = (cent%y(i,j+1,k)-cent%y(i,j,k)) 
      ijac%a32(i,j,k) = (cent%z(i,j+1,k)-cent%z(i,j,k)) 
      ! first order
      ! dx/dzita, dy/dzita, dz/dzita
      ijac%a13(i,j,k) = (cent%x(i,j,k+1)-cent%x(i,j,k))
      ijac%a23(i,j,k) = (cent%y(i,j,k+1)-cent%y(i,j,k))
      ijac%a33(i,j,k) = (cent%z(i,j,k+1)-cent%z(i,j,k))

      j = ijac%iy
      k = ijac%jz
      ! dx/dcsi, dy/dcsi, dz/dcsi
      ijac%a11(i,j,k) = 0.5*(cent%x(i+1,j,k)-cent%x(i-1,j,k)) 
      ijac%a21(i,j,k) = 0.5*(cent%y(i+1,j,k)-cent%y(i-1,j,k)) 
      ijac%a31(i,j,k) = 0.5*(cent%z(i+1,j,k)-cent%z(i-1,j,k)) 
      ! first order
      ! dx/deta, dy/deta, dz/deta
      ijac%a12(i,j,k) = (cent%x(i,j+1,k)-cent%x(i,j,k)) 
      ijac%a22(i,j,k) = (cent%y(i,j+1,k)-cent%y(i,j,k)) 
      ijac%a32(i,j,k) = (cent%z(i,j+1,k)-cent%z(i,j,k)) 
      ! first order
      ! dx/dzita, dy/dzita, dz/dzita
      ijac%a13(i,j,k) = (cent%x(i,j,k)-cent%x(i,j,k-1))
      ijac%a23(i,j,k) = (cent%y(i,j,k)-cent%y(i,j,k-1))
      ijac%a33(i,j,k) = (cent%z(i,j,k)-cent%z(i,j,k-1))

      j = ijac%jy
      k = ijac%iz
      ! dx/dcsi, dy/dcsi, dz/dcsi
      ijac%a11(i,j,k) = 0.5*(cent%x(i+1,j,k)-cent%x(i-1,j,k)) 
      ijac%a21(i,j,k) = 0.5*(cent%y(i+1,j,k)-cent%y(i-1,j,k)) 
      ijac%a31(i,j,k) = 0.5*(cent%z(i+1,j,k)-cent%z(i-1,j,k)) 
      ! first order
      ! dx/deta, dy/deta, dz/deta
      ijac%a12(i,j,k) = (cent%x(i,j,k)-cent%x(i,j-1,k)) 
      ijac%a22(i,j,k) = (cent%y(i,j,k)-cent%y(i,j-1,k)) 
      ijac%a32(i,j,k) = (cent%z(i,j,k)-cent%z(i,j-1,k)) 
      ! first order
      ! dx/dzita, dy/dzita, dz/dzita
      ijac%a13(i,j,k) = (cent%x(i,j,k+1)-cent%x(i,j,k))
      ijac%a23(i,j,k) = (cent%y(i,j,k+1)-cent%y(i,j,k))
      ijac%a33(i,j,k) = (cent%z(i,j,k+1)-cent%z(i,j,k))

      j = ijac%jy
      k = ijac%jz
      ! dx/dcsi, dy/dcsi, dz/dcsi
      ijac%a11(i,j,k) = 0.5*(cent%x(i+1,j,k)-cent%x(i-1,j,k)) 
      ijac%a21(i,j,k) = 0.5*(cent%y(i+1,j,k)-cent%y(i-1,j,k)) 
      ijac%a31(i,j,k) = 0.5*(cent%z(i+1,j,k)-cent%z(i-1,j,k)) 
      ! first order
      ! dx/deta, dy/deta, dz/deta
      ijac%a12(i,j,k) = (cent%x(i,j,k)-cent%x(i,j-1,k)) 
      ijac%a22(i,j,k) = (cent%y(i,j,k)-cent%y(i,j-1,k)) 
      ijac%a32(i,j,k) = (cent%z(i,j,k)-cent%z(i,j-1,k)) 
      ! first order
      ! dx/dzita, dy/dzita, dz/dzita
      ijac%a13(i,j,k) = (cent%x(i,j,k)-cent%x(i,j,k-1))
      ijac%a23(i,j,k) = (cent%y(i,j,k)-cent%y(i,j,k-1))
      ijac%a33(i,j,k) = (cent%z(i,j,k)-cent%z(i,j,k-1))
    end do
    ! vertices
    i = ijac%ix
    j = ijac%iy
    k = ijac%iz
    ! dx/dcsi, dy/dcsi, dz/dcsi
    ijac%a11(i,j,k) = (cent%x(i+1,j,k)-cent%x(i,j,k))
    ijac%a21(i,j,k) = (cent%y(i+1,j,k)-cent%y(i,j,k))
    ijac%a31(i,j,k) = (cent%z(i+1,j,k)-cent%z(i,j,k))
    ! first order
    ! dx/deta, dy/deta, dz/deta
    ijac%a12(i,j,k) = (cent%x(i,j+1,k)-cent%x(i,j,k)) 
    ijac%a22(i,j,k) = (cent%y(i,j+1,k)-cent%y(i,j,k)) 
    ijac%a32(i,j,k) = (cent%z(i,j+1,k)-cent%z(i,j,k)) 
    ! first order
    ! dx/dzita, dy/dzita, dz/dzita
    ijac%a13(i,j,k) = (cent%x(i,j,k+1)-cent%x(i,j,k))
    ijac%a23(i,j,k) = (cent%y(i,j,k+1)-cent%y(i,j,k))
    ijac%a33(i,j,k) = (cent%z(i,j,k+1)-cent%z(i,j,k))

    i = ijac%ix
    j = ijac%jy
    k = ijac%iz
    ! dx/dcsi, dy/dcsi, dz/dcsi
    ijac%a11(i,j,k) = (cent%x(i+1,j,k)-cent%x(i,j,k))
    ijac%a21(i,j,k) = (cent%y(i+1,j,k)-cent%y(i,j,k))
    ijac%a31(i,j,k) = (cent%z(i+1,j,k)-cent%z(i,j,k))
    ! first order
    ! dx/deta, dy/deta, dz/deta
    ijac%a12(i,j,k) = (cent%x(i,j,k)-cent%x(i,j-1,k)) 
    ijac%a22(i,j,k) = (cent%y(i,j,k)-cent%y(i,j-1,k)) 
    ijac%a32(i,j,k) = (cent%z(i,j,k)-cent%z(i,j-1,k)) 
    ! first order
    ! dx/dzita, dy/dzita, dz/dzita
    ijac%a13(i,j,k) = (cent%x(i,j,k+1)-cent%x(i,j,k))
    ijac%a23(i,j,k) = (cent%y(i,j,k+1)-cent%y(i,j,k))
    ijac%a33(i,j,k) = (cent%z(i,j,k+1)-cent%z(i,j,k))

    i = ijac%jx
    j = ijac%iy
    k = ijac%jz
    ! dx/dcsi, dy/dcsi, dz/dcsi
    ijac%a11(i,j,k) = (cent%x(i,j,k)-cent%x(i-1,j,k))
    ijac%a21(i,j,k) = (cent%y(i,j,k)-cent%y(i-1,j,k))
    ijac%a31(i,j,k) = (cent%z(i,j,k)-cent%z(i-1,j,k))
    ! first order
    ! dx/deta, dy/deta, dz/deta
    ijac%a12(i,j,k) = (cent%x(i,j+1,k)-cent%x(i,j,k)) 
    ijac%a22(i,j,k) = (cent%y(i,j+1,k)-cent%y(i,j,k)) 
    ijac%a32(i,j,k) = (cent%z(i,j+1,k)-cent%z(i,j,k)) 
    ! first order
    ! dx/dzita, dy/dzita, dz/dzita
    ijac%a13(i,j,k) = (cent%x(i,j,k)-cent%x(i,j,k-1))
    ijac%a23(i,j,k) = (cent%y(i,j,k)-cent%y(i,j,k-1))
    ijac%a33(i,j,k) = (cent%z(i,j,k)-cent%z(i,j,k-1))

    i = ijac%ix
    j = ijac%iy
    k = ijac%jz
    ! dx/dcsi, dy/dcsi, dz/dcsi
    ijac%a11(i,j,k) = (cent%x(i+1,j,k)-cent%x(i,j,k))
    ijac%a21(i,j,k) = (cent%y(i+1,j,k)-cent%y(i,j,k))
    ijac%a31(i,j,k) = (cent%z(i+1,j,k)-cent%z(i,j,k))
    ! first order
    ! dx/deta, dy/deta, dz/deta
    ijac%a12(i,j,k) = (cent%x(i,j+1,k)-cent%x(i,j,k)) 
    ijac%a22(i,j,k) = (cent%y(i,j+1,k)-cent%y(i,j,k)) 
    ijac%a32(i,j,k) = (cent%z(i,j+1,k)-cent%z(i,j,k)) 
    ! first order
    ! dx/dzita, dy/dzita, dz/dzita
    ijac%a13(i,j,k) = (cent%x(i,j,k)-cent%x(i,j,k-1))
    ijac%a23(i,j,k) = (cent%y(i,j,k)-cent%y(i,j,k-1))
    ijac%a33(i,j,k) = (cent%z(i,j,k)-cent%z(i,j,k-1))

    i = ijac%ix
    j = ijac%jy
    k = ijac%jz
    ! dx/dcsi, dy/dcsi, dz/dcsi
    ijac%a11(i,j,k) = (cent%x(i+1,j,k)-cent%x(i,j,k))
    ijac%a21(i,j,k) = (cent%y(i+1,j,k)-cent%y(i,j,k))
    ijac%a31(i,j,k) = (cent%z(i+1,j,k)-cent%z(i,j,k))
    ! first order
    ! dx/deta, dy/deta, dz/deta
    ijac%a12(i,j,k) = (cent%x(i,j,k)-cent%x(i,j-1,k)) 
    ijac%a22(i,j,k) = (cent%y(i,j,k)-cent%y(i,j-1,k)) 
    ijac%a32(i,j,k) = (cent%z(i,j,k)-cent%z(i,j-1,k)) 
    ! first order
    ! dx/dzita, dy/dzita, dz/dzita
    ijac%a13(i,j,k) = (cent%x(i,j,k)-cent%x(i,j,k-1))
    ijac%a23(i,j,k) = (cent%y(i,j,k)-cent%y(i,j,k-1))
    ijac%a33(i,j,k) = (cent%z(i,j,k)-cent%z(i,j,k-1))

    i = ijac%jx
    j = ijac%iy
    k = ijac%iz
    ! dx/dcsi, dy/dcsi, dz/dcsi
    ijac%a11(i,j,k) = (cent%x(i,j,k)-cent%x(i-1,j,k))
    ijac%a21(i,j,k) = (cent%y(i,j,k)-cent%y(i-1,j,k))
    ijac%a31(i,j,k) = (cent%z(i,j,k)-cent%z(i-1,j,k))
    ! first order
    ! dx/deta, dy/deta, dz/deta
    ijac%a12(i,j,k) = (cent%x(i,j+1,k)-cent%x(i,j,k)) 
    ijac%a22(i,j,k) = (cent%y(i,j+1,k)-cent%y(i,j,k)) 
    ijac%a32(i,j,k) = (cent%z(i,j+1,k)-cent%z(i,j,k)) 
    ! first order
    ! dx/dzita, dy/dzita, dz/dzita
    ijac%a13(i,j,k) = (cent%x(i,j,k+1)-cent%x(i,j,k))
    ijac%a23(i,j,k) = (cent%y(i,j,k+1)-cent%y(i,j,k))
    ijac%a33(i,j,k) = (cent%z(i,j,k+1)-cent%z(i,j,k))

    i = ijac%jx
    j = ijac%jy
    k = ijac%iz
    ! dx/dcsi, dy/dcsi, dz/dcsi
    ijac%a11(i,j,k) = (cent%x(i,j,k)-cent%x(i-1,j,k))
    ijac%a21(i,j,k) = (cent%y(i,j,k)-cent%y(i-1,j,k))
    ijac%a31(i,j,k) = (cent%z(i,j,k)-cent%z(i-1,j,k))
    ! first order
    ! dx/deta, dy/deta, dz/deta
    ijac%a12(i,j,k) = (cent%x(i,j,k)-cent%x(i,j-1,k)) 
    ijac%a22(i,j,k) = (cent%y(i,j,k)-cent%y(i,j-1,k)) 
    ijac%a32(i,j,k) = (cent%z(i,j,k)-cent%z(i,j-1,k)) 
    ! first order
    ! dx/dzita, dy/dzita, dz/dzita
    ijac%a13(i,j,k) = (cent%x(i,j,k+1)-cent%x(i,j,k))
    ijac%a23(i,j,k) = (cent%y(i,j,k+1)-cent%y(i,j,k))
    ijac%a33(i,j,k) = (cent%z(i,j,k+1)-cent%z(i,j,k))

    i = ijac%jx
    j = ijac%jy
    k = ijac%jz
    ! dx/dcsi, dy/dcsi, dz/dcsi
    ijac%a11(i,j,k) = (cent%x(i,j,k)-cent%x(i-1,j,k))
    ijac%a21(i,j,k) = (cent%y(i,j,k)-cent%y(i-1,j,k))
    ijac%a31(i,j,k) = (cent%z(i,j,k)-cent%z(i-1,j,k))
    ! first order
    ! dx/deta, dy/deta, dz/deta
    ijac%a12(i,j,k) = (cent%x(i,j,k)-cent%x(i,j-1,k)) 
    ijac%a22(i,j,k) = (cent%y(i,j,k)-cent%y(i,j-1,k)) 
    ijac%a32(i,j,k) = (cent%z(i,j,k)-cent%z(i,j-1,k)) 
    ! first order
    ! dx/dzita, dy/dzita, dz/dzita
    ijac%a13(i,j,k) = (cent%x(i,j,k)-cent%x(i,j,k-1))
    ijac%a23(i,j,k) = (cent%y(i,j,k)-cent%y(i,j,k-1))
    ijac%a33(i,j,k) = (cent%z(i,j,k)-cent%z(i,j,k-1))
    !***********************************************************************
    ! calculate determinant of 1/J
    !***********************************************************************
    do k = ijac%iz , ijac%jz 
      do j = ijac%iy , ijac%jy 
        do i = ijac%ix , ijac%jx 
        ijac%a_det(i,j,k) = ijac%a11(i,j,k)*(ijac%a22(i,j,k)*ijac%a33(i,j,k) - &
                            ijac%a32(i,j,k)*ijac%a33(i,j,k))-ijac%a12(i,j,k) * &
                            (ijac%a21(i,j,k)*ijac%a33(i,j,k)-ijac%a31(i,j,k) * &
                            ijac%a23(i,j,k))+ijac%a13(i,j,k)*(ijac%a21(i,j,k)* &
                            ijac%a32(i,j,k)-ijac%a31(i,j,k)*ijac%a22(i,j,k))
        end do
      end do
    end do
    !***********************************************************************
    ! calculate Jacobian J = d csi_i / d x_j
    !***********************************************************************
    do k = ijac%iz , ijac%jz 
      do j = ijac%iy , ijac%jy 
        do i = ijac%ix , ijac%jx
          this%a11(i,j,k) = (ijac%a22(i,j,k)*ijac%a33(i,j,k)-                  &
                             ijac%a23(i,j,k)*ijac%a32(i,j,k))/ijac%a_det(i,j,k)
          this%a21(i,j,k) =-(ijac%a21(i,j,k)*ijac%a33(i,j,k)-                  &
                             ijac%a31(i,j,k)*ijac%a23(i,j,k))/ijac%a_det(i,j,k)
          this%a31(i,j,k) = (ijac%a21(i,j,k)*ijac%a32(i,j,k)-                  &
                             ijac%a22(i,j,k)*ijac%a31(i,j,k))/ijac%a_det(i,j,k)

          
          this%a12(i,j,k) = (ijac%a13(i,j,k)*ijac%a32(i,j,k)-                  &
                             ijac%a12(i,j,k)*ijac%a33(i,j,k))/ijac%a_det(i,j,k)
          this%a22(i,j,k) = (ijac%a11(i,j,k)*ijac%a33(i,j,k)-                  &
                             ijac%a13(i,j,k)*ijac%a31(i,j,k))/ijac%a_det(i,j,k)
          this%a32(i,j,k) = (ijac%a12(i,j,k)*ijac%a31(i,j,k)-                  &
                             ijac%a11(i,j,k)*ijac%a32(i,j,k))/ijac%a_det(i,j,k)


          this%a13(i,j,k) = (ijac%a12(i,j,k)*ijac%a23(i,j,k)-                  &
                             ijac%a13(i,j,k)*ijac%a22(i,j,k))/ijac%a_det(i,j,k)
          this%a23(i,j,k) = (ijac%a13(i,j,k)*ijac%a21(i,j,k)-                  &
                             ijac%a11(i,j,k)*ijac%a23(i,j,k))/ijac%a_det(i,j,k)
          this%a33(i,j,k) = (ijac%a11(i,j,k)*ijac%a22(i,j,k)-                  &
                             ijac%a12(i,j,k)*ijac%a21(i,j,k))/ijac%a_det(i,j,k)

          this%a_det(i,j,k) = 1./ijac%a_det(i,j,k)
        end do
      end do
    end do
  end subroutine calculateMetrics
end module mod_grid
