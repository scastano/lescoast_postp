module mod_printFields
  use mod_fields
  implicit none
  private

contains
  subroutine printInstantToVTK(this,filename)
    class(instantField), intent(in) :: this
    character(*), intent(in)        :: filename

    integer :: i,j,k,si,sj,sk,ni,nj,nk,sizx,sizy,sizz

    si = this%cGrid%centroids%ix
    sj = this%cGrid%centroids%iy
    sk = this%cGrid%centroids%iz
    ni = this%cGrid%centroids%jx
    nj = this%cGrid%centroids%jy
    nk = this%cGrid%centroids%jz

    sizx = (ni-si+1)
    sizy = (nj-sj+1)
    sizz = (nk-sk+1)

    open(unit=11,file=filename,status='replace')

    write(11,*) "# vtk DataFile Version 2.0"
    write(11,*) "LESCOAST OUTPUT FIELD"
    write(11,*) "ASCII"
    write(11,*) "DATASET STRUCTURED_GRID"
    write(11,'(A,3I)') "DIMENSIONS ", sizx, sizy, sizz
    write(11,'(A,I,A)') "POINTS ", sizx*sizy*sizz, " double"

    cents: do k = sk, nk
      do j = sj, nj
        do i = si, ni
          write(11, '(3F18.10)') this%cGrid%centroids%x(i,j,k), &
            this%cGrid%centroids%y(i,j,k),  this%cGrid%centroids%z(i,j,k)
        end do
      end do
    end do cents
    
    write(11,'(A,I)') "POINT_DATA ", sizx*sizy*sizz
    write(11,*) "SCALARS fi double 1"
    write(11,*) "LOOKUP_TABLE default"
    press: do k = sk, nk
      do j = sj, nj
        do i = si, ni
          write(11, '(F18.10)') this%pressure%v(i,j,k)
        end do
      end do
    end do press
    write(11,*) "VECTORS velocity double"
    vel: do k = sk, nk
      do j = sj, nj
        do i = si, ni
          write(11, '(3F18.10)') this%velocity%x(i,j,k), &
            this%velocity%y(i,j,k),  this%velocity%z(i,j,k)
        end do
      end do
    end do vel
    write(11,*) "TENSORS gradU double"
    gradu: do k = sk, nk
      do j = sj, nj
        do i = si, ni
          write(11, '(3F18.10)') this%velocityGradient%a11(i,j,k), &
            this%velocityGradient%a12(i,j,k),  this%velocityGradient%a13(i,j,k)
          write(11, '(3F18.10)') this%velocityGradient%a21(i,j,k), &
            this%velocityGradient%a22(i,j,k),  this%velocityGradient%a23(i,j,k)
          write(11, '(3F18.10)') this%velocityGradient%a31(i,j,k), &
            this%velocityGradient%a32(i,j,k),  this%velocityGradient%a33(i,j,k)
          write(11,*)
        end do
      end do
    end do gradu
    close(11)
  end subroutine printInstantToVTK
end module mod_printFields
