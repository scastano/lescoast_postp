module mod_basicTypes
  use mod_typeKinds
  implicit none
  private

  type, public :: tensor
    real(kind=d_kind), allocatable, dimension(:,:,:) :: a11, a21, a31
    real(kind=d_kind), allocatable, dimension(:,:,:) :: a12, a22, a32
    real(kind=d_kind), allocatable, dimension(:,:,:) :: a13, a23, a33
    real(kind=d_kind), allocatable, dimension(:,:,:) :: a_det
    integer :: ix, iy, iz ! start indices
    integer :: jx, jy, jz ! end indices
    logical :: isInitialized = .FALSE.
  contains
    procedure :: alloc   => t_alloc
    procedure :: dealloc => t_dealloc
  end type tensor

  interface operator(*)
    module procedure ttimes
  end interface operator(*)
  interface operator(+)
    module procedure tadd
  end interface operator(+)
  interface operator(-)
    module procedure tres
  end interface operator(-)
  interface assignment(=)
    module procedure teq
    module procedure teqq
  end interface assignment(=)

  type, public :: vector
    real(kind=d_kind), allocatable, dimension(:,:,:) :: x, y, z
    integer :: ix, iy, iz ! start indices
    integer :: jx, jy, jz ! end indices
    logical :: isInitialized = .FALSE.
  contains
    procedure :: alloc
    procedure :: dealloc => v_dealloc
  end type vector

  interface operator(.ddot.)
    module procedure vttimes
  end interface operator(.ddot.)
  interface operator(*)
    module procedure vtimes
    module procedure vvtimes
  end interface operator(*)
  interface operator(+)
    module procedure vadd
  end interface operator(+)
  interface operator(-)
    module procedure vres
  end interface operator(-)
  interface assignment(=)
    module procedure veq
    module procedure veqq
  end interface assignment(=)
  
  type, public :: scalar
    real(kind=d_kind), allocatable, dimension(:,:,:) :: v
    integer :: ix, iy, iz ! start indices
    integer :: jx, jy, jz ! end indices
    logical :: isInitialized = .FALSE.
  contains
    procedure :: alloc => s_alloc 
  end type scalar

  interface operator(*)
    module procedure stimes
    module procedure stime
  end interface operator(*)
  interface operator(+)
    module procedure sadd
  end interface operator(+)
  interface operator(-)
    module procedure sres
  end interface operator(-)
  interface assignment(=)
    module procedure seq
    module procedure seqq
  end interface assignment(=)

  public :: operator(+), operator(-), operator(*), assignment(=), operator(.ddot.)
contains
  function stime(v1,v2) result(v3)
    real(kind=d_kind), intent(in) :: v1
    class(scalar), intent(in)     :: v2
    type (scalar) :: v3

    v3%v = v1 * v2%v
  end function stime
  function stimes(v1,v2) result(v3)
    class(scalar), intent(in) :: v1, v2
    type (scalar) :: v3

    v3%v = v1%v * v2%v
  end function stimes
  function sres(v1,v2) result(v3)
    class(scalar), intent(in) :: v1, v2
    type (scalar) :: v3

    v3%v = v1%v - v2%v
  end function sres
  function sadd(v1,v2) result(v3)
    class(scalar), intent(in) :: v1, v2
    type (scalar) :: v3

    v3%v = v1%v + v2%v
  end function sadd
  subroutine seq(v1,v2)
    class(scalar), intent(in out) :: v1
    class(scalar), intent(in) :: v2

    v1%v = v2%v
  end subroutine seq
  subroutine seqq(v1,v2)
    class(scalar), intent(in out) :: v1
    real(kind=d_kind), intent(in) :: v2

    v1%v = v2
  end subroutine seqq



  function vttimes(v1,v2) result(v3)
    class(vector), intent(in) :: v1, v2
    type (tensor) :: v3

    v3%a11 = v1%x*v2%x
    v3%a12 = v1%x*v2%y
    v3%a13 = v1%x*v2%z
    v3%a21 = v1%y*v2%x
    v3%a22 = v1%y*v2%y
    v3%a23 = v1%y*v2%z
    v3%a31 = v1%z*v2%x
    v3%a32 = v1%z*v2%y
    v3%a33 = v1%z*v2%z
  end function vttimes
  subroutine teqq(v1,v2)
    !real(kind=d_kind), intent(in) :: v1
    real(kind=d_kind), intent(in)     :: v2
    class(tensor), intent(in out) :: v1

    v1%a11 = v2
    v1%a12 = v2
    v1%a13 = v2
    v1%a21 = v2
    v1%a22 = v2
    v1%a23 = v2
    v1%a31 = v2
    v1%a32 = v2
    v1%a33 = v2
  end subroutine teqq
  subroutine teq(v1,v2)
    !real(kind=d_kind), intent(in) :: v1
    class(tensor), intent(in)     :: v2
    class(tensor), intent(in out) :: v1

    v1%a11 = v2%a11
    v1%a12 = v2%a12
    v1%a13 = v2%a13
    v1%a21 = v2%a21
    v1%a22 = v2%a22
    v1%a23 = v2%a23
    v1%a31 = v2%a31
    v1%a32 = v2%a32
    v1%a33 = v2%a33
  end subroutine teq
  subroutine veqq(v1,v2)
    class(vector), intent(in out) :: v1
    real(kind=d_kind), intent(in) :: v2

    v1%x = v2
    v1%y = v2
    v1%z = v2
  end subroutine veqq
  subroutine veq(v1,v2)
    class(vector), intent(in out) :: v1
    class(vector), intent(in) :: v2

    v1%x = v2%x
    v1%y = v2%y
    v1%z = v2%z
  end subroutine veq
  function vvtimes(v1,v2) result(v3)
    class(vector), intent(in) :: v1,v2
    type (vector) :: v3

    v3%x = v1%x*v2%x
    v3%y = v1%y*v2%y
    v3%z = v1%z*v2%z
  end function vvtimes
  function vtimes(v1,v2) result(v3)
    real(kind=d_kind), intent(in) :: v1
    class(vector), intent(in) :: v2
    type (vector) :: v3

    v3%x = v1*v2%x
    v3%y = v1*v2%y
    v3%z = v1*v2%z
  end function vtimes
  function vres(v1,v2) result(v3)
    class(vector), intent(in) :: v1, v2
    type (vector) :: v3

    v3%x = v1%x - v2%x
    v3%y = v1%y - v2%y
    v3%z = v1%z - v2%z
  end function vres
  function vadd(v1,v2) result(v3)
    class(vector), intent(in) :: v1, v2
    type (vector) :: v3

    v3%x = v1%x + v2%x
    v3%y = v1%y + v2%y
    v3%z = v1%z + v2%z
  end function vadd
  function ttimes(v1,v2) result(v3)
    real(kind=d_kind), intent(in) :: v1
    class(tensor), intent(in)     :: v2
    type (tensor) :: v3

    v3%a11 = v1*v2%a11
    v3%a12 = v1*v2%a12
    v3%a13 = v1*v2%a13
    v3%a21 = v1*v2%a21
    v3%a22 = v1*v2%a22
    v3%a23 = v1*v2%a23
    v3%a31 = v1*v2%a31
    v3%a32 = v1*v2%a32
    v3%a33 = v1*v2%a33
  end function ttimes
  function tadd(v1,v2) result(v3)
    class(tensor), intent(in) :: v1, v2
    type (tensor) :: v3

    v3%a11 = v1%a11 + v2%a11
    v3%a12 = v1%a12 + v2%a12
    v3%a13 = v1%a13 + v2%a13
    v3%a21 = v1%a21 + v2%a21
    v3%a22 = v1%a22 + v2%a22
    v3%a23 = v1%a23 + v2%a23
    v3%a31 = v1%a31 + v2%a31
    v3%a32 = v1%a32 + v2%a32
    v3%a33 = v1%a33 + v2%a33
  end function tadd
  function tres(v1,v2) result(v3)
    class(tensor), intent(in) :: v1, v2
    type (tensor) :: v3

    v3%a11 = v1%a11 - v2%a11
    v3%a12 = v1%a12 - v2%a12
    v3%a13 = v1%a13 - v2%a13
    v3%a21 = v1%a21 - v2%a21
    v3%a22 = v1%a22 - v2%a22
    v3%a23 = v1%a23 - v2%a23
    v3%a31 = v1%a31 - v2%a31
    v3%a32 = v1%a32 - v2%a32
    v3%a33 = v1%a33 - v2%a33
  end function tres
  subroutine s_alloc(this, sx, sy, sz ,nx, ny, nz)
    class(scalar), intent(inout) :: this
    integer, intent(in) :: sx, sy, sz
    integer, intent(in) :: nx, ny, nz
  
    if (this%isInitialized) &
      stop "Cannot reinitialize class. STOP."
    
    allocate(this%v(sx:nx,sy:ny,sz:nz))

    this%jx=nx
    this%jy=ny
    this%jz=nz
    this%ix=sx
    this%iy=sy
    this%iz=sz
    this%isInitialized = .TRUE.
  end subroutine s_alloc 
  subroutine alloc(this, sx, sy, sz ,nx, ny, nz)
    class(vector), intent(inout) :: this
    integer, intent(in) :: sx, sy, sz
    integer, intent(in) :: nx, ny, nz
  
    if (this%isInitialized) &
      stop "Cannot reinitialize class. STOP."
    
    allocate(this%x(sx:nx,sy:ny,sz:nz))
    allocate(this%y(sx:nx,sy:ny,sz:nz))
    allocate(this%z(sx:nx,sy:ny,sz:nz))

    this%jx=nx
    this%jy=ny
    this%jz=nz
    this%ix=sx
    this%iy=sy
    this%iz=sz
    this%isInitialized = .TRUE.
  end subroutine alloc 
  subroutine t_alloc(this, sx, sy, sz, nx, ny, nz)
    class(tensor), intent(inout) :: this
    integer, intent(in) :: sx, sy, sz
    integer, intent(in) :: nx, ny, nz
  
    if (this%isInitialized) &
      stop "Cannot reinitialize class. STOP."
    
    this%jx=nx
    this%jy=ny
    this%jz=nz
    this%ix=sx
    this%iy=sy
    this%iz=sz
    allocate(this%a11(this%ix:this%jx,this%iy:this%jy,this%iz:this%jz))
    allocate(this%a12(this%ix:this%jx,this%iy:this%jy,this%iz:this%jz))
    allocate(this%a13(this%ix:this%jx,this%iy:this%jy,this%iz:this%jz))
    allocate(this%a21(this%ix:this%jx,this%iy:this%jy,this%iz:this%jz))
    allocate(this%a22(this%ix:this%jx,this%iy:this%jy,this%iz:this%jz))
    allocate(this%a23(this%ix:this%jx,this%iy:this%jy,this%iz:this%jz))
    allocate(this%a31(this%ix:this%jx,this%iy:this%jy,this%iz:this%jz))
    allocate(this%a32(this%ix:this%jx,this%iy:this%jy,this%iz:this%jz))
    allocate(this%a33(this%ix:this%jx,this%iy:this%jy,this%iz:this%jz))
    allocate(this%a_det(this%ix:this%jx,this%iy:this%jy,this%iz:this%jz))
    this%isInitialized = .TRUE.
  end subroutine t_alloc 
  subroutine v_dealloc(this)
    class(vector), intent(inout) :: this
  
    if (.not.this%isInitialized) &
      stop "Cannot deallocate class. STOP."
    
    deallocate(this%x)
    deallocate(this%y)
    deallocate(this%z)
    this%isInitialized = .false.
  end subroutine v_dealloc 
  subroutine t_dealloc(this)
    class(tensor), intent(inout) :: this
  
    if (.not.this%isInitialized) &
      stop "Cannot deallocate class. STOP."
    
    deallocate(this%a11)
    deallocate(this%a12)
    deallocate(this%a13)
    deallocate(this%a21)
    deallocate(this%a22)
    deallocate(this%a23)
    deallocate(this%a31)
    deallocate(this%a32)
    deallocate(this%a33)
    this%isInitialized = .false.
  end subroutine t_dealloc 
end module mod_basicTypes
