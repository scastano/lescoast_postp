module mod_stats
  use mod_typeKinds
  use mod_basicTypes
  use mod_fields
  use mod_grid
  implicit none
  private
  type, public :: meanField
    type(vector) :: velocity
    type(vector) :: vorticity
    type(tensor) :: uv
    type(tensor) :: enstrophy
    type(tensor) :: uv_f
    type(tensor) :: enstrophy_f
    type(scalar) :: re
    type(scalar) :: condAvg
    type(vector) :: skew
    type(vector) :: flat
    type(vector) :: skew_f
    type(vector) :: flat_f
    type(grid), pointer :: cGrid
  contains
    procedure :: alloc => malloc
    procedure :: printField => printInstantToVTK
  end type meanField

  public :: calculateMeanFields,calculateVerticalProfile
contains
  subroutine calculateVerticalProfile(y,field,filename)
    real(kind=d_kind), dimension(1:,1:,1:), intent(in) :: field
    real(kind=d_kind), dimension(1:), intent(in) :: y
    character(*), intent(in) :: filename
  
    integer :: i, j, k, sizx, sizy, sizz
    real(kind=d_kind), dimension(:), allocatable :: avg

    sizx = size(field,dim=1)
    sizy = size(field,dim=2)
    sizz = size(field,dim=3)

    allocate(avg(sizy))

    avg = 0.0
    do j = 1, sizy
      do k = 1, sizz
        do i = 1, sizx
          avg(j) = avg(j) + field(i,j,k)      
        end do  
      end do
      avg(j) = avg(j)/real((sizz+1)*(sizx+1))
    end do  

    open(unit=11, file=trim(filename), status= 'replace')
    do j = 1, sizy
      write(11,'(2F18.10)') y(j),avg(j)
    end do  
    close(11)
    deallocate(avg)
  end subroutine calculateVerticalProfile
  subroutine calculateMeanFields(this, inst, ntime)
    class(meanField), intent(in out)              :: this
    class(instantField), intent(in), dimension(:) :: inst
    integer, intent(in) :: ntime

    integer :: nt
    real(kind=d_kind) :: r
  
    this%velocity = 0.0_d_kind    
    this%vorticity = 0.0_d_kind    
    this%enstrophy = 0.0_d_kind    
    this%uv = 0.0_d_kind
    this%re = 0.0_d_kind
    this%skew = 0.0_d_kind
    this%flat = 0.0_d_kind

    r = 1./ntime
    do nt = 1, ntime
      this%velocity   = this%velocity + r*(inst(nt)%velocity)
      this%re         = this%re + r*(inst(nt)%re)
      this%vorticity  = this%vorticity + r*(inst(nt)%vorticity)
      this%uv         = this%uv + r*(inst(nt)%velocity.ddot.inst(nt)%velocity)
      this%enstrophy  = this%enstrophy + r*(inst(nt)%velocity.ddot.inst(nt)%velocity)
      this%flat       = this%flat + r*(inst(nt)%velocity*inst(nt)%velocity*inst(nt)%velocity*inst(nt)%velocity)
      this%skew       = this%skew + r*(inst(nt)%velocity*inst(nt)%velocity*inst(nt)%velocity)
    end do    
    this%uv_f         = this%uv - ((this%velocity).ddot.(this%velocity))
    this%enstrophy_f  = this%enstrophy - ((this%vorticity).ddot.(this%vorticity))
    this%flat_f       = this%flat - (this%velocity*this%velocity*this%velocity*this%velocity)
    this%skew_f       = this%skew - (this%velocity*this%velocity*this%velocity)
  end subroutine calculateMeanFields
  subroutine malloc(this,gr)
    class(meanField), intent(in out) :: this
    class(grid), intent(in),target :: gr
    
    integer :: si,sj,sk,ni,nj,nk

    this%cGrid => gr

    si = gr%centroids%ix
    sj = gr%centroids%iy
    sk = gr%centroids%iz
    ni = gr%centroids%jx
    nj = gr%centroids%jy
    nk = gr%centroids%jz

    call this%velocity%alloc(si,sj,sk,ni,nj,nk)
    call this%vorticity%alloc(si,sj,sk,ni,nj,nk)
    call this%uv%alloc(si,sj,sk,ni,nj,nk)
    call this%enstrophy%alloc(si,sj,sk,ni,nj,nk)
    call this%uv_f%alloc(si,sj,sk,ni,nj,nk)
    call this%enstrophy_f%alloc(si,sj,sk,ni,nj,nk)
    call this%re%alloc(si,sj,sk,ni,nj,nk)
  end subroutine malloc
  subroutine printInstantToVTK(this,filename)
    class(meanField), intent(in) :: this
    character(*), intent(in)        :: filename

    integer :: i,j,k,si,sj,sk,ni,nj,nk,sizx,sizy,sizz

    si = this%cGrid%centroids%ix
    sj = this%cGrid%centroids%iy
    sk = this%cGrid%centroids%iz
    ni = this%cGrid%centroids%jx
    nj = this%cGrid%centroids%jy
    nk = this%cGrid%centroids%jz

    sizx = (ni-si+1)
    sizy = (nj-sj+1)
    sizz = (nk-sk+1)

    open(unit=11,file=filename,status='replace')

    write(11,'(A)') "# vtk DataFile Version 2.0"
    write(11,'(A)') "LESCOAST MEAN FIELD"
    write(11,'(A)') "ASCII"
    write(11,'(A)') "DATASET STRUCTURED_GRID"
    write(11,'(A,3I10)') "DIMENSIONS ", sizx, sizy, sizz
    write(11,'(A,I10,A)') "POINTS ", sizx*sizy*sizz, " double"

    cents: do k = sk, nk
      do j = sj, nj
        do i = si, ni
          write(11, '(3F18.10)') this%cGrid%centroids%x(i,j,k), &
            this%cGrid%centroids%y(i,j,k),  this%cGrid%centroids%z(i,j,k)
        end do
      end do
    end do cents
    
    write(11,'(A,I10)') "POINT_DATA ", sizx*sizy*sizz
    write(11,'(A)') "SCALARS nu_total double 1"
    write(11,'(A)') "LOOKUP_TABLE default"
    press: do k = sk, nk
      do j = sj, nj
        do i = si, ni
          write(11, '(F18.10)') this%re%v(i,j,k)
        end do
      end do
    end do press
    write(11,'(A)') "VECTORS velocity double"
    vel: do k = sk, nk
      do j = sj, nj
        do i = si, ni
          write(11, '(3F18.10)') this%velocity%x(i,j,k), &
            this%velocity%y(i,j,k),  this%velocity%z(i,j,k)
        end do
      end do
    end do vel
    write(11,'(A)') "VECTORS vorticity double"
    vor: do k = sk, nk
      do j = sj, nj
        do i = si, ni
          write(11, '(3F18.10)') this%vorticity%x(i,j,k), &
            this%vorticity%y(i,j,k),  this%vorticity%z(i,j,k)
        end do
      end do
    end do vor
    write(11,'(A)') "TENSORS uv double"
    uv: do k = sk, nk
      do j = sj, nj
        do i = si, ni
          write(11, '(3F18.10)') this%uv_f%a11(i,j,k), &
            this%uv_f%a12(i,j,k),  this%uv_f%a13(i,j,k)
          write(11, '(3F18.10)') this%uv_f%a21(i,j,k), &
            this%uv_f%a22(i,j,k),  this%uv_f%a23(i,j,k)
          write(11, '(3F18.10)') this%uv_f%a31(i,j,k), &
            this%uv_f%a32(i,j,k),  this%uv_f%a33(i,j,k)
          write(11,*)
        end do
      end do
    end do uv
    write(11,'(A)') "TENSORS enstrophy double"
    ens: do k = sk, nk
      do j = sj, nj
        do i = si, ni
          write(11, '(3F18.10)') this%enstrophy_f%a11(i,j,k), &
            this%enstrophy_f%a12(i,j,k),  this%enstrophy_f%a13(i,j,k)
          write(11, '(3F18.10)') this%enstrophy_f%a21(i,j,k), &
            this%enstrophy_f%a22(i,j,k),  this%enstrophy_f%a23(i,j,k)
          write(11, '(3F18.10)') this%enstrophy_f%a31(i,j,k), &
            this%enstrophy_f%a32(i,j,k),  this%enstrophy_f%a33(i,j,k)
          write(11,*)
        end do
      end do
    end do ens

    si = this%condAvg%ix
    sj = this%condAvg%iy
    sk = this%condAvg%iz
    ni = this%condAvg%jx
    nj = this%condAvg%jy
    nk = this%condAvg%jz

    write(11,'(A)') "SCALARS condAvg double 1"
    write(11,'(A)') "LOOKUP_TABLE default"
    cond: do k = sk, nk
      do j = sj, nj
        do i = si, ni
          write(11, '(F18.10)') this%condAvg%v(i,j,k)
        end do
      end do
    end do cond

    close(11)
  end subroutine printInstantToVTK
end module mod_stats
