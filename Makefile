_dummy := $(shell mkdir -p obj)
IDIR =./include
#CC=gcc
CC=icc
CPP=g++
DEBUG= -g -traceback -debug all
PROD=  -pipe -gcc -O3 -march=native -fomit-frame-pointer -xHost
#CFLAGS=-I$(IDIR) -Wno-unused-result --std=c99 $(PROD)
CFLAGS=-I$(IDIR)   -I$(HDF5_INCLUDE)  $(PROD) # $(DEBUG) 
#CPPFLAGS=-I$(IDIR) -Wno-unused-result $(PROD) 

CDIR=./src
ODIR=obj
LDIR =./lib

H5LIB   = $(HDF5_LIB)

#LIBS=-lm -lgfortran  -llapack  -lblas
# CINECA'S FLAGS
#LIBS=-lm -lgfortran -L$(LAPACK_LIB) -llapack -L$(BLAS_LIB) -lblas
LIBS=-mkl -L$(H5LIB) -lhdf5  -L${SZIP_LIB} -lz -lsz

_DEPS = stru.h topologyUtil.h derivativeUtil.h jacobianUtil.h meanUtil.h util.h energyUtil.h fft.h correlations.h PDF.h visa.h
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

_OBJ = stru.o topologyUtil.o derivativeUtil.o jacobianUtil.o meanUtil.o util.o calculateMean.o energyUtil.o correlations.o fft.o PDF.o visa.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))



$(ODIR)/%.o: $(CDIR)/%.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

$(ODIR)/%.o: $(CDIR)/%.cc $(DEPS)
	$(CPP) -c -o $@ $< $(CPPFLAGS)

CODE: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

.PHONY: clean

clean:
	rm -f $(ODIR)/*.o *~ core $(INCDIR)/*~ -r $(ODIR)
