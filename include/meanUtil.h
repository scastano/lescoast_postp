/* Copyright, 2015, Santiago Lopez Castano
 *=========================================================================
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.

 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */
#ifndef INCL_mean
#define INCL_mean

#include "stru.h"

void calculateTaylorMicroScales (
																 int nx,
																 int ny,
																 int nz,
																 struct matrix **** const uu,
																 struct matrix **** const grad,
																 double ** res
																);
void calculateAnisotropicTensor (int nx,
																 int ny,
																 int nz,
																 struct matrix **** const uv,
																 struct matrix **** anisotropy);
void  lineMeanFieldMatr (	int nx, int ny, 
										 			int nz, enum direction d,
													int elem,
										 			double scale,
										 			struct matrix **** const mean,
										 			double **meanline);
void vorticityCorrelation(int ntime, 
                   				int nx,
                   				int ny,
                   				int nz, 
                   				struct field* campo,
                   				struct field* mean); 
void pressureStrain(int ntime, 
                   	int nx,
                   	int ny,
                   	int nz, 
                   	struct field* campo,
                   	struct field* mean); 
double calculateIntegralShearStress ( int nx, 
																			int nz, 
																			int j, 
																			double re,
																			struct field * const mean);
//enum direction {X, Y, Z};
void meanField(int ntime, 
							 int nx,
							 int ny,
							 int nz, int nscal,
							 const struct field *f, 
							 struct field *campo); // this one is assumed to be undefined

void lineRMSMeanField   (int nx, int ny, 
												 int nz, enum direction d, // averaging direction
												 const struct field *mean,
												 enum direction e, // which component to choose (<uu>, <vv>, <ww>)
												 double **meanline);
/* NOT GOOD IDEA
void calculateShearRate(int nx,
												int ny,
												int nz,
												struct field* campo);
void calculateShearProfile(int nx,
													 int ny,
												   int nz,
													 double re,
												   struct field* campo);
*/
void  lineMeanField (int nx, int ny, 
										 int nz, enum direction d,
										 double scale,
										 double ***mean,
										 double **meanline);
void perturbations(	int ntime, 
							 			int nx,
							 			int ny,
							 			int nz, int nscal,
										struct field* campo,
										struct field* mean);
void quadrantAnalysis ( int npert,
												int nx,
												int ny,
												int nz,
												struct field* campo,// this arg comprises the perturbation fields
												struct field* mean);
// "overloaded" version 
void perturbationsMeanLineU(	int ntime, 
															int nx,
															int ny,
															int nz, int nscal, enum direction d,
															struct field* campo,// this arg is the instantaneous field, which is converted to only perturbations
															const double* Uline,
															const struct field* mean);
// deprecated functions, kept for debug
void lineShearBalanceMeanField (int nx, int ny, 
												 				int nz, enum direction d,
												 				struct field *const mean,
												 				double **meanline);
void lineShearMeanField (int nx, int ny, 
												 int nz, enum direction d,
												 struct field *const mean,
												 double **meanline);

void lineUMeanField (int nx, int ny, 
										 int nz, enum direction d,
										 const struct field *mean,
										 double **meanline);

#endif
