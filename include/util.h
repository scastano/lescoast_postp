
/* Copyright, 2015, Santiago Lopez Castano
 *=========================================================================
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.

 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */
#ifndef INCL_util
#define INCL_util

#include "stru.h"
void calculateCentroids(struct grid g,
												struct cent *gc, 
												int nx, 
												int ny, 
												int nz);
void readFields(int nx, 
								int ny, 
								int nz, 
								double u_star, 
								char *filename, // input
								int *nscal,
								struct field *campo);
void readFieldsOld (int nx, 
								    int ny, 
								    int nz,
								    double u_star, double re, 
								    char *filename, // input
								    int *nscal,
								    struct field *campo);
void readGrid(struct grid *xyz,
							int *nx,
							int *ny,
							int *nz,
							char *gridfilename);
void printAllToVTK (	char *output, 
											int nx, 
											int ny, 
											int nz, 
											int nscal,
											struct cent gc,
											struct field campo	); 
void printLineDir 	(	char *output, 
											int ny, 
											const struct cent* gc,
											const double* line); 
void printXLineDir 	(	char *output, 
											int ny, 
											const struct cent* gc,
											const double* line); 
void readFields_oldLESCOAST(int nx, 
														int ny, 
														int nz, 
														char *filename, // input
														int *nscal,
														struct field *campo);
void print1dArrays 	(	char *output, 
											int ny, 
											const double* line1,
											const double* line2); 
void printPointerAllToVTK (	char *output, 
														int nx, 
														int ny, 
														int nz, 
														int nscal,
														struct cent  *const gc,
														struct field *const campo	); 
void printPointerFaceScalarToVTK (	char *output, 
											    			    int nx, 
													    	    int ny, 
														        int nz,
                                    char *name, enum direction d, 
														        struct grid *const gc,
														        double   ****const campo	); 
void print1dArray 	(	char *output, 
											int ny, 
											const double* line2);
void readFieldsH5(int nx, 
									int ny, 
									int nz,
									int nscal,
									double u_star,
									char* folder, 
									int group, // input
									struct field *campo);
 
void printPlaneHorDir		(	char *output, 
												int nx,
												int  j,
												int nz,
												double **** const data);
#endif
