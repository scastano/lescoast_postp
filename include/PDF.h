/* Copyright, 2015, Santiago Lopez Castano
 *=========================================================================
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.

 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */
#ifndef INCL_pdf
#define INCL_pdf

#include "stru.h"

double VarMatr (
								int nx, int  j, int nz,
								struct matrix **** const matr 
							 );
void PDFMatr (
							int nx, int  j, int nz,
							int bins,
							struct matrix **** const matr, 
							double ** list,
							double ** b
						 );
void PDFList (
							int nx, int  j, int nz,
							struct matrix **** const matr, 
							double ** m
						 );
#endif
