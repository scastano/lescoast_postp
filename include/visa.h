#ifndef INCL_visa
#define INCL_visa

#include "stru.h"
void VISA	(
						int nx,
						int ny,
						int nz,
						int ntime,
						//double **** const input,
						struct field* campo,// this arg comprises the perturbation fields
						double **** out
					);
#endif
