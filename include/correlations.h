#ifndef INCL_corr
#define INCL_corr

#include "stru.h"

void correlationXMean(int nx, int ny, int nz, int j, int time,
											const struct field *pert,
											double **corrLine);
void correlationTauVert(	int nx, int ny, int nz,int jj ,int time,
													const struct field *pert,
													double **corrLine);
void correlationY(int nx, int ny, int nz,
									const double ***ui,
									const double ***uj,
									double **corrLine);
void correlationX(int nx,  int nz, int j,
									double ***const ui,
									double ***const uj,
									double **corrLine);
void correlationXX(	int nx, int ny, int nz, int j, int time,
										const struct field *pert,
										double **corrLine);
void correlationYX(	int nx, int ny, int nz, int k, int time,
										const struct field *pert,
										double **corrLine);
void correlationYY(	int nx, int ny, int nz, int k, int time,
										const struct field *pert,
										double **corrLine);
void correlationYStrain(int nx, int ny, int nz,int jj,
												struct matrix *** const ui,
												double **corrLine);
#endif
