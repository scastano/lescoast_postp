
/* Copyright, 2015, Santiago Lopez Castano
 *=========================================================================
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.

 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */
#ifndef INCL_stru
#define INCL_stru
#include <stdlib.h>
enum direction {X, Y, Z};
struct matrix
{
	double a11;
	double a12;
	double a13;
	double a21;
	double a22;
	double a23;
	double a31;
	double a32;
	double a33;
	double det;
};
struct grid
{
	double ***x;
	double ***y;
	double ***z;
	double alx;
	double aly;
	double alz;
	struct matrix ***jac;
	struct matrix ***invjac;
};
struct cent
{
	double ***xc;
	double ***yc;
	double ***zc;
	struct matrix ***jac;
	struct matrix ***invjac;
};
struct field
{
	double ***u;
	double ***v;
	double ***w;
	double ***vortX;
	double ***vortY;
	double ***vortZ;
	double ***re;
	double ***fi;
//	double ***uc;
//	double ***vc;
//	double ***wc;
	double ****rho;
//	double ***div;
// topology of CS, turbulence
	double ***lambda2;
	double ***q;
	double ***wwsecInv;
	double ***wwthirdInv;
	double ***pcsecInv;
	double ***pcthirdInv;
	double ***secInv;
	double ***thirdInv;
	double *quadrants[4];
//	double *S; // BAD IDEA
// end of CS
// energy budget
	double ***k;
  double ***turb_t_vec[3]; // helper variable: <u_i u_i u_j>

	double ***prod;
	double ***press_diff;
	double ***turb_transp;
	double ***visc_diff;
	double ***dissip;
	double ***wwS; // vortex stretching term
	double ***SLambda1; // first eigenvalue of Strain
	double ***SLambda2; // second eigenvalue of Strain
	double ***SLambda3; // third eigenvalue of Strain
	double ***kurtosisU;
	double ***skewnessU;
	double ***kurtosisV;
	double ***skewnessV;
	double ***kurtosisW;
	double ***skewnessW;
	double ***VISA;
// end energy budget
// < C u_i >
	double ***RhoU;
	double ***RhoV;
	double ***RhoW;
// good guys tensors
	struct matrix 		***deriv;
	struct matrix 		***strain;
	struct matrix 		***skew_strain;
	struct matrix 		***uv; // reynolds stresses
	struct matrix 	 	***visc_diss;
	struct matrix			***anisotropy;
	struct matrix			***pressureStrain;
	struct matrix			***anisotrpc;
	struct matrix			***ww; // vorticity fluc. 
	struct matrix			***anisotrww;
};
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// SOME TYPE-BOUND OPERATORS
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
struct matrix scalarMatrixMult(double a,
															 struct matrix r);
struct matrix matrixSum(const struct matrix l,
												const struct matrix r);
struct matrix matrixTranspose(const struct matrix a);
struct matrix matrixRest(const struct matrix l,
												const struct matrix r);
struct matrix matMul(const struct matrix l,
										 const struct matrix r);
double matrixTrace(struct matrix a);
double matrixDeterminant(struct matrix a);
void sumFields(int nx, int ny, int nz, double ***a, double ***b, double ****c);
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ALLOC- and FREE-MEMORY
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void init_null_field(struct field *f);
void free_memory_gc	(int nx, int ny, struct cent* f);
void free_memory_f	(int nx, int ny, int nscal, struct field* f);
void free_memory_budget	(int nx, int ny, int nscal, struct field* f);
void dealloc_3d(int nx, int ny, void ****ptr);
void init_3dArray(int nx, int ny, int nz, double **** t);
void init_3dMatrix(int nx, int ny, int nz, struct matrix **** t);
void passTensorElemTo3dArray(int nx,int ny,int nz, int elem,
														 struct matrix ****a, 
                             double ****b);
#endif
