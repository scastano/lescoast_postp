#ifndef INCL_fft
#define INCL_fft

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>



typedef double real;
typedef struct{real Re; real Im;} complex;


static void
fftprint_vector(
	     					const char *title,
	     					complex *x,
	     					int n)
{
	FILE *outFile = fopen(title,"w");
  int i;

	if ( outFile == NULL )
	{
		printf ("ERROR: file %s couldn't be opened\n", title);
		exit(EXIT_FAILURE);
	}

  fprintf(outFile, "# %s (dim=%d):\n", title, n);
  for(i=0; i<n; i++ ) 
		fprintf(outFile, " %16.10f,%16.10f\n", x[i].Re,x[i].Im);
	fclose(outFile);
  return;
}

void
fft( complex *v, int n, complex *tmp );

void
ifft( complex *v, int n, complex *tmp );


/*
#ifdef __cplusplus
	extern "C" {
#endif
void fft(double *real, double *im, int n);
void ifft(double *real, double *im, int n);
#ifdef __cplusplus
	}
#endif
*/
#endif
