pkg load io
pkg load statistics

load("-ascii","vortXPlaneStats.xy");
load("-ascii","vortYPlaneStats.xy");

JPDFU = vortXPlaneStats;
JPDFV = vortYPlaneStats;

min_x = min(JPDFU);
min_y = min(JPDFV);

max_x = max(JPDFU);
max_y = max(JPDFV);

step = 0.1;

x = min_x:step:max_x; % axis x, which you want to see
y = min_y:step:max_y; % axis y, which you want to see

[X,Y] = meshgrid(x,y); %*important for "surf" - makes defined grid*

%pdf = hist3([data_x , data_y],{x y}); %standard hist3 (calculated for yours axis)
pdf = hist3([JPDFU , JPDFV],{x y}); %standard hist3 (calculated for yours axis)
pdf_normalize = (pdf'./length(JPDFU)); %normalization means devide it by length of data_x (or data_y)
covariance = (X.*Y).*pdf_normalize;
%figure()
%surf(X,Y,pdf_normalize) % plot distribution
colormap("default");
figure(1)
contour(X,Y,pdf_normalize);
figure(2)
contour(X,Y,covariance);
