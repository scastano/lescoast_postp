
/* Copyright, 2015, Santiago Lopez Castano
 *=========================================================================
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.

 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */
#include <stdlib.h>
#include "stru.h"
#include "jacobianUtil.h"

void calculateInvJacobian_c(int nx,
												  	int ny, 
														int nz,
														struct cent* g)
{
	int i, j, k;

	// allocation of inverse jacobian matrix
	g->invjac = (struct matrix ***) malloc ((nx+1) * sizeof(*g->invjac));
	for(i = 0; i < nx+1; ++i)
	{
		g->invjac[i] = (struct matrix **) malloc((ny+1)*sizeof(**g->invjac));
		for(j = 0; j < ny+1; ++j)
			g->invjac[i][j] = (struct matrix *) malloc((nz+1)*sizeof(***g->invjac));		
	}
  // the grid has nx+1 elements with indices from 0 to nx
  // calculating the field inside w/ Central differences
	for(i = 0; i < nx+1; ++i)
		for(j = 0; j < ny+1; ++j)
			for(k = 0; k < nz+1; ++k)
			{
				if(i==0)
				{
					// dx/dcsi, dy/dcsi, dz/dcsi 
					g->invjac[i][j][k].a11 = 1.0*(g->xc[i+1][j][k]-g->xc[i][j][k]);			
					g->invjac[i][j][k].a21 = 1.0*(g->yc[i+1][j][k]-g->yc[i][j][k]);			
					g->invjac[i][j][k].a31 = 1.0*(g->zc[i+1][j][k]-g->zc[i][j][k]);
					// dx/deta, dy/deta, dz/deta
					if(j==0)
					{
						g->invjac[i][j][k].a12 = 1.0*(g->xc[i][j+1][k]-g->xc[i][j][k]);			
						g->invjac[i][j][k].a22 = 1.0*(g->yc[i][j+1][k]-g->yc[i][j][k]);			
						g->invjac[i][j][k].a32 = 1.0*(g->zc[i][j+1][k]-g->zc[i][j][k]);
					}
					else if(j==ny)
					{
						g->invjac[i][j][k].a12 = 1.0*(g->xc[i][j][k]-g->xc[i][j-1][k]);			
						g->invjac[i][j][k].a22 = 1.0*(g->yc[i][j][k]-g->yc[i][j-1][k]);			
						g->invjac[i][j][k].a32 = 1.0*(g->zc[i][j][k]-g->zc[i][j-1][k]);
					}
					else
					{
						g->invjac[i][j][k].a12 = 0.5*(g->xc[i][j+1][k]-g->xc[i][j-1][k]);			
						g->invjac[i][j][k].a22 = 0.5*(g->yc[i][j+1][k]-g->yc[i][j-1][k]);			
						g->invjac[i][j][k].a32 = 0.5*(g->zc[i][j+1][k]-g->zc[i][j-1][k]);
					}
					// dx/dzita, dy/dzita, dz/dzita
					if(k==0)
					{
						g->invjac[i][j][k].a13 = 1.0*(g->xc[i][j][k+1]-g->xc[i][j][k]);			
						g->invjac[i][j][k].a23 = 1.0*(g->yc[i][j][k+1]-g->yc[i][j][k]);			
						g->invjac[i][j][k].a33 = 1.0*(g->zc[i][j][k+1]-g->zc[i][j][k]);
					}
					else if(k==nz)
					{
						g->invjac[i][j][k].a13 = 1.0*(g->xc[i][j][k]-g->xc[i][j][k-1]);			
						g->invjac[i][j][k].a23 = 1.0*(g->yc[i][j][k]-g->yc[i][j][k-1]);			
						g->invjac[i][j][k].a33 = 1.0*(g->zc[i][j][k]-g->zc[i][j][k-1]);
					}
					else
					{
						g->invjac[i][j][k].a13 = 0.5*(g->xc[i][j][k+1]-g->xc[i][j][k-1]);			
						g->invjac[i][j][k].a23 = 0.5*(g->yc[i][j][k+1]-g->yc[i][j][k-1]);			
						g->invjac[i][j][k].a33 = 0.5*(g->zc[i][j][k+1]-g->zc[i][j][k-1]);
					}
				}
				else if (i==nx)
				{
					// dx/dcsi, dy/dcsi, dz/dcsi 
					g->invjac[i][j][k].a11 = 1.0*(g->xc[i][j][k]-g->xc[i-1][j][k]);			
					g->invjac[i][j][k].a21 = 1.0*(g->yc[i][j][k]-g->yc[i-1][j][k]);			
					g->invjac[i][j][k].a31 = 1.0*(g->zc[i][j][k]-g->zc[i-1][j][k]);
					// dx/deta, dy/deta, dz/deta
					if(j==0)
					{
						g->invjac[i][j][k].a12 = 1.0*(g->xc[i][j+1][k]-g->xc[i][j][k]);			
						g->invjac[i][j][k].a22 = 1.0*(g->yc[i][j+1][k]-g->yc[i][j][k]);			
						g->invjac[i][j][k].a32 = 1.0*(g->zc[i][j+1][k]-g->zc[i][j][k]);
					}
					else if(j==ny)
					{
						g->invjac[i][j][k].a12 = 1.0*(g->xc[i][j][k]-g->xc[i][j-1][k]);			
						g->invjac[i][j][k].a22 = 1.0*(g->yc[i][j][k]-g->yc[i][j-1][k]);			
						g->invjac[i][j][k].a32 = 1.0*(g->zc[i][j][k]-g->zc[i][j-1][k]);
					}
					else
					{
						g->invjac[i][j][k].a12 = 0.5*(g->xc[i][j+1][k]-g->xc[i][j-1][k]);			
						g->invjac[i][j][k].a22 = 0.5*(g->yc[i][j+1][k]-g->yc[i][j-1][k]);			
						g->invjac[i][j][k].a32 = 0.5*(g->zc[i][j+1][k]-g->zc[i][j-1][k]);
					}
					// dx/dzita, dy/dzita, dz/dzita
					if(k==0)
					{
						g->invjac[i][j][k].a13 = 1.0*(g->xc[i][j][k+1]-g->xc[i][j][k]);			
						g->invjac[i][j][k].a23 = 1.0*(g->yc[i][j][k+1]-g->yc[i][j][k]);			
						g->invjac[i][j][k].a33 = 1.0*(g->zc[i][j][k+1]-g->zc[i][j][k]);
					}
					else if(k==nz)
					{
						g->invjac[i][j][k].a13 = 1.0*(g->xc[i][j][k]-g->xc[i][j][k-1]);			
						g->invjac[i][j][k].a23 = 1.0*(g->yc[i][j][k]-g->yc[i][j][k-1]);			
						g->invjac[i][j][k].a33 = 1.0*(g->zc[i][j][k]-g->zc[i][j][k-1]);
					}
					else
					{
						g->invjac[i][j][k].a13 = 0.5*(g->xc[i][j][k+1]-g->xc[i][j][k-1]);			
						g->invjac[i][j][k].a23 = 0.5*(g->yc[i][j][k+1]-g->yc[i][j][k-1]);			
						g->invjac[i][j][k].a33 = 0.5*(g->zc[i][j][k+1]-g->zc[i][j][k-1]);
					}
				}
				else if(j==0)
				{
					// dx/deta, dy/deta, dz/deta
					g->invjac[i][j][k].a12 = 1.0*(g->xc[i][j+1][k]-g->xc[i][j][k]);			
					g->invjac[i][j][k].a22 = 1.0*(g->yc[i][j+1][k]-g->yc[i][j][k]);			
					g->invjac[i][j][k].a32 = 1.0*(g->zc[i][j+1][k]-g->zc[i][j][k]);
					// dx/dcsi, dy/dcsi, dz/dcsi
					if(i==0)
					{
						g->invjac[i][j][k].a11 = (g->xc[i+1][j][k]-g->xc[i][j][k]);			
						g->invjac[i][j][k].a21 = (g->yc[i+1][j][k]-g->yc[i][j][k]);			
						g->invjac[i][j][k].a31 = (g->zc[i+1][j][k]-g->zc[i][j][k]);
					}
					else if(i==nx)
					{ 
						g->invjac[i][j][k].a11 = (g->xc[i][j][k]-g->xc[i-1][j][k]);			
						g->invjac[i][j][k].a21 = (g->yc[i][j][k]-g->yc[i-1][j][k]);			
						g->invjac[i][j][k].a31 = (g->zc[i][j][k]-g->zc[i-1][j][k]);
					}
					else
					{ 
						g->invjac[i][j][k].a11 = 0.5*(g->xc[i+1][j][k]-g->xc[i-1][j][k]);			
						g->invjac[i][j][k].a21 = 0.5*(g->yc[i+1][j][k]-g->yc[i-1][j][k]);			
						g->invjac[i][j][k].a31 = 0.5*(g->zc[i+1][j][k]-g->zc[i-1][j][k]);
					}
					// dx/dzita, dy/dzita, dz/dzita
					if(k==0)
					{
						g->invjac[i][j][k].a13 = (g->xc[i][j][k+1]-g->xc[i][j][k]);			
						g->invjac[i][j][k].a23 = (g->yc[i][j][k+1]-g->yc[i][j][k]);			
						g->invjac[i][j][k].a33 = (g->zc[i][j][k+1]-g->zc[i][j][k]);
					}
					else if(k==nz)
					{
						g->invjac[i][j][k].a13 = (g->xc[i][j][k]-g->xc[i][j][k-1]);			
						g->invjac[i][j][k].a23 = (g->yc[i][j][k]-g->yc[i][j][k-1]);			
						g->invjac[i][j][k].a33 = (g->zc[i][j][k]-g->zc[i][j][k-1]);
					}
					else
					{
						g->invjac[i][j][k].a13 = 0.5*(g->xc[i][j][k+1]-g->xc[i][j][k-1]);			
						g->invjac[i][j][k].a23 = 0.5*(g->yc[i][j][k+1]-g->yc[i][j][k-1]);			
						g->invjac[i][j][k].a33 = 0.5*(g->zc[i][j][k+1]-g->zc[i][j][k-1]);
					}
				}
				else if(j==ny)
				{
					// dx/deta, dy/deta, dz/deta
					g->invjac[i][j][k].a12 = 1.0*(g->xc[i][j][k]-g->xc[i][j-1][k]);			
					g->invjac[i][j][k].a22 = 1.0*(g->yc[i][j][k]-g->yc[i][j-1][k]);			
					g->invjac[i][j][k].a32 = 1.0*(g->zc[i][j][k]-g->zc[i][j-1][k]);
					// dx/dcsi, dy/dcsi, dz/dcsi
					if(i==0)
					{
						g->invjac[i][j][k].a11 = (g->xc[i+1][j][k]-g->xc[i][j][k]);			
						g->invjac[i][j][k].a21 = (g->yc[i+1][j][k]-g->yc[i][j][k]);			
						g->invjac[i][j][k].a31 = (g->zc[i+1][j][k]-g->zc[i][j][k]);
					}
					else if(i==nx)
					{ 
						g->invjac[i][j][k].a11 = (g->xc[i][j][k]-g->xc[i-1][j][k]);			
						g->invjac[i][j][k].a21 = (g->yc[i][j][k]-g->yc[i-1][j][k]);			
						g->invjac[i][j][k].a31 = (g->zc[i][j][k]-g->zc[i-1][j][k]);
					}
					else
					{ 
						g->invjac[i][j][k].a11 = 0.5*(g->xc[i+1][j][k]-g->xc[i-1][j][k]);			
						g->invjac[i][j][k].a21 = 0.5*(g->yc[i+1][j][k]-g->yc[i-1][j][k]);			
						g->invjac[i][j][k].a31 = 0.5*(g->zc[i+1][j][k]-g->zc[i-1][j][k]);
					}
					// dx/dzita, dy/dzita, dz/dzita
					if(k==0)
					{
						g->invjac[i][j][k].a13 = (g->xc[i][j][k+1]-g->xc[i][j][k]);			
						g->invjac[i][j][k].a23 = (g->yc[i][j][k+1]-g->yc[i][j][k]);			
						g->invjac[i][j][k].a33 = (g->zc[i][j][k+1]-g->zc[i][j][k]);
					}
					else if(k==nz)
					{
						g->invjac[i][j][k].a13 = (g->xc[i][j][k]-g->xc[i][j][k-1]);			
						g->invjac[i][j][k].a23 = (g->yc[i][j][k]-g->yc[i][j][k-1]);			
						g->invjac[i][j][k].a33 = (g->zc[i][j][k]-g->zc[i][j][k-1]);
					}
					else
					{
						g->invjac[i][j][k].a13 = 0.5*(g->xc[i][j][k+1]-g->xc[i][j][k-1]);			
						g->invjac[i][j][k].a23 = 0.5*(g->yc[i][j][k+1]-g->yc[i][j][k-1]);			
						g->invjac[i][j][k].a33 = 0.5*(g->zc[i][j][k+1]-g->zc[i][j][k-1]);
					}
				}
				else if(k==0)
				{
					// dx/dzita, dy/dzita, dz/dzita
					g->invjac[i][j][k].a13 = (g->xc[i][j][k+1]-g->xc[i][j][k]);			
					g->invjac[i][j][k].a23 = (g->yc[i][j][k+1]-g->yc[i][j][k]);			
					g->invjac[i][j][k].a33 = (g->zc[i][j][k+1]-g->zc[i][j][k]);
					// dx/dcsi, dy/dcsi, dz/dcsi 
					if(i==0)
					{
						g->invjac[i][j][k].a11 = (g->xc[i+1][j][k]-g->xc[i][j][k]);			
						g->invjac[i][j][k].a21 = (g->yc[i+1][j][k]-g->yc[i][j][k]);			
						g->invjac[i][j][k].a31 = (g->zc[i+1][j][k]-g->zc[i][j][k]);
					}
					else if(i==nx)
					{
						g->invjac[i][j][k].a11 = (g->xc[i][j][k]-g->xc[i-1][j][k]);			
						g->invjac[i][j][k].a21 = (g->yc[i][j][k]-g->yc[i-1][j][k]);			
						g->invjac[i][j][k].a31 = (g->zc[i][j][k]-g->zc[i-1][j][k]);
					}
					else
					{
						g->invjac[i][j][k].a11 = 0.5*(g->xc[i+1][j][k]-g->xc[i-1][j][k]);			
						g->invjac[i][j][k].a21 = 0.5*(g->yc[i+1][j][k]-g->yc[i-1][j][k]);			
						g->invjac[i][j][k].a31 = 0.5*(g->zc[i+1][j][k]-g->zc[i-1][j][k]);
					}
					// dx/deta, dy/deta, dz/deta
					if(j==0)
					{
						g->invjac[i][j][k].a12 = (g->xc[i][j+1][k]-g->xc[i][j][k]);			
						g->invjac[i][j][k].a22 = (g->yc[i][j+1][k]-g->yc[i][j][k]);			
						g->invjac[i][j][k].a32 = (g->zc[i][j+1][k]-g->zc[i][j][k]);
					}
					else if (j==ny)
					{
						g->invjac[i][j][k].a12 = (g->xc[i][j][k]-g->xc[i][j-1][k]);			
						g->invjac[i][j][k].a22 = (g->yc[i][j][k]-g->yc[i][j-1][k]);			
						g->invjac[i][j][k].a32 = (g->zc[i][j][k]-g->zc[i][j-1][k]);
					}
					else
					{
						g->invjac[i][j][k].a12 = 0.5*(g->xc[i][j+1][k]-g->xc[i][j-1][k]);			
						g->invjac[i][j][k].a22 = 0.5*(g->yc[i][j+1][k]-g->yc[i][j-1][k]);			
						g->invjac[i][j][k].a32 = 0.5*(g->zc[i][j+1][k]-g->zc[i][j-1][k]);
					}
				}
				else if(k==nz)
				{
					// dx/dzita, dy/dzita, dz/dzita
					g->invjac[i][j][k].a13 = (g->xc[i][j][k]-g->xc[i][j][k-1]);			
					g->invjac[i][j][k].a23 = (g->yc[i][j][k]-g->yc[i][j][k-1]);			
					g->invjac[i][j][k].a33 = (g->zc[i][j][k]-g->zc[i][j][k-1]);
					// dx/dcsi, dy/dcsi, dz/dcsi 
					if(i==0)
					{
						g->invjac[i][j][k].a11 = (g->xc[i+1][j][k]-g->xc[i][j][k]);			
						g->invjac[i][j][k].a21 = (g->yc[i+1][j][k]-g->yc[i][j][k]);			
						g->invjac[i][j][k].a31 = (g->zc[i+1][j][k]-g->zc[i][j][k]);
					}
					else if(i==nx)
					{
						g->invjac[i][j][k].a11 = (g->xc[i][j][k]-g->xc[i-1][j][k]);			
						g->invjac[i][j][k].a21 = (g->yc[i][j][k]-g->yc[i-1][j][k]);			
						g->invjac[i][j][k].a31 = (g->zc[i][j][k]-g->zc[i-1][j][k]);
					}
					else
					{
						g->invjac[i][j][k].a11 = 0.5*(g->xc[i+1][j][k]-g->xc[i-1][j][k]);			
						g->invjac[i][j][k].a21 = 0.5*(g->yc[i+1][j][k]-g->yc[i-1][j][k]);			
						g->invjac[i][j][k].a31 = 0.5*(g->zc[i+1][j][k]-g->zc[i-1][j][k]);
					}
					// dx/deta, dy/deta, dz/deta
					if(j==0)
					{
						g->invjac[i][j][k].a12 = (g->xc[i][j+1][k]-g->xc[i][j][k]);			
						g->invjac[i][j][k].a22 = (g->yc[i][j+1][k]-g->yc[i][j][k]);			
						g->invjac[i][j][k].a32 = (g->zc[i][j+1][k]-g->zc[i][j][k]);
					}
					else if (j==ny)
					{
						g->invjac[i][j][k].a12 = (g->xc[i][j][k]-g->xc[i][j-1][k]);			
						g->invjac[i][j][k].a22 = (g->yc[i][j][k]-g->yc[i][j-1][k]);			
						g->invjac[i][j][k].a32 = (g->zc[i][j][k]-g->zc[i][j-1][k]);
					}
					else
					{
						g->invjac[i][j][k].a12 = 0.5*(g->xc[i][j+1][k]-g->xc[i][j-1][k]);			
						g->invjac[i][j][k].a22 = 0.5*(g->yc[i][j+1][k]-g->yc[i][j-1][k]);			
						g->invjac[i][j][k].a32 = 0.5*(g->zc[i][j+1][k]-g->zc[i][j-1][k]);
					}
				}
				else
				{
					// dx/dcsi, dy/dcsi, dz/dcsi 
					g->invjac[i][j][k].a11 = 0.5*(g->xc[i+1][j][k]-g->xc[i-1][j][k]);			
					g->invjac[i][j][k].a21 = 0.5*(g->yc[i+1][j][k]-g->yc[i-1][j][k]);			
					g->invjac[i][j][k].a31 = 0.5*(g->zc[i+1][j][k]-g->zc[i-1][j][k]);
					// dx/deta, dy/deta, dz/deta
					g->invjac[i][j][k].a12 = 0.5*(g->xc[i][j+1][k]-g->xc[i][j-1][k]);			
					g->invjac[i][j][k].a22 = 0.5*(g->yc[i][j+1][k]-g->yc[i][j-1][k]);			
					g->invjac[i][j][k].a32 = 0.5*(g->zc[i][j+1][k]-g->zc[i][j-1][k]);
					// dx/dzita, dy/dzita, dz/dzita
					g->invjac[i][j][k].a13 = 0.5*(g->xc[i][j][k+1]-g->xc[i][j][k-1]);			
					g->invjac[i][j][k].a23 = 0.5*(g->yc[i][j][k+1]-g->yc[i][j][k-1]);			
					g->invjac[i][j][k].a33 = 0.5*(g->zc[i][j][k+1]-g->zc[i][j][k-1]);
				}
				g->invjac[i][j][k].det = (g->invjac[i][j][k].a11)*(g->invjac[i][j][k].a22*g->invjac[i][j][k].a33 - g->invjac[i][j][k].a32*g->invjac[i][j][k].a23)
													   -(g->invjac[i][j][k].a12)*(g->invjac[i][j][k].a21*g->invjac[i][j][k].a33 - g->invjac[i][j][k].a31*g->invjac[i][j][k].a23)
													   +(g->invjac[i][j][k].a13)*(g->invjac[i][j][k].a21*g->invjac[i][j][k].a32 - g->invjac[i][j][k].a31*g->invjac[i][j][k].a22);
			}
		
	// allocation of jacobian matrix
	g->jac = (struct matrix ***) malloc ((nx+1) * sizeof(*g->jac));
	for(i = 0; i < nx+1; ++i)
	{
		g->jac[i] = (struct matrix **) malloc((ny+1)*sizeof(**g->jac));
		for(j = 0; j < ny+1; ++j)
			g->jac[i][j] = (struct matrix *) malloc((nz+1)*sizeof(***g->jac));		
	}
  // the grid has nx+1 elements with indices from 0 to nx
	for(i = 0; i < nx+1; ++i)
		for(j = 0; j < ny+1; ++j)
			for(k = 0; k < nz+1; ++k)
			{
				g->jac[i][j][k].a11 = (g->invjac[i][j][k].a22*g->invjac[i][j][k].a33-g->invjac[i][j][k].a23*g->invjac[i][j][k].a32)/(g->invjac[i][j][k].det);
				g->jac[i][j][k].a21 =-(g->invjac[i][j][k].a21*g->invjac[i][j][k].a33-g->invjac[i][j][k].a31*g->invjac[i][j][k].a23)/(g->invjac[i][j][k].det);
				g->jac[i][j][k].a31 = (g->invjac[i][j][k].a21*g->invjac[i][j][k].a32-g->invjac[i][j][k].a22*g->invjac[i][j][k].a31)/(g->invjac[i][j][k].det);

				g->jac[i][j][k].a12 = (g->invjac[i][j][k].a13*g->invjac[i][j][k].a32-g->invjac[i][j][k].a12*g->invjac[i][j][k].a33)/(g->invjac[i][j][k].det);
				g->jac[i][j][k].a22 = (g->invjac[i][j][k].a11*g->invjac[i][j][k].a33-g->invjac[i][j][k].a13*g->invjac[i][j][k].a31)/(g->invjac[i][j][k].det);
				g->jac[i][j][k].a32 = (g->invjac[i][j][k].a12*g->invjac[i][j][k].a31-g->invjac[i][j][k].a11*g->invjac[i][j][k].a32)/(g->invjac[i][j][k].det);

				g->jac[i][j][k].a13 = (g->invjac[i][j][k].a12*g->invjac[i][j][k].a23-g->invjac[i][j][k].a13*g->invjac[i][j][k].a22)/(g->invjac[i][j][k].det);
				g->jac[i][j][k].a23 = (g->invjac[i][j][k].a13*g->invjac[i][j][k].a21-g->invjac[i][j][k].a11*g->invjac[i][j][k].a23)/(g->invjac[i][j][k].det);
				g->jac[i][j][k].a33 = (g->invjac[i][j][k].a11*g->invjac[i][j][k].a22-g->invjac[i][j][k].a12*g->invjac[i][j][k].a21)/(g->invjac[i][j][k].det);

				g->jac[i][j][k].det = 1.0/g->invjac[i][j][k].det;
			}
}
void calculateInvJacobian(int nx,
												  int ny, 
													int nz,
													struct grid* g)
{
	int i, j, k;

	// allocation of jacobian matrix
	g->invjac = (struct matrix ***) malloc ((nx+1) * sizeof(*g->invjac));
	for(i = 0; i < nx+1; ++i)
	{
		g->invjac[i] = (struct matrix **) malloc((ny+1)*sizeof(**g->invjac));
		for(j = 0; j < ny+1; ++j)
			g->invjac[i][j] = (struct matrix *) malloc((nz+1)*sizeof(***g->invjac));		
	}
  // the grid has nx+1 elements with indices from 0 to nx
  // calculating the field inside w/ Central differences
	for(i = 0; i < nx+1; ++i)
		for(j = 0; j < ny+1; ++j)
			for(k = 0; k < nz+1; ++k)
			{
				if(i==0)
				{
					// dx/dcsi, dy/dcsi, dz/dcsi 
					g->invjac[i][j][k].a11 = 1.0*(g->x[i+1][j][k]-g->x[i][j][k]);			
					g->invjac[i][j][k].a21 = 1.0*(g->y[i+1][j][k]-g->y[i][j][k]);			
					g->invjac[i][j][k].a31 = 1.0*(g->z[i+1][j][k]-g->z[i][j][k]);
					// dx/deta, dy/deta, dz/deta
					if(j==0)
					{
						g->invjac[i][j][k].a12 = 1.0*(g->x[i][j+1][k]-g->x[i][j][k]);			
						g->invjac[i][j][k].a22 = 1.0*(g->y[i][j+1][k]-g->y[i][j][k]);			
						g->invjac[i][j][k].a32 = 1.0*(g->z[i][j+1][k]-g->z[i][j][k]);
					}
					else if(j==ny)
					{
						g->invjac[i][j][k].a12 = 1.0*(g->x[i][j][k]-g->x[i][j-1][k]);			
						g->invjac[i][j][k].a22 = 1.0*(g->y[i][j][k]-g->y[i][j-1][k]);			
						g->invjac[i][j][k].a32 = 1.0*(g->z[i][j][k]-g->z[i][j-1][k]);
					}
					else
					{
						g->invjac[i][j][k].a12 = 0.5*(g->x[i][j+1][k]-g->x[i][j-1][k]);			
						g->invjac[i][j][k].a22 = 0.5*(g->y[i][j+1][k]-g->y[i][j-1][k]);			
						g->invjac[i][j][k].a32 = 0.5*(g->z[i][j+1][k]-g->z[i][j-1][k]);
					}
					// dx/dzita, dy/dzita, dz/dzita
					if(k==0)
					{
						g->invjac[i][j][k].a13 = 1.0*(g->x[i][j][k+1]-g->x[i][j][k]);			
						g->invjac[i][j][k].a23 = 1.0*(g->y[i][j][k+1]-g->y[i][j][k]);			
						g->invjac[i][j][k].a33 = 1.0*(g->z[i][j][k+1]-g->z[i][j][k]);
					}
					else if(k==nz)
					{
						g->invjac[i][j][k].a13 = 1.0*(g->x[i][j][k]-g->x[i][j][k-1]);			
						g->invjac[i][j][k].a23 = 1.0*(g->y[i][j][k]-g->y[i][j][k-1]);			
						g->invjac[i][j][k].a33 = 1.0*(g->z[i][j][k]-g->z[i][j][k-1]);
					}
					else
					{
						g->invjac[i][j][k].a13 = 0.5*(g->x[i][j][k+1]-g->x[i][j][k-1]);			
						g->invjac[i][j][k].a23 = 0.5*(g->y[i][j][k+1]-g->y[i][j][k-1]);			
						g->invjac[i][j][k].a33 = 0.5*(g->z[i][j][k+1]-g->z[i][j][k-1]);
					}
				}
				else if (i==nx)
				{
					// dx/dcsi, dy/dcsi, dz/dcsi 
					g->invjac[i][j][k].a11 = 1.0*(g->x[i][j][k]-g->x[i-1][j][k]);			
					g->invjac[i][j][k].a21 = 1.0*(g->y[i][j][k]-g->y[i-1][j][k]);			
					g->invjac[i][j][k].a31 = 1.0*(g->z[i][j][k]-g->z[i-1][j][k]);
					// dx/deta, dy/deta, dz/deta
					if(j==0)
					{
						g->invjac[i][j][k].a12 = 1.0*(g->x[i][j+1][k]-g->x[i][j][k]);			
						g->invjac[i][j][k].a22 = 1.0*(g->y[i][j+1][k]-g->y[i][j][k]);			
						g->invjac[i][j][k].a32 = 1.0*(g->z[i][j+1][k]-g->z[i][j][k]);
					}
					else if(j==ny)
					{
						g->invjac[i][j][k].a12 = 1.0*(g->x[i][j][k]-g->x[i][j-1][k]);			
						g->invjac[i][j][k].a22 = 1.0*(g->y[i][j][k]-g->y[i][j-1][k]);			
						g->invjac[i][j][k].a32 = 1.0*(g->z[i][j][k]-g->z[i][j-1][k]);
					}
					else
					{
						g->invjac[i][j][k].a12 = 0.5*(g->x[i][j+1][k]-g->x[i][j-1][k]);			
						g->invjac[i][j][k].a22 = 0.5*(g->y[i][j+1][k]-g->y[i][j-1][k]);			
						g->invjac[i][j][k].a32 = 0.5*(g->z[i][j+1][k]-g->z[i][j-1][k]);
					}
					// dx/dzita, dy/dzita, dz/dzita
					if(k==0)
					{
						g->invjac[i][j][k].a13 = 1.0*(g->x[i][j][k+1]-g->x[i][j][k]);			
						g->invjac[i][j][k].a23 = 1.0*(g->y[i][j][k+1]-g->y[i][j][k]);			
						g->invjac[i][j][k].a33 = 1.0*(g->z[i][j][k+1]-g->z[i][j][k]);
					}
					else if(k==nz)
					{
						g->invjac[i][j][k].a13 = 1.0*(g->x[i][j][k]-g->x[i][j][k-1]);			
						g->invjac[i][j][k].a23 = 1.0*(g->y[i][j][k]-g->y[i][j][k-1]);			
						g->invjac[i][j][k].a33 = 1.0*(g->z[i][j][k]-g->z[i][j][k-1]);
					}
					else
					{
						g->invjac[i][j][k].a13 = 0.5*(g->x[i][j][k+1]-g->x[i][j][k-1]);			
						g->invjac[i][j][k].a23 = 0.5*(g->y[i][j][k+1]-g->y[i][j][k-1]);			
						g->invjac[i][j][k].a33 = 0.5*(g->z[i][j][k+1]-g->z[i][j][k-1]);
					}
				}
				else if(j==0)
				{
					// dx/deta, dy/deta, dz/deta
					g->invjac[i][j][k].a12 = 1.0*(g->x[i][j+1][k]-g->x[i][j][k]);			
					g->invjac[i][j][k].a22 = 1.0*(g->y[i][j+1][k]-g->y[i][j][k]);			
					g->invjac[i][j][k].a32 = 1.0*(g->z[i][j+1][k]-g->z[i][j][k]);
					// dx/dcsi, dy/dcsi, dz/dcsi
					if(i==0)
					{
						g->invjac[i][j][k].a11 = (g->x[i+1][j][k]-g->x[i][j][k]);			
						g->invjac[i][j][k].a21 = (g->y[i+1][j][k]-g->y[i][j][k]);			
						g->invjac[i][j][k].a31 = (g->z[i+1][j][k]-g->z[i][j][k]);
					}
					else if(i==nx)
					{ 
						g->invjac[i][j][k].a11 = (g->x[i][j][k]-g->x[i-1][j][k]);			
						g->invjac[i][j][k].a21 = (g->y[i][j][k]-g->y[i-1][j][k]);			
						g->invjac[i][j][k].a31 = (g->z[i][j][k]-g->z[i-1][j][k]);
					}
					else
					{ 
						g->invjac[i][j][k].a11 = 0.5*(g->x[i+1][j][k]-g->x[i-1][j][k]);			
						g->invjac[i][j][k].a21 = 0.5*(g->y[i+1][j][k]-g->y[i-1][j][k]);			
						g->invjac[i][j][k].a31 = 0.5*(g->z[i+1][j][k]-g->z[i-1][j][k]);
					}
					// dx/dzita, dy/dzita, dz/dzita
					if(k==0)
					{
						g->invjac[i][j][k].a13 = (g->x[i][j][k+1]-g->x[i][j][k]);			
						g->invjac[i][j][k].a23 = (g->y[i][j][k+1]-g->y[i][j][k]);			
						g->invjac[i][j][k].a33 = (g->z[i][j][k+1]-g->z[i][j][k]);
					}
					else if(k==nz)
					{
						g->invjac[i][j][k].a13 = (g->x[i][j][k]-g->x[i][j][k-1]);			
						g->invjac[i][j][k].a23 = (g->y[i][j][k]-g->y[i][j][k-1]);			
						g->invjac[i][j][k].a33 = (g->z[i][j][k]-g->z[i][j][k-1]);
					}
					else
					{
						g->invjac[i][j][k].a13 = 0.5*(g->x[i][j][k+1]-g->x[i][j][k-1]);			
						g->invjac[i][j][k].a23 = 0.5*(g->y[i][j][k+1]-g->y[i][j][k-1]);			
						g->invjac[i][j][k].a33 = 0.5*(g->z[i][j][k+1]-g->z[i][j][k-1]);
					}
				}
				else if(j==ny)
				{
					// dx/deta, dy/deta, dz/deta
					g->invjac[i][j][k].a12 = 1.0*(g->x[i][j][k]-g->x[i][j-1][k]);			
					g->invjac[i][j][k].a22 = 1.0*(g->y[i][j][k]-g->y[i][j-1][k]);			
					g->invjac[i][j][k].a32 = 1.0*(g->z[i][j][k]-g->z[i][j-1][k]);
					// dx/dcsi, dy/dcsi, dz/dcsi
					if(i==0)
					{
						g->invjac[i][j][k].a11 = (g->x[i+1][j][k]-g->x[i][j][k]);			
						g->invjac[i][j][k].a21 = (g->y[i+1][j][k]-g->y[i][j][k]);			
						g->invjac[i][j][k].a31 = (g->z[i+1][j][k]-g->z[i][j][k]);
					}
					else if(i==nx)
					{ 
						g->invjac[i][j][k].a11 = (g->x[i][j][k]-g->x[i-1][j][k]);			
						g->invjac[i][j][k].a21 = (g->y[i][j][k]-g->y[i-1][j][k]);			
						g->invjac[i][j][k].a31 = (g->z[i][j][k]-g->z[i-1][j][k]);
					}
					else
					{ 
						g->invjac[i][j][k].a11 = 0.5*(g->x[i+1][j][k]-g->x[i-1][j][k]);			
						g->invjac[i][j][k].a21 = 0.5*(g->y[i+1][j][k]-g->y[i-1][j][k]);			
						g->invjac[i][j][k].a31 = 0.5*(g->z[i+1][j][k]-g->z[i-1][j][k]);
					}
					// dx/dzita, dy/dzita, dz/dzita
					if(k==0)
					{
						g->invjac[i][j][k].a13 = (g->x[i][j][k+1]-g->x[i][j][k]);			
						g->invjac[i][j][k].a23 = (g->y[i][j][k+1]-g->y[i][j][k]);			
						g->invjac[i][j][k].a33 = (g->z[i][j][k+1]-g->z[i][j][k]);
					}
					else if(k==nz)
					{
						g->invjac[i][j][k].a13 = (g->x[i][j][k]-g->x[i][j][k-1]);			
						g->invjac[i][j][k].a23 = (g->y[i][j][k]-g->y[i][j][k-1]);			
						g->invjac[i][j][k].a33 = (g->z[i][j][k]-g->z[i][j][k-1]);
					}
					else
					{
						g->invjac[i][j][k].a13 = 0.5*(g->x[i][j][k+1]-g->x[i][j][k-1]);			
						g->invjac[i][j][k].a23 = 0.5*(g->y[i][j][k+1]-g->y[i][j][k-1]);			
						g->invjac[i][j][k].a33 = 0.5*(g->z[i][j][k+1]-g->z[i][j][k-1]);
					}
				}
				else if(k==0)
				{
					// dx/dzita, dy/dzita, dz/dzita
					g->invjac[i][j][k].a13 = (g->x[i][j][k+1]-g->x[i][j][k]);			
					g->invjac[i][j][k].a23 = (g->y[i][j][k+1]-g->y[i][j][k]);			
					g->invjac[i][j][k].a33 = (g->z[i][j][k+1]-g->z[i][j][k]);
					// dx/dcsi, dy/dcsi, dz/dcsi 
					if(i==0)
					{
						g->invjac[i][j][k].a11 = (g->x[i+1][j][k]-g->x[i][j][k]);			
						g->invjac[i][j][k].a21 = (g->y[i+1][j][k]-g->y[i][j][k]);			
						g->invjac[i][j][k].a31 = (g->z[i+1][j][k]-g->z[i][j][k]);
					}
					else if(i==nx)
					{
						g->invjac[i][j][k].a11 = (g->x[i][j][k]-g->x[i-1][j][k]);			
						g->invjac[i][j][k].a21 = (g->y[i][j][k]-g->y[i-1][j][k]);			
						g->invjac[i][j][k].a31 = (g->z[i][j][k]-g->z[i-1][j][k]);
					}
					else
					{
						g->invjac[i][j][k].a11 = 0.5*(g->x[i+1][j][k]-g->x[i-1][j][k]);			
						g->invjac[i][j][k].a21 = 0.5*(g->y[i+1][j][k]-g->y[i-1][j][k]);			
						g->invjac[i][j][k].a31 = 0.5*(g->z[i+1][j][k]-g->z[i-1][j][k]);
					}
					// dx/deta, dy/deta, dz/deta
					if(j==0)
					{
						g->invjac[i][j][k].a12 = (g->x[i][j+1][k]-g->x[i][j][k]);			
						g->invjac[i][j][k].a22 = (g->y[i][j+1][k]-g->y[i][j][k]);			
						g->invjac[i][j][k].a32 = (g->z[i][j+1][k]-g->z[i][j][k]);
					}
					else if (j==ny)
					{
						g->invjac[i][j][k].a12 = (g->x[i][j][k]-g->x[i][j-1][k]);			
						g->invjac[i][j][k].a22 = (g->y[i][j][k]-g->y[i][j-1][k]);			
						g->invjac[i][j][k].a32 = (g->z[i][j][k]-g->z[i][j-1][k]);
					}
					else
					{
						g->invjac[i][j][k].a12 = 0.5*(g->x[i][j+1][k]-g->x[i][j-1][k]);			
						g->invjac[i][j][k].a22 = 0.5*(g->y[i][j+1][k]-g->y[i][j-1][k]);			
						g->invjac[i][j][k].a32 = 0.5*(g->z[i][j+1][k]-g->z[i][j-1][k]);
					}
				}
				else if(k==nz)
				{
					// dx/dzita, dy/dzita, dz/dzita
					g->invjac[i][j][k].a13 = (g->x[i][j][k]-g->x[i][j][k-1]);			
					g->invjac[i][j][k].a23 = (g->y[i][j][k]-g->y[i][j][k-1]);			
					g->invjac[i][j][k].a33 = (g->z[i][j][k]-g->z[i][j][k-1]);
					// dx/dcsi, dy/dcsi, dz/dcsi 
					if(i==0)
					{
						g->invjac[i][j][k].a11 = (g->x[i+1][j][k]-g->x[i][j][k]);			
						g->invjac[i][j][k].a21 = (g->y[i+1][j][k]-g->y[i][j][k]);			
						g->invjac[i][j][k].a31 = (g->z[i+1][j][k]-g->z[i][j][k]);
					}
					else if(i==nx)
					{
						g->invjac[i][j][k].a11 = (g->x[i][j][k]-g->x[i-1][j][k]);			
						g->invjac[i][j][k].a21 = (g->y[i][j][k]-g->y[i-1][j][k]);			
						g->invjac[i][j][k].a31 = (g->z[i][j][k]-g->z[i-1][j][k]);
					}
					else
					{
						g->invjac[i][j][k].a11 = 0.5*(g->x[i+1][j][k]-g->x[i-1][j][k]);			
						g->invjac[i][j][k].a21 = 0.5*(g->y[i+1][j][k]-g->y[i-1][j][k]);			
						g->invjac[i][j][k].a31 = 0.5*(g->z[i+1][j][k]-g->z[i-1][j][k]);
					}
					// dx/deta, dy/deta, dz/deta
					if(j==0)
					{
						g->invjac[i][j][k].a12 = (g->x[i][j+1][k]-g->x[i][j][k]);			
						g->invjac[i][j][k].a22 = (g->y[i][j+1][k]-g->y[i][j][k]);			
						g->invjac[i][j][k].a32 = (g->z[i][j+1][k]-g->z[i][j][k]);
					}
					else if (j==ny)
					{
						g->invjac[i][j][k].a12 = (g->x[i][j][k]-g->x[i][j-1][k]);			
						g->invjac[i][j][k].a22 = (g->y[i][j][k]-g->y[i][j-1][k]);			
						g->invjac[i][j][k].a32 = (g->z[i][j][k]-g->z[i][j-1][k]);
					}
					else
					{
						g->invjac[i][j][k].a12 = 0.5*(g->x[i][j+1][k]-g->x[i][j-1][k]);			
						g->invjac[i][j][k].a22 = 0.5*(g->y[i][j+1][k]-g->y[i][j-1][k]);			
						g->invjac[i][j][k].a32 = 0.5*(g->z[i][j+1][k]-g->z[i][j-1][k]);
					}
				}
				else
				{
					// dx/dcsi, dy/dcsi, dz/dcsi 
					g->invjac[i][j][k].a11 = 0.5*(g->x[i+1][j][k]-g->x[i-1][j][k]);			
					g->invjac[i][j][k].a21 = 0.5*(g->y[i+1][j][k]-g->y[i-1][j][k]);			
					g->invjac[i][j][k].a31 = 0.5*(g->z[i+1][j][k]-g->z[i-1][j][k]);
					// dx/deta, dy/deta, dz/deta
					g->invjac[i][j][k].a12 = 0.5*(g->x[i][j+1][k]-g->x[i][j-1][k]);			
					g->invjac[i][j][k].a22 = 0.5*(g->y[i][j+1][k]-g->y[i][j-1][k]);			
					g->invjac[i][j][k].a32 = 0.5*(g->z[i][j+1][k]-g->z[i][j-1][k]);
					// dx/dzita, dy/dzita, dz/dzita
					g->invjac[i][j][k].a13 = 0.5*(g->x[i][j][k+1]-g->x[i][j][k-1]);			
					g->invjac[i][j][k].a23 = 0.5*(g->y[i][j][k+1]-g->y[i][j][k-1]);			
					g->invjac[i][j][k].a33 = 0.5*(g->z[i][j][k+1]-g->z[i][j][k-1]);
				}
				g->invjac[i][j][k].det = (g->invjac[i][j][k].a11)*(g->invjac[i][j][k].a22*g->invjac[i][j][k].a33 - g->invjac[i][j][k].a32*g->invjac[i][j][k].a23)
													   -(g->invjac[i][j][k].a12)*(g->invjac[i][j][k].a21*g->invjac[i][j][k].a33 - g->invjac[i][j][k].a31*g->invjac[i][j][k].a23)
													   +(g->invjac[i][j][k].a13)*(g->invjac[i][j][k].a21*g->invjac[i][j][k].a32 - g->invjac[i][j][k].a31*g->invjac[i][j][k].a22);
			}
		
	// allocation of jacobian matrix
	g->jac = (struct matrix ***) malloc ((nx+1) * sizeof(*g->jac));
	for(i = 0; i < nx+1; ++i)
	{
		g->jac[i] = (struct matrix **) malloc((ny+1)*sizeof(**g->jac));
		for(j = 0; j < ny+1; ++j)
			g->jac[i][j] = (struct matrix *) malloc((nz+1)*sizeof(***g->jac));		
	}
  // the grid has nx+1 elements with indices from 0 to nx
	for(i = 0; i < nx+1; ++i)
		for(j = 0; j < ny+1; ++j)
			for(k = 0; k < nz+1; ++k)
			{
				g->jac[i][j][k].a11 = (g->invjac[i][j][k].a22*g->invjac[i][j][k].a33-g->invjac[i][j][k].a23*g->invjac[i][j][k].a32)/(g->invjac[i][j][k].det);
				g->jac[i][j][k].a21 =-(g->invjac[i][j][k].a21*g->invjac[i][j][k].a33-g->invjac[i][j][k].a31*g->invjac[i][j][k].a23)/(g->invjac[i][j][k].det);
				g->jac[i][j][k].a31 = (g->invjac[i][j][k].a21*g->invjac[i][j][k].a32-g->invjac[i][j][k].a22*g->invjac[i][j][k].a31)/(g->invjac[i][j][k].det);

				g->jac[i][j][k].a12 = (g->invjac[i][j][k].a13*g->invjac[i][j][k].a32-g->invjac[i][j][k].a12*g->invjac[i][j][k].a33)/(g->invjac[i][j][k].det);
				g->jac[i][j][k].a22 = (g->invjac[i][j][k].a11*g->invjac[i][j][k].a33-g->invjac[i][j][k].a13*g->invjac[i][j][k].a31)/(g->invjac[i][j][k].det);
				g->jac[i][j][k].a32 = (g->invjac[i][j][k].a12*g->invjac[i][j][k].a31-g->invjac[i][j][k].a11*g->invjac[i][j][k].a32)/(g->invjac[i][j][k].det);

				g->jac[i][j][k].a13 = (g->invjac[i][j][k].a12*g->invjac[i][j][k].a23-g->invjac[i][j][k].a13*g->invjac[i][j][k].a22)/(g->invjac[i][j][k].det);
				g->jac[i][j][k].a23 = (g->invjac[i][j][k].a13*g->invjac[i][j][k].a21-g->invjac[i][j][k].a11*g->invjac[i][j][k].a23)/(g->invjac[i][j][k].det);
				g->jac[i][j][k].a33 = (g->invjac[i][j][k].a11*g->invjac[i][j][k].a22-g->invjac[i][j][k].a12*g->invjac[i][j][k].a21)/(g->invjac[i][j][k].det);

				g->jac[i][j][k].det = 1.0/g->invjac[i][j][k].det;
			}
}
