/* Copyright, 2015, Santiago Lopez Castano
 *=========================================================================
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.

 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */
#include <stdlib.h>
#include <math.h>
#include "stru.h"
#include "visa.h"

void conditionalAvg(
										int nx, 
										int ny, 
										int nz,
										int nn,
										double c, 
										double **** const val,
										double **** d
									 );

void VISA	(
						int nx,
						int ny,
						int nz,
						int ntime,
						//double **** const input,
						struct field* campo,// this arg comprises the perturbation fields
						double **** out
					)
{
	const struct field *fin = campo + ntime;
	double **** d, *** dd;
	double ****find;
	int i, j, k;

	d = malloc(ntime*sizeof(***d));	
	dd= NULL;	
	while(campo < fin)
	{
		conditionalAvg(nx,ny,nz,3,5.0,&(campo->lambda2),d);
		++campo;
		++d;
	}
	init_3dArray(nx,ny,nz,out);
	init_3dArray(nx,ny,nz,&dd);

	find = d-ntime;
	while((--d) >= find)
		for (k = 0; k < nz+2; k++)
			for (j = 0; j < ny+2; j++)
				for (i = 0; i < nx+2; i++)
				{
					(*out)[i][j][k] += (*d)[i][j][k]*campo->lambda2[i][j][k];
					dd[i][j][k] += (*d)[i][j][k];
				}		
	for (k = 0; k < nz+2; k++)
		for (j = 0; j < ny+2; j++)
			for (i = 0; i < nx+2; i++)
				if(dd[i][j][k] > 0)
					(*out)[i][j][k] /= dd[i][j][k];

	find = d + ntime;
	while((d++) < find)
		dealloc_3d(nx,ny,(void ****)d);
	free(d);
	dealloc_3d(nx,ny,(void ****)dd);
}


void conditionalAvg(
										int nx, 
										int ny, 
										int nz,
										int nn,
										double c, 
										double **** const val,
										double **** d
									 )
{
	double ***vval;// 
	double ***condAvg;//[nx+2][ny+2][nz+2];
	double ***condVar;//[nx+2][ny+2][nz+2];
	double ***valSq;//[nx+2][ny+2][nz+2];
	double mean[ny+2];
	double valRMS[ny+2];

	
	double sum, valPrime;
	int i, j, k, ii, kk;

	vval=NULL; 
	condAvg=NULL;
	condVar=NULL;
	valSq=NULL;

	
	init_3dArray(nx+2*nn,ny,nz+2*nn,&vval);
	init_3dArray(nx,ny,nz,&condAvg);
	init_3dArray(nx,ny,nz,&condVar);
	init_3dArray(nx,ny,nz,&valSq);
	init_3dArray(nx,ny,nz,d);

	for (k = nn-1; k < nz+2 + nn; k++)
		for (j = 0; j < ny+2; j++)
			for (i = nn-1; i < nx+2 + nn; i++)
				vval[i][j][k] = (*val)[i-nn+1][j][k-nn+1];

	for (k = 0; k < nn; k++)
		for (j = 0; j < ny+2; j++)
			for (i = 0; i < nn; i++)
			{
				vval[i][j][k] = (*val)[nx+1-i][j][nz+1-k];
				vval[i+nx+2][j][k+nz+2] = (*val)[i+1][j][k+1];
			}

	for (k = nn-1; k < nz+2 + nn; k++)
		for (j = 0; j < ny+2; j++)
			for (i = nn-1; i < nx+2 + nn; i++)
			{
				sum = 0.0;
				for (ii = i-nn+1; ii < i+nn; ii++)
					for (kk = k-nn+1; kk < k+nn; kk++)
						sum += vval[ii][j][kk];
				condAvg[i-nn+1][j][k-nn+1] = (1./(4.*nn*nn))*sum;
			}
	for (k = 0; k < nz+2; k++)
		for (j = 0; j < ny+2; j++)
			for (i = 0; i < nx+2; i++)
				condVar[i][j][k] = (*val)[i][j][k]*(*val)[i][j][k] 
												 - condAvg[i][j][k]*condAvg[i][j][k];
	for (j = 0; j < ny+2; j++)
	{
		sum = 0.0;
		for (k = 0; k < nz+2; k++)
			for (i = 0; i < nx+2; i++)
				sum += (*val)[i][j][k];
		mean[j] = sum / ((double)nx*nz);
	}
	for (k = 0; k < nz+2; k++)
		for (j = 0; j < ny+2; j++)
			for (i = 0; i < nx+2; i++)
			{
				valPrime = (*val)[i][j][k] - mean[j];
				valSq[i][j][k] = valPrime*valPrime;
			}
	for (j = 0; j < ny+2; j++)
	{
		sum = 0.0;
		for (k = 0; k < nz+2; k++)
			for (i = 0; i < nx+2; i++)
				sum += valSq[i][j][k];
		valRMS[j] = sqrt(sum / ((double)nx*nz));
	}
	for (k = 0; k < nz+2; k++)
		for (j = 0; j < ny+2; j++)
			for (i = 0; i < nx+2; i++)
				(*d)[i][j][k] = (double)(condVar[i][j][k] > c*valRMS[j]);

	dealloc_3d(nx+2*nn,ny,(void ****)&vval);
	dealloc_3d(nx,ny,(void ****)&condAvg);
	dealloc_3d(nx,ny,(void ****)&condVar);
	dealloc_3d(nx,ny,(void ****)&valSq);
}
