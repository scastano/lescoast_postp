/* Copyright, 2015, Santiago Lopez Castano
 *=========================================================================
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.

 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "util.h"
#include "meanUtil.h"
#include "derivativeUtil.h"
#include "topologyUtil.h"
#include "energyUtil.h"
#include "fft.h"
#include "correlations.h"
#include "PDF.h"
#include "visa.h"

extern char kProgramVersion[];
const char kFieldFolder[] = "RUN_ITER_";
const char kFieldFile[] = "new_res_iter";
int main (void)
{
//*******************************************************************************
//************** VARIABLES RELATED TO THE GRID SIZE AND NAME FILES **************
//*******************************************************************************
	int nx, ny, nz, nscal, nfields, finit, fend, fstep; // size of the grid
	double re; // THE REYNOLDS NUMBER
	char filename[100], digit[7];
	char *gridfilename=NULL, *fieldfilename=NULL, cont,cont1, oldFormat;
	char *ptr = &kProgramVersion[0];
//*******************************************************************************
//*************** GRID'S, FIELD'S RELATED VARIABLES AND STRUC'S *****************
//*******************************************************************************
	struct grid Grid;
	struct cent cGrid;
	struct field mean;
	double u_star,varS12,divs;
	double Cf[2]; // friction coefficient times the square of ref. Velocity
	double *meanLine=NULL; // line mean for velocity
	double *rhoMeanLine=NULL; // line mean for scalar 0
	double *rhoCorrMeanLine[3]; // line corr for scalar 0
	double *meanShearLine=NULL;
	double *meanViscLine=NULL;
	double *meanKurtosisLine[3]={NULL,NULL,NULL};
	double *meanSkewnessLine[3]={NULL,NULL,NULL};
	double *PDFlist=NULL;
	double *lineTemp=NULL;
	double *corrShear=NULL;
	double *corrLine=NULL, *taylorScales=NULL;
	double *corrXLine[6];
	double *wwMean[9];
	double *wwSMean=NULL, *meanDuDy=NULL;
	double *SLambdaMean[3];
	double *pcMean[9];
	double *epMean[9];
	double *powerSpecReal=NULL;
	double *powerSpecImag=NULL;
	double  *meanBudget[5];
	double  *meanRMS[3];
	double  *meanVor[3];
	double  *meanInvs[2];
	double  *histo = NULL,*meanInvsww[2];
	double  *meanInvspc[2], *bins = NULL;
	double  ***S   = NULL;
	double  ***S12 = NULL;
	double  ***S32 = NULL;
  complex *powerfft = NULL;
  complex *scratch  = NULL;
	struct field *Field, *Pert; // it's an array
	struct field *Finit=NULL,*Pinit=NULL; // pointer to the first element of Fields

	int ii, jj; // counters

	divs = 20;


	while (*ptr != '\0')
	{
		putchar(*ptr);
		++ptr;
	}

  for(ii = 0; ii < 3; ++ii)
	  SLambdaMean[ii] = NULL;
  for(ii = 0; ii < 3; ++ii)
	  meanRMS[ii] = NULL;
  for(ii = 0; ii < 3; ++ii)
	  meanVor[ii] = NULL;
  for(ii = 0; ii < 2; ++ii)
	  meanInvs[ii] = NULL;
  for(ii = 0; ii < 2; ++ii)
	  meanInvsww[ii] = NULL;
  for(ii = 0; ii < 2; ++ii)
	  meanInvspc[ii] = NULL;
  for(ii = 0; ii < 5; ++ii)
	  meanBudget[ii] = NULL;
  for(ii = 0; ii < 6; ++ii)
	  corrXLine[ii] = NULL;
  for(ii = 0; ii < 9; ++ii)
	  wwMean[ii] = NULL;
  for(ii = 0; ii < 9; ++ii)
	  pcMean[ii] = NULL;
  for(ii = 0; ii < 9; ++ii)
	  epMean[ii] = NULL;
  for(ii = 0; ii < 3; ++ii)
	  rhoCorrMeanLine[ii] = NULL;


	init_null_field(&mean);
//*******************************************************************************
//************************* GRID'S, FIELD'S INPUT *******************************
//*******************************************************************************
	printf ("$> Please enter the root directory"
					" where the grid and new_res files are saved:\n");
	scanf("%s", filename);
	printf ("$> Please enter the start field, the size of "
					"steps, and the final field count \n"
					" to be read (separated by whitespaces):\n");
	scanf("%i%i%i", &finit, &fstep, &fend);
  printf("$> Are the inst. field's files inside RUN_* folders (y/n)?\n");
  scanf(" %c",&cont1);

  printf("$> Is the new_res generated by classic LESCOAST (y/n)?\n");
  scanf(" %c",&oldFormat);
	
	printf("$> Please enter the Reynolds Number of the run:\n");
	scanf("%lf",&re);

	printf("$> Please enter the equivalent channel-flow shear "
				 "stress intensity (u*^2) of the run:\n");
	scanf("%lf",&u_star);
	u_star=sqrt(u_star);

  printf("$> Do you want to do wall-bounded channel Statistics (y/n)?\n");
  scanf(" %c",&cont);

	nfields = (fend-finit)/(fstep)+1;
	asprintf(&gridfilename,"%s/%s", filename,"gri3dp_in.dat");
// let's read the grid
	printf("Reading: %s\n",gridfilename);
	readGrid(&Grid,&nx,&ny,&nz,gridfilename);

	calculateCentroids(Grid, &cGrid, nx, ny, nz);
	free((void *)gridfilename);
// let's read the fields
	Field = malloc((nfields)*sizeof(struct field));
	Pert = malloc((nfields)*sizeof(struct field));
	corrShear = calloc(nx+2,sizeof(double));
	Finit = Field;
	Pinit = Pert;
	for(ii=0; ii<nfields; ++ii)
	{
		init_null_field(Finit);
		init_null_field(Pinit);
//*******************************************************************************
// start reading instantaneous fields
//*******************************************************************************
		sprintf(digit,"%06d",finit+ii*fstep);

		if( cont1 == 'y' ) {
			asprintf(&fieldfilename,"%s/%s%s/%s%s%s",
							 filename,kFieldFolder,digit,
							 kFieldFile,digit,".dat");
			printf("Reading: %s\n",fieldfilename);
		} 
		else {
			asprintf(&fieldfilename,"%s/%s%s%s",
							 filename,
							 kFieldFile,digit,".dat");
			printf("Reading: %s\n",fieldfilename);
		} 

    if(oldFormat == 'n')
		{
			readFieldsH5(nx,ny,nz,1,u_star,filename,finit+ii*fstep,Finit);
			//readFields(nx,ny,nz,u_star,fieldfilename,&nscal,Finit);
			nscal = 1;
		}
		else if(oldFormat == 'y')
			readFieldsOld(nx,ny,nz,u_star,re,fieldfilename,&nscal,Finit);

		free((void *)fieldfilename);
// end reading instantaneous fields
//*******************************************************************************
// print the fields in a friendlier format
//*******************************************************************************
		asprintf(&fieldfilename,"%s%s%s",
						 "instField",digit,".vtk");
		printPointerAllToVTK (fieldfilename,nx,ny,nz,nscal,&cGrid,Finit);
		printf("Written: %s\n",fieldfilename);
		free((void *)fieldfilename);
//*******************************************************************************
// Print the fluxes in a separate format (It has to be like that in Legacy VTK)
// I made a poor choice in output format, sorry
//*******************************************************************************
/*
		asprintf(&fieldfilename,"%s%s%s",
						 "UFlux",digit,".vtk");
    printPointerFaceScalarToVTK(fieldfilename,nx,ny,nz,"uc",X,&Grid,
                                &(Finit->uc));
		printf("Written: %s\n",fieldfilename);
		free((void *)fieldfilename);

		asprintf(&fieldfilename,"%s%s%s",
						 "VFlux",digit,".vtk");
    printPointerFaceScalarToVTK(fieldfilename,nx,ny,nz,"vc",Y,&Grid,
                                &(Finit->vc));
		printf("Written: %s\n",fieldfilename);
		free((void *)fieldfilename);

		asprintf(&fieldfilename,"%s%s%s",
						 "WFlux",digit,".vtk");
    printPointerFaceScalarToVTK(fieldfilename,nx,ny,nz,"wc",Z,&Grid,
                                &(Finit->wc));
		printf("Written: %s\n",fieldfilename);
		free((void *)fieldfilename);
*/
//*******************************************************************************
// 
//*******************************************************************************
  	calculateFieldDerivatives(nx,ny,nz,&(cGrid.jac),Finit);
		calculateLambdaCriterion(nx,ny,nz,re,Finit);
		printPlaneHorDir("vortXPlaneStatsJ1.xy",nx,1,nz,&(Finit->vortX));
		printPlaneHorDir("vortYPlaneStatsJ1.xy",nx,1,nz,&(Finit->vortY));
		printPlaneHorDir("vortZPlaneStatsJ1.xy",nx,1,nz,&(Finit->vortZ));
		printPlaneHorDir("vortXPlaneStatsJny.xy",nx,ny,nz,&(Finit->vortX));
		printPlaneHorDir("vortYPlaneStatsJny.xy",nx,ny,nz,&(Finit->vortY));
		printPlaneHorDir("vortZPlaneStatsJny.xy",nx,ny,nz,&(Finit->vortZ));

//*******************************************************************************
// Some statistics
//*******************************************************************************
//		passTensorElemTo3dArray(nx,ny,nz,1,&(Finit->strain),&S12);	
//		passTensorElemTo3dArray(nx,ny,nz,5,&(Finit->strain),&S32);
//		correlationX(nx,nz,0,(Finit->u),S,&lineTemp);
		passTensorElemTo3dArray(nx,ny,nz,7,&(Finit->deriv),&S32);
		passTensorElemTo3dArray(nx,ny,nz,1,&(Finit->deriv),&S12);	
		sumFields(nx,ny,nz,S12,S32,&S);	
		//printPlaneHorDir("shearStressWJ0.xy",nx,0,nz,&S12);
		//printPlaneHorDir("shearStressWJny.xy",nx,ny+1,nz,&S12);
		printPlaneHorDir("shearStressJ0.xy",nx,0,nz,&S);
		printPlaneHorDir("shearStressJny.xy",nx,ny+1,nz,&S);
		dealloc_3d(nx,ny,(void****)&S12);
		dealloc_3d(nx,ny,(void****)&S32);
		dealloc_3d(nx,ny,(void****)&S);
/*
		for (jj = 0; jj < nx + 2; jj++)
			corrShear[jj] += lineTemp[jj];
*/
		*(Pinit++)=*(Finit++);
	}
/*
	for (jj = 0; jj < nx + 2; jj++)
		corrShear[jj] /= (double)(nx+2);
  printXLineDir("shearVelocityCorrelation.xy", nx, &cGrid, corrShear);
//	dealloc_3d(nx,ny,(void****)&S12);
//	dealloc_3d(nx,ny,(void****)&S32);
	dealloc_3d(nx,ny,(void****)&S);
	free(corrShear);
*/
//*******************************************************************************
//********** OPERATIONS/CALCULATIONS EXTRACTIONS OVER THE FIELD/GRID ************
//*******************************************************************************
// lets calculate the mean field
  if(cont == 'y')
  {
    meanField(nfields,nx,ny,nz,nscal,Field,&mean);
    calculateFieldDerivatives(nx,ny,nz,&(cGrid.jac),&mean);
  // I need the dU/dy
  // I need the gradients tensor
    calculateLambdaCriterion(nx,ny,nz,re,&mean);
  // lets calculate the perturbations
    Pinit = Pert;
    perturbations(nfields,nx,ny,nz,nscal,Pinit,&mean);

    Pinit = Pert;
		for(ii=0; ii<nfields; ++ii)
		{
			printPlaneHorDir("UPlaneY40BOT.xy",nx,22,nz,&(Pinit->u));
			printPlaneHorDir("VPlaneY40BOT.xy",nx,22,nz,&(Pinit->v));
			printPlaneHorDir("UPlaneY40TOP.xy",nx,ny-21,nz,&(Pinit->u));
			printPlaneHorDir("VPlaneY40TOP.xy",nx,ny-21,nz,&(Pinit->v));

			printPlaneHorDir("UPlaneY1BOT.xy",nx,1,nz,&(Pinit->u));
			printPlaneHorDir("VPlaneY1BOT.xy",nx,1,nz,&(Pinit->v));
			printPlaneHorDir("UPlaneYnyTOP.xy",nx,ny,nz,&(Pinit->u));
			printPlaneHorDir("VPlaneYnyTOP.xy",nx,ny,nz,&(Pinit->v));

			printPlaneHorDir("UPlaneY2BOT.xy",nx,2,nz,&(Pinit->u));
			printPlaneHorDir("VPlaneY2BOT.xy",nx,2,nz,&(Pinit->v));
			printPlaneHorDir("UPlaneYny1TOP.xy",nx,ny-1,nz,&(Pinit->u));
			printPlaneHorDir("VPlaneYny1TOP.xy",nx,ny-1,nz,&(Pinit->v));

//*******************************************************************************
			printPlaneHorDir("fiPlaneStatsJ1.xy",nx,1,nz,&(Pinit->fi));
			printPlaneHorDir("fiPlaneStatsJny.xy",nx,ny,nz,&(Pinit->fi));
//*******************************************************************************

			printPlaneHorDir("UPlaneY3BOT.xy",nx,3,nz,&(Pinit->u));
			printPlaneHorDir("VPlaneY3BOT.xy",nx,3,nz,&(Pinit->v));
			printPlaneHorDir("UPlaneYny2TOP.xy",nx,ny-2,nz,&(Pinit->u));
			printPlaneHorDir("VPlaneYny2TOP.xy",nx,ny-2,nz,&(Pinit->v));

			Pinit++;
		}
		// NOT YET WORKING, ALTHOUGH IT SHOULD BE
    //Pinit = Pert;
		//VISA(nx,ny,nz,nfields,Pinit,&(mean.VISA));
  // QUADRANT ANALYSIS
    Pinit = Pert;
		quadrantAnalysis(nfields,nx,ny,nz,Pinit,&mean);
  // anisotropy tensor: I need Reynolds stresses
    calculateSecondThirdInvariants(nx,ny,nz,&(mean.anisotropy),
        &(mean.secInv),&(mean.thirdInv));
  // lets do the energy budget (I need reynolds stresses first)
    Pinit = Pert;
    calculateEnergyBudget(nx,ny,nz,nfields,re,&cGrid,Pinit,&mean);
  // lets calculate the normalized shear-rate (need dissipation, R. Stress, and du/dy)
  //  calculateShearRate(nx,ny,nz,&mean);
  //  calculates shear profile <uv>-(1/re)d<u>/dy
  // let's calculate the shear profile
    lineShearMeanField(nx,ny,nz,Y,&mean,&meanShearLine);
    lineShearBalanceMeanField(nx,ny,nz,Y,&mean,&meanDuDy);
  // let's calculate the velocity profile
    lineMeanField(nx,ny,nz,Y,1.0,mean.u,&meanLine);
    lineMeanField(nx,ny,nz,Y,1.0,mean.re,&meanViscLine);
    lineMeanField(nx,ny,nz,Y,1.0,mean.rho[0],&rhoMeanLine);
		lineMeanField(nx,ny,nz,Y,1.0,mean.RhoU,&rhoCorrMeanLine[0]);
		lineMeanField(nx,ny,nz,Y,1.0,mean.RhoV,&rhoCorrMeanLine[1]);
		lineMeanField(nx,ny,nz,Y,1.0,mean.RhoW,&rhoCorrMeanLine[2]);

    lineMeanField(nx,ny,nz,Y,1.0,mean.skewnessU,&meanSkewnessLine[0]);
    lineMeanField(nx,ny,nz,Y,1.0,mean.kurtosisU,&meanKurtosisLine[0]);
    lineMeanField(nx,ny,nz,Y,1.0,mean.skewnessV,&meanSkewnessLine[1]);
    lineMeanField(nx,ny,nz,Y,1.0,mean.kurtosisV,&meanKurtosisLine[1]);
    lineMeanField(nx,ny,nz,Y,1.0,mean.skewnessW,&meanSkewnessLine[2]);
    lineMeanField(nx,ny,nz,Y,1.0,mean.kurtosisW,&meanKurtosisLine[2]);
  // let's calculate the budget profiles
    lineMeanField(nx,ny,nz,Y,1.0,mean.prod,&meanBudget[0]);
    lineMeanField(nx,ny,nz,Y,1.0,mean.press_diff,&meanBudget[1]);
    lineMeanField(nx,ny,nz,Y,1.0,mean.turb_transp,&meanBudget[2]);
    lineMeanField(nx,ny,nz,Y,1.0,mean.visc_diff,&meanBudget[3]);
    lineMeanField(nx,ny,nz,Y,1.0,mean.dissip,&meanBudget[4]);
    lineMeanField(nx,ny,nz,Y,1.0,mean.secInv,&meanInvs[0]);
    lineMeanField(nx,ny,nz,Y,1.0,mean.thirdInv,&meanInvs[1]);
  // lets do the Urms, Vrms, Wrms
    lineRMSMeanField(nx,ny,nz,Y,&mean,X,&meanRMS[0]);
    lineRMSMeanField(nx,ny,nz,Y,&mean,Y,&meanRMS[1]);
    lineRMSMeanField(nx,ny,nz,Y,&mean,Z,&meanRMS[2]);
	// calculate Skin friction coefficient
		Cf[0] = calculateIntegralShearStress(nx,nz,0,re,&mean); 
		Cf[1] = calculateIntegralShearStress(nx,nz,ny+1,re,&mean); 
  // calculate correlations
    Pinit = Pert;
		correlationTauVert(nx,ny,nz,2,nfields,Pinit,&corrLine);
		correlationXMean(nx,ny,nz, 5,nfields,Pinit,&corrXLine[0]);
		correlationXMean(nx,ny,nz,10,nfields,Pinit,&corrXLine[1]);
		correlationXMean(nx,ny,nz,20,nfields,Pinit,&corrXLine[2]);
		correlationXMean(nx,ny,nz,ny-4,nfields,Pinit,&corrXLine[3]);
		correlationXMean(nx,ny,nz,ny-9,nfields,Pinit,&corrXLine[4]);
		correlationXMean(nx,ny,nz,ny-19,nfields,Pinit,&corrXLine[5]);
/*
    correlationYX(nx,ny,nz,10,nfields,Pinit,&corrLine);
    correlationXX(nx,ny,nz,5,nfields,Pinit,&corrLine);
		correlationX(nx,ny,nz,2,Pert[0].u, Pert[0].u, &corrLine[0]);
		correlationX(nx,ny,nz,5,Pert[0].u, Pert[0].u, &corrLine[1]);
		correlationX(nx,ny,nz,10,Pert[0].u, Pert[0].u, &corrLine[2]);
		powerfft = malloc((nx+2)*sizeof(complex));
		scratch = malloc((nx+2)*sizeof(complex));
		for (ii = 0; ii < nx+2; ii++){
			powerfft[ii].Re = corrLine[2][ii];
			powerfft[ii].Im = 0.0;
		}
		fft(powerfft,nx+2,scratch);
*/
  //  optional:perturbationsMeanLineU(nfields,nx,ny,nz,nscal,Y,Pert,meanLine,&mean);
  //*******************************************************************************
  //*************************** PRINTING/OUTPUT ***********************************
  //*******************************************************************************
    Pinit = Pert;
    pressureStrain(nscal,nx,ny,nz,Pinit,&mean);
    vorticityCorrelation(nscal,nx,ny,nz,Pinit,&mean);
	  calculateEigenvalueStrain(nx,ny,nz,&mean);	
		calculateAnisotropicTensor(nx,ny,nz,&(mean.ww),&(mean.anisotrww));
    calculateSecondThirdInvariants(nx,ny,nz,&(mean.anisotrww),
        &(mean.wwsecInv),&(mean.wwthirdInv));
    lineMeanField(nx,ny,nz,Y,1.0,mean.wwsecInv,  &meanInvsww[0]);
    lineMeanField(nx,ny,nz,Y,1.0,mean.wwthirdInv,&meanInvsww[1]);

		PDFMatr(nx,0,nz,divs,&(mean.strain),&histo,&bins);
    varS12 = VarMatr(nx,0,nz,&(mean.strain));
		PDFList(nx,0,nz,&(mean.strain),&PDFlist);

		calculateAnisotropicTensor(nx,ny,nz,&(mean.pressureStrain),
				&(mean.anisotrpc));
    calculateSecondThirdInvariants(nx,ny,nz,&(mean.anisotrpc),
        &(mean.pcsecInv),&(mean.pcthirdInv));
    lineMeanField(nx,ny,nz,Y,1.0,mean.pcsecInv,  &meanInvspc[0]);
    lineMeanField(nx,ny,nz,Y,1.0,mean.pcthirdInv,&meanInvspc[1]);

    lineMeanField(nx,ny,nz,Y,1.0,mean.wwS,&wwSMean);
    lineMeanField(nx,ny,nz,Y,1.0,mean.SLambda1,&SLambdaMean[0]);
    lineMeanField(nx,ny,nz,Y,1.0,mean.SLambda2,&SLambdaMean[1]);
    lineMeanField(nx,ny,nz,Y,1.0,mean.SLambda3,&SLambdaMean[2]);
    lineMeanFieldMatr(nx,ny,nz,Y,1,1.0,&(mean.skew_strain),&meanVor[0]);
    lineMeanFieldMatr(nx,ny,nz,Y,2,1.0,&(mean.skew_strain),&meanVor[1]);
    lineMeanFieldMatr(nx,ny,nz,Y,5,1.0,&(mean.skew_strain),&meanVor[2]);

    lineMeanFieldMatr(nx,ny,nz,Y,0,1.0,&(mean.ww),&wwMean[0]);
    lineMeanFieldMatr(nx,ny,nz,Y,1,1.0,&(mean.ww),&wwMean[1]);
    lineMeanFieldMatr(nx,ny,nz,Y,2,1.0,&(mean.ww),&wwMean[2]);
    lineMeanFieldMatr(nx,ny,nz,Y,3,1.0,&(mean.ww),&wwMean[3]);
    lineMeanFieldMatr(nx,ny,nz,Y,4,1.0,&(mean.ww),&wwMean[4]);
    lineMeanFieldMatr(nx,ny,nz,Y,5,1.0,&(mean.ww),&wwMean[5]);
    lineMeanFieldMatr(nx,ny,nz,Y,6,1.0,&(mean.ww),&wwMean[6]);
    lineMeanFieldMatr(nx,ny,nz,Y,7,1.0,&(mean.ww),&wwMean[7]);
    lineMeanFieldMatr(nx,ny,nz,Y,8,1.0,&(mean.ww),&wwMean[8]);


    lineMeanFieldMatr(nx,ny,nz,Y,0,1.0,&(mean.pressureStrain),&pcMean[0]);
    lineMeanFieldMatr(nx,ny,nz,Y,1,1.0,&(mean.pressureStrain),&pcMean[1]);
    lineMeanFieldMatr(nx,ny,nz,Y,2,1.0,&(mean.pressureStrain),&pcMean[2]);
    lineMeanFieldMatr(nx,ny,nz,Y,3,1.0,&(mean.pressureStrain),&pcMean[3]);
    lineMeanFieldMatr(nx,ny,nz,Y,4,1.0,&(mean.pressureStrain),&pcMean[4]);
    lineMeanFieldMatr(nx,ny,nz,Y,5,1.0,&(mean.pressureStrain),&pcMean[5]);
    lineMeanFieldMatr(nx,ny,nz,Y,6,1.0,&(mean.pressureStrain),&pcMean[6]);
    lineMeanFieldMatr(nx,ny,nz,Y,7,1.0,&(mean.pressureStrain),&pcMean[7]);
    lineMeanFieldMatr(nx,ny,nz,Y,8,1.0,&(mean.pressureStrain),&pcMean[8]);

    lineMeanFieldMatr(nx,ny,nz,Y,0,1.0,&(mean.strain),&epMean[0]);
    lineMeanFieldMatr(nx,ny,nz,Y,1,1.0,&(mean.strain),&epMean[1]);
    lineMeanFieldMatr(nx,ny,nz,Y,2,1.0,&(mean.strain),&epMean[2]);
    lineMeanFieldMatr(nx,ny,nz,Y,3,1.0,&(mean.strain),&epMean[3]);
    lineMeanFieldMatr(nx,ny,nz,Y,4,1.0,&(mean.strain),&epMean[4]);
    lineMeanFieldMatr(nx,ny,nz,Y,5,1.0,&(mean.strain),&epMean[5]);
    lineMeanFieldMatr(nx,ny,nz,Y,6,1.0,&(mean.strain),&epMean[6]);
    lineMeanFieldMatr(nx,ny,nz,Y,7,1.0,&(mean.strain),&epMean[7]);
    lineMeanFieldMatr(nx,ny,nz,Y,8,1.0,&(mean.strain),&epMean[8]);

		calculateTaylorMicroScales(nx,ny,nz,&(mean.uv),&(mean.deriv),&taylorScales);

    for(ii=0;ii<nfields;++ii)
    {
			if(ii == 0)  // just print one perturbation profile
			{
				sprintf(digit,"%06d",finit+ii*fstep);
				asprintf(&fieldfilename,"%s%s%s",
								 "pert",digit,".vtk");
		//		printAllToVTK (fieldfilename,nx,ny,nz,nscal,cGrid,*(Pinit++));
				printPointerAllToVTK (fieldfilename,nx,ny,nz,nscal,&cGrid,Pinit++);
				printf("Written: %s\n",fieldfilename);
      	free((void *)fieldfilename);
			}
    }
  //	printAllToVTK ("meanField.vtk",nx,ny,nz,nscal,cGrid,mean);
    printPointerAllToVTK ("meanField.vtk",nx,ny,nz,nscal,&cGrid,&mean);
    printf("Written: %s\n","meanField.vtk");

    printf("Bottom lid Cf= %18.10f\n",Cf[0]);
    printf("Top 	 lid Cf= %18.10f\n",Cf[1]);
    printf("Shear Variance = %18.10f\n",varS12);

	
    printLineDir ("velocityLine.xy",ny,&cGrid,meanLine);	
    printLineDir ("scalarLine.xy",ny,&cGrid,rhoMeanLine);
		for (ii = 0; ii < 3;++ii)
			for (jj = 0; jj < ny+2; ++jj)
			{
				meanKurtosisLine[ii][jj] /= meanRMS[ii][jj]*meanRMS[ii][jj]*meanRMS[ii][jj]*meanRMS[ii][jj];
				meanSkewnessLine[ii][jj] /= meanRMS[ii][jj]*meanRMS[ii][jj]*meanRMS[ii][jj];
			}
    printLineDir ("kurtosisULine.xy",ny,&cGrid,meanKurtosisLine[0]);	
    printLineDir ("skewnessULine.xy",ny,&cGrid,meanSkewnessLine[0]);	
    printLineDir ("kurtosisVLine.xy",ny,&cGrid,meanKurtosisLine[1]);	
    printLineDir ("skewnessVLine.xy",ny,&cGrid,meanSkewnessLine[1]);	
    printLineDir ("kurtosisWLine.xy",ny,&cGrid,meanKurtosisLine[2]);	
    printLineDir ("skewnessWLine.xy",ny,&cGrid,meanSkewnessLine[2]);	
    printLineDir ("UVLine.xy",ny,&cGrid,meanShearLine);	
    printLineDir ("viscosityLine.xy",ny,&cGrid,meanViscLine);	
    printLineDir ("RhoULine.xy",ny,&cGrid,rhoCorrMeanLine[0]);	
    printLineDir ("RhoVLine.xy",ny,&cGrid,rhoCorrMeanLine[1]);	
    printLineDir ("RhoWLine.xy",ny,&cGrid,rhoCorrMeanLine[2]);	
    printLineDir ("StrainLambda1.xy",ny,&cGrid,SLambdaMean[0]);	
    printLineDir ("StrainLambda2.xy",ny,&cGrid,SLambdaMean[1]);	
    printLineDir ("StrainLambda3.xy",ny,&cGrid,SLambdaMean[2]);	
    printLineDir ("energyProdLine.xy",ny,&cGrid,meanBudget[0]);	
    printLineDir ("energyPressDiffLine.xy",ny,&cGrid,meanBudget[1]);	
    printLineDir ("energyTurbTranspLine.xy",ny,&cGrid,meanBudget[2]);	
    printLineDir ("energyViscDiffLine.xy",ny,&cGrid,meanBudget[3]);	
    printLineDir ("energyDissipLine.xy",ny,&cGrid,meanBudget[4]);	
    printLineDir ("quadrant_1.xy",ny,&cGrid,mean.quadrants[0]);
    printLineDir ("quadrant_2.xy",ny,&cGrid,mean.quadrants[1]);
    printLineDir ("quadrant_3.xy",ny,&cGrid,mean.quadrants[2]);
    printLineDir ("quadrant_4.xy",ny,&cGrid,mean.quadrants[3]);
    //printLineDir ("normalShearRate.xy",ny,&cGrid,mean.S);
    printLineDir ("correlationTauVert.xy",ny,&cGrid,corrLine);
    printLineDir ("taylorScalesSquared.xy",ny,&cGrid,taylorScales);
    printLineDir ("wwSMean.xy",ny,&cGrid,wwSMean);
    printLineDir ("DuDyLine.xy",ny,&cGrid,meanDuDy);

    printLineDir ("Vorticity0.xy",ny,&cGrid,meanVor[0]);
    printLineDir ("Vorticity1.xy",ny,&cGrid,meanVor[1]);
    printLineDir ("Vorticity2.xy",ny,&cGrid,meanVor[2]);

    printLineDir ("wwMean0.xy",ny,&cGrid,wwMean[0]);
    printLineDir ("wwMean1.xy",ny,&cGrid,wwMean[1]);
    printLineDir ("wwMean2.xy",ny,&cGrid,wwMean[2]);
    printLineDir ("wwMean3.xy",ny,&cGrid,wwMean[3]);
    printLineDir ("wwMean4.xy",ny,&cGrid,wwMean[4]);
    printLineDir ("wwMean5.xy",ny,&cGrid,wwMean[5]);
    printLineDir ("wwMean6.xy",ny,&cGrid,wwMean[6]);
    printLineDir ("wwMean7.xy",ny,&cGrid,wwMean[7]);
    printLineDir ("wwMean8.xy",ny,&cGrid,wwMean[8]);
    printLineDir ("pcMean0.xy",ny,&cGrid,pcMean[0]);
    printLineDir ("pcMean1.xy",ny,&cGrid,pcMean[1]);
    printLineDir ("pcMean2.xy",ny,&cGrid,pcMean[2]);
    printLineDir ("pcMean3.xy",ny,&cGrid,pcMean[3]);
    printLineDir ("pcMean4.xy",ny,&cGrid,pcMean[4]);
    printLineDir ("pcMean5.xy",ny,&cGrid,pcMean[5]);
    printLineDir ("pcMean6.xy",ny,&cGrid,pcMean[6]);
    printLineDir ("pcMean7.xy",ny,&cGrid,pcMean[7]);
    printLineDir ("pcMean8.xy",ny,&cGrid,pcMean[8]);
    printLineDir ("epMean0.xy",ny,&cGrid,epMean[0]);
    printLineDir ("epMean1.xy",ny,&cGrid,epMean[1]);
    printLineDir ("epMean2.xy",ny,&cGrid,epMean[2]);
    printLineDir ("epMean3.xy",ny,&cGrid,epMean[3]);
    printLineDir ("epMean4.xy",ny,&cGrid,epMean[4]);
    printLineDir ("epMean5.xy",ny,&cGrid,epMean[5]);
    printLineDir ("epMean6.xy",ny,&cGrid,epMean[6]);
    printLineDir ("epMean7.xy",ny,&cGrid,epMean[7]);
    printLineDir ("epMean8.xy",ny,&cGrid,epMean[8]);
//    printXLineDir ("corrXUU2.xy",ny,&cGrid,corrLine[0]);
//    printXLineDir ("corrXUU5.xy",ny,&cGrid,corrLine[1]);
    printXLineDir ("corrXUU05Top.xy",nx,&cGrid,corrXLine[0]);
    printXLineDir ("corrXUU10Top.xy",nx,&cGrid,corrXLine[1]);
    printXLineDir ("corrXUU20Top.xy",nx,&cGrid,corrXLine[2]);
    printXLineDir ("corrXUU05Bot.xy",nx,&cGrid,corrXLine[3]);
    printXLineDir ("corrXUU10Bot.xy",nx,&cGrid,corrXLine[4]);
    printXLineDir ("corrXUU20Bot.xy",nx,&cGrid,corrXLine[5]);
//		fftprint_vector("powerSpectCorr.xy", powerfft, nx+2);
    //printXLineDir ("corrXUU.xy",nx,&cGrid,corrLine);
    //printXLineDir ("fftXUU.xy",nx,&cGrid,powerSpecReal);
    print1dArrays("invariantsFile.xy",ny,meanInvs[0],meanInvs[1]);
    print1dArrays("invariantsFileww.xy",ny,meanInvsww[0],meanInvsww[1]);
    print1dArrays("invariantsFilepc.xy",ny,meanInvspc[0],meanInvspc[1]);
    print1dArrays("histogramsStrain.xy",divs-1,histo,bins);
    print1dArray("PDFList.xy",(nx+2)*(nz+2),PDFlist);
    printLineDir ("uRMS.xy",ny,&cGrid,meanRMS[0]);
    printLineDir ("vRMS.xy",ny,&cGrid,meanRMS[1]);
    printLineDir ("wRMS.xy",ny,&cGrid,meanRMS[2]);
  }
//*******************************************************************************
//********************* CLEANUP/DEALLOCATION OF MEMORY **************************
//*******************************************************************************
	free_memory_f(nx,ny,nscal,&mean);
	free_memory_budget(nx,ny,nscal,&mean);
	free_memory_gc(nx,ny,&cGrid);
	Pinit = Pert;
	Finit = Field;
	for(ii=0; ii<nfields; ++ii)
		free_memory_f(nx,ny,nscal,Pinit++);
  if(meanLine != NULL)
	  free((void *) meanLine);
  if(meanShearLine != NULL)
	  free((void *) meanShearLine);
  if(corrLine != NULL)
	  free((void *) corrLine);
/*
	for(ii=0; ii<3; ++ii)
  	if(corrLine[ii] != NULL)
	  	free((void *) corrLine[ii]);
*/
	free((void *) Field);
	free((void *) Pert);
	free((void *)powerfft);
	free((void *)scratch);
//*******************************************************************************
//*************************** END CLEANUP ***************************************
//*******************************************************************************
	return 0;
}
