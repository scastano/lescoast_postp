/* Copyright, 2015, Santiago Lopez Castano
 *=========================================================================
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.

 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "stru.h"
#include "jacobianUtil.h"
#include "hdf5.h"

char kProgramVersion[] =
"===========================================================================================\n"
"                            LESCOAST-to-VTK CONVERSION TOOL    \n"
"===========================================================================================\n"
"Copyright (C) 2015,  Santiago Lopez Castano\n"
"This program comes with ABSOLUTELY NO WARRANTY; for details open the LICENSE file included.\n"
"This is free software, and you are welcome to redistribute it\n"
"under certain conditions; for details open the LICENSE file included.\n\n"
"===========================================================================================\n"
"    LAST UPDATE: Mar/21/2016 -- Version 1.10  \n"
"===========================================================================================\n";

void calculateCentroids(struct grid g,
												struct cent *gc, 
												int nx, 
												int ny, 
												int nz)
{/* MNEMOTECHNICS
	double x[nx+1][ny+1][nz+1];
	double y[nx+1][ny+1][nz+1];
	double z[nx+1][ny+1][nz+1];
	
	double xc[nx+2][ny+2][nz+2];
	double yc[nx+2][ny+2][nz+2];
	double zc[nx+2][ny+2][nz+2];*/

	int i, j, k;
// allocation
	gc->xc = (double ***) malloc (((nx+2)) * sizeof(*gc->xc));
	for (i = 0; i < (nx+2); i++)
	{
		gc->xc[i] = (double **) malloc(((ny+2))*sizeof(**gc->xc));
		for (j = 0; j < (ny+2); j++)
			gc->xc[i][j] = (double *) malloc(((nz+2))*sizeof(***gc->xc));
	}
	gc->yc = (double ***) malloc (((nx+2)) * sizeof(*gc->yc));
	for (i = 0; i < (nx+2); i++)
	{
		gc->yc[i] = (double **) malloc(((ny+2))*sizeof(**gc->yc));
		for (j = 0; j < (ny+2); j++)
			gc->yc[i][j] = (double *) malloc(((nz+2))*sizeof(***gc->yc));
	}
	gc->zc = (double ***) malloc (((nx+2)) * sizeof(*gc->zc));
	for (i = 0; i < (nx+2); i++)
	{
		gc->zc[i] = (double **) malloc(((ny+2))*sizeof(**gc->zc));
		for (j = 0; j < (ny+2); j++)
			gc->zc[i][j] = (double *) malloc(((nz+2))*sizeof(***gc->zc));
	}
// end allocation
	for (k = 1; k < nz+1; k++)
		for (j = 1; j < ny+1; j++)
			for (i = 1; i < nx+1; i++)
			{
				gc->xc[i][j][k] = 0.125 * (g.x[i][j][k]+g.x[i][j][k-1]+g.x[i-1][j][k-1]
					+g.x[i-1][j][k]+g.x[i][j-1][k]+g.x[i][j-1][k-1]+g.x[i-1][j-1][k-1]
					+g.x[i-1][j-1][k]); 
				gc->yc[i][j][k] = 0.125 * (g.y[i][j][k]+g.y[i][j][k-1]+g.y[i-1][j][k-1]
					+g.y[i-1][j][k]+g.y[i][j-1][k]+g.y[i][j-1][k-1]+g.y[i-1][j-1][k-1]
					+g.y[i-1][j-1][k]); 
				gc->zc[i][j][k] = 0.125 * (g.z[i][j][k]+g.z[i][j][k-1]+g.z[i-1][j][k-1]
					+g.z[i-1][j][k]+g.z[i][j-1][k]+g.z[i][j-1][k-1]+g.z[i-1][j-1][k-1]
					+g.z[i-1][j-1][k]); 
			}
 // walls 1 and 2
	for (k = 1; k < nz+1; k++)
		for (j = 1; j < ny+1; j++)
		{
			i = 0;
			gc->xc[i][j][k] = 0.25 * (g.x[i][j][k]+g.x[i][j][k-1]+g.x[i][j-1][k-1]+g.x[i][j-1][k]);
			gc->yc[i][j][k] = 0.25 * (g.y[i][j][k]+g.y[i][j][k-1]+g.y[i][j-1][k-1]+g.y[i][j-1][k]);
			gc->zc[i][j][k] = 0.25 * (g.z[i][j][k]+g.z[i][j][k-1]+g.z[i][j-1][k-1]+g.z[i][j-1][k]);
			i = nx;
			gc->xc[i+1][j][k] = 0.25 * (g.x[i][j][k]+g.x[i][j][k-1]+g.x[i][j-1][k-1]+g.x[i][j-1][k]);
			gc->yc[i+1][j][k] = 0.25 * (g.y[i][j][k]+g.y[i][j][k-1]+g.y[i][j-1][k-1]+g.y[i][j-1][k]);
			gc->zc[i+1][j][k] = 0.25 * (g.z[i][j][k]+g.z[i][j][k-1]+g.z[i][j-1][k-1]+g.z[i][j-1][k]);
		}
	for (i = 1; i < nx+1; i++)
	{
		j = 0;
		k = 0;

		gc->xc[i][j][k] = 0.5 * (g.x[i][j][k]+g.x[i-1][j][k]);
		gc->yc[i][j][k] = 0.5 * (g.y[i][j][k]+g.y[i-1][j][k]);
		gc->zc[i][j][k] = 0.5 * (g.z[i][j][k]+g.z[i-1][j][k]);
		
		j = 0;
		k = nz;

		gc->xc[i][j][k+1] = 0.5 * (g.x[i][j][k]+g.x[i-1][j][k]);
		gc->yc[i][j][k+1] = 0.5 * (g.y[i][j][k]+g.y[i-1][j][k]);
		gc->zc[i][j][k+1] = 0.5 * (g.z[i][j][k]+g.z[i-1][j][k]);
		
		j = ny;
		k = nz;

		gc->xc[i][j+1][k+1] = 0.5 * (g.x[i][j][k]+g.x[i-1][j][k]);
		gc->yc[i][j+1][k+1] = 0.5 * (g.y[i][j][k]+g.y[i-1][j][k]);
		gc->zc[i][j+1][k+1] = 0.5 * (g.z[i][j][k]+g.z[i-1][j][k]);

		j = ny;
		k = 0;

		gc->xc[i][j+1][k] = 0.5 * (g.x[i][j][k]+g.x[i-1][j][k]);
		gc->yc[i][j+1][k] = 0.5 * (g.y[i][j][k]+g.y[i-1][j][k]);
		gc->zc[i][j+1][k] = 0.5 * (g.z[i][j][k]+g.z[i-1][j][k]);
	}
// wall 3 and 4
	for (k = 1; k < nz+1; k++)
		for (i = 1; i < nx+1; i++)
		{
			j = 0;
			gc->xc[i][j][k] = 0.25 * (g.x[i][j][k]+g.x[i][j][k-1]+g.x[i-1][j][k-1]+g.x[i-1][j][k]);
			gc->yc[i][j][k] = 0.25 * (g.y[i][j][k]+g.y[i][j][k-1]+g.y[i-1][j][k-1]+g.y[i-1][j][k]);
			gc->zc[i][j][k] = 0.25 * (g.z[i][j][k]+g.z[i][j][k-1]+g.z[i-1][j][k-1]+g.z[i-1][j][k]);

			j = ny;
			gc->xc[i][j+1][k] = 0.25 * (g.x[i][j][k]+g.x[i][j][k-1]+g.x[i-1][j][k-1]+g.x[i-1][j][k]);
			gc->yc[i][j+1][k] = 0.25 * (g.y[i][j][k]+g.y[i][j][k-1]+g.y[i-1][j][k-1]+g.y[i-1][j][k]);
			gc->zc[i][j+1][k] = 0.25 * (g.z[i][j][k]+g.z[i][j][k-1]+g.z[i-1][j][k-1]+g.z[i-1][j][k]);
		}

	for (j = 1; j < ny + 1; j++)
	{
		k = 0;
		i = 0;
		gc->xc[i][j][k] = 0.5 * (g.x[i][j][k]+g.x[i][j-1][k]);
		gc->yc[i][j][k] = 0.5 * (g.y[i][j][k]+g.y[i][j-1][k]);
		gc->zc[i][j][k] = 0.5 * (g.z[i][j][k]+g.z[i][j-1][k]);

		k = 0;
		i = nx;
		gc->xc[i+1][j][k] = 0.5 * (g.x[i][j][k]+g.x[i][j-1][k]);
		gc->yc[i+1][j][k] = 0.5 * (g.y[i][j][k]+g.y[i][j-1][k]);
		gc->zc[i+1][j][k] = 0.5 * (g.z[i][j][k]+g.z[i][j-1][k]);

		k = nz;
		i = nx;
		gc->xc[i+1][j][k+1] = 0.5 * (g.x[i][j][k]+g.x[i][j-1][k]);
		gc->yc[i+1][j][k+1] = 0.5 * (g.y[i][j][k]+g.y[i][j-1][k]);
		gc->zc[i+1][j][k+1] = 0.5 * (g.z[i][j][k]+g.z[i][j-1][k]);

		k = nz;
		i = 0;
		gc->xc[i][j][k+1] = 0.5 * (g.x[i][j][k]+g.x[i][j-1][k]);
		gc->yc[i][j][k+1] = 0.5 * (g.y[i][j][k]+g.y[i][j-1][k]);
		gc->zc[i][j][k+1] = 0.5 * (g.z[i][j][k]+g.z[i][j-1][k]);
	}
// wall 5 and 6 
	for (j = 1; j < ny+1; j++)
		for (i = 1; i < nx+1; i++)
		{
			k = 0;
			gc->xc[i][j][k] = 0.25 * (g.x[i][j][k]+g.x[i][j-1][k]+g.x[i-1][j-1][k]+g.x[i-1][j][k]);
			gc->yc[i][j][k] = 0.25 * (g.y[i][j][k]+g.y[i][j-1][k]+g.y[i-1][j-1][k]+g.y[i-1][j][k]);
			gc->zc[i][j][k] = 0.25 * (g.z[i][j][k]+g.z[i][j-1][k]+g.z[i-1][j-1][k]+g.z[i-1][j][k]);

			k = nz;
			gc->xc[i][j][k+1] = 0.25 * (g.x[i][j][k]+g.x[i][j-1][k]+g.x[i-1][j-1][k]+g.x[i-1][j][k]);
			gc->yc[i][j][k+1] = 0.25 * (g.y[i][j][k]+g.y[i][j-1][k]+g.y[i-1][j-1][k]+g.y[i-1][j][k]);
			gc->zc[i][j][k+1] = 0.25 * (g.z[i][j][k]+g.z[i][j-1][k]+g.z[i-1][j-1][k]+g.z[i-1][j][k]);
		}
	for (k = 1; k < nz+1;k++)
	{
		j = 0;
		i = 0;
		gc->xc[i][j][k] = 0.5 * (g.x[i][j][k-1]+g.x[i][j][k]);	
		gc->yc[i][j][k] = 0.5 * (g.y[i][j][k-1]+g.y[i][j][k]);	
		gc->zc[i][j][k] = 0.5 * (g.z[i][j][k-1]+g.z[i][j][k]);	

		j = 0;
		i = nx;
		gc->xc[i+1][j][k] = 0.5 * (g.x[i][j][k-1]+g.x[i][j][k]);	
		gc->yc[i+1][j][k] = 0.5 * (g.y[i][j][k-1]+g.y[i][j][k]);	
		gc->zc[i+1][j][k] = 0.5 * (g.z[i][j][k-1]+g.z[i][j][k]);	

		j = ny;
		i = nx;
		gc->xc[i+1][j+1][k] = 0.5 * (g.x[i][j][k-1]+g.x[i][j][k]);	
		gc->yc[i+1][j+1][k] = 0.5 * (g.y[i][j][k-1]+g.y[i][j][k]);	
		gc->zc[i+1][j+1][k] = 0.5 * (g.z[i][j][k-1]+g.z[i][j][k]);	

		j = ny;
		i = 0;
		gc->xc[i][j+1][k] = 0.5 * (g.x[i][j][k-1]+g.x[i][j][k]);	
		gc->yc[i][j+1][k] = 0.5 * (g.y[i][j][k-1]+g.y[i][j][k]);	
		gc->zc[i][j+1][k] = 0.5 * (g.z[i][j][k-1]+g.z[i][j][k]);	
	}

	i = 0;
	j = 0;
	k = 0;
	gc->xc[i][j][k] = g.x[i][j][k];
	gc->yc[i][j][k] = g.y[i][j][k];
	gc->zc[i][j][k] = g.z[i][j][k];

	i = 0;
	j = 0;
	k = nz;
	gc->xc[i][j][k+1] = g.x[i][j][k];
	gc->yc[i][j][k+1] = g.y[i][j][k];
	gc->zc[i][j][k+1] = g.z[i][j][k];

	i = 0;
	j = ny;
	k = nz;
	gc->xc[i][j+1][k+1] = g.x[i][j][k];
	gc->yc[i][j+1][k+1] = g.y[i][j][k];
	gc->zc[i][j+1][k+1] = g.z[i][j][k];

	i = 0;
	j = ny;
	k = 0;
	gc->xc[i][j+1][k] = g.x[i][j][k];
	gc->yc[i][j+1][k] = g.y[i][j][k];
	gc->zc[i][j+1][k] = g.z[i][j][k];
//..................................
	i = nx;
	j = 0;
	k = 0;
	gc->xc[i+1][j][k] = g.x[i][j][k];
	gc->yc[i+1][j][k] = g.y[i][j][k];
	gc->zc[i+1][j][k] = g.z[i][j][k];

	i = nx;
	j = 0;
	k = nz;
	gc->xc[i+1][j][k+1] = g.x[i][j][k];
	gc->yc[i+1][j][k+1] = g.y[i][j][k];
	gc->zc[i+1][j][k+1] = g.z[i][j][k];

	i = nx;
	j = ny;
	k = nz;
	gc->xc[i+1][j+1][k+1] = g.x[i][j][k];
	gc->yc[i+1][j+1][k+1] = g.y[i][j][k];
	gc->zc[i+1][j+1][k+1] = g.z[i][j][k];

	i = nx;
	j = ny;
	k = 0;
	gc->xc[i+1][j+1][k] = g.x[i][j][k];
	gc->yc[i+1][j+1][k] = g.y[i][j][k];
	gc->zc[i+1][j+1][k] = g.z[i][j][k];

	calculateInvJacobian_c(nx+1,ny+1,nz+1,gc);	

}

void readGrid(struct grid *xyz,
							int *nx,
							int *ny,
							int *nz,
							char *gridfilename)
{

	FILE *gridFile = NULL;	
//	char buffer[100];
	int i, j, k;
	double a, b, c;

	gridFile = fopen(gridfilename,"r");
	if ( gridFile == NULL )
	{
		printf ("ERROR: file %s couldn't be read\n", gridfilename);
		exit(EXIT_FAILURE);
	}
	
	// TO DISCARD first LINES OF THE FILE
	//fgets(buffer,100,gridFile);
	fscanf (gridFile, "%lf %lf %lf", &(xyz->alx),&(xyz->aly),&(xyz->alz));
	fscanf (gridFile, "%i %i %i", nx, ny, nz);
	
// start allocation of variables
	xyz->x = (double ***) malloc (((*nx+1)) * sizeof(*xyz->x));
	for (i = 0; i < (*nx+1); i++)
	{
		xyz->x[i] = (double **) malloc(((*ny+1))*sizeof(**xyz->x));
		for (j = 0; j < (*ny+1); j++)
			xyz->x[i][j] = (double *) malloc(((*nz+1))*sizeof(***xyz->x));
	}
	xyz->y = (double ***)malloc (((*nx+1)) * sizeof(*xyz->y));
	for (i = 0; i < (*nx+1); i++)
	{
		xyz->y[i] = (double **)malloc(((*ny+1))*sizeof(**xyz->y));
		for (j = 0; j < (*ny+1); j++)
			xyz->y[i][j] = (double *)malloc(((*nz+1))*sizeof(***xyz->y));
	}
	xyz->z = (double ***)malloc (((*nx+1)) * sizeof(*xyz->z));
	for (i = 0; i < (*nx+1); i++)
	{
		xyz->z[i] = (double **)malloc(((*ny+1))*sizeof(**xyz->z));
		for (j = 0; j < (*ny+1); j++)
			xyz->z[i][j] = (double *)malloc(((*nz+1))*sizeof(***xyz->z));
	}
// end allocation

	for (k = 0; k < (*nz)+1; k++)
		for (j = 0; j < (*ny)+1; j++)
			for (i = 0; i < (*nx)+1; i++)
			{
				fscanf(gridFile,"%le", &a);//x[i][j][k]);
				fscanf(gridFile,"%le", &b);//y[i][j][k]);
				fscanf(gridFile,"%le", &c);//z[i][j][k]);
				xyz->x[i][j][k]=a;
				xyz->y[i][j][k]=b;
				xyz->z[i][j][k]=c;
			}
	fclose(gridFile);
	calculateInvJacobian(*nx,*ny,*nz,xyz);
}

void readFields_oldLESCOAST(int nx, 
														int ny, 
														int nz, 
														char *filename, // input
														int *nscal,
														struct field *campo)
{
	FILE *fileToConvert = fopen(filename,"r");
	char buffer[100];
	int i, j, k, isc;
	double a,b,c;

	if ( fileToConvert == NULL )
	{
		printf ("ERROR: file %s couldn't be read\n", filename);
		exit(EXIT_FAILURE);
	}
// start allocation of variables
	campo->uv = malloc ((nx+2) * sizeof(*campo->uv));
	for (i = 0; i < nx+2; i++)
	{
		campo->uv[i] = malloc((ny+2)*sizeof(**campo->uv));
		for (j = 0; j < ny+2; j++)
			campo->uv[i][j] = malloc((nz+2)*sizeof(***campo->uv));
	}
	campo->u = malloc ((nx+2) * sizeof(*campo->u));
	for (i = 0; i < nx+2; i++)
	{
		campo->u[i] = malloc((ny+2)*sizeof(**campo->u));
		for (j = 0; j < ny + 2; j++)
			campo->u[i][j] = malloc((nz+2)*sizeof(***campo->u));
	}
	campo->v = malloc ((nx+2) * sizeof(*campo->v));
	for (i = 0; i < nx+2; i++)
	{
		campo->v[i] = malloc((ny+2)*sizeof(**campo->v));
		for (j = 0; j < ny + 2; j++)
			campo->v[i][j] = malloc((nz+2)*sizeof(***campo->v));
	}
	campo->w = malloc ((nx+2) * sizeof(*campo->w));
	for (i = 0; i < nx+2; i++)
	{
		campo->w[i] = malloc((ny+2)*sizeof(**campo->w));
		for (j = 0; j < ny + 2; j++)
			campo->w[i][j] = malloc((nz+2)*sizeof(***campo->w));
	}
	campo->fi = malloc ((nx+2) * sizeof(*campo->fi));
	for (i = 0; i < nx+2; i++)
	{
		campo->fi[i] = malloc((ny+2)*sizeof(**campo->fi));
		for (j = 0; j < ny + 2; j++)
			campo->fi[i][j] = malloc((nz+2)*sizeof(***campo->fi));
	}
/*
	campo->uc = malloc ((nx+1) * sizeof(*campo->uc));
	for (i = 0; i < nx+1; i++)
	{
		campo->uc[i] = malloc((ny)*sizeof(**campo->uc));
		for (j = 0; j < ny; j++)
			campo->uc[i][j] = malloc((nz)*sizeof(***campo->uc));
	}
	campo->vc = malloc ((nx) * sizeof(*campo->vc));
	for (i = 0; i < nx; i++)
	{
		campo->vc[i] = malloc((ny+1)*sizeof(**campo->vc));
		for (j = 0; j < ny+1; j++)
			campo->vc[i][j] = malloc((nz)*sizeof(***campo->vc));
	}
	campo->wc = malloc ((nx) * sizeof(*campo->wc));
	for (i = 0; i < nx; i++)
	{
		campo->wc[i] = malloc((ny)*sizeof(**campo->wc));
		for (j = 0; j < ny; j++)
			campo->wc[i][j] = malloc((nz+1)*sizeof(***campo->wc));
	}

	campo->div = malloc ((nx+2) * sizeof(*campo->div));
	for (i = 0; i < nx+2; i++)
	{
		campo->div[i] = malloc((ny+2)*sizeof(**campo->div));
		for (j = 0; j < ny + 2; j++)
			campo->div[i][j] = malloc((nz+2)*sizeof(***campo->div));
	}

*/
// allocate rho, just for the fun
	*nscal=1;
	campo->rho = malloc (*nscal * sizeof(*campo->rho));
	for (isc=0; isc < (*nscal); isc++)
	{
		campo->rho[isc] = malloc((nx+2)*sizeof(**campo->rho));
		for (i = 0; i < nx+2; i++)
		{
			campo->rho[isc][i] = malloc((ny+2)*sizeof(***campo->rho));
			for (j = 0; j < ny+2; j++)
				campo->rho[isc][i][j] = malloc((nz+2)*sizeof(****campo->rho));
		}		
	}
// end allocation
// TO DISCARD n LINES OF THE FILE
	fgets(buffer,100,fileToConvert);	
// let us read the field	
	for (k = 0; k < nz + 2; k++ )
		for (j = 0; j < ny + 2; j++)
			for (i = 0; i < nx + 2; i++)
			{
				fscanf(fileToConvert,"%lf %lf %lf",&a,&b,&c);	
				campo->u[i][j][k] 	= a;
				campo->v[i][j][k] 	= b;  
				campo->w[i][j][k] 	= c;  
				campo->fi[i][j][k] 	= 0.0;
				for (isc = 0; isc < (*nscal); isc++)
				{
					campo->rho[isc][i][j][k] = 0.0;
				}
			}	
/*	
	for (k = 0; k < nz; k++)
		for (j = 0; j < ny; j++)
			for (i = 0; i < nx + 1; i++)
				fscanf(fileToConvert,"%lf",&campo->uc[i][j][k]);
	for (k = 0; k < nz; k++)
		for (j = 0; j < ny + 1; j++)
			for (i = 0; i < nx; i++)
				fscanf(fileToConvert,"%lf",&campo->vc[i][j][k]);
	for (k = 0; k < nz + 1; k++)
		for (j = 0; j < ny; j++)
			for (i = 0; i < nx; i++)
				fscanf(fileToConvert,"%lf",&campo->wc[i][j][k]);
// initialize
	for (i = 0; i < nx+2; i++)
		for (j = 0; j < ny+2; j++)
			for (k = 0; k < nz+2;k++)
				campo->div[i][j][k] = 0.0f;
 
// MASS CONSERVATION (TOTAL OF FLUXES SHOULD AMOUNT TO...........)
	for (i = 1; i < nx+1; i++)
		for (j = 1; j < ny; j++)
			for (k = 1; k < nz;k++)
				campo->div[i][j][k] = campo->uc[i][j][k] - campo->uc[i-1][j][k];

	for (i = 1; i < nx; i++)
		for (j = 1; j < ny+1; j++)
			for (k = 1; k < nz;k++)
				campo->div[i][j][k] = campo->div[i][j][k] +
														 campo->vc[i][j][k] - campo->vc[i][j-1][k];
	for (i = 1; i < nx; i++)
		for (j = 1; j < ny; j++)
			for (k = 1; k < nz+1;k++)
				campo->div[i][j][k] = campo->div[i][j][k] +
														 campo->wc[i][j][k] - campo->wc[i][j][k-1];
*/
	fclose(fileToConvert);
}

void readFieldsOld (int nx, 
								    int ny, 
								    int nz,
								    double u_star, double re, 
								    char *filename, // input
								    int *nscal,
								    struct field *campo)
{
	FILE *fileToConvert = fopen(filename,"r");
	char buffer[100];
	int i, j, k, isc;
	double a,b,c,d,e,ff;

	if ( fileToConvert == NULL )
	{
		printf ("ERROR: file %s couldn't be read\n", filename);
		exit(EXIT_FAILURE);
	}
// start allocation of variables
	campo->u = malloc ((nx+2) * sizeof(*campo->u));
	for (i = 0; i < nx+2; i++)
	{
		campo->u[i] = malloc((ny+2)*sizeof(**campo->u));
		for (j = 0; j < ny + 2; j++)
			campo->u[i][j] = malloc((nz+2)*sizeof(***campo->u));
	}
	campo->v = malloc ((nx+2) * sizeof(*campo->v));
	for (i = 0; i < nx+2; i++)
	{
		campo->v[i] = malloc((ny+2)*sizeof(**campo->v));
		for (j = 0; j < ny + 2; j++)
			campo->v[i][j] = malloc((nz+2)*sizeof(***campo->v));
	}
	campo->w = malloc ((nx+2) * sizeof(*campo->w));
	for (i = 0; i < nx+2; i++)
	{
		campo->w[i] = malloc((ny+2)*sizeof(**campo->w));
		for (j = 0; j < ny + 2; j++)
			campo->w[i][j] = malloc((nz+2)*sizeof(***campo->w));
	}
	campo->fi = malloc ((nx+2) * sizeof(*campo->fi));
	for (i = 0; i < nx+2; i++)
	{
		campo->fi[i] = malloc((ny+2)*sizeof(**campo->fi));
		for (j = 0; j < ny + 2; j++)
			campo->fi[i][j] = malloc((nz+2)*sizeof(***campo->fi));
	}
	campo->re = malloc ((nx+2) * sizeof(*campo->re));
	for (i = 0; i < nx+2; i++)
	{
		campo->re[i] = malloc((ny+2)*sizeof(**campo->re));
		for (j = 0; j < ny + 2; j++)
			campo->re[i][j] = malloc((nz+2)*sizeof(***campo->re));
	}
/*
	campo->uc = malloc ((nx+1) * sizeof(*campo->uc));
	for (i = 0; i < nx+1; i++)
	{
		campo->uc[i] = malloc((ny)*sizeof(**campo->uc));
		for (j = 0; j < ny; j++)
			campo->uc[i][j] = malloc((nz)*sizeof(***campo->uc));
	}
	campo->vc = malloc ((nx) * sizeof(*campo->vc));
	for (i = 0; i < nx; i++)
	{
		campo->vc[i] = malloc((ny+1)*sizeof(**campo->vc));
		for (j = 0; j < ny+1; j++)
			campo->vc[i][j] = malloc((nz)*sizeof(***campo->vc));
	}
	campo->wc = malloc ((nx) * sizeof(*campo->wc));
	for (i = 0; i < nx; i++)
	{
		campo->wc[i] = malloc((ny)*sizeof(**campo->wc));
		for (j = 0; j < ny; j++)
			campo->wc[i][j] = malloc((nz+1)*sizeof(***campo->wc));
	}

	campo->div = malloc ((nx+2) * sizeof(*campo->div));
	for (i = 0; i < nx+2; i++)
	{
		campo->div[i] = malloc((ny+2)*sizeof(**campo->div));
		for (j = 0; j < ny + 2; j++)
			campo->div[i][j] = malloc((nz+2)*sizeof(***campo->div));
	}

*/
	fscanf (fileToConvert, "%i", nscal);
// allocate rho
	campo->rho = malloc (*nscal * sizeof(*campo->rho));
	for (isc=0; isc < (*nscal); isc++)
	{
		campo->rho[isc] = malloc((nx+2)*sizeof(**campo->rho));
		for (i = 0; i < nx+2; i++)
		{
			campo->rho[isc][i] = malloc((ny+2)*sizeof(***campo->rho));
			for (j = 0; j < ny+2; j++)
				campo->rho[isc][i][j] = malloc((nz+2)*sizeof(****campo->rho));
		}		
	}
// end allocation
// TO DISCARD n LINES OF THE FILE
	fgets(buffer,100,fileToConvert);	
	fgets(buffer,100,fileToConvert);
	fgets(buffer,100,fileToConvert);
// let us read the field	
	for (k = 0; k < nz + 2; k++ )
		for (j = 0; j < ny + 2; j++)
			for (i = 0; i < nx + 2; i++)
			{
				fscanf(fileToConvert,"%lf %lf %lf %lf",&a,&b,&c,&d);	
				campo->u[i][j][k] 	= a/u_star;
				campo->v[i][j][k] 	= b/u_star;  
				campo->w[i][j][k] 	= c/u_star;  
				campo->fi[i][j][k] 	= d;
				campo->re[i][j][k]  = re;
				for (isc = 0; isc < (*nscal); isc++)
				{
					fscanf(fileToConvert,"%lf",&e); 
					campo->rho[isc][i][j][k] = e;
				}
			}	
/*
	for (k = 0; k < nz; k++)
		for (j = 0; j < ny; j++)
			for (i = 0; i < nx + 1; i++)
				fscanf(fileToConvert,"%lf",&campo->uc[i][j][k]);
	for (k = 0; k < nz; k++)
		for (j = 0; j < ny + 1; j++)
			for (i = 0; i < nx; i++)
				fscanf(fileToConvert,"%lf",&campo->vc[i][j][k]);
	for (k = 0; k < nz + 1; k++)
		for (j = 0; j < ny; j++)
			for (i = 0; i < nx; i++)
				fscanf(fileToConvert,"%lf",&campo->wc[i][j][k]);
// initialize
	for (i = 0; i < nx+2; i++)
		for (j = 0; j < ny+2; j++)
			for (k = 0; k < nz+2;k++)
				campo->div[i][j][k] = 0.0f;
 
// MASS CONSERVATION (TOTAL OF FLUXES SHOULD AMOUNT TO...........)
	for (i = 1; i < nx+1; i++)
		for (j = 1; j < ny; j++)
			for (k = 1; k < nz;k++)
				campo->div[i][j][k] = campo->uc[i][j][k] - campo->uc[i-1][j][k];

	for (i = 1; i < nx; i++)
		for (j = 1; j < ny+1; j++)
			for (k = 1; k < nz;k++)
				campo->div[i][j][k] = campo->div[i][j][k] +
														 campo->vc[i][j][k] - campo->vc[i][j-1][k];
	for (i = 1; i < nx; i++)
		for (j = 1; j < ny; j++)
			for (k = 1; k < nz+1;k++)
				campo->div[i][j][k] = campo->div[i][j][k] +
														 campo->wc[i][j][k] - campo->wc[i][j][k-1];
*/
	fclose(fileToConvert);

}
void readFields(int nx, 
								int ny, 
								int nz,
								double u_star, 
								char *filename, // input
								int *nscal,
								struct field *campo)
{
	FILE *fileToConvert = fopen(filename,"r");
	char buffer[100];
	int i, j, k, isc;
	double a,b,c,d,e,ff;

	if ( fileToConvert == NULL )
	{
		printf ("ERROR: file %s couldn't be read\n", filename);
		exit(EXIT_FAILURE);
	}
// start allocation of variables
	campo->u = malloc ((nx+2) * sizeof(*campo->u));
	for (i = 0; i < nx+2; i++)
	{
		campo->u[i] = malloc((ny+2)*sizeof(**campo->u));
		for (j = 0; j < ny + 2; j++)
			campo->u[i][j] = malloc((nz+2)*sizeof(***campo->u));
	}
	campo->v = malloc ((nx+2) * sizeof(*campo->v));
	for (i = 0; i < nx+2; i++)
	{
		campo->v[i] = malloc((ny+2)*sizeof(**campo->v));
		for (j = 0; j < ny + 2; j++)
			campo->v[i][j] = malloc((nz+2)*sizeof(***campo->v));
	}
	campo->w = malloc ((nx+2) * sizeof(*campo->w));
	for (i = 0; i < nx+2; i++)
	{
		campo->w[i] = malloc((ny+2)*sizeof(**campo->w));
		for (j = 0; j < ny + 2; j++)
			campo->w[i][j] = malloc((nz+2)*sizeof(***campo->w));
	}
	campo->fi = malloc ((nx+2) * sizeof(*campo->fi));
	for (i = 0; i < nx+2; i++)
	{
		campo->fi[i] = malloc((ny+2)*sizeof(**campo->fi));
		for (j = 0; j < ny + 2; j++)
			campo->fi[i][j] = malloc((nz+2)*sizeof(***campo->fi));
	}
	campo->re = malloc ((nx+2) * sizeof(*campo->re));
	for (i = 0; i < nx+2; i++)
	{
		campo->re[i] = malloc((ny+2)*sizeof(**campo->re));
		for (j = 0; j < ny + 2; j++)
			campo->re[i][j] = malloc((nz+2)*sizeof(***campo->re));
	}
/*
	campo->uc = malloc ((nx+1) * sizeof(*campo->uc));
	for (i = 0; i < nx+1; i++)
	{
		campo->uc[i] = malloc((ny)*sizeof(**campo->uc));
		for (j = 0; j < ny; j++)
			campo->uc[i][j] = malloc((nz)*sizeof(***campo->uc));
	}
	campo->vc = malloc ((nx) * sizeof(*campo->vc));
	for (i = 0; i < nx; i++)
	{
		campo->vc[i] = malloc((ny+1)*sizeof(**campo->vc));
		for (j = 0; j < ny+1; j++)
			campo->vc[i][j] = malloc((nz)*sizeof(***campo->vc));
	}
	campo->wc = malloc ((nx) * sizeof(*campo->wc));
	for (i = 0; i < nx; i++)
	{
		campo->wc[i] = malloc((ny)*sizeof(**campo->wc));
		for (j = 0; j < ny; j++)
			campo->wc[i][j] = malloc((nz+1)*sizeof(***campo->wc));
	}

	campo->div = malloc ((nx+2) * sizeof(*campo->div));
	for (i = 0; i < nx+2; i++)
	{
		campo->div[i] = malloc((ny+2)*sizeof(**campo->div));
		for (j = 0; j < ny + 2; j++)
			campo->div[i][j] = malloc((nz+2)*sizeof(***campo->div));
	}

*/
	fscanf (fileToConvert, "%i", nscal);
// allocate rho
	campo->rho = malloc (*nscal * sizeof(*campo->rho));
	for (isc=0; isc < (*nscal); isc++)
	{
		campo->rho[isc] = malloc((nx+2)*sizeof(**campo->rho));
		for (i = 0; i < nx+2; i++)
		{
			campo->rho[isc][i] = malloc((ny+2)*sizeof(***campo->rho));
			for (j = 0; j < ny+2; j++)
				campo->rho[isc][i][j] = malloc((nz+2)*sizeof(****campo->rho));
		}		
	}
// end allocation
// TO DISCARD n LINES OF THE FILE
	fgets(buffer,100,fileToConvert);	
	fgets(buffer,100,fileToConvert);
	fgets(buffer,100,fileToConvert);
// let us read the field	
	for (k = 0; k < nz + 2; k++ )
		for (j = 0; j < ny + 2; j++)
			for (i = 0; i < nx + 2; i++)
			{
				fscanf(fileToConvert,"%lf %lf %lf %lf %lf",&a,&b,&c,&d,&ff);	
				campo->u[i][j][k] 	= a/u_star;
				campo->v[i][j][k] 	= b/u_star;  
				campo->w[i][j][k] 	= c/u_star;  
				campo->fi[i][j][k] 	= d;
				campo->re[i][j][k] 	= ff;
				for (isc = 0; isc < (*nscal); isc++)
				{
					fscanf(fileToConvert,"%lf",&e); 
					campo->rho[isc][i][j][k] = e;
				}
			}	
/*
	for (k = 0; k < nz; k++)
		for (j = 0; j < ny; j++)
			for (i = 0; i < nx + 1; i++)
				fscanf(fileToConvert,"%lf",&campo->uc[i][j][k]);
	for (k = 0; k < nz; k++)
		for (j = 0; j < ny + 1; j++)
			for (i = 0; i < nx; i++)
				fscanf(fileToConvert,"%lf",&campo->vc[i][j][k]);
	for (k = 0; k < nz + 1; k++)
		for (j = 0; j < ny; j++)
			for (i = 0; i < nx; i++)
				fscanf(fileToConvert,"%lf",&campo->wc[i][j][k]);
// initialize
	for (i = 0; i < nx+2; i++)
		for (j = 0; j < ny+2; j++)
			for (k = 0; k < nz+2;k++)
				campo->div[i][j][k] = 0.0f;
 
// MASS CONSERVATION (TOTAL OF FLUXES SHOULD AMOUNT TO...........)
	for (i = 1; i < nx+1; i++)
		for (j = 1; j < ny; j++)
			for (k = 1; k < nz;k++)
				campo->div[i][j][k] = campo->uc[i][j][k] - campo->uc[i-1][j][k];

	for (i = 1; i < nx; i++)
		for (j = 1; j < ny+1; j++)
			for (k = 1; k < nz;k++)
				campo->div[i][j][k] = campo->div[i][j][k] +
														 campo->vc[i][j][k] - campo->vc[i][j-1][k];
	for (i = 1; i < nx; i++)
		for (j = 1; j < ny; j++)
			for (k = 1; k < nz+1;k++)
				campo->div[i][j][k] = campo->div[i][j][k] +
														 campo->wc[i][j][k] - campo->wc[i][j][k-1];
*/
	fclose(fileToConvert);

}

void printAllToVTK (	char *output, 
											int nx, 
											int ny, 
											int nz, 
											int nscal,
											struct cent gc,
											struct field campo	) 
{
	FILE *outFile = fopen(output,"w");
	int i, j, k, isc;

	if ( outFile == NULL )
	{
		printf ("ERROR: file %s couldn't be read", output);
		exit(EXIT_FAILURE);
	}
	
	fprintf(outFile, "# vtk DataFile Version 2.0\n");
	fprintf(outFile, "LESCOAST OUTPUT FIELD\n");
	fprintf(outFile, "ASCII\n");
	fprintf(outFile, "DATASET STRUCTURED_GRID\n");
	fprintf(outFile, "DIMENSIONS %i %i %i\n", nx+2,ny+2,nz+2);
	fprintf(outFile, "POINTS %i double\n", (nx+2)*(ny+2)*(nz+2));

	for (k = 0; k < nz+2; k++)
		for (j = 0; j < ny+2; j++)
			for (i = 0; i < nx+2; i++) 
				fprintf(outFile, "%18.10le %18.10le %18.10le\n", gc.xc[i][j][k],
																		 gc.yc[i][j][k],	
																		 gc.zc[i][j][k]);
	

	fprintf(outFile, "\nPOINT_DATA %i\n",(nx+2)*(ny+2)*(nz+2));
	fprintf(outFile, "SCALARS fi double 1\n");
	fprintf(outFile, "LOOKUP_TABLE default\n");
	for (k = 0; k < nz+2; k++)
		for (j = 0; j < ny+2; j++)
			for (i = 0; i < nx+2; i++)
				fprintf(outFile, "%18.10le\n", campo.fi[i][j][k]);

//	fprintf(outFile, "\nPOINT_DATA %i\n",(nx+2)*(ny+2)*(nz+2));
/*
	fprintf(outFile, "SCALARS div double 1\n");
	fprintf(outFile, "LOOKUP_TABLE default\n");
	for (k = 0; k < nz+2; k++)
		for (j = 0; j < ny+2; j++)
			for (i = 0; i < nx+2; i++)
				fprintf(outFile, "%18.10le\n", campo.div[i][j][k]);
*/
	for (isc = 0; isc < nscal; isc++)
	{
//		fprintf(outFile, "\nPOINT_DATA %i\n",(nx+2)*(ny+2)*(nz+2));
		fprintf(outFile, "SCALARS scalar_%i double 1\n",isc+1);
		fprintf(outFile, "LOOKUP_TABLE default\n");
		for (k = 0; k < nz+2; k++)
			for (j = 0; j < ny+2; j++)
				for (i = 0; i < nx+2; i++)
					fprintf(outFile, "%18.10le\n", campo.rho[isc][i][j][k]);
	}
	fprintf(outFile,"VECTORS velocity double\n");	
	for (k = 0; k < nz+2; k++)
		for (j = 0; j < ny+2; j++)
			for (i = 0; i < nx+2; i++)
				fprintf(outFile, "%18.10le %18.10le %18.10le\n", campo.u[i][j][k],
																		 campo.v[i][j][k],	
																		 campo.w[i][j][k]);
	if(campo.RhoU != NULL)
	{
		fprintf(outFile, "SCALARS CU double 1\n");
		fprintf(outFile, "LOOKUP_TABLE default\n");
		for (k = 0; k < nz+2; k++)
			for (j = 0; j < ny+2; j++)
				for (i = 0; i < nx+2; i++)
					fprintf(outFile, "%18.10le\n", campo.RhoU[i][j][k]);
	}
	if(campo.RhoV != NULL)
	{
		fprintf(outFile, "SCALARS CV double 1\n");
		fprintf(outFile, "LOOKUP_TABLE default\n");
		for (k = 0; k < nz+2; k++)
			for (j = 0; j < ny+2; j++)
				for (i = 0; i < nx+2; i++)
					fprintf(outFile, "%18.10le\n", campo.RhoV[i][j][k]);
	}
	if(campo.RhoW != NULL)
	{
		fprintf(outFile, "SCALARS CW double 1\n");
		fprintf(outFile, "LOOKUP_TABLE default\n");
		for (k = 0; k < nz+2; k++)
			for (j = 0; j < ny+2; j++)
				for (i = 0; i < nx+2; i++)
					fprintf(outFile, "%18.10le\n", campo.RhoW[i][j][k]);
	}
	if(campo.lambda2 != NULL)
	{
		fprintf(outFile, "SCALARS lambda2 double 1\n");
		fprintf(outFile, "LOOKUP_TABLE default\n");
		for (k = 0; k < nz+2; k++)
			for (j = 0; j < ny+2; j++)
				for (i = 0; i < nx+2; i++)
					fprintf(outFile, "%18.10le\n", campo.lambda2[i][j][k]);
	}
	if(campo.q != NULL)
	{
		fprintf(outFile, "SCALARS Q double 1\n");
		fprintf(outFile, "LOOKUP_TABLE default\n");
		for (k = 0; k < nz+2; k++)
			for (j = 0; j < ny+2; j++)
				for (i = 0; i < nx+2; i++)
					fprintf(outFile, "%18.10le\n", campo.q[i][j][k]);
	}
	// This code was made by Santiago Lopez Castano, UNITs, 2016;
	if(campo.uv != NULL)
	{ 
		fprintf(outFile,"TENSORS reynolds_stress double\n");	
		for (k = 0; k < nz+2; k++)
			for (j = 0; j < ny+2; j++)
				for (i = 0; i < nx+2; i++)
					fprintf(outFile, "%18.10le %18.10le %18.10le\n"
													 "%18.10le %18.10le %18.10le\n" 
													 "%18.10le %18.10le %18.10le\n\n", 
													 campo.uv[i][j][k].a11,campo.uv[i][j][k].a12,campo.uv[i][j][k].a13,
													 campo.uv[i][j][k].a21,campo.uv[i][j][k].a22,campo.uv[i][j][k].a23,
													 campo.uv[i][j][k].a31,campo.uv[i][j][k].a32,campo.uv[i][j][k].a33);
	}
	if(campo.visc_diss != NULL)
	{
		fprintf(outFile,"TENSORS visc_diss double\n");	
		for (k = 0; k < nz+2; k++)
			for (j = 0; j < ny+2; j++)
				for (i = 0; i < nx+2; i++)
					fprintf(outFile, "%18.10le %18.10le %18.10le\n"
													 "%18.10le %18.10le %18.10le\n" 
													 "%18.10le %18.10le %18.10le\n\n", 
													 campo.visc_diss[i][j][k].a11,campo.visc_diss[i][j][k].a12,campo.visc_diss[i][j][k].a13,
													 campo.visc_diss[i][j][k].a21,campo.visc_diss[i][j][k].a22,campo.visc_diss[i][j][k].a23,
													 campo.visc_diss[i][j][k].a31,campo.visc_diss[i][j][k].a32,campo.visc_diss[i][j][k].a33);
	}
	if(campo.anisotropy != NULL )
	{
		fprintf(outFile,"TENSORS anisotropy double\n");	
		for (k = 0; k < nz+2; k++)
			for (j = 0; j < ny+2; j++)
				for (i = 0; i < nx+2; i++)
					fprintf(outFile, "%18.10le %18.10le %18.10le\n"
													 "%18.10le %18.10le %18.10le\n" 
													 "%18.10le %18.10le %18.10le\n\n", 
													 campo.anisotropy[i][j][k].a11,campo.anisotropy[i][j][k].a12,campo.anisotropy[i][j][k].a13,
													 campo.anisotropy[i][j][k].a21,campo.anisotropy[i][j][k].a22,campo.anisotropy[i][j][k].a23,
													 campo.anisotropy[i][j][k].a31,campo.anisotropy[i][j][k].a32,campo.anisotropy[i][j][k].a33);
	}
/*
	fprintf(outFile, "SCALARS dissip double 1\n");
	fprintf(outFile, "LOOKUP_TABLE default\n");
	for (k = 0; k < nz+2; k++)
		for (j = 0; j < ny+2; j++)
			for (i = 0; i < nx+2; i++)
				fprintf(outFile, "%18.10le\n", campo.dissip[i][j][k]);
	fprintf(outFile,"TENSORS strain double\n");	
	for (k = 0; k < nz+2; k++)
		for (j = 0; j < ny+2; j++)
			for (i = 0; i < nx+2; i++)
				fprintf(outFile, "%18.10le %18.10le %18.10le\n"
												 "%18.10le %18.10le %18.10le\n" 
												 "%18.10le %18.10le %18.10le\n\n", 
												 campo.strain[i][j][k].a11,campo.strain[i][j][k].a12,campo.strain[i][j][k].a13,
												 campo.strain[i][j][k].a21,campo.strain[i][j][k].a22,campo.strain[i][j][k].a23,
												 campo.strain[i][j][k].a31,campo.strain[i][j][k].a32,campo.strain[i][j][k].a33);
	fprintf(outFile,"TENSORS skew_strain double\n");	
	for (k = 0; k < nz+2; k++)
		for (j = 0; j < ny+2; j++)
			for (i = 0; i < nx+2; i++)
				fprintf(outFile, "%18.10le %18.10le %18.10le\n"
												 "%18.10le %18.10le %18.10le\n" 
												 "%18.10le %18.10le %18.10le\n\n", 
												 campo.skew_strain[i][j][k].a11,campo.skew_strain[i][j][k].a12,campo.skew_strain[i][j][k].a13,
												 campo.skew_strain[i][j][k].a21,campo.skew_strain[i][j][k].a22,campo.skew_strain[i][j][k].a23,
												 campo.skew_strain[i][j][k].a31,campo.skew_strain[i][j][k].a32,campo.skew_strain[i][j][k].a33);
*/
	fclose(outFile);
}
// the following function is temporary, a more general one should be implemented
void printPlaneHorDir		(	char *output, 
												int nx,
												int  j,
												int nz,
												double **** const data)
{
	FILE *outFile = fopen(output,"a");
	int i,k;

	if ( outFile == NULL )
	{
		printf ("ERROR: file %s couldn't be opened\n", output);
		exit(EXIT_FAILURE);
	}
	
	for(i=0;i<nx+2;++i)
		for(k=0;k<nz+2;++k)
			fprintf(outFile, "%16.10lf\n",(*data)[i][j][k]);

	fclose(outFile);
}
void printLineDir 	(	char *output, 
											int ny, 
											const struct cent* gc,
											const double* line) 
{
	FILE *outFile = fopen(output,"w");
	int j;

	if ( outFile == NULL )
	{
		printf ("ERROR: file %s couldn't be opened\n", output);
		exit(EXIT_FAILURE);
	}
	
	for(j=0;j<ny+2;++j)
		fprintf(outFile, "%16.10lf\t%16.10lf\n",gc->yc[0][j][0], *(line++));

	fclose(outFile);
}
void printXLineDir 	(	char *output, 
											int ny, 
											const struct cent* gc,
											const double* line) 
{
	FILE *outFile = fopen(output,"w");
	int j;

	if ( outFile == NULL )
	{
		printf ("ERROR: file %s couldn't be opened\n", output);
		exit(EXIT_FAILURE);
	}
	
	for(j=0;j<ny+2;++j)
		fprintf(outFile, "%16.10lf\t%16.10lf\n",gc->xc[j][0][0], *(line++));

	fclose(outFile);
}
void print1dArrays 	(	char *output, 
											int ny, 
											const double* line1,
											const double* line2) 
{
	FILE *outFile = fopen(output,"w");
	int j;

	if ( outFile == NULL )
	{
		printf ("ERROR: file %s couldn't be opened\n", output);
		exit(EXIT_FAILURE);
	}
	
	for(j=0;j<ny+2;++j)
		fprintf(outFile, "%16.10lf\t%16.10lf\n",*(line1++), *(line2++));

	fclose(outFile);
}
void print1dArray 	(	char *output, 
											int ny, 
											const double* line2) 
{
	FILE *outFile = fopen(output,"w");
	int j;

	if ( outFile == NULL )
	{
		printf ("ERROR: file %s couldn't be opened\n", output);
		exit(EXIT_FAILURE);
	}
	
	for(j=0;j<ny;++j)
		fprintf(outFile, "%16.10lf\n",*(line2++));

	fclose(outFile);
}
void printPointerAllToVTK (	char *output, 
														int nx, 
														int ny, 
														int nz, 
														int nscal,
														struct cent  *const gc,
														struct field *const campo	) 
{
	FILE *outFile = fopen(output,"w");
	int i, j, k, isc;

	if ( outFile == NULL )
	{
		printf ("ERROR: file %s couldn't be read", output);
		exit(EXIT_FAILURE);
	}
	
	fprintf(outFile, "# vtk DataFile Version 2.0\n");
	fprintf(outFile, "LESCOAST OUTPUT FIELD\n");
	fprintf(outFile, "ASCII\n");
	fprintf(outFile, "DATASET STRUCTURED_GRID\n");
	fprintf(outFile, "DIMENSIONS %i %i %i\n", nx+2,ny+2,nz+2);
	fprintf(outFile, "POINTS %i double\n", (nx+2)*(ny+2)*(nz+2));

	for (k = 0; k < nz+2; k++)
		for (j = 0; j < ny+2; j++)
			for (i = 0; i < nx+2; i++) 
				fprintf(outFile, "%18.10le %18.10le %18.10le\n", gc->xc[i][j][k],
																		 gc->yc[i][j][k],	
																		 gc->zc[i][j][k]);
	

	fprintf(outFile, "\nPOINT_DATA %i\n",(nx+2)*(ny+2)*(nz+2));
	fprintf(outFile, "SCALARS fi double 1\n");
	fprintf(outFile, "LOOKUP_TABLE default\n");
	for (k = 0; k < nz+2; k++)
		for (j = 0; j < ny+2; j++)
			for (i = 0; i < nx+2; i++)
				fprintf(outFile, "%18.10le\n", campo->fi[i][j][k]);

//	fprintf(outFile, "\nPOINT_DATA %i\n",(nx+2)*(ny+2)*(nz+2));
/*
	fprintf(outFile, "SCALARS div double 1\n");
	fprintf(outFile, "LOOKUP_TABLE default\n");
	for (k = 0; k < nz+2; k++)
		for (j = 0; j < ny+2; j++)
			for (i = 0; i < nx+2; i++)
				fprintf(outFile, "%18.10le\n", campo->div[i][j][k]);
*/
	for (isc = 0; isc < nscal; isc++)
	{
//		fprintf(outFile, "\nPOINT_DATA %i\n",(nx+2)*(ny+2)*(nz+2));
		fprintf(outFile, "SCALARS scalar_%i double 1\n",isc+1);
		fprintf(outFile, "LOOKUP_TABLE default\n");
		for (k = 0; k < nz+2; k++)
			for (j = 0; j < ny+2; j++)
				for (i = 0; i < nx+2; i++)
					fprintf(outFile, "%18.10le\n", campo->rho[isc][i][j][k]);
	}
	fprintf(outFile,"VECTORS velocity double\n");	
	for (k = 0; k < nz+2; k++)
		for (j = 0; j < ny+2; j++)
			for (i = 0; i < nx+2; i++)
				fprintf(outFile, "%18.10le %18.10le %18.10le\n", 
																			campo->u[i][j][k],
																		 	campo->v[i][j][k],	
																		 	campo->w[i][j][k]);
	if(campo->RhoU != NULL)
	{
		fprintf(outFile, "SCALARS CU double 1\n");
		fprintf(outFile, "LOOKUP_TABLE default\n");
		for (k = 0; k < nz+2; k++)
			for (j = 0; j < ny+2; j++)
				for (i = 0; i < nx+2; i++)
					fprintf(outFile, "%18.10le\n", campo->RhoU[i][j][k]);
	}
	if(campo->VISA != NULL)
	{
		fprintf(outFile, "SCALARS VISA double 1\n");
		fprintf(outFile, "LOOKUP_TABLE default\n");
		for (k = 0; k < nz+2; k++)
			for (j = 0; j < ny+2; j++)
				for (i = 0; i < nx+2; i++)
					fprintf(outFile, "%18.10le\n", campo->VISA[i][j][k]);
	}
	if(campo->RhoV != NULL)
	{
		fprintf(outFile, "SCALARS CV double 1\n");
		fprintf(outFile, "LOOKUP_TABLE default\n");
		for (k = 0; k < nz+2; k++)
			for (j = 0; j < ny+2; j++)
				for (i = 0; i < nx+2; i++)
					fprintf(outFile, "%18.10le\n", campo->RhoV[i][j][k]);
	}
	if(campo->RhoW != NULL)
	{
		fprintf(outFile, "SCALARS CW double 1\n");
		fprintf(outFile, "LOOKUP_TABLE default\n");
		for (k = 0; k < nz+2; k++)
			for (j = 0; j < ny+2; j++)
				for (i = 0; i < nx+2; i++)
					fprintf(outFile, "%18.10le\n", campo->RhoW[i][j][k]);
	}
	if(campo->lambda2 != NULL)
	{
		fprintf(outFile, "SCALARS lambda2 double 1\n");
		fprintf(outFile, "LOOKUP_TABLE default\n");
		for (k = 0; k < nz+2; k++)
			for (j = 0; j < ny+2; j++)
				for (i = 0; i < nx+2; i++)
					fprintf(outFile, "%18.10le\n", campo->lambda2[i][j][k]);
	}
	if(campo->q != NULL)
	{
		fprintf(outFile, "SCALARS Q double 1\n");
		fprintf(outFile, "LOOKUP_TABLE default\n");
		for (k = 0; k < nz+2; k++)
			for (j = 0; j < ny+2; j++)
				for (i = 0; i < nx+2; i++)
					fprintf(outFile, "%18.10le\n", campo->q[i][j][k]);
	}
	if(campo->re != NULL)
	{
		fprintf(outFile, "SCALARS re double 1\n");
		fprintf(outFile, "LOOKUP_TABLE default\n");
		for (k = 0; k < nz+2; k++)
			for (j = 0; j < ny+2; j++)
				for (i = 0; i < nx+2; i++)
					fprintf(outFile, "%18.10le\n", campo->re[i][j][k]);
	}
	// This code was made by Santiago Lopez Castano, UNITs, 2016;
	if(campo->uv != NULL)
	{ 
		fprintf(outFile,"TENSORS reynolds_stress double\n");	
		for (k = 0; k < nz+2; k++)
			for (j = 0; j < ny+2; j++)
				for (i = 0; i < nx+2; i++)
					fprintf(outFile, "%18.10le %18.10le %18.10le\n"
													 "%18.10le %18.10le %18.10le\n" 
													 "%18.10le %18.10le %18.10le\n\n", 
													 campo->uv[i][j][k].a11,campo->uv[i][j][k].a12,campo->uv[i][j][k].a13,
													 campo->uv[i][j][k].a21,campo->uv[i][j][k].a22,campo->uv[i][j][k].a23,
													 campo->uv[i][j][k].a31,campo->uv[i][j][k].a32,campo->uv[i][j][k].a33);
	}
	if(campo->visc_diss != NULL)
	{
		fprintf(outFile,"TENSORS visc_diss double\n");	
		for (k = 0; k < nz+2; k++)
			for (j = 0; j < ny+2; j++)
				for (i = 0; i < nx+2; i++)
					fprintf(outFile, "%18.10le %18.10le %18.10le\n"
													 "%18.10le %18.10le %18.10le\n" 
													 "%18.10le %18.10le %18.10le\n\n", 
													 campo->visc_diss[i][j][k].a11,campo->visc_diss[i][j][k].a12,campo->visc_diss[i][j][k].a13,
													 campo->visc_diss[i][j][k].a21,campo->visc_diss[i][j][k].a22,campo->visc_diss[i][j][k].a23,
													 campo->visc_diss[i][j][k].a31,campo->visc_diss[i][j][k].a32,campo->visc_diss[i][j][k].a33);
	}
	if(campo->anisotropy != NULL )
	{
		fprintf(outFile,"TENSORS anisotropy double\n");	
		for (k = 0; k < nz+2; k++)
			for (j = 0; j < ny+2; j++)
				for (i = 0; i < nx+2; i++)
					fprintf(outFile, "%18.10le %18.10le %18.10le\n"
													 "%18.10le %18.10le %18.10le\n" 
													 "%18.10le %18.10le %18.10le\n\n", 
													 campo->anisotropy[i][j][k].a11,campo->anisotropy[i][j][k].a12,campo->anisotropy[i][j][k].a13,
													 campo->anisotropy[i][j][k].a21,campo->anisotropy[i][j][k].a22,campo->anisotropy[i][j][k].a23,
													 campo->anisotropy[i][j][k].a31,campo->anisotropy[i][j][k].a32,campo->anisotropy[i][j][k].a33);
	}
	if(campo->ww != NULL )
	{
		fprintf(outFile,"TENSORS ww double\n");	
		for (k = 0; k < nz+2; k++)
			for (j = 0; j < ny+2; j++)
				for (i = 0; i < nx+2; i++)
					fprintf(outFile, "%18.10le %18.10le %18.10le\n"
													 "%18.10le %18.10le %18.10le\n" 
													 "%18.10le %18.10le %18.10le\n\n", 
													 campo->ww[i][j][k].a11,campo->ww[i][j][k].a12,campo->ww[i][j][k].a13,
													 campo->ww[i][j][k].a21,campo->ww[i][j][k].a22,campo->ww[i][j][k].a23,
													 campo->ww[i][j][k].a31,campo->ww[i][j][k].a32,campo->ww[i][j][k].a33);
	}
	if(campo->wwS != NULL )
	{
		fprintf(outFile,"SCALARS wwS double\n");	
		for (k = 0; k < nz+2; k++)
			for (j = 0; j < ny+2; j++)
				for (i = 0; i < nx+2; i++)
					fprintf(outFile, "%18.10le\n", campo->wwS[i][j][k]);
	}
	if(campo->pressureStrain != NULL )
	{
		fprintf(outFile,"TENSORS pressureStrain double\n");	
		for (k = 0; k < nz+2; k++)
			for (j = 0; j < ny+2; j++)
				for (i = 0; i < nx+2; i++)
					fprintf(outFile, "%18.10le %18.10le %18.10le\n"
													 "%18.10le %18.10le %18.10le\n" 
													 "%18.10le %18.10le %18.10le\n\n", 
													 campo->pressureStrain[i][j][k].a11,campo->pressureStrain[i][j][k].a12,campo->pressureStrain[i][j][k].a13,
													 campo->pressureStrain[i][j][k].a21,campo->pressureStrain[i][j][k].a22,campo->pressureStrain[i][j][k].a23,
													 campo->pressureStrain[i][j][k].a31,campo->pressureStrain[i][j][k].a32,campo->pressureStrain[i][j][k].a33);
	}
/*
	fprintf(outFile, "SCALARS dissip double 1\n");
	fprintf(outFile, "LOOKUP_TABLE default\n");
	for (k = 0; k < nz+2; k++)
		for (j = 0; j < ny+2; j++)
			for (i = 0; i < nx+2; i++)
				fprintf(outFile, "%18.10le\n", campo->dissip[i][j][k]);
	fprintf(outFile,"TENSORS strain double\n");	
	for (k = 0; k < nz+2; k++)
		for (j = 0; j < ny+2; j++)
			for (i = 0; i < nx+2; i++)
				fprintf(outFile, "%18.10le %18.10le %18.10le\n"
												 "%18.10le %18.10le %18.10le\n" 
												 "%18.10le %18.10le %18.10le\n\n", 
												 campo->strain[i][j][k].a11,campo->strain[i][j][k].a12,campo->strain[i][j][k].a13,
												 campo->strain[i][j][k].a21,campo->strain[i][j][k].a22,campo->strain[i][j][k].a23,
												 campo->strain[i][j][k].a31,campo->strain[i][j][k].a32,campo->strain[i][j][k].a33);
	fprintf(outFile,"TENSORS skew_strain double\n");	
	for (k = 0; k < nz+2; k++)
		for (j = 0; j < ny+2; j++)
			for (i = 0; i < nx+2; i++)
				fprintf(outFile, "%18.10le %18.10le %18.10le\n"
												 "%18.10le %18.10le %18.10le\n" 
												 "%18.10le %18.10le %18.10le\n\n", 
												 campo->skew_strain[i][j][k].a11,campo->skew_strain[i][j][k].a12,campo->skew_strain[i][j][k].a13,
												 campo->skew_strain[i][j][k].a21,campo->skew_strain[i][j][k].a22,campo->skew_strain[i][j][k].a23,
												 campo->skew_strain[i][j][k].a31,campo->skew_strain[i][j][k].a32,campo->skew_strain[i][j][k].a33);
*/
	fclose(outFile);
}
void printPointerFaceScalarToVTK (	char *output, 
											    			    int nx, 
													    	    int ny, 
														        int nz,
                                    char *name, enum direction d, 
														        struct grid *const gc,
														        double   ****const campo	) 
{
	FILE *outFile = fopen(output,"w");
	int i, j, k;
  int nxt, nyt, nzt;
  int sxt, syt, szt;
  int totx, toty, totz;


	if ( outFile == NULL )
	{
		printf ("ERROR: file %s couldn't be read", output);
		exit(EXIT_FAILURE);
	}

  switch(d)
  {
    case X:
      nxt = nx;
      nyt = ny-1;
      nzt = nz-1;
      sxt = 0;
      syt = 0;
      szt = 0;
      // the size of the rest is obvious, but I want to be consistent
      totx = nxt - sxt + 1;
      toty = nyt - syt + 1;
      totz = nzt - szt + 1;
    break;
    case Y:
      nxt = nx-1;
      nyt = ny;
      nzt = nz-1;
      sxt = 0;
      syt = 0;
      szt = 0;
      // the size of the rest is obvious, but I want to be consistent
      totx = nxt - sxt + 1;
      toty = nyt - syt + 1;
      totz = nzt - szt + 1;
    break;
    case Z:
      nxt = nx-1;
      nyt = ny-1;
      nzt = nz;
      sxt = 0;
      syt = 0;
      szt = 0;
      // the size of the rest is obvious, but I want to be consistent
      totx = nxt - sxt + 1;
      toty = nyt - syt + 1;
      totz = nzt - szt + 1;
    break;
  }

	fprintf(outFile, "# vtk DataFile Version 2.0\n");
	fprintf(outFile, "LESCOAST OUTPUT FIELD\n");
	fprintf(outFile, "ASCII\n");
	fprintf(outFile, "DATASET STRUCTURED_GRID\n");
	fprintf(outFile, "DIMENSIONS %i %i %i\n", totx,toty,totz);
	fprintf(outFile, "POINTS %i double\n", (totx)*(toty)*(totz));

	for (k = szt; k < nzt+1; k++)
		for (j = syt; j < nyt+1; j++)
			for (i = sxt; i < nxt+1; i++) 
				fprintf(outFile, "%18.10le %18.10le %18.10le\n", gc->x[i][j][k],
																		 gc->y[i][j][k],	
																		 gc->z[i][j][k]);
	

	fprintf(outFile, "\nPOINT_DATA %i\n",(totx)*(toty)*(totz));
	fprintf(outFile, "SCALARS %s double 1\n", name);
	fprintf(outFile, "LOOKUP_TABLE default\n");
	for (k = szt; k < nzt+1; k++)
		for (j = syt; j < nyt+1; j++)
			for (i = sxt; i < nxt+1; i++) 
				fprintf(outFile, "%18.10le\n", (*campo)[i][j][k]);
    	

	fclose(outFile);
}
#define FIL "new_res_forms.h5"
#define   U(i,j,k,mj,mk)  U[(i)*(mj)*(mk)+(j)*(mk)+(k)]
#define   V(i,j,k,mj,mk)  V[(i)*(mj)*(mk)+(j)*(mk)+(k)] 
#define   W(i,j,k,mj,mk)  W[(i)*(mj)*(mk)+(j)*(mk)+(k)] 
#define  UC(i,j,k,mj,mk) UC[(i)*(mj)*(mk)+(j)*(mk)+(k)] 
#define  VC(i,j,k,mj,mk) VC[(i)*(mj)*(mk)+(j)*(mk)+(k)] 
#define  WC(i,j,k,mj,mk) WC[(i)*(mj)*(mk)+(j)*(mk)+(k)] 
#define  FI(i,j,k,mj,mk) FI[(i)*(mj)*(mk)+(j)*(mk)+(k)]
#define  RE(i,j,k,mj,mk) RE[(i)*(mj)*(mk)+(j)*(mk)+(k)]
#define RHE(i,j,k,mj,mk) RE[(i)*(mj)*(mk)+(j)*(mk)+(k)]
#define RHO(i,j,k,l,mj,mk,ml) RHO[(i)*(mj)*(mk)*(ml)+(j)*(mk)*(ml)+(k)*(ml)+l]
//char FIL[] = "new_res_forms.h5";
void readFieldsH5(int nx, 
									int ny, 
									int nz,
									int nscal,
									double u_star,
									char* folder, 
									int group, // input
									struct field *campo)
{
	char *buffer;
	int i, j, k, isc, sz_b;
	double a,b,c,d,e,ff;
	hid_t  file_id, dset_id;
	herr_t status;
/*
	double U[nz+2][ny+2][nx+2];
	double V[nz+2][ny+2][nx+2];
	double W[nz+2][ny+2][nx+2];
	double FI[nz+2][ny+2][nx+2];
	double RE[nz+2][ny+2][nx+2];

	double RHO[nz+2][ny+2][nx+2][nscal];

	double UC[nz][ny][nx+1];
	double VC[nz][ny+1][nx];
	double WC[nz][ny][nx];


	double ***U=NULL;
	double ***V=NULL;
	double ***W=NULL;
	double ***FI=NULL;
	double ***RE=NULL;

	double ****RHO=NULL;

	double ***UC=NULL;
	double ***VC=NULL;
	double ***WC=NULL;


	init_3dArray(nz,ny,nx,&U);
	init_3dArray(nz,ny,nx,&V);
	init_3dArray(nz,ny,nx,&W);
	init_3dArray(nz,ny,nx,&FI);
	init_3dArray(nz,ny,nx,&RE);

	RHO = malloc ((nz+2) * sizeof(*RHO));
	for (k=0;k<nz+2;++k)
	{
		RHO[k] = malloc((ny+2)*sizeof(**RHO));
		for (j = 0; j < ny+2; j++)
		{
			RHO[k][j] = malloc((nx+2)*sizeof(***RHO));
			for (i = 0; i < nx+2; i++)
				RHO[k][j][i] = malloc((nscal)*sizeof(****RHO));
		}		
	}

	init_3dArray(nz-2,ny-2,nx-1,&UC);
	init_3dArray(nz-2,ny-1,nx-2,&VC);
	init_3dArray(nz-1,ny-2,nx-2,&WC);

*/
	double *U=NULL;
	double *V=NULL;
	double *W=NULL;
	double *FI=NULL;
	double *RE=NULL;

	double *RHO=NULL;

	double *UC=NULL;
	double *VC=NULL;
	double *WC=NULL;

	U = malloc((nx+2)*(ny+2)*(nz+2)*sizeof(*U));
	V = malloc((nx+2)*(ny+2)*(nz+2)*sizeof(*V));
	W = malloc((nx+2)*(ny+2)*(nz+2)*sizeof(*W));
	FI = malloc((nx+2)*(ny+2)*(nz+2)*sizeof(*FI));
	RE = malloc((nx+2)*(ny+2)*(nz+2)*sizeof(*RE));
	RHO= malloc((nx+2)*(ny+2)*(nz+2)*nscal*sizeof(*RHO));
	UC = malloc((nx+1)*(ny)*(nz)*sizeof(*UC));
	VC = malloc((nx)*(ny+1)*(nz)*sizeof(*VC));
	WC = malloc((nx)*(ny)*(nz+1)*sizeof(*WC));


	sz_b = (strlen(folder) + strlen(FIL) + 3);
	//sz_b = 100;
	buffer = (char*)malloc(sz_b*sizeof(char));

	strcpy(buffer, folder);
	strcat(buffer,"/");
	strcat(buffer,FIL);

	file_id = H5Fopen(buffer, H5F_ACC_RDWR, H5P_DEFAULT);

	free(buffer);

// start allocation of variables
	init_3dArray(nx,ny,nz,&(campo->u));
	init_3dArray(nx,ny,nz,&(campo->v));
	init_3dArray(nx,ny,nz,&(campo->w));
	init_3dArray(nx,ny,nz,&(campo->fi));
	init_3dArray(nx,ny,nz,&(campo->re));
/*
	init_3dArray(nx,ny,nz,&(campo->div));
	init_3dArray(nx-1,ny-2,nz-2,&(campo->uc));
	init_3dArray(nx-2,ny-1,nz-2,&(campo->vc));
	init_3dArray(nx-2,ny-2,nz-1,&(campo->wc));
*/
// allocate rho
	campo->rho = malloc (nscal * sizeof(*campo->rho));
	for (isc=0; isc < (nscal); isc++)
	{
		campo->rho[isc] = malloc((nx+2)*sizeof(**campo->rho));
		for (i = 0; i < nx+2; i++)
		{
			campo->rho[isc][i] = malloc((ny+2)*sizeof(***campo->rho));
			for (j = 0; j < ny+2; j++)
				campo->rho[isc][i][j] = malloc((nz+2)*sizeof(****campo->rho));
		}		
	}
// end allocation
// let us read the field	
// U
	buffer = malloc((10)*sizeof(*buffer));
	sprintf(buffer,"%d",group);
	strcat(buffer,"/u");
	dset_id = H5Dopen2(file_id,buffer,H5P_DEFAULT);
	status = H5Dread(dset_id,H5T_NATIVE_DOUBLE,H5S_ALL,H5S_ALL,H5P_DEFAULT,
										U);
	free(buffer);
	status = H5Dclose(dset_id);
// V
	buffer = malloc((10)*sizeof(*buffer));
	sprintf(buffer,"%d",group);
	strcat(buffer,"/v");
	dset_id = H5Dopen2(file_id,buffer,H5P_DEFAULT);
	status = H5Dread(dset_id,H5T_NATIVE_DOUBLE,H5S_ALL,H5S_ALL,H5P_DEFAULT,
										V);
	free(buffer);
	status = H5Dclose(dset_id);
// W
	buffer = malloc((10)*sizeof(*buffer));
	sprintf(buffer,"%d",group);
	strcat(buffer,"/w");
	dset_id = H5Dopen2(file_id,buffer,H5P_DEFAULT);
	status = H5Dread(dset_id,H5T_NATIVE_DOUBLE,H5S_ALL,H5S_ALL,H5P_DEFAULT,
										W);
	free(buffer);
	status = H5Dclose(dset_id);
// fi
	buffer = malloc((10)*sizeof(*buffer));
	sprintf(buffer,"%d",group);
	strcat(buffer,"/fi");
	dset_id = H5Dopen2(file_id,buffer,H5P_DEFAULT);
	status = H5Dread(dset_id,H5T_NATIVE_DOUBLE,H5S_ALL,H5S_ALL,H5P_DEFAULT,
										FI);
	free(buffer);
	status = H5Dclose(dset_id);
// RE
	buffer = malloc((10)*sizeof(*buffer));
	sprintf(buffer,"%d",group);
	strcat(buffer,"/nuT");
	dset_id = H5Dopen2(file_id,buffer,H5P_DEFAULT);
	status = H5Dread(dset_id,H5T_NATIVE_DOUBLE,H5S_ALL,H5S_ALL,H5P_DEFAULT,
										RE);
	free(buffer);
	status = H5Dclose(dset_id);
// UC
/*
	buffer = malloc((10)*sizeof(*buffer));
	sprintf(buffer,"%d",group);
	strcat(buffer,"/uc");
	dset_id = H5Dopen2(file_id,buffer,H5P_DEFAULT);
	status = H5Dread(dset_id,H5T_NATIVE_DOUBLE,H5S_ALL,H5S_ALL,H5P_DEFAULT,
										UC);
	free(buffer);
	status = H5Dclose(dset_id);
// VC
	buffer = malloc((10)*sizeof(*buffer));
	sprintf(buffer,"%d",group);
	strcat(buffer,"/vc");
	dset_id = H5Dopen2(file_id,buffer,H5P_DEFAULT);
	status = H5Dread(dset_id,H5T_NATIVE_DOUBLE,H5S_ALL,H5S_ALL,H5P_DEFAULT,
										VC);
	free(buffer);
	status = H5Dclose(dset_id);
// WC
	buffer = malloc((10)*sizeof(*buffer));
	sprintf(buffer,"%d",group);
	strcat(buffer,"/wc");
	dset_id = H5Dopen2(file_id,buffer,H5P_DEFAULT);
	status = H5Dread(dset_id,H5T_NATIVE_DOUBLE,H5S_ALL,H5S_ALL,H5P_DEFAULT,
										WC);
	free(buffer);
	status = H5Dclose(dset_id);
*/
// RHO
	buffer = malloc((10)*sizeof(*buffer));
	sprintf(buffer,"%d",group);
	strcat(buffer,"/rho");
	dset_id = H5Dopen2(file_id,buffer,H5P_DEFAULT);
	status = H5Dread(dset_id,H5T_NATIVE_DOUBLE,H5S_ALL,H5S_ALL,H5P_DEFAULT,
										RHO);
	free(buffer);
	status = H5Dclose(dset_id);
// FIN
	status = H5Fclose(file_id);


	for (k = 0; k < nz + 2; k++ )
		for (j = 0; j < ny + 2; j++)
			for (i = 0; i < nx + 2; i++)
			{
				campo->u[i][j][k]  = U(k,j,i,ny+2,nx+2)/u_star;
				campo->v[i][j][k]  = V(k,j,i,ny+2,nx+2)/u_star;  
				campo->w[i][j][k]  = W(k,j,i,ny+2,nx+2)/u_star;  
				campo->fi[i][j][k] = FI(k,j,i,ny+2,nx+2);  
				campo->re[i][j][k] = 1./RE(k,j,i,ny+2,nx+2); // for some strange reason, LESCOAST gives Re as 1/Re
// 				campo->rho[0][i][j][k] = RHE(k,j,i,ny+2,nx+2);  
				for (isc = 0; isc < (nscal); isc++)
				{					
					campo->rho[isc][i][j][k] = RHO(k,j,i,isc,ny+2,nx+2,nscal);//[k][j][i][isc];
//					campo->rho[isc][i][j][k] = RHO(k,i,j,isc,nx+2,ny+2,nscal);//[k][j][i][isc];
				}
/*
				campo->u[i][j][k]  = U[k][j][i]/u_star;
				campo->v[i][j][k]  = V[k][j][i]/u_star;  
				campo->w[i][j][k]  = W[k][j][i]/u_star;  
				campo->fi[i][j][k] = FI[k][j][i];  
				campo->re[i][j][k] = 1./RE[k][j][i]; // for some strange reason, LESCOAST gives Re as 1/Re 
				for (isc = 0; isc < (nscal); isc++)
				{
					campo->rho[isc][i][j][k] = RHO[k][i][j][isc];
				}
*/
			}	
/*
	for (k = 0; k < nz; k++)
		for (j = 0; j < ny; j++)
			for (i = 0; i < nx + 1; i++)
				campo->uc[i][j][k] = UC(k,j,i,ny+2,nx+2);//[k][j][i];
	for (k = 0; k < nz; k++)
		for (j = 0; j < ny + 1; j++)
			for (i = 0; i < nx; i++)
				campo->vc[i][j][k] = VC(k,j,i,ny+2,nx+2);//[k][j][i];
	for (k = 0; k < nz + 1; k++)
		for (j = 0; j < ny; j++)
			for (i = 0; i < nx; i++)
				campo->wc[i][j][k] = WC(k,j,i,ny+2,nx+2);//[k][j][i];
	
// initialize
	for (i = 0; i < nx+2; i++)
		for (j = 0; j < ny+2; j++)
			for (k = 0; k < nz+2;k++)
				campo->div[i][j][k] = 0.0f;
 
// MASS CONSERVATION (TOTAL OF FLUXES SHOULD AMOUNT TO...........)
	for (i = 1; i < nx+1; i++)
		for (j = 1; j < ny; j++)
			for (k = 1; k < nz;k++)
				campo->div[i][j][k] = campo->uc[i][j][k] - campo->uc[i-1][j][k];

	for (i = 1; i < nx; i++)
		for (j = 1; j < ny+1; j++)
			for (k = 1; k < nz;k++)
				campo->div[i][j][k] = campo->div[i][j][k] +
														 campo->vc[i][j][k] - campo->vc[i][j-1][k];
	for (i = 1; i < nx; i++)
		for (j = 1; j < ny; j++)
			for (k = 1; k < nz+1;k++)
				campo->div[i][j][k] = campo->div[i][j][k] +
														 campo->wc[i][j][k] - campo->wc[i][j][k-1];
*/

	free(U);
	free(V);
	free(W);
	free(FI);
	free(RE);
	free(RHO);
/*
	free(UC);
	free(VC);
	free(WC);
*/
}
