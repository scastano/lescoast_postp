
/* Copyright, 2015, Santiago Lopez Castano
 *=========================================================================
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.

 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */
#include <stdlib.h>
#include <stdio.h>
#include "stru.h"
#include "topologyUtil.h"
/*
extern "C"{
	void ssyev_ (char *JOBZ, char *UPLO, int *N, double *A,
							int *LDA,	double *W, double *WORK, int *LWORK, int *INFO);
}*/
void dsyev_ (char *JOBZ, char *UPLO, int *N, double *A,
							int *LDA,	double *W, double *WORK, int *LWORK, int *INFO);

void calculateEigenvalueStrain(	int nx,
																int ny,
																int nz,
																struct field *campo)
{
	int i, j, k;
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// variables needed for the eigenvalues calculator	
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	int  n=3, lwork =1000, lda=3;
	double mat[n][n], valre[n], work[lwork];	
	int err;
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	// allocate derivatives tensor !!!!
	campo->SLambda1 = malloc ((nx+2) * sizeof(*campo->SLambda1));
	for (i = 0; i < nx+2; i++)
	{
		campo->SLambda1[i] = malloc((ny+2)*sizeof(**campo->SLambda1));
		for (j = 0; j < ny + 2; j++)
			campo->SLambda1[i][j] = malloc((nz+2)*sizeof(***campo->SLambda1));
	}
	campo->SLambda2 = malloc ((nx+2) * sizeof(*campo->SLambda2));
	for (i = 0; i < nx+2; i++)
	{
		campo->SLambda2[i] = malloc((ny+2)*sizeof(**campo->SLambda2));
		for (j = 0; j < ny + 2; j++)
			campo->SLambda2[i][j] = malloc((nz+2)*sizeof(***campo->SLambda2));
	}
	campo->SLambda3 = malloc ((nx+2) * sizeof(*campo->SLambda3));
	for (i = 0; i < nx+2; i++)
	{
		campo->SLambda3[i] = malloc((ny+2)*sizeof(**campo->SLambda3));
		for (j = 0; j < ny + 2; j++)
			campo->SLambda3[i][j] = malloc((nz+2)*sizeof(***campo->SLambda3));
	}

	for(i=0;i<nx+2;++i)
		for(j=0;j<ny+2;++j)
			for(k=0;k<nz+2;++k)
			{
				// input for eigen
				mat[0][0] = campo->strain[i][j][k].a11;
				mat[0][1] = campo->strain[i][j][k].a12;
				mat[0][2] = campo->strain[i][j][k].a13;
				mat[1][0] = campo->strain[i][j][k].a21;
				mat[1][1] = campo->strain[i][j][k].a22;
				mat[1][2] = campo->strain[i][j][k].a23;
				mat[2][0] = campo->strain[i][j][k].a31;
				mat[2][1] = campo->strain[i][j][k].a32;
				mat[2][2] = campo->strain[i][j][k].a33;
				// calculate eigenvalues				
				dsyev_ ("N","U",&n,&mat[0][0],&lda,valre,work,&lwork,&err);	
				if(err != 0)
					printf("WARNING: ssyev_ threw an error of type %d in element (%d,%d,%d)\n",err,i,j,k);
			  // sort eigenvalues in ascending order	
				// extract lambda 2
				campo->SLambda1[i][j][k] = valre[0];
				campo->SLambda2[i][j][k] = valre[1];
				campo->SLambda3[i][j][k] = valre[2];
			}
}
void calculateLambdaCriterion(int nx,
															int ny,
															int nz, double re,
															struct field *campo)
{
	int i, j, k;
	struct matrix sqStrain, sqSStrain, symmVortEqn;
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// variables needed for the eigenvalues calculator	
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	int  n=3, lwork =1000, lda=3;
	double mat[n][n], valre[n], work[lwork];	
	int err;
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	// allocate derivatives tensor !!!!
	campo->visc_diss = malloc ((nx+2) * sizeof(*campo->visc_diss));
	for (i = 0; i < nx+2; i++)
	{
		campo->visc_diss[i] = malloc((ny+2)*sizeof(**campo->visc_diss));
		for (j = 0; j < ny + 2; j++)
			campo->visc_diss[i][j] = malloc((nz+2)*sizeof(***campo->visc_diss));
	}
	campo->strain = malloc ((nx+2) * sizeof(*campo->strain));
	for (i = 0; i < nx+2; i++)
	{
		campo->strain[i] = malloc((ny+2)*sizeof(**campo->strain));
		for (j = 0; j < ny + 2; j++)
			campo->strain[i][j] = malloc((nz+2)*sizeof(***campo->strain));
	}
	campo->skew_strain = malloc ((nx+2) * sizeof(*campo->skew_strain));
	for (i = 0; i < nx+2; i++)
	{
		campo->skew_strain[i] = malloc((ny+2)*sizeof(**campo->skew_strain));
		for (j = 0; j < ny + 2; j++)
			campo->skew_strain[i][j] = malloc((nz+2)*sizeof(***campo->skew_strain));
	}
	campo->vortX = malloc ((nx+2) * sizeof(*campo->vortX));
	for (i = 0; i < nx+2; i++)
	{
		campo->vortX[i] = malloc((ny+2)*sizeof(**campo->vortX));
		for (j = 0; j < ny + 2; j++)
			campo->vortX[i][j] = malloc((nz+2)*sizeof(***campo->vortX));
	}
	campo->vortY = malloc ((nx+2) * sizeof(*campo->vortY));
	for (i = 0; i < nx+2; i++)
	{
		campo->vortY[i] = malloc((ny+2)*sizeof(**campo->vortY));
		for (j = 0; j < ny + 2; j++)
			campo->vortY[i][j] = malloc((nz+2)*sizeof(***campo->vortY));
	}
	campo->vortZ = malloc ((nx+2) * sizeof(*campo->vortZ));
	for (i = 0; i < nx+2; i++)
	{
		campo->vortZ[i] = malloc((ny+2)*sizeof(**campo->vortZ));
		for (j = 0; j < ny + 2; j++)
			campo->vortZ[i][j] = malloc((nz+2)*sizeof(***campo->vortZ));
	}
	campo->lambda2 = malloc ((nx+2) * sizeof(*campo->lambda2));
	for (i = 0; i < nx+2; i++)
	{
		campo->lambda2[i] = malloc((ny+2)*sizeof(**campo->lambda2));
		for (j = 0; j < ny + 2; j++)
			campo->lambda2[i][j] = malloc((nz+2)*sizeof(***campo->lambda2));
	}
	campo->q = malloc ((nx+2) * sizeof(*campo->q));
	for (i = 0; i < nx+2; i++)
	{
		campo->q[i] = malloc((ny+2)*sizeof(**campo->q));
		for (j = 0; j < ny + 2; j++)
			campo->q[i][j] = malloc((nz+2)*sizeof(***campo->q));
	}

	for(i=0;i<nx+2;++i)
		for(j=0;j<ny+2;++j)
			for(k=0;k<nz+2;++k)
			{
				campo->strain[i][j][k] = scalarMatrixMult(0.5,
																 matrixSum(campo->deriv[i][j][k],
																 matrixTranspose(campo->deriv[i][j][k])));			
				campo->skew_strain[i][j][k] = scalarMatrixMult(0.5,
																 			matrixRest(campo->deriv[i][j][k],
																 			matrixTranspose(campo->deriv[i][j][k])));

				campo->vortX[i][j][k] = 2.0*campo->skew_strain[i][j][k].a23;
				campo->vortY[i][j][k] = 2.0*campo->skew_strain[i][j][k].a13;
				campo->vortZ[i][j][k] = 2.0*campo->skew_strain[i][j][k].a12;

				sqStrain 	= matMul(campo->strain[i][j][k],campo->strain[i][j][k]);
				sqSStrain = matMul(campo->skew_strain[i][j][k],campo->skew_strain[i][j][k]);
				symmVortEqn = matrixTranspose(matrixSum(sqStrain, sqSStrain)); // the transpose is needed for ssyev_
				// input for eigen
				mat[0][0] = symmVortEqn.a11;
				mat[0][1] = symmVortEqn.a12;
				mat[0][2] = symmVortEqn.a13;
				mat[1][0] = symmVortEqn.a21;
				mat[1][1] = symmVortEqn.a22;
				mat[1][2] = symmVortEqn.a23;
				mat[2][0] = symmVortEqn.a31;
				mat[2][1] = symmVortEqn.a32;
				mat[2][2] = symmVortEqn.a33;
				// calculate eigenvalues				
				dsyev_ ("N","U",&n,&mat[0][0],&lda,valre,work,&lwork,&err);	
				if(err != 0)
					printf("WARNING: ssyev_ threw an error of type %d in element (%d,%d,%d)\n",err,i,j,k);
			  // sort eigenvalues in ascending order	
				// extract lambda 2
				campo->lambda2[i][j][k] = valre[1];
				campo->q[i][j][k] = -0.5*(valre[1]+valre[0]+valre[2]);
				campo->visc_diss[i][j][k] = scalarMatrixMult(2.0/campo->re[i][j][k],sqStrain);
//				campo->dissip[i][j][k] = -0.5*matrixTrace(campo->visc_diss[i][j][k]);
			}
}
void calculateSecondThirdInvariants(int nx, 
																		int ny,
																		int nz,
																		struct matrix **** const mat,
																		double ****second,
																		double ****third)
/*
 *
 * ASSUMES THAT SECOND AND THIRD ARE NOT ALLOCATED
 *
 */
{
	int i, j, k;
	(*second) = malloc ((nx+2) * sizeof(*(*second)));
	for (i = 0; i < nx+2; i++)
	{
		(*second)[i] = malloc((ny+2)*sizeof(**(*second)));
		for (j = 0; j < ny + 2; j++)
			(*second)[i][j] = malloc((nz+2)*sizeof(***(*second)));
	}
	(*third) = malloc ((nx+2) * sizeof(*(*third)));
	for (i = 0; i < nx+2; i++)
	{
		(*third)[i] = malloc((ny+2)*sizeof(**(*third)));
		for (j = 0; j < ny + 2; j++)
			(*third)[i][j] = malloc((nz+2)*sizeof(***(*third)));
	}
	for(i=0;i<nx+2;++i)
		for(j=0;j<ny+2;++j)
			for(k=0;k<nz+2;++k)
			{
				(*second)[i][j][k] = -0.5 *((*mat)[i][j][k].a11*(*mat)[i][j][k].a11+(*mat)[i][j][k].a22*(*mat)[i][j][k].a22+(*mat)[i][j][k].a33*(*mat)[i][j][k].a33+
																		2.0*(*mat)[i][j][k].a12*(*mat)[i][j][k].a21+2.0*(*mat)[i][j][k].a13*(*mat)[i][j][k].a31+2.0*(*mat)[i][j][k].a23*(*mat)[i][j][k].a32);
				(*third) [i][j][k] = matrixDeterminant((*mat)[i][j][k]);
			}
}
