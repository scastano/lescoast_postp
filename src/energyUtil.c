/* Copyright, 2015, Santiago Lopez Castano
 *=========================================================================
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.

 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */
#include <stdlib.h>
#include "stru.h"
#include "derivativeUtil.h"
#include "energyUtil.h"

void calculateEnergyBudget (int nx,
														int ny,
														int nz,
														int ntime,
														double re,
														struct cent  *grid,
														struct field *campo,
														struct field *mean)
/*
 * NOTE TO SELF: SOME OF THE TERMS OF THE ENERGY BUDGET ARE CALCULATED 
 * WHERE IS MORE EXPEDITE/EASIER/LESS MEMORY CONSUMING. TKE IS CALCULATED
 * INSIDE MEANUTIL.C, DISSIPATION IS CALCULATED IN TOPOLOGYUTIL.C BECAUSE
 * IN THE CONSTRUCTION OF LAMBDA2/Q THE INNER PRODUCT OF THE STRAIN TENSOR
 * APPEARS. THE REST OF THE TERMS ARE CALCULATED HERE.
 *
 */
{
	int i, j, k, l;
	double ***temp[3];
	struct field *tempc, *finc = campo + ntime;
	
	// ALLOCATION
	for(l = 0; l < 3; ++l)
	{
		mean->turb_t_vec[l] = calloc ((nx+2),sizeof(**mean->turb_t_vec));
		for (i = 0; i < nx+2; i++)
		{
			mean->turb_t_vec[l][i] = calloc((ny+2),sizeof(***mean->turb_t_vec));
			for (j = 0; j < ny+2; j++)
				mean->turb_t_vec[l][i][j] = calloc((nz+2),sizeof(****mean->turb_t_vec));
		}
	}
	mean->turb_transp = calloc ((nx+2), sizeof(*mean->turb_transp));
	for (i = 0; i < nx+2; i++)
	{
		mean->turb_transp[i] = calloc((ny+2),sizeof(**mean->turb_transp));
		for (j = 0; j < ny+2; j++)
			mean->turb_transp[i][j] = calloc((nz+2),sizeof(***mean->turb_transp));
	}
	// END ALLOCATION
	// TURBULENT TRANSPORT
	tempc = campo;
	while(tempc < finc)
	{	
		for (k = 0; k < nz+2; k++)
			for (j = 0; j < ny+2; j++)
				for (i = 0; i < nx+2; i++)
				{
					mean->turb_t_vec[0][i][j][k] += (1.0/ntime)*tempc->k[i][j][k]*tempc->u[i][j][k];
					mean->turb_t_vec[1][i][j][k] += (1.0/ntime)*tempc->k[i][j][k]*tempc->v[i][j][k];
					mean->turb_t_vec[2][i][j][k] += (1.0/ntime)*tempc->k[i][j][k]*tempc->w[i][j][k];
				}
		++tempc;
	}
	temp[0] = NULL;
	calculateScalarDerivatives(nx,ny,nz,dirX,grid->jac,
			mean->turb_t_vec[0],&temp[0]);
	temp[1] = NULL;
	calculateScalarDerivatives(nx,ny,nz,dirY,grid->jac,
			mean->turb_t_vec[1],&temp[1]);
	temp[2] = NULL;
	calculateScalarDerivatives(nx,ny,nz,dirZ,grid->jac,
			mean->turb_t_vec[2],&temp[2]);
	for (k = 0; k < nz+2; k++)
		for (j = 0; j < ny+2; j++)
			for (i = 0; i < nx+2; i++)
				mean->turb_transp[i][j][k] = -(temp[0][i][j][k]+
                                       temp[1][i][j][k]+ 
																			 temp[2][i][j][k]);
	dealloc_3d(nx,ny,(void*)&(mean->turb_t_vec[0]));
	dealloc_3d(nx,ny,(void*)&(mean->turb_t_vec[1]));
	dealloc_3d(nx,ny,(void*)&(mean->turb_t_vec[2]));
	dealloc_3d(nx,ny,(void*)&temp[0]);
	dealloc_3d(nx,ny,(void*)&temp[1]);
	dealloc_3d(nx,ny,(void*)&temp[2]);
  // PRESSURE DIFFUSION
	// ALLOCATION
	for(l = 0; l < 3; ++l)
	{
		mean->turb_t_vec[l] = calloc ((nx+2),sizeof(**mean->turb_t_vec));
		for (i = 0; i < nx+2; i++)
		{
			mean->turb_t_vec[l][i] = calloc((ny+2),sizeof(***mean->turb_t_vec));
			for (j = 0; j < ny+2; j++)
				mean->turb_t_vec[l][i][j] = calloc((nz+2),sizeof(****mean->turb_t_vec));
		}
	}
	mean->press_diff = calloc ((nx+2), sizeof(*mean->press_diff));
	for (i = 0; i < nx+2; i++)
	{
		mean->press_diff[i] = calloc((ny+2),sizeof(**mean->press_diff));
		for (j = 0; j < ny+2; j++)
			mean->press_diff[i][j] = calloc((nz+2),sizeof(***mean->press_diff));
	}
	// END ALLOCATION
	// TURBULENT TRANSPORT
	tempc = campo;
	while(tempc < finc)
	{	
		for (k = 0; k < nz+2; k++)
			for (j = 0; j < ny+2; j++)
				for (i = 0; i < nx+2; i++)
				{
					mean->turb_t_vec[0][i][j][k] += (1.0/ntime)*tempc->fi[i][j][k]*tempc->u[i][j][k];
					mean->turb_t_vec[1][i][j][k] += (1.0/ntime)*tempc->fi[i][j][k]*tempc->v[i][j][k];
					mean->turb_t_vec[2][i][j][k] += (1.0/ntime)*tempc->fi[i][j][k]*tempc->w[i][j][k];
				}
		++tempc;
	}
	temp[0] = NULL;
	calculateScalarDerivatives(nx,ny,nz,dirX,grid->jac,
			mean->turb_t_vec[0],&temp[0]);
	temp[1] = NULL;
	calculateScalarDerivatives(nx,ny,nz,dirY,grid->jac,
			mean->turb_t_vec[1],&temp[1]);
	temp[2] = NULL;
	calculateScalarDerivatives(nx,ny,nz,dirZ,grid->jac,
			mean->turb_t_vec[2],&temp[2]);
	for (k = 0; k < nz+2; k++)
		for (j = 0; j < ny+2; j++)
			for (i = 0; i < nx+2; i++)
				mean->press_diff[i][j][k] = -(temp[0][i][j][k]+
                                       temp[1][i][j][k]+ 
																			 temp[2][i][j][k]);
	dealloc_3d(nx,ny,(void*)&(mean->turb_t_vec[0]));
	dealloc_3d(nx,ny,(void*)&(mean->turb_t_vec[1]));
	dealloc_3d(nx,ny,(void*)&(mean->turb_t_vec[2]));
	dealloc_3d(nx,ny,(void*)&temp[0]);
	dealloc_3d(nx,ny,(void*)&temp[1]);
	dealloc_3d(nx,ny,(void*)&temp[2]);
// VISCOUS TRANSPORT
	// ALLOCATION
	for(l = 0; l < 3; ++l)
	{
		mean->turb_t_vec[l] = calloc ((nx+2),sizeof(**mean->turb_t_vec));
		for (i = 0; i < nx+2; i++)
		{
			mean->turb_t_vec[l][i] = calloc((ny+2),sizeof(***mean->turb_t_vec));
			for (j = 0; j < ny+2; j++)
				mean->turb_t_vec[l][i][j] = calloc((nz+2),sizeof(****mean->turb_t_vec));
		}
	}
	mean->visc_diff = calloc ((nx+2), sizeof(*mean->visc_diff));
	for (i = 0; i < nx+2; i++)
	{
		mean->visc_diff[i] = calloc((ny+2),sizeof(**mean->visc_diff));
		for (j = 0; j < ny+2; j++)
			mean->visc_diff[i][j] = calloc((nz+2),sizeof(***mean->visc_diff));
	}
	tempc = campo;
	while(tempc < finc)
	{
		dealloc_3d(nx,ny,(void*)&(tempc->deriv));
		dealloc_3d(nx,ny,(void*)&(tempc->strain));
		tempc->strain = calloc ((nx+2), sizeof(*tempc->strain));
		for (i = 0; i < nx+2; i++)
		{
			tempc->strain[i] = calloc((ny+2),sizeof(**tempc->strain));
			for (j = 0; j < ny+2; j++)
				tempc->strain[i][j] = calloc((nz+2),sizeof(***tempc->strain));
		}
		tempc->deriv = calloc ((nx+2), sizeof(*tempc->deriv));
		for (i = 0; i < nx+2; i++)
		{
			tempc->deriv[i] = calloc((ny+2),sizeof(**tempc->deriv));
			for (j = 0; j < ny+2; j++)
				tempc->deriv[i][j] = calloc((nz+2),sizeof(***tempc->deriv));
		}
		calculateFieldDerivatives(nx,ny,nz,&(grid->jac),tempc);	
		for (k = 0; k < nz+2; k++)
			for (j = 0; j < ny+2; j++)
				for (i = 0; i < nx+2; i++)
				{
					tempc->strain[i][j][k] = scalarMatrixMult(0.5,
                        					 matrixSum(tempc->deriv[i][j][k],
                        					 matrixTranspose(tempc->deriv[i][j][k])));
					mean->turb_t_vec[0][i][j][k] += (1.0/ntime)*
																					(tempc->u[i][j][k]*tempc->strain[i][j][k].a11+
																					 tempc->v[i][j][k]*tempc->strain[i][j][k].a21+
																					 tempc->w[i][j][k]*tempc->strain[i][j][k].a31);
					mean->turb_t_vec[1][i][j][k] += (1.0/ntime)*
																				  (tempc->u[i][j][k]*tempc->strain[i][j][k].a12+
																				   tempc->v[i][j][k]*tempc->strain[i][j][k].a22+
																				   tempc->w[i][j][k]*tempc->strain[i][j][k].a32);
					mean->turb_t_vec[2][i][j][k] += (1.0/ntime)*
																					(tempc->u[i][j][k]*tempc->strain[i][j][k].a13+
																					 tempc->v[i][j][k]*tempc->strain[i][j][k].a23+
																					 tempc->w[i][j][k]*tempc->strain[i][j][k].a33);
				}
		++tempc;
	}
	temp[0] = NULL;
	calculateScalarDerivatives(nx,ny,nz,dirX,grid->jac,
			mean->turb_t_vec[0],&temp[0]);
	temp[1] = NULL;
	calculateScalarDerivatives(nx,ny,nz,dirY,grid->jac,
			mean->turb_t_vec[1],&temp[1]);
	temp[2] = NULL;
	calculateScalarDerivatives(nx,ny,nz,dirZ,grid->jac,
			mean->turb_t_vec[2],&temp[2]);
	for (k = 0; k < nz+2; k++)
		for (j = 0; j < ny+2; j++)
			for (i = 0; i < nx+2; i++)
				mean->visc_diff[i][j][k] = 2.0/mean->re[i][j][k]*(temp[0][i][j][k]+
                                           temp[1][i][j][k]+ 
																	         temp[2][i][j][k]);
	dealloc_3d(nx,ny,(void*)&(mean->turb_t_vec[0]));
	dealloc_3d(nx,ny,(void*)&(mean->turb_t_vec[1]));
	dealloc_3d(nx,ny,(void*)&(mean->turb_t_vec[2]));



	dealloc_3d(nx,ny,(void*)&temp[0]);
	dealloc_3d(nx,ny,(void*)&temp[1]);
	dealloc_3d(nx,ny,(void*)&temp[2]);
	// DISSIPATION 
	mean->dissip = calloc ((nx+2), sizeof(*mean->dissip));
	for (i = 0; i < nx+2; i++)
	{
		mean->dissip[i] = calloc((ny+2),sizeof(**mean->dissip));
		for (j = 0; j < ny+2; j++)
			mean->dissip[i][j] = calloc((nz+2),sizeof(***mean->dissip));
	}
	// END ALLOCATION
	tempc = campo;
	while(tempc < finc)
	{	
		for (k = 0; k < nz+2; k++)
			for (j = 0; j < ny+2; j++)
				for (i = 0; i < nx+2; i++)
					mean->dissip[i][j][k] =-2.0/mean->re[i][j][k]*(tempc->strain[i][j][k].a11*tempc->strain[i][j][k].a11+
																					 tempc->strain[i][j][k].a12*tempc->strain[i][j][k].a12+
																					 tempc->strain[i][j][k].a13*tempc->strain[i][j][k].a13+
																					 tempc->strain[i][j][k].a21*tempc->strain[i][j][k].a21+
																					 tempc->strain[i][j][k].a22*tempc->strain[i][j][k].a22+
																					 tempc->strain[i][j][k].a23*tempc->strain[i][j][k].a23+
																					 tempc->strain[i][j][k].a31*tempc->strain[i][j][k].a31+
																					 tempc->strain[i][j][k].a32*tempc->strain[i][j][k].a32+
																					 tempc->strain[i][j][k].a33*tempc->strain[i][j][k].a33)*(1.0/ntime);
		++tempc;
	}
	// PRODUCTION 
	mean->prod = calloc ((nx+2), sizeof(*mean->prod));
	for (i = 0; i < nx+2; i++)
	{
		mean->prod[i] = calloc((ny+2),sizeof(**mean->prod));
		for (j = 0; j < ny+2; j++)
			mean->prod[i][j] = calloc((nz+2),sizeof(***mean->prod));
	}
	// END ALLOCATION
	tempc = campo;
	while(tempc < finc)
	{	
		for (k = 0; k < nz+2; k++)
			for (j = 0; j < ny+2; j++)
				for (i = 0; i < nx+2; i++)
					mean->prod[i][j][k] =-(tempc->uv[i][j][k].a11*mean->strain[i][j][k].a11+
																 tempc->uv[i][j][k].a12*mean->strain[i][j][k].a12+
																 tempc->uv[i][j][k].a13*mean->strain[i][j][k].a13+
																 tempc->uv[i][j][k].a21*mean->strain[i][j][k].a21+
																 tempc->uv[i][j][k].a22*mean->strain[i][j][k].a22+
																 tempc->uv[i][j][k].a23*mean->strain[i][j][k].a23+
																 tempc->uv[i][j][k].a31*mean->strain[i][j][k].a31+
																 tempc->uv[i][j][k].a32*mean->strain[i][j][k].a32+
																 tempc->uv[i][j][k].a33*mean->strain[i][j][k].a33)*(1.0/ntime);
		++tempc;
	}
//	mean->turb_t_vec[0] = NULL;
//	mean->turb_t_vec[1] = NULL;
//	mean->turb_t_vec[2] = NULL;
}

