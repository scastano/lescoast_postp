/* Copyright, 2015, Santiago Lopez Castano
 *=========================================================================
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.

 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */
#define QUADHOLE 1.0e-6
#include <stdlib.h>
#include <math.h>
#include "stru.h"
#include "meanUtil.h"


void calculateTaylorMicroScales (
																 int nx,
																 int ny,
																 int nz,
																 struct matrix **** const uu,
																 struct matrix **** const grad,
																 double ** res
																)
{
	int i, j, k;

	
	*res = calloc((ny+2),sizeof(**res));	
	for (j = 0; j < ny+2; j++)
	{
		for (k = 0; k < nz+2; k++)
			for (i = 0; i < nx+2; i++)
				(*res)[j] = -2.0 * (*uu)[i][j][k].a11/pow((*grad)[i][j][k].a12,2.0);
		(*res)[j] /= (double)(nx+2)*(nz+2);
	}
}
void calculateAnisotropicTensor (int nx,
																 int ny,
																 int nz,
																 struct matrix **** const uv,
																 struct matrix **** anisotropy)
{
	int i, j, k;
	double kk;

	(*anisotropy) = calloc ((nx+2), sizeof(**anisotropy));
	for (i = 0; i < nx+2; i++)
	{
		(*anisotropy)[i] = calloc((ny+2),sizeof(***anisotropy));
		for (j = 0; j < ny+2; j++)
			(*anisotropy)[i][j] = calloc((nz+2),sizeof(****anisotropy));
	}
	for (k = 0; k < nz+2; k++)
		for (j = 0; j < ny+2; j++)
			for (i = 0; i < nx+2; i++)
			{
					kk = 0.5 * matrixTrace((*uv)[i][j][k]);
					(*anisotropy)[i][j][k].a11 = (*uv)[i][j][k].a11/(2.0*kk)-1.0/3.0;
					(*anisotropy)[i][j][k].a12 = (*uv)[i][j][k].a12/(2.0*kk);
					(*anisotropy)[i][j][k].a13 = (*uv)[i][j][k].a13/(2.0*kk);
					(*anisotropy)[i][j][k].a23 = (*uv)[i][j][k].a23/(2.0*kk);
					(*anisotropy)[i][j][k].a22 = (*uv)[i][j][k].a22/(2.0*kk)-1.0/3.0;
					(*anisotropy)[i][j][k].a33 = (*uv)[i][j][k].a33/(2.0*kk)-1.0/3.0;
			}
}
double calculateIntegralShearStress ( int nx, int nz, int j, double re,
																			struct field * const mean)
{
	int i, k;
	double result = 0;

	for(i = 0; i < nx+2; i++)
		for(k = 0; k < nz+2; k++)
			result = mean->strain[i][j][k].a12 + mean->strain[i][j][k].a32;	

	result /= ((double)(nx+2)*(nz+2)*(nx+2)*(nz+2))*re;
	return 4.0*result;	
} 
void lineRMSMeanField   (int nx, int ny, 
												 int nz, enum direction d, // averaging direction
												 const struct field *mean,
												 enum direction e, // which component to choose (<uu>, <vv>, <ww>)
												 double **meanline)
{
	int i,j,k;
	double val;
  if (*meanline != NULL)
		free((void*)*meanline);	
	switch(d)
	{
		case Y:
			*meanline = malloc((ny+2)*sizeof(double));	

			for (j = 0; j < ny+2; j++) 
			{
				val = 0.0;
				for (k = 0; k < nz+2; k++)
					for (i = 0; i < nx+2; i++)
						switch(e)
						{
							case X:
								val += mean->uv[i][j][k].a11;
							break;
							case Y:
								val += mean->uv[i][j][k].a22;
							break;
							case Z:
								val += mean->uv[i][j][k].a33;
							break;
						}
				(*meanline)[j] = sqrt(val/((double)(nx+2)*(nz+2)));
			}
			break;
		case X:
			*meanline = malloc((nx+2)*sizeof(double));	

			for (i = 0; i < nx+2; i++) 
			{
				val = 0.0;
				for (k = 0; k < nz+2; k++)
					for (j = 0; j < ny+2; j++) 
						switch(e)
						{
							case X:
								val += mean->uv[i][j][k].a11;
							break;
							case Y:
								val += mean->uv[i][j][k].a22;
							break;
							case Z:
								val += mean->uv[i][j][k].a33;
							break;
						}
				(*meanline)[i]  = val/((double)(ny+2)*(nz+2));
			}
			break;
		case Z:
			*meanline = malloc((nz+2)*sizeof(double));	

			for  (k = 0; k < nz+2; k++)
			{
				val = 0.0;
				for (i = 0; i < nx+2; i++)
					for (j = 0; j < ny+2; j++) 
						switch(e)
						{
							case X:
								val += mean->uv[i][j][k].a11;
							break;
							case Y:
								val += mean->uv[i][j][k].a22;
							break;
							case Z:
								val += mean->uv[i][j][k].a33;
							break;
						}
				(*meanline)[k]  = val/((double)(ny+2)*(nx+2));
			}
			break;
		default:
			meanline=NULL;

	}
}
void  lineMeanFieldMatr (	int nx, int ny, 
										 			int nz, enum direction d,
													int elem,
										 			double scale,
										 			struct matrix **** const mean,
										 			double **meanline)
{
	int i,j,k;
	double val;

	switch(d)
	{
		case Y:
			*meanline = malloc((ny+2)*sizeof(double));	

			for (j = 0; j < ny+2; j++) 
			{
				val = 0.0;
				for (k = 0; k < nz+2; k++)
					for (i = 0; i < nx+2; i++)
						if     (elem == 0)
							val += (*mean)[i][j][k].a11;
						else if(elem == 1)
							val += (*mean)[i][j][k].a12;
						else if(elem == 2)
							val += (*mean)[i][j][k].a13;
						else if(elem == 3)
							val += (*mean)[i][j][k].a21;
						else if(elem == 4)
							val += (*mean)[i][j][k].a22;
						else if(elem == 5)
							val += (*mean)[i][j][k].a23;
						else if(elem == 6)
							val += (*mean)[i][j][k].a31;
						else if(elem == 7)
							val += (*mean)[i][j][k].a32;
						else if(elem == 8)
							val += (*mean)[i][j][k].a33;
				(*meanline)[j] = scale*val/((double)(nx+2)*(nz+2));
			}
			break;
		case X:
			*meanline = malloc((nx+2)*sizeof(double));	

			for (i = 0; i < nx+2; i++) 
			{
				val = 0.0;
				for (k = 0; k < nz+2; k++)
					for (j = 0; j < ny+2; j++) 
						if     (elem == 0)
							val += (*mean)[i][j][k].a11;
						else if(elem == 1)
							val += (*mean)[i][j][k].a12;
						else if(elem == 2)
							val += (*mean)[i][j][k].a13;
						else if(elem == 3)
							val += (*mean)[i][j][k].a21;
						else if(elem == 4)
							val += (*mean)[i][j][k].a22;
						else if(elem == 5)
							val += (*mean)[i][j][k].a23;
						else if(elem == 6)
							val += (*mean)[i][j][k].a31;
						else if(elem == 7)
							val += (*mean)[i][j][k].a32;
						else if(elem == 8)
							val += (*mean)[i][j][k].a33;
				(*meanline)[i]  = scale*val/((double)(ny+2)*(nz+2));
			}
			break;
		case Z:
			*meanline = malloc((nz+2)*sizeof(double));	

			for  (k = 0; k < nz+2; k++)
			{
				val = 0.0;
				for (i = 0; i < nx+2; i++)
					for (j = 0; j < ny+2; j++) 
						if     (elem == 0)
							val += (*mean)[i][j][k].a11;
						else if(elem == 1)
							val += (*mean)[i][j][k].a12;
						else if(elem == 2)
							val += (*mean)[i][j][k].a13;
						else if(elem == 3)
							val += (*mean)[i][j][k].a21;
						else if(elem == 4)
							val += (*mean)[i][j][k].a22;
						else if(elem == 5)
							val += (*mean)[i][j][k].a23;
						else if(elem == 6)
							val += (*mean)[i][j][k].a31;
						else if(elem == 7)
							val += (*mean)[i][j][k].a32;
						else if(elem == 8)
							val += (*mean)[i][j][k].a33;
				(*meanline)[k]  = scale*val/((double)(ny+2)*(nx+2));
			}
			break;
		default:
			meanline=NULL;

	}
}
void  lineMeanField (int nx, int ny, 
										 int nz, enum direction d,
										 double scale,
										 double ***mean,
										 double **meanline)
{
	int i,j,k;
	double val;

	switch(d)
	{
		case Y:
			*meanline = malloc((ny+2)*sizeof(double));	

			for (j = 0; j < ny+2; j++) 
			{
				val = 0.0;
				for (k = 0; k < nz+2; k++)
					for (i = 0; i < nx+2; i++)
						val += mean[i][j][k];
				(*meanline)[j] = scale*val/((double)(nx+2)*(nz+2));
			}
			break;
		case X:
			*meanline = malloc((nx+2)*sizeof(double));	

			for (i = 0; i < nx+2; i++) 
			{
				val = 0.0;
				for (k = 0; k < nz+2; k++)
					for (j = 0; j < ny+2; j++) 
						val += mean[i][j][k];
				(*meanline)[i]  = scale*val/((double)(ny+2)*(nz+2));
			}
			break;
		case Z:
			*meanline = malloc((nz+2)*sizeof(double));	

			for  (k = 0; k < nz+2; k++)
			{
				val = 0.0;
				for (i = 0; i < nx+2; i++)
					for (j = 0; j < ny+2; j++) 
						val += mean[i][j][k];
				(*meanline)[k]  = scale*val/((double)(ny+2)*(nx+2));
			}
			break;
		default:
			meanline=NULL;

	}
}
void lineShearBalanceMeanField (int nx, int ny, 
												 				int nz, enum direction d,
												 				struct field *const mean,
												 				double **meanline)
{
	int i,j,k;
	double val;

	switch(d)
	{
		case Y:
			*meanline = malloc((ny+2)*sizeof(double));	

			for (j = 0; j < ny+2; j++) 
			{
				val = 0.0;
				for (k = 0; k < nz+2; k++)
					for (i = 0; i < nx+2; i++)
						val -= (mean->deriv[i][j][k].a12/(mean->re[i][j][k]) - mean->uv[i][j][k].a12);
				(*meanline)[j] = val/((double)(nx+2)*(nz+2));
			}
			break;
		case X:
			*meanline = malloc((nx+2)*sizeof(double));	

			for (i = 0; i < nx+2; i++) 
			{
				val = 0.0;
				for (k = 0; k < nz+2; k++)
					for (j = 0; j < ny+2; j++) 
						val -= (mean->deriv[i][j][k].a12/(mean->re[i][j][k]) - mean->uv[i][j][k].a12);
				(*meanline)[i]  = val/((double)(ny+2)*(nz+2));
			}
			break;
		case Z:
			*meanline = malloc((nz+2)*sizeof(double));	

			for  (k = 0; k < nz+2; k++)
			{
				val = 0.0;
				for (i = 0; i < nx+2; i++)
					for (j = 0; j < ny+2; j++) 
						val -= (mean->deriv[i][j][k].a12/(mean->re[i][j][k]) - mean->uv[i][j][k].a12);
				(*meanline)[k]  = val/((double)(ny+2)*(nx+2));
			}
			break;
		default:
			meanline=NULL;

	}
}
void lineShearMeanField (int nx, int ny, 
												 int nz, enum direction d,
												 struct field *const mean,
												 double **meanline)
{
	int i,j,k;
	double val;

	switch(d)
	{
		case Y:
			*meanline = malloc((ny+2)*sizeof(double));	

			for (j = 0; j < ny+2; j++) 
			{
				val = 0.0;
				for (k = 0; k < nz+2; k++)
					for (i = 0; i < nx+2; i++)
						val -= mean->uv[i][j][k].a12;//(mean->deriv[i][j][k].a12/(re) - mean->uv[i][j][k].a12);
				(*meanline)[j] = val/((double)(nx+2)*(nz+2));
			}
			break;
		case X:
			*meanline = malloc((nx+2)*sizeof(double));	

			for (i = 0; i < nx+2; i++) 
			{
				val = 0.0;
				for (k = 0; k < nz+2; k++)
					for (j = 0; j < ny+2; j++) 
						val -=  mean->uv[i][j][k].a12;//mean->deriv[i][j][k].a12/(re) - mean->uv[i][j][k].a12;
				(*meanline)[i]  = val/((double)(ny+2)*(nz+2));
			}
			break;
		case Z:
			*meanline = malloc((nz+2)*sizeof(double));	

			for  (k = 0; k < nz+2; k++)
			{
				val = 0.0;
				for (i = 0; i < nx+2; i++)
					for (j = 0; j < ny+2; j++) 
						val -= mean->uv[i][j][k].a12; //mean->deriv[i][j][k].a12/(re) - mean->uv[i][j][k].a12;
				(*meanline)[k]  = val/((double)(ny+2)*(nx+2));
			}
			break;
		default:
			meanline=NULL;

	}
}
void lineUMeanField (int nx, int ny, 
										 int nz, enum direction d,
										 const struct field *mean,
										 double **meanline)
{
	int i,j,k;
	double val;

	switch(d)
	{
		case Y:
			*meanline = malloc((ny+2)*sizeof(double));	

			for (j = 0; j < ny+2; j++) 
			{
				val = 0.0;
				for (k = 0; k < nz+2; k++)
					for (i = 0; i < nx+2; i++)
						val += mean->u[i][j][k];
				(*meanline)[j] = val/((double)(nx+2)*(nz+2));
			}
			break;
		case X:
			*meanline = malloc((nx+2)*sizeof(double));	

			for (i = 0; i < nx+2; i++) 
			{
				val = 0.0;
				for (k = 0; k < nz+2; k++)
					for (j = 0; j < ny+2; j++) 
						val += mean->u[i][j][k];
				(*meanline)[i]  = val/((double)(ny+2)*(nz+2));
			}
			break;
		case Z:
			*meanline = malloc((nz+2)*sizeof(double));	

			for  (k = 0; k < nz+2; k++)
			{
				val = 0.0;
				for (i = 0; i < nx+2; i++)
					for (j = 0; j < ny+2; j++) 
						val += mean->u[i][j][k];
				(*meanline)[k]  = val/((double)(ny+2)*(nx+2));
			}
			break;
		default:
			meanline=NULL;

	}
}
void meanField(int ntime, 
							 int nx,
							 int ny,
							 int nz, int nscal,
							 const struct field *f, 
							 struct field *campo) // this one is assumed to be undefined
{
	const struct field *fin = f + ntime;
	int i, j, k, isc;
	struct matrix uvtemp;
// start allocation of variables
	campo->u = malloc ((nx+2) * sizeof(*campo->u));
	for (i = 0; i < nx+2; i++)
	{
		campo->u[i] = malloc((ny+2)*sizeof(**campo->u));
		for (j = 0; j < ny + 2; j++)
			campo->u[i][j] = malloc((nz+2)*sizeof(***campo->u));
	}
	campo->v = malloc ((nx+2) * sizeof(*campo->v));
	for (i = 0; i < nx+2; i++)
	{
		campo->v[i] = malloc((ny+2)*sizeof(**campo->v));
		for (j = 0; j < ny + 2; j++)
			campo->v[i][j] = malloc((nz+2)*sizeof(***campo->v));
	}
	campo->w = malloc ((nx+2) * sizeof(*campo->w));
	for (i = 0; i < nx+2; i++)
	{
		campo->w[i] = malloc((ny+2)*sizeof(**campo->w));
		for (j = 0; j < ny + 2; j++)
			campo->w[i][j] = malloc((nz+2)*sizeof(***campo->w));
	}
	campo->fi = malloc ((nx+2) * sizeof(*campo->fi));
	for (i = 0; i < nx+2; i++)
	{
		campo->fi[i] = malloc((ny+2)*sizeof(**campo->fi));
		for (j = 0; j < ny + 2; j++)
			campo->fi[i][j] = malloc((nz+2)*sizeof(***campo->fi));
	}
	campo->re = malloc ((nx+2) * sizeof(*campo->re));
	for (i = 0; i < nx+2; i++)
	{
		campo->re[i] = malloc((ny+2)*sizeof(**campo->re));
		for (j = 0; j < ny + 2; j++)
			campo->re[i][j] = malloc((nz+2)*sizeof(***campo->re));
	}
// THESE VALUES ARE USUALLY NOT NEEDED IN THE MEAN FIELD
/*
	campo->uc = malloc ((nx+1) * sizeof(*campo->uc));
	for (i = 0; i < nx+1; i++)
	{
		campo->uc[i] = malloc((ny)*sizeof(**campo->uc));
		for (j = 0; j < ny; j++)
			campo->uc[i][j] = malloc((nz)*sizeof(***campo->uc));
	}
	campo->vc = malloc ((nx) * sizeof(*campo->vc));
	for (i = 0; i < nx; i++)
	{
		campo->vc[i] = malloc((ny+1)*sizeof(**campo->vc));
		for (j = 0; j < ny+1; j++)
			campo->vc[i][j] = malloc((nz)*sizeof(***campo->vc));
	}
	campo->wc = malloc ((nx) * sizeof(*campo->wc));
	for (i = 0; i < nx; i++)
	{
		campo->wc[i] = malloc((ny)*sizeof(**campo->wc));
		for (j = 0; j < ny; j++)
			campo->wc[i][j] = malloc((nz+1)*sizeof(***campo->wc));
	}
	campo->div = malloc ((nx+2) * sizeof(*campo->div));
	for (i = 0; i < nx+2; i++)
	{
		campo->div[i] = malloc((ny+2)*sizeof(**campo->div));
		for (j = 0; j < ny + 2; j++)
			campo->div[i][j] = malloc((nz+2)*sizeof(***campo->div));
	}
*/

// allocate rho
	campo->rho = malloc (nscal * sizeof(*campo->rho));
	for (isc=0; isc < (nscal); isc++)
	{
		campo->rho[isc] = malloc((nx+2)*sizeof(**campo->rho));
		for (i = 0; i < nx+2; i++)
		{
			campo->rho[isc][i] = malloc((ny+2)*sizeof(***campo->rho));
			for (j = 0; j < ny+2; j++)
				campo->rho[isc][i][j] = malloc((nz+2)*sizeof(****campo->rho));
		}		
	}

	campo->uv = malloc ((nx+2) * sizeof(*campo->uv));
	for (i = 0; i < nx+2; i++)
	{
		campo->uv[i] = malloc((ny+2)*sizeof(**campo->uv));
		for (j = 0; j < ny + 2; j++)
			campo->uv[i][j] = malloc((nz+2)*sizeof(***campo->uv));
	}

	uvtemp.a11=0.0;
	uvtemp.a12=0.0;
	uvtemp.a13=0.0;
	uvtemp.a21=0.0;
	uvtemp.a22=0.0;
	uvtemp.a23=0.0;
	uvtemp.a31=0.0;
	uvtemp.a32=0.0;
	uvtemp.a33=0.0;
// initialize to zero
	for (k = 0; k < nz+2; k++)
		for (j = 0; j < ny+2; j++)
			for (i = 0; i < nx+2; i++)
			{
				campo->u[i][j][k] = 0.0;
				campo->v[i][j][k] = 0.0;
				campo->w[i][j][k] = 0.0;
				//campo->div[i][j][k] = 0.0;
				campo->fi[i][j][k] = 0.0;
				campo->re[i][j][k] = 0.0;
				for (isc = 0; isc < (nscal); isc++)
				{
					campo->rho[isc][i][j][k] = 0.0;
				}
				campo->uv[i][j][k] = uvtemp;
			}
/*
	for (k = 0; k < nz; k++)
		for (j = 0; j < ny; j++)
			for (i = 0; i < nx + 1; i++)
				campo->uc[i][j][k] = 0.0;
	for (k = 0; k < nz; k++)
		for (j = 0; j < ny + 1; j++)
			for (i = 0; i < nx; i++)
				campo->vc[i][j][k] = 0.0;
	for (k = 0; k < nz + 1; k++)
		for (j = 0; j < ny; j++)
			for (i = 0; i < nx; i++)
				campo->wc[i][j][k] = 0.0;
*/
	// Now, the mean
	while(f < fin)
	{
		for (k = 0; k < nz+2; k++)
			for (j = 0; j < ny+2; j++)
				for (i = 0; i < nx+2; i++)
				{
					campo->u[i][j][k] 	+= (1.0/((double)ntime))*f->u[i][j][k];
					campo->v[i][j][k] 	+= (1.0/((double)ntime))*f->v[i][j][k];
					campo->w[i][j][k] 	+= (1.0/((double)ntime))*f->w[i][j][k];
//					campo->div[i][j][k] += (1.0/((double)ntime))*f->div[i][j][k];
					campo->fi[i][j][k]  += (1.0/((double)ntime))*f->fi[i][j][k];
					campo->re[i][j][k]  += (1.0/((double)ntime))*f->re[i][j][k];
					for (isc = 0; isc < (nscal); isc++)
					{
						campo->rho[isc][i][j][k] += (1.0/((double)ntime))*f->rho[isc][i][j][k];
					}
				}
/*
		for (k = 0; k < nz; k++)
			for (j = 0; j < ny; j++)
				for (i = 0; i < nx + 1; i++)
					campo->uc[i][j][k] 	+= (1.0/((double)ntime))*f->uc[i][j][k];
		for (k = 0; k < nz; k++)
			for (j = 0; j < ny + 1; j++)
				for (i = 0; i < nx; i++)
					campo->vc[i][j][k] 	+= (1.0/((double)ntime))*f->vc[i][j][k];
		for (k = 0; k < nz + 1; k++)
			for (j = 0; j < ny; j++)
				for (i = 0; i < nx; i++)
					campo->wc[i][j][k] 	+= (1.0/((double)ntime))*f->wc[i][j][k];
*/
		++f;
	}
}
/* NOT A GOOD IDEA
void calculateShearProfile(int nx,
													 int ny,
												   int nz,
													 double re,
												   struct field* campo)
{
	int i, j, k;
	double uvmean[ny+2];

	if (campo->S == NULL)
		campo->S = calloc(ny+2,sizeof(*campo->S));
	else 
		return;
	for (j = 0; j < ny+2; j++)
	{
		uvmean[j] = 0.0;
		for (k = 0; k < nz+2; k++)
			for (i = 0; i < nx+2; i++)
			{
				uvmean[j] += campo->uv[i][j][k].a12;
				campo->S[j] += campo->deriv[i][j][k].a12;					
			}
		uvmean[j] /= ((double)(nx+2)*(ny+2)); // plane average
		campo->S[j] /= -((double)(nx+2)*(ny+2))*re; // plane average 
		campo->S[j] += uvmean[j]; // Normalized mean shear profile
	}
}
void calculateShearRate(int nx,
												int ny,
												int nz,
												struct field* campo)
{
	int i, j, k;
	double uvmean[ny+2], emean[ny+2];

	if (campo->S == NULL)
		campo->S = calloc(ny+2,sizeof(*campo->S));
	else 
		return;
	for (j = 0; j < ny+2; j++)
	{
		uvmean[j] = 0.0;
		emean[j]  = 0.0;
		for (k = 0; k < nz+2; k++)
			for (i = 0; i < nx+2; i++)
			{
				uvmean[j] += campo->uv[i][j][k].a12;
				emean[j]  += campo->dissip[i][j][k];
				campo->S[j] += campo->deriv[i][j][k].a12;					
			}
		uvmean[j] /= ((double)(nx+2)*(ny+2)); // plane average
		emean[j]  /= ((double)(nx+2)*(ny+2)); // plane average  
		campo->S[j] /= ((double)(nx+2)*(ny+2)); // plane average 
		campo->S[j] *= fabs(uvmean[j])/emean[j]; // Normalized shear Rate S-TILDE
	}
}
*/
void vorticityCorrelation(int ntime, 
                   				int nx,
                   				int ny,
                   				int nz, 
                   				struct field* campo,
                   				struct field* mean) 
{
	const struct field *fin = campo + ntime;
	int i, j, k, ii;

	mean->ww = calloc ((nx+2), sizeof(*mean->ww));
	for (i = 0; i < nx+2; i++)
	{
		mean->ww[i] = calloc((ny+2),sizeof(**mean->ww));
		for (j = 0; j < ny+2; j++)
			mean->ww[i][j] = calloc((nz+2),sizeof(***mean->ww));
	}
	mean->wwS = calloc ((nx+2), sizeof(*mean->wwS));
	for (i = 0; i < nx+2; i++)
	{
		mean->wwS[i] = calloc((ny+2),sizeof(**mean->wwS));
		for (j = 0; j < ny+2; j++)
			mean->wwS[i][j] = calloc((nz+2),sizeof(***mean->wwS));
	}
	for (k = 0; k < nz+2; k++)
		for (j = 0; j < ny+2; j++)
			for (i = 0; i < nx+2; i++)
			{
				mean->ww[i][j][k].a11 = 0.; 
				mean->ww[i][j][k].a12 = 0.;
				mean->ww[i][j][k].a13 = 0.;
				mean->ww[i][j][k].a22 = 0.;
				mean->ww[i][j][k].a23 = 0.;
				mean->ww[i][j][k].a33 = 0.;
				mean->ww[i][j][k].a21 = 0.;
				mean->ww[i][j][k].a31 = 0.;
				mean->ww[i][j][k].a32 = 0.;
			}

	while(campo < fin)
	{
		for (k = 0; k < nz+2; k++)
			for (j = 0; j < ny+2; j++)
				for (i = 0; i < nx+2; i++)
				{
					mean->ww[i][j][k].a11 += (4./((double)ntime))*campo->skew_strain[i][j][k].a23*campo->skew_strain[i][j][k].a23;
					mean->ww[i][j][k].a12 += (4./((double)ntime))*campo->skew_strain[i][j][k].a23*campo->skew_strain[i][j][k].a13;
					mean->ww[i][j][k].a13 += (4./((double)ntime))*campo->skew_strain[i][j][k].a23*campo->skew_strain[i][j][k].a12;
					mean->ww[i][j][k].a22 += (4./((double)ntime))*campo->skew_strain[i][j][k].a13*campo->skew_strain[i][j][k].a13;
					mean->ww[i][j][k].a23 += (4./((double)ntime))*campo->skew_strain[i][j][k].a13*campo->skew_strain[i][j][k].a12;
					mean->ww[i][j][k].a33 += (4./((double)ntime))*campo->skew_strain[i][j][k].a12*campo->skew_strain[i][j][k].a12;
					// vortex stretching
					mean->wwS[i][j][k]   += (4./((double)ntime))*campo->skew_strain[i][j][k].a23*campo->skew_strain[i][j][k].a23*campo->strain[i][j][k].a11
															  + (8./((double)ntime))*campo->skew_strain[i][j][k].a23*campo->skew_strain[i][j][k].a13*campo->strain[i][j][k].a12
					 										  + (8./((double)ntime))*campo->skew_strain[i][j][k].a23*campo->skew_strain[i][j][k].a12*campo->strain[i][j][k].a13
					                      + (4./((double)ntime))*campo->skew_strain[i][j][k].a13*campo->skew_strain[i][j][k].a13*campo->strain[i][j][k].a22
					                      + (8./((double)ntime))*campo->skew_strain[i][j][k].a13*campo->skew_strain[i][j][k].a12*campo->strain[i][j][k].a23
					                      + (8./((double)ntime))*campo->skew_strain[i][j][k].a12*campo->skew_strain[i][j][k].a12*campo->strain[i][j][k].a33;
				}
		campo++;
	}
		for (k = 0; k < nz+2; k++)
			for (j = 0; j < ny+2; j++)
				for (i = 0; i < nx+2; i++)
				{
					mean->ww[i][j][k].a21  = -mean->ww[i][j][k].a12;
					mean->ww[i][j][k].a31  = -mean->ww[i][j][k].a13;
					mean->ww[i][j][k].a32  = -mean->ww[i][j][k].a23;
				}
}
void pressureStrain(int ntime, 
                   	int nx,
                   	int ny,
                   	int nz, 
                   	struct field* campo,
                   	struct field* mean) 
{
	const struct field *fin = campo + ntime;
	int i, j, k, ii;

	mean->pressureStrain = calloc ((nx+2), sizeof(*mean->pressureStrain));
	for (i = 0; i < nx+2; i++)
	{
		mean->pressureStrain[i] = calloc((ny+2),sizeof(**mean->pressureStrain));
		for (j = 0; j < ny+2; j++)
			mean->pressureStrain[i][j] = calloc((nz+2),sizeof(***mean->pressureStrain));
	}
	for (k = 0; k < nz+2; k++)
		for (j = 0; j < ny+2; j++)
			for (i = 0; i < nx+2; i++)
			{
				mean->pressureStrain[i][j][k].a11 = 0.; 
				mean->pressureStrain[i][j][k].a12 = 0.;
				mean->pressureStrain[i][j][k].a13 = 0.;
				mean->pressureStrain[i][j][k].a22 = 0.;
				mean->pressureStrain[i][j][k].a23 = 0.;
				mean->pressureStrain[i][j][k].a33 = 0.;
				mean->pressureStrain[i][j][k].a21 = 0.;
				mean->pressureStrain[i][j][k].a31 = 0.;
				mean->pressureStrain[i][j][k].a32 = 0.;
			}

	while(campo < fin)
	{
		for (k = 0; k < nz+2; k++)
			for (j = 0; j < ny+2; j++)
				for (i = 0; i < nx+2; i++)
				{
					mean->pressureStrain[i][j][k].a11 += (1./((double)ntime))*campo->fi[i][j][k]*campo->strain[i][j][k].a11;
					mean->pressureStrain[i][j][k].a12 += (1./((double)ntime))*campo->fi[i][j][k]*campo->strain[i][j][k].a12;
					mean->pressureStrain[i][j][k].a13 += (1./((double)ntime))*campo->fi[i][j][k]*campo->strain[i][j][k].a13;
					mean->pressureStrain[i][j][k].a22 += (1./((double)ntime))*campo->fi[i][j][k]*campo->strain[i][j][k].a22;
					mean->pressureStrain[i][j][k].a23 += (1./((double)ntime))*campo->fi[i][j][k]*campo->strain[i][j][k].a23;
					mean->pressureStrain[i][j][k].a33 += (1./((double)ntime))*campo->fi[i][j][k]*campo->strain[i][j][k].a33;
				}
		campo++;
	}
		for (k = 0; k < nz+2; k++)
			for (j = 0; j < ny+2; j++)
				for (i = 0; i < nx+2; i++)
				{
					mean->pressureStrain[i][j][k].a21  = mean->pressureStrain[i][j][k].a12;
					mean->pressureStrain[i][j][k].a31  = mean->pressureStrain[i][j][k].a13;
					mean->pressureStrain[i][j][k].a32  = mean->pressureStrain[i][j][k].a23;
				}
}


void perturbations(	int ntime, 
							 			int nx,
							 			int ny,
							 			int nz, int nscal,
										struct field* campo,// this arg is the instantaneous field, which is converted to only perturbations
										struct field* mean) 
{
	const struct field *fin = campo + ntime;
	int i, j, k, l, isc;
// allocation of things
	for(l = 0; l < 3; ++l)
	{
		mean->turb_t_vec[l] = calloc ((nx+2), sizeof(**mean->turb_t_vec));
		for (i = 0; i < nx+2; i++)
		{
			mean->turb_t_vec[l][i] = calloc((ny+2),sizeof(***mean->turb_t_vec));
			for (j = 0; j < ny+2; j++)
				mean->turb_t_vec[l][i][j] = calloc((nz+2),sizeof(****mean->turb_t_vec));
		}
	}

	mean->anisotropy = calloc ((nx+2), sizeof(*mean->anisotropy));
	for (i = 0; i < nx+2; i++)
	{
		mean->anisotropy[i] = calloc((ny+2),sizeof(**mean->anisotropy));
		for (j = 0; j < ny+2; j++)
			mean->anisotropy[i][j] = calloc((nz+2),sizeof(***mean->anisotropy));
	}
	mean->k = calloc ((nx+2), sizeof(*mean->k));
	for (i = 0; i < nx+2; i++)
	{
		mean->k[i] = calloc((ny+2),sizeof(**mean->k));
		for (j = 0; j < ny+2; j++)
			mean->k[i][j] = calloc((nz+2),sizeof(***mean->k));
	}
	mean->RhoU = calloc ((nx+2), sizeof(*mean->RhoU));
	for (i = 0; i < nx+2; i++)
	{
		mean->RhoU[i] = calloc((ny+2),sizeof(**mean->RhoU));
		for (j = 0; j < ny+2; j++)
			mean->RhoU[i][j] = calloc((nz+2),sizeof(***mean->RhoU));
	}
	mean->RhoV = calloc ((nx+2), sizeof(*mean->RhoV));
	for (i = 0; i < nx+2; i++)
	{
		mean->RhoV[i] = calloc((ny+2),sizeof(**mean->RhoV));
		for (j = 0; j < ny+2; j++)
			mean->RhoV[i][j] = calloc((nz+2),sizeof(***mean->RhoV));
	}
	mean->RhoW = calloc ((nx+2), sizeof(*mean->RhoW));
	for (i = 0; i < nx+2; i++)
	{
		mean->RhoW[i] = calloc((ny+2),sizeof(**mean->RhoW));
		for (j = 0; j < ny+2; j++)
			mean->RhoW[i][j] = calloc((nz+2),sizeof(***mean->RhoW));
	}
	init_3dArray(nx,ny,nz,&(mean->kurtosisU)); 	
	init_3dArray(nx,ny,nz,&(mean->skewnessU)); 	
	init_3dArray(nx,ny,nz,&(mean->kurtosisV)); 	
	init_3dArray(nx,ny,nz,&(mean->skewnessV)); 	
	init_3dArray(nx,ny,nz,&(mean->kurtosisW)); 	
	init_3dArray(nx,ny,nz,&(mean->skewnessW)); 	
/*
	for ( l = 0; l < 4; l++)
		mean->quadrants[l] = calloc((ny+2),sizeof(**mean->quadrants));
*/
// here we start allocations in the perturbation fields
	while(campo < fin)
	{
		campo->anisotropy = calloc ((nx+2), sizeof(*campo->anisotropy));
		for (i = 0; i < nx+2; i++)
		{
			campo->anisotropy[i] = calloc((ny+2),sizeof(**campo->anisotropy));
			for (j = 0; j < ny+2; j++)
				campo->anisotropy[i][j] = calloc((nz+2),sizeof(***campo->anisotropy));
		}
		campo->k = malloc ((nx+2) * sizeof(*campo->k));
		for (i = 0; i < nx+2; i++)
		{
			campo->k[i] = malloc((ny+2)*sizeof(**campo->k));
			for (j = 0; j < ny+2; j++)
				campo->k[i][j] = malloc((nz+2)*sizeof(***campo->k));
		}

		campo->uv = calloc ((nx+2), sizeof(*campo->uv));
		for (i = 0; i < nx+2; i++)
		{
			campo->uv[i] = calloc((ny+2),sizeof(**campo->uv));
			for (j = 0; j < ny+2; j++)
				campo->uv[i][j] = calloc((nz+2),sizeof(***campo->uv));
		}
		campo->RhoU = calloc ((nx+2), sizeof(*campo->RhoU));
		for (i = 0; i < nx+2; i++)
		{
			campo->RhoU[i] = calloc((ny+2),sizeof(**campo->RhoU));
			for (j = 0; j < ny+2; j++)
				campo->RhoU[i][j] = calloc((nz+2),sizeof(***campo->RhoU));
		}
		campo->RhoV = calloc ((nx+2), sizeof(*campo->RhoV));
		for (i = 0; i < nx+2; i++)
		{
			campo->RhoV[i] = calloc((ny+2),sizeof(**campo->RhoV));
			for (j = 0; j < ny+2; j++)
				campo->RhoV[i][j] = calloc((nz+2),sizeof(***campo->RhoV));
		}
		campo->RhoW = calloc ((nx+2), sizeof(*campo->RhoW));
		for (i = 0; i < nx+2; i++)
		{
			campo->RhoW[i] = calloc((ny+2),sizeof(**campo->RhoW));
			for (j = 0; j < ny+2; j++)
				campo->RhoW[i][j] = calloc((nz+2),sizeof(***campo->RhoW));
		}
	  init_3dArray(nx,ny,nz,&(campo->kurtosisU)); 	
	  init_3dArray(nx,ny,nz,&(campo->skewnessU)); 	
	  init_3dArray(nx,ny,nz,&(campo->kurtosisV)); 	
	  init_3dArray(nx,ny,nz,&(campo->skewnessV)); 	
	  init_3dArray(nx,ny,nz,&(campo->kurtosisW)); 	
	  init_3dArray(nx,ny,nz,&(campo->skewnessW)); 	
// end of allocation and stuff
// START OF SEVERAL CALCULATIONS AND STUFF

		for (k = 0; k < nz+2; k++)
			for (j = 0; j < ny+2; j++)
				for (i = 0; i < nx+2; i++)
				{
					campo->u[i][j][k] 	= campo->u[i][j][k] - mean->u[i][j][k];	
					campo->v[i][j][k] 	= campo->v[i][j][k] - mean->v[i][j][k];
					campo->w[i][j][k] 	= campo->w[i][j][k] - mean->w[i][j][k];	
//					campo->div[i][j][k] = campo->div[i][j][k] - mean->div[i][j][k];
					campo->fi[i][j][k]  = campo->fi[i][j][k] - mean->fi[i][j][k];
					campo->re[i][j][k]  = campo->re[i][j][k] - mean->re[i][j][k];
					for (isc = 0; isc < (nscal); isc++)
					{
						campo->rho[isc][i][j][k] = campo->rho[isc][i][j][k]-mean->rho[isc][i][j][k];
					}
					// REYNOLDS STRESS TENSOR
					campo->uv[i][j][k].a11 = campo->u[i][j][k]*campo->u[i][j][k];
					campo->uv[i][j][k].a12 = campo->u[i][j][k]*campo->v[i][j][k];
					campo->uv[i][j][k].a13 = campo->u[i][j][k]*campo->w[i][j][k];
					campo->uv[i][j][k].a23 = campo->v[i][j][k]*campo->w[i][j][k];
					campo->uv[i][j][k].a22 = campo->v[i][j][k]*campo->v[i][j][k];
					campo->uv[i][j][k].a33 = campo->w[i][j][k]*campo->w[i][j][k];
					campo->uv[i][j][k].a21 =	campo->uv[i][j][k].a12;
					campo->uv[i][j][k].a31 =	campo->uv[i][j][k].a13;
					campo->uv[i][j][k].a32 =	campo->uv[i][j][k].a23;

					campo->RhoU[i][j][k] = campo->u[i][j][k]*campo->rho[0][i][j][k];
					campo->RhoV[i][j][k] = campo->v[i][j][k]*campo->rho[0][i][j][k];
					campo->RhoW[i][j][k] = campo->w[i][j][k]*campo->rho[0][i][j][k];

					campo->k[i][j][k] = 0.5	*	matrixTrace(campo->uv[i][j][k]);

					campo->skewnessU[i][j][k] = campo->u[i][j][k]*campo->u[i][j][k]*campo->u[i][j][k];
					campo->kurtosisU[i][j][k] = campo->u[i][j][k]*campo->u[i][j][k]*campo->u[i][j][k]*campo->u[i][j][k];
					campo->skewnessV[i][j][k] = campo->v[i][j][k]*campo->v[i][j][k]*campo->v[i][j][k];
					campo->kurtosisV[i][j][k] = campo->v[i][j][k]*campo->v[i][j][k]*campo->v[i][j][k]*campo->v[i][j][k];
					campo->skewnessW[i][j][k] = campo->w[i][j][k]*campo->w[i][j][k]*campo->w[i][j][k];
					campo->kurtosisW[i][j][k] = campo->w[i][j][k]*campo->w[i][j][k]*campo->w[i][j][k]*campo->w[i][j][k];
					// ANISOTROPY TENSOR
					if (campo->k[i][j][k] != 0.0)
					{
						campo->anisotropy[i][j][k].a11 = campo->uv[i][j][k].a11/(2.0*campo->k[i][j][k])-1.0/3.0;
						campo->anisotropy[i][j][k].a12 = campo->uv[i][j][k].a12/(2.0*campo->k[i][j][k]);
						campo->anisotropy[i][j][k].a13 = campo->uv[i][j][k].a13/(2.0*campo->k[i][j][k]);
						campo->anisotropy[i][j][k].a23 = campo->uv[i][j][k].a23/(2.0*campo->k[i][j][k]);
						campo->anisotropy[i][j][k].a22 = campo->uv[i][j][k].a22/(2.0*campo->k[i][j][k])-1.0/3.0;
						campo->anisotropy[i][j][k].a33 = campo->uv[i][j][k].a33/(2.0*campo->k[i][j][k])-1.0/3.0;
					}
					else if (campo->k[i][j][k] == 0.0)
					// IF IN A WALL, RETURN A NULL MATRIX, WICH YIELDS A NULL SPACE AND RETURNS HAS PERFECTLY ISOTROPIC ZERO-STATE
					{
						campo->anisotropy[i][j][k].a11 = 0.0;
						campo->anisotropy[i][j][k].a12 = 0.0;
						campo->anisotropy[i][j][k].a13 = 0.0;
						campo->anisotropy[i][j][k].a23 = 0.0;
						campo->anisotropy[i][j][k].a22 = 0.0;
						campo->anisotropy[i][j][k].a33 = 0.0;
					}
					campo->anisotropy[i][j][k].a21 =	campo->anisotropy[i][j][k].a12;
					campo->anisotropy[i][j][k].a31 =	campo->anisotropy[i][j][k].a13;
					campo->anisotropy[i][j][k].a32 =	campo->anisotropy[i][j][k].a23;
/*
					campo->turb_t_vec[0][i][j][k] = campo->uv[i][j][k].a11*campo->u[i][j][k]
																				 +campo->uv[i][j][k].a22*campo->u[i][j][k]
																				 +campo->uv[i][j][k].a33*campo->u[i][j][k];

					campo->turb_t_vec[1][i][j][k] = campo->uv[i][j][k].a11*campo->v[i][j][k]
																				 +campo->uv[i][j][k].a22*campo->v[i][j][k]
																				 +campo->uv[i][j][k].a33*campo->v[i][j][k];

					campo->turb_t_vec[2][i][j][k] = campo->uv[i][j][k].a11*campo->w[i][j][k]
																				 +campo->uv[i][j][k].a22*campo->w[i][j][k]
																				 +campo->uv[i][j][k].a33*campo->w[i][j][k];
*/
					mean->uv[i][j][k].a11 += (1.0/((double)ntime))*campo->uv[i][j][k].a11;
					mean->uv[i][j][k].a12 += (1.0/((double)ntime))*campo->uv[i][j][k].a12;
					mean->uv[i][j][k].a13 += (1.0/((double)ntime))*campo->uv[i][j][k].a13;
					mean->uv[i][j][k].a21 += (1.0/((double)ntime))*campo->uv[i][j][k].a21;
					mean->uv[i][j][k].a22 += (1.0/((double)ntime))*campo->uv[i][j][k].a22;
					mean->uv[i][j][k].a23 += (1.0/((double)ntime))*campo->uv[i][j][k].a23;
					mean->uv[i][j][k].a31 += (1.0/((double)ntime))*campo->uv[i][j][k].a31;
					mean->uv[i][j][k].a32 += (1.0/((double)ntime))*campo->uv[i][j][k].a32;
					mean->uv[i][j][k].a33 += (1.0/((double)ntime))*campo->uv[i][j][k].a33;

					mean->RhoU[i][j][k] += (1.0/((double)ntime))*campo->RhoU[i][j][k] ;
					mean->RhoV[i][j][k] += (1.0/((double)ntime))*campo->RhoV[i][j][k] ;
					mean->RhoW[i][j][k] += (1.0/((double)ntime))*campo->RhoW[i][j][k] ;
/*
					mean->turb_t_vec[0][i][j][k] += (1.0/((double)ntime))*campo->turb_t_vec[0][i][j][k];
					mean->turb_t_vec[1][i][j][k] += (1.0/((double)ntime))*campo->turb_t_vec[1][i][j][k];
					mean->turb_t_vec[2][i][j][k] += (1.0/((double)ntime))*campo->turb_t_vec[2][i][j][k];
*/
					mean->k[i][j][k] += (1.0/((double)ntime))*campo->k[i][j][k];																			 
					// ANISOTROPY TENSOR
					mean->anisotropy[i][j][k].a11 += (1.0/((double)ntime))*campo->anisotropy[i][j][k].a11;
					mean->anisotropy[i][j][k].a12 += (1.0/((double)ntime))*campo->anisotropy[i][j][k].a12;
					mean->anisotropy[i][j][k].a13 += (1.0/((double)ntime))*campo->anisotropy[i][j][k].a13;
					mean->anisotropy[i][j][k].a23 += (1.0/((double)ntime))*campo->anisotropy[i][j][k].a23;
					mean->anisotropy[i][j][k].a22 += (1.0/((double)ntime))*campo->anisotropy[i][j][k].a22;
					mean->anisotropy[i][j][k].a33 += (1.0/((double)ntime))*campo->anisotropy[i][j][k].a33;
					mean->anisotropy[i][j][k].a21 += (1.0/((double)ntime))*campo->anisotropy[i][j][k].a21;
					mean->anisotropy[i][j][k].a31 += (1.0/((double)ntime))*campo->anisotropy[i][j][k].a31;
					mean->anisotropy[i][j][k].a32 += (1.0/((double)ntime))*campo->anisotropy[i][j][k].a32;
																							 
					mean->skewnessU[i][j][k] += (1.0/((double)ntime))*campo->skewnessU[i][j][k];
					mean->kurtosisU[i][j][k] += (1.0/((double)ntime))*campo->kurtosisU[i][j][k];
					mean->skewnessV[i][j][k] += (1.0/((double)ntime))*campo->skewnessV[i][j][k];
					mean->kurtosisV[i][j][k] += (1.0/((double)ntime))*campo->kurtosisV[i][j][k];
					mean->skewnessW[i][j][k] += (1.0/((double)ntime))*campo->skewnessW[i][j][k];
					mean->kurtosisW[i][j][k] += (1.0/((double)ntime))*campo->kurtosisW[i][j][k];
				}
/*
		for (k = 0; k < nz; k++)
			for (j = 0; j < ny; j++)
				for (i = 0; i < nx + 1; i++)
					campo->uc[i][j][k] 	= campo->uc[i][j][k] - mean->uc[i][j][k];	
		for (k = 0; k < nz; k++)
			for (j = 0; j < ny + 1; j++)
				for (i = 0; i < nx; i++)
					campo->vc[i][j][k] 	= campo->vc[i][j][k] - mean->vc[i][j][k]; 	
		for (k = 0; k < nz + 1; k++)
			for (j = 0; j < ny; j++)
				for (i = 0; i < nx; i++)
					campo->wc[i][j][k] 	= campo->wc[i][j][k] - mean->wc[i][j][k];	
*/
		++campo;
	}
}
void quadrantAnalysis ( int npert,
												int nx,
												int ny,
												int nz,
												struct field* campo,// this arg comprises the perturbation fields
												struct field* mean)
{
	int l, i, j, k, t;
	double quad_temp[4][ny+2], uvmean[ny+2], temp[4];
	int tot,tot1[4];
	const struct field *fin = campo + npert;

	for ( l = 0; l < 4; l++)
		mean->quadrants[l] = calloc((ny+2),sizeof(**mean->quadrants));

	tot = (nz+2)*(nx+2);
	for (j = 0; j < ny+2; j++)
	{
		uvmean[j] = 0.0;
		for (k = 0; k < nz+2; k++)
			for (i = 0; i < nx+2; i++)
				uvmean[j] += mean->uv[i][j][k].a12;
		uvmean[j] /= ((double)tot); // plane average of time average 
	}
	for (j = 0; j < ny+2; j++) 
		for (l = 0; l < 4; ++l)
			quad_temp[l][j] = 0.0;

	while(campo < fin)
	{
		for (j = 0; j < ny+2; j++) 
		{
			for (l = 0; l < 4; ++l){
				tot1[l] = 0;
				temp[l] = 0;
			}

/*
			uvmean[j] = 0.0;
			for (k = 0; k < nz+2; k++)
				for (i = 0; i < nx+2; i++)
					uvmean[j] += campo->u[i][j][k]*campo->v[i][j][k];
			uvmean[j] /= ((double)tot); // plane average of time average 
*/
			for (k = 0; k < nz+2; k++)
				for (i = 0; i < nx+2; i++)
				{
					if((campo->u[i][j][k] > 0.0) && (campo->v[i][j][k] > 0.0)) // Q1
					{
						temp[0] += campo->u[i][j][k]*campo->v[i][j][k];
						++tot1[0];
					}
					if((campo->u[i][j][k] < 0.0) && (campo->v[i][j][k] > 0.0)) // Q2
					{
						temp[1] += campo->u[i][j][k]*campo->v[i][j][k];
						++tot1[1];
					}
					if((campo->u[i][j][k] < 0.0) && (campo->v[i][j][k] < 0.0)) // Q3
					{
						temp[2] += campo->u[i][j][k]*campo->v[i][j][k];
						++tot1[2];
					}
					if((campo->u[i][j][k] > 0.0) && (campo->v[i][j][k] < 0.0)) // Q4
					{
						temp[3] += campo->u[i][j][k]*campo->v[i][j][k];
						++tot1[3];
					}
				}
			for (l = 0; l < 4; ++l)
				//quad_temp[l][j] += temp[l]/((double)tot1[l])/((double)tot*uvmean[j]);
				quad_temp[l][j] += temp[l]*((double)tot1[l]/((double)tot))/((double)tot*uvmean[j]);
				//quad_temp[l][j] += temp[l]/((double)tot1[l]);
		}
		++campo;
	}
	// quadrants
	for (l = 0; l < 4; ++l)
		for (j = 0; j < ny+2; j++) // IGNORE BORDERS
			mean->quadrants[l][j] = -quad_temp[l][j]/((double)npert);
}
/*
	while(campo < fin)
	{
		for (l = 0; l < 4; ++l)
		{
			for (j = 0; j < ny+2; j++){
				quad_temp[l][j] = 0.0;
			}
		}
		for (j = 0; j < ny+2; j++) 
		{
			for (l = 0; l < 4; ++l){
				tot1[l] = 0;
				temp[l] = 0;
			}
			for (k = 0; k < nz+2; k++)
				for (i = 0; i < nx+2; i++)
				{
					if(abs(campo->uv[i][j][k].a12/uvmean[j]) < QUADHOLE)
						continue;
					if(campo->u[i][j][k] > 0.0)
					{
						if(campo->v[i][j][k] > 0.0) // first quadrant
						{
							temp[0] += campo->uv[i][j][k].a12;
							++tot1[0];
						}
						else if	(campo->v[i][j][k] < 0.0) // fourth quad (sweeps)
						{
							temp[3] += campo->uv[i][j][k].a12;
							++tot1[3];
						}
					}
					else if	(campo->u[i][j][k] < 0.0)
					{
						if(campo->v[i][j][k] > 0.0) // second quadrant (ejections)
						{
							temp[1] += campo->uv[i][j][k].a12;
							++tot1[1];
						}
						else if	(campo->v[i][j][k] < 0.0) // third quad
						{
							temp[2] += campo->uv[i][j][k].a12;
							++tot1[2];
						}
					}
				}
				for (l = 0; l < 4; ++l)
					quad_temp[l][j] += temp[l]/((double)tot1[l]);
		}
		++campo;
	}	
	for (l = 0; l < 4; ++l)
		for (j = 0; j < ny+2; j++)
			frac[l][j] /= pow((double)tot1[j],(double)npert);
			//frac[l][j] /= tot1[j];
*/			
// end of event counting



											
void perturbationsMeanLineU(	int ntime, 
															int nx,
															int ny,
															int nz, int nscal, enum direction d,
															struct field* campo,// this arg is the instantaneous field, which is converted to only perturbations
															const double* Uline,
															const struct field* mean)

{
	const struct field *fin = campo + ntime;
	int i, j, k, isc;
	const double* Ustart=NULL;

	while(campo < fin)
	{
		switch(d)
		{
			case X:
				for (k = 0; k < nz+2; k++)
					for (j = 0; j < ny+2; j++)
					{
						Ustart=Uline;
						for (i = 0; i < nx+2; i++)
							campo->u[i][j][k] 	= campo->u[i][j][k] - *(Ustart++);//mean->u[i][j][k];
					}
			break;
			case Y:
				for (k = 0; k < nz+2; k++)
					for (i = 0; i < nx+2; i++)
					{ 
						Ustart=Uline;
						for (j = 0; j < ny+2; j++)
							campo->u[i][j][k] 	= campo->u[i][j][k] - *(Ustart++);//mean->u[i][j][k];	
					}
			break;
			case Z:
				for (j = 0; j < ny+2; j++) 
					for (i = 0; i < nx+2; i++) 
					{
						Ustart=Uline;
						for (k = 0; k < nz+2; k++)
							campo->u[i][j][k] 	= campo->u[i][j][k] - *(Ustart++);//mean->u[i][j][k];	
					}
			break;
		}
		for (k = 0; k < nz+2; k++)
			for (j = 0; j < ny+2; j++)
				for (i = 0; i < nx+2; i++)
				{
					campo->v[i][j][k] 	= campo->v[i][j][k] - mean->v[i][j][k];
					campo->w[i][j][k] 	= campo->w[i][j][k] - mean->w[i][j][k];	
					//campo->div[i][j][k] = campo->div[i][j][k] - mean->div[i][j][k];
					campo->fi[i][j][k]  = campo->fi[i][j][k] - mean->fi[i][j][k];
					for (isc = 0; isc < (nscal); isc++)
					{
						campo->rho[isc][i][j][k] = campo->rho[isc][i][j][k]-mean->rho[isc][i][j][k];
					}
				}
/*
		for (k = 0; k < nz; k++)
			for (j = 0; j < ny; j++)
				for (i = 0; i < nx + 1; i++)
					campo->uc[i][j][k] 	= campo->uc[i][j][k] - mean->uc[i][j][k];	
		for (k = 0; k < nz; k++)
			for (j = 0; j < ny + 1; j++)
				for (i = 0; i < nx; i++)
					campo->vc[i][j][k] 	= campo->vc[i][j][k] - mean->vc[i][j][k]; 	
		for (k = 0; k < nz + 1; k++)
			for (j = 0; j < ny; j++)
				for (i = 0; i < nx; i++)
					campo->wc[i][j][k] 	= campo->wc[i][j][k] - mean->wc[i][j][k];	
*/
		++campo;
	}
}
