/* Copyright, 2015, Santiago Lopez Castano
 *=========================================================================
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.

 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

#include <stdlib.h>
#include "stru.h"
#include "derivativeUtil.h"

void calculateFieldDerivatives (int nx, 
																int ny, 
																int nz,
																struct matrix ****jac,
																struct field *campo)
{
	int i, j, k;
	// allocate derivatives tensor !!!!
	if(campo->deriv == NULL)
		init_3dMatrix(nx,ny,nz,&(campo->deriv));
/*
	{
		campo->deriv = malloc ((nx+2) * sizeof(*campo->deriv));
		for (i = 0; i < nx+2; i++)
		{
			campo->deriv[i] = malloc((ny+2)*sizeof(**campo->deriv));
			for (j = 0; j < ny + 2; j++)
				campo->deriv[i][j] = malloc((nz+2)*sizeof(***campo->deriv));
		}
	}
*/
// the interior field is calculated using centered differences
	for(i=1;i<nx+1;i++)
		for(j=1;j<ny+1;j++)
			for(k=1;k<nz+1;k++)
			{
					// du/dx, du/dy, du/dz
					campo->deriv[i][j][k].a11= (*jac)[i][j][k].a11*0.5*(campo->u[i+1][j][k]-campo->u[i-1][j][k])
																		+(*jac)[i][j][k].a21*0.5*(campo->u[i][j+1][k]-campo->u[i][j-1][k])
																		+(*jac)[i][j][k].a31*0.5*(campo->u[i][j][k+1]-campo->u[i][j][k-1]);
					campo->deriv[i][j][k].a12= (*jac)[i][j][k].a12*0.5*(campo->u[i+1][j][k]-campo->u[i-1][j][k])
																		+(*jac)[i][j][k].a22*0.5*(campo->u[i][j+1][k]-campo->u[i][j-1][k])
																		+(*jac)[i][j][k].a32*0.5*(campo->u[i][j][k+1]-campo->u[i][j][k-1]);
					campo->deriv[i][j][k].a13= (*jac)[i][j][k].a13*0.5*(campo->u[i+1][j][k]-campo->u[i-1][j][k])
																		+(*jac)[i][j][k].a23*0.5*(campo->u[i][j+1][k]-campo->u[i][j-1][k])
																		+(*jac)[i][j][k].a33*0.5*(campo->u[i][j][k+1]-campo->u[i][j][k-1]);
					// dv/dx, dv/dy, dv/dz
					campo->deriv[i][j][k].a21= (*jac)[i][j][k].a11*0.5*(campo->v[i+1][j][k]-campo->v[i-1][j][k])
																		+(*jac)[i][j][k].a21*0.5*(campo->v[i][j+1][k]-campo->v[i][j-1][k])
																		+(*jac)[i][j][k].a31*0.5*(campo->v[i][j][k+1]-campo->v[i][j][k-1]);
					campo->deriv[i][j][k].a22= (*jac)[i][j][k].a12*0.5*(campo->v[i+1][j][k]-campo->v[i-1][j][k])
																		+(*jac)[i][j][k].a22*0.5*(campo->v[i][j+1][k]-campo->v[i][j-1][k])
																		+(*jac)[i][j][k].a32*0.5*(campo->v[i][j][k+1]-campo->v[i][j][k-1]);
					campo->deriv[i][j][k].a23= (*jac)[i][j][k].a13*0.5*(campo->v[i+1][j][k]-campo->v[i-1][j][k])
																		+(*jac)[i][j][k].a23*0.5*(campo->v[i][j+1][k]-campo->v[i][j-1][k])
																		+(*jac)[i][j][k].a33*0.5*(campo->v[i][j][k+1]-campo->v[i][j][k-1]);
					// dw/dx, dw/dy, dw/dz
					campo->deriv[i][j][k].a31= (*jac)[i][j][k].a11*0.5*(campo->w[i+1][j][k]-campo->w[i-1][j][k])
																		+(*jac)[i][j][k].a21*0.5*(campo->w[i][j+1][k]-campo->w[i][j-1][k])
																		+(*jac)[i][j][k].a31*0.5*(campo->w[i][j][k+1]-campo->w[i][j][k-1]);
					campo->deriv[i][j][k].a32= (*jac)[i][j][k].a12*0.5*(campo->w[i+1][j][k]-campo->w[i-1][j][k])
																		+(*jac)[i][j][k].a22*0.5*(campo->w[i][j+1][k]-campo->w[i][j-1][k])
																		+(*jac)[i][j][k].a32*0.5*(campo->w[i][j][k+1]-campo->w[i][j][k-1]);
					campo->deriv[i][j][k].a33= (*jac)[i][j][k].a13*0.5*(campo->w[i+1][j][k]-campo->w[i-1][j][k])
																		+(*jac)[i][j][k].a23*0.5*(campo->w[i][j+1][k]-campo->w[i][j-1][k])
																		+(*jac)[i][j][k].a33*0.5*(campo->w[i][j][k+1]-campo->w[i][j][k-1]);
			}
// faces 1 and 2	
	for	(j=1;j<ny+1;j++)
		for	(k=1;k<nz+1;k++)
		{
			i = 0;
			// du/dx, du/dy, du/dz
			campo->deriv[i][j][k].a11= (*jac)[i][j][k].a11*(campo->u[i+1][j][k]-campo->u[i][j][k])
																+(*jac)[i][j][k].a21*0.5*(campo->u[i][j+1][k]-campo->u[i][j-1][k])
																+(*jac)[i][j][k].a31*0.5*(campo->u[i][j][k+1]-campo->u[i][j][k-1]);
			campo->deriv[i][j][k].a12= (*jac)[i][j][k].a12*(campo->u[i+1][j][k]-campo->u[i][j][k])
																+(*jac)[i][j][k].a22*0.5*(campo->u[i][j+1][k]-campo->u[i][j-1][k])
																+(*jac)[i][j][k].a32*0.5*(campo->u[i][j][k+1]-campo->u[i][j][k-1]);
			campo->deriv[i][j][k].a13= (*jac)[i][j][k].a13*(campo->u[i+1][j][k]-campo->u[i][j][k])
																+(*jac)[i][j][k].a23*0.5*(campo->u[i][j+1][k]-campo->u[i][j-1][k])
																+(*jac)[i][j][k].a33*0.5*(campo->u[i][j][k+1]-campo->u[i][j][k-1]);
			// dv/dx, dv/dy, dv/dz
			campo->deriv[i][j][k].a21= (*jac)[i][j][k].a11*(campo->v[i+1][j][k]-campo->v[i][j][k])
																+(*jac)[i][j][k].a21*0.5*(campo->v[i][j+1][k]-campo->v[i][j-1][k])
																+(*jac)[i][j][k].a31*0.5*(campo->v[i][j][k+1]-campo->v[i][j][k-1]);
			campo->deriv[i][j][k].a22= (*jac)[i][j][k].a12*(campo->v[i+1][j][k]-campo->v[i][j][k])
																+(*jac)[i][j][k].a22*0.5*(campo->v[i][j+1][k]-campo->v[i][j-1][k])
																+(*jac)[i][j][k].a32*0.5*(campo->v[i][j][k+1]-campo->v[i][j][k-1]);
			campo->deriv[i][j][k].a23= (*jac)[i][j][k].a13*(campo->v[i+1][j][k]-campo->v[i][j][k])
																+(*jac)[i][j][k].a23*0.5*(campo->v[i][j+1][k]-campo->v[i][j-1][k])
																+(*jac)[i][j][k].a33*0.5*(campo->v[i][j][k+1]-campo->v[i][j][k-1]);
			// dw/dx, dw/dy, dw/dz
			campo->deriv[i][j][k].a31= (*jac)[i][j][k].a11*(campo->w[i+1][j][k]-campo->w[i][j][k])
																+(*jac)[i][j][k].a21*0.5*(campo->w[i][j+1][k]-campo->w[i][j-1][k])
																+(*jac)[i][j][k].a31*0.5*(campo->w[i][j][k+1]-campo->w[i][j][k-1]);
			campo->deriv[i][j][k].a32= (*jac)[i][j][k].a12*(campo->w[i+1][j][k]-campo->w[i][j][k])
																+(*jac)[i][j][k].a22*0.5*(campo->w[i][j+1][k]-campo->w[i][j-1][k])
																+(*jac)[i][j][k].a32*0.5*(campo->w[i][j][k+1]-campo->w[i][j][k-1]);
			campo->deriv[i][j][k].a33= (*jac)[i][j][k].a13*(campo->w[i+1][j][k]-campo->w[i][j][k])
																+(*jac)[i][j][k].a23*0.5*(campo->w[i][j+1][k]-campo->w[i][j-1][k])
																+(*jac)[i][j][k].a33*0.5*(campo->w[i][j][k+1]-campo->w[i][j][k-1]);
			i = nx + 1;
			// du/dx, du/dy, du/dz
			campo->deriv[i][j][k].a11= (*jac)[i][j][k].a11*(campo->u[i][j][k]-campo->u[i-1][j][k])
																+(*jac)[i][j][k].a21*0.5*(campo->u[i][j+1][k]-campo->u[i][j-1][k])
																+(*jac)[i][j][k].a31*0.5*(campo->u[i][j][k+1]-campo->u[i][j][k-1]);
			campo->deriv[i][j][k].a12= (*jac)[i][j][k].a12*(campo->u[i][j][k]-campo->u[i-1][j][k])
																+(*jac)[i][j][k].a22*0.5*(campo->u[i][j+1][k]-campo->u[i][j-1][k])
																+(*jac)[i][j][k].a32*0.5*(campo->u[i][j][k+1]-campo->u[i][j][k-1]);
			campo->deriv[i][j][k].a13= (*jac)[i][j][k].a13*(campo->u[i][j][k]-campo->u[i-1][j][k])
																+(*jac)[i][j][k].a23*0.5*(campo->u[i][j+1][k]-campo->u[i][j-1][k])
																+(*jac)[i][j][k].a33*0.5*(campo->u[i][j][k+1]-campo->u[i][j][k-1]);
			// dv/dx, dv/dy, dv/dz
			campo->deriv[i][j][k].a21= (*jac)[i][j][k].a11*(campo->v[i][j][k]-campo->v[i-1][j][k])
																+(*jac)[i][j][k].a21*0.5*(campo->v[i][j+1][k]-campo->v[i][j-1][k])
																+(*jac)[i][j][k].a31*0.5*(campo->v[i][j][k+1]-campo->v[i][j][k-1]);
			campo->deriv[i][j][k].a22= (*jac)[i][j][k].a12*(campo->v[i][j][k]-campo->v[i-1][j][k])
																+(*jac)[i][j][k].a22*0.5*(campo->v[i][j+1][k]-campo->v[i][j-1][k])
																+(*jac)[i][j][k].a32*0.5*(campo->v[i][j][k+1]-campo->v[i][j][k-1]);
			campo->deriv[i][j][k].a23= (*jac)[i][j][k].a13*(campo->v[i][j][k]-campo->v[i-1][j][k])
																+(*jac)[i][j][k].a23*0.5*(campo->v[i][j+1][k]-campo->v[i][j-1][k])
																+(*jac)[i][j][k].a33*0.5*(campo->v[i][j][k+1]-campo->v[i][j][k-1]);
			// dw/dx, dw/dy, dw/dz
			campo->deriv[i][j][k].a31= (*jac)[i][j][k].a11*(campo->w[i][j][k]-campo->w[i-1][j][k])
																+(*jac)[i][j][k].a21*0.5*(campo->w[i][j+1][k]-campo->w[i][j-1][k])
																+(*jac)[i][j][k].a31*0.5*(campo->w[i][j][k+1]-campo->w[i][j][k-1]);
			campo->deriv[i][j][k].a32= (*jac)[i][j][k].a12*(campo->w[i][j][k]-campo->w[i-1][j][k])
																+(*jac)[i][j][k].a22*0.5*(campo->w[i][j+1][k]-campo->w[i][j-1][k])
																+(*jac)[i][j][k].a32*0.5*(campo->w[i][j][k+1]-campo->w[i][j][k-1]);
			campo->deriv[i][j][k].a33= (*jac)[i][j][k].a13*(campo->w[i][j][k]-campo->w[i-1][j][k])
																+(*jac)[i][j][k].a23*0.5*(campo->w[i][j+1][k]-campo->w[i][j-1][k])
																+(*jac)[i][j][k].a33*0.5*(campo->w[i][j][k+1]-campo->w[i][j][k-1]);
		}
// faces right and left
	for(i=1;i<nx+1;i++)
		for(j=1;j<ny+1;j++)
		{
			k = 0;
			// du/dx, du/dy, du/dz
			campo->deriv[i][j][k].a11= (*jac)[i][j][k].a11*0.5*(campo->u[i+1][j][k]-campo->u[i-1][j][k])
																+(*jac)[i][j][k].a21*0.5*(campo->u[i][j+1][k]-campo->u[i][j-1][k])
																+((*jac)[i][j][k].a31)*(campo->u[i][j][k+1]-campo->u[i][j][k]);
			campo->deriv[i][j][k].a12= (*jac)[i][j][k].a12*0.5*(campo->u[i+1][j][k]-campo->u[i-1][j][k])
																+(*jac)[i][j][k].a22*0.5*(campo->u[i][j+1][k]-campo->u[i][j-1][k])
																+((*jac)[i][j][k].a32)*(campo->u[i][j][k+1]-campo->u[i][j][k]);
			campo->deriv[i][j][k].a13= (*jac)[i][j][k].a13*0.5*(campo->u[i+1][j][k]-campo->u[i-1][j][k])
																+(*jac)[i][j][k].a23*0.5*(campo->u[i][j+1][k]-campo->u[i][j-1][k])
																+((*jac)[i][j][k].a33)*(campo->u[i][j][k+1]-campo->u[i][j][k]);
			// dv/dx, dv/dy, dv/dz
			campo->deriv[i][j][k].a21= (*jac)[i][j][k].a11*0.5*(campo->v[i+1][j][k]-campo->v[i-1][j][k])
																+(*jac)[i][j][k].a21*0.5*(campo->v[i][j+1][k]-campo->v[i][j-1][k])
																+((*jac)[i][j][k].a31)*(campo->v[i][j][k+1]-campo->v[i][j][k]);
			campo->deriv[i][j][k].a22= (*jac)[i][j][k].a12*0.5*(campo->v[i+1][j][k]-campo->v[i-1][j][k])
																+(*jac)[i][j][k].a22*0.5*(campo->v[i][j+1][k]-campo->v[i][j-1][k])
																+((*jac)[i][j][k].a32)*(campo->v[i][j][k+1]-campo->v[i][j][k]);
			campo->deriv[i][j][k].a23= (*jac)[i][j][k].a13*0.5*(campo->v[i+1][j][k]-campo->v[i-1][j][k])
																+(*jac)[i][j][k].a23*0.5*(campo->v[i][j+1][k]-campo->v[i][j-1][k])
																+((*jac)[i][j][k].a33)*(campo->v[i][j][k+1]-campo->v[i][j][k]);
			// dw/dx, dw/dy, dw/dz
			campo->deriv[i][j][k].a31= (*jac)[i][j][k].a11*0.5*(campo->w[i+1][j][k]-campo->w[i-1][j][k])
																+(*jac)[i][j][k].a21*0.5*(campo->w[i][j+1][k]-campo->w[i][j-1][k])
																+((*jac)[i][j][k].a31)*(campo->w[i][j][k+1]-campo->w[i][j][k]);
			campo->deriv[i][j][k].a32= (*jac)[i][j][k].a12*0.5*(campo->w[i+1][j][k]-campo->w[i-1][j][k])
																+(*jac)[i][j][k].a22*0.5*(campo->w[i][j+1][k]-campo->w[i][j-1][k])
																+((*jac)[i][j][k].a32)*(campo->w[i][j][k+1]-campo->w[i][j][k]);
			campo->deriv[i][j][k].a33= (*jac)[i][j][k].a13*0.5*(campo->w[i+1][j][k]-campo->w[i-1][j][k])
																+(*jac)[i][j][k].a23*0.5*(campo->w[i][j+1][k]-campo->w[i][j-1][k])
																+((*jac)[i][j][k].a33)*(campo->w[i][j][k+1]-campo->w[i][j][k]);
			k = nz+1;

			// du/dx, du/dy, du/dz
			campo->deriv[i][j][k].a11= (*jac)[i][j][k].a11*0.5*(campo->u[i+1][j][k]-campo->u[i-1][j][k])
																+(*jac)[i][j][k].a21*0.5*(campo->u[i][j+1][k]-campo->u[i][j-1][k])
																+((*jac)[i][j][k].a31)*(campo->u[i][j][k]-campo->u[i][j][k-1]);
			campo->deriv[i][j][k].a12= (*jac)[i][j][k].a12*0.5*(campo->u[i+1][j][k]-campo->u[i-1][j][k])
																+(*jac)[i][j][k].a22*0.5*(campo->u[i][j+1][k]-campo->u[i][j-1][k])
																+((*jac)[i][j][k].a32)*(campo->u[i][j][k]-campo->u[i][j][k-1]);
			campo->deriv[i][j][k].a13= (*jac)[i][j][k].a13*0.5*(campo->u[i+1][j][k]-campo->u[i-1][j][k])
																+(*jac)[i][j][k].a23*0.5*(campo->u[i][j+1][k]-campo->u[i][j-1][k])
																+((*jac)[i][j][k].a33)*(campo->u[i][j][k]-campo->u[i][j][k-1]);
			// dv/dx, dv/dy, dv/dz
			campo->deriv[i][j][k].a21= (*jac)[i][j][k].a11*0.5*(campo->v[i+1][j][k]-campo->v[i-1][j][k])
																+(*jac)[i][j][k].a21*0.5*(campo->v[i][j+1][k]-campo->v[i][j-1][k])
																+((*jac)[i][j][k].a31)*(campo->v[i][j][k]-campo->v[i][j][k-1]);
			campo->deriv[i][j][k].a22= (*jac)[i][j][k].a12*0.5*(campo->v[i+1][j][k]-campo->v[i-1][j][k])
																+(*jac)[i][j][k].a22*0.5*(campo->v[i][j+1][k]-campo->v[i][j-1][k])
																+((*jac)[i][j][k].a32)*(campo->v[i][j][k]-campo->v[i][j][k-1]);
			campo->deriv[i][j][k].a23= (*jac)[i][j][k].a13*0.5*(campo->v[i+1][j][k]-campo->v[i-1][j][k])
																+(*jac)[i][j][k].a23*0.5*(campo->v[i][j+1][k]-campo->v[i][j-1][k])
																+((*jac)[i][j][k].a33)*(campo->v[i][j][k]-campo->v[i][j][k-1]);
			// dw/dx, dw/dy, dw/dz
			campo->deriv[i][j][k].a31= (*jac)[i][j][k].a11*0.5*(campo->w[i+1][j][k]-campo->w[i-1][j][k])
																+(*jac)[i][j][k].a21*0.5*(campo->w[i][j+1][k]-campo->w[i][j-1][k])
																+((*jac)[i][j][k].a31)*(campo->w[i][j][k]-campo->w[i][j][k-1]);
			campo->deriv[i][j][k].a32= (*jac)[i][j][k].a12*0.5*(campo->w[i+1][j][k]-campo->w[i-1][j][k])
																+(*jac)[i][j][k].a22*0.5*(campo->w[i][j+1][k]-campo->w[i][j-1][k])
																+((*jac)[i][j][k].a32)*(campo->w[i][j][k]-campo->w[i][j][k-1]);
			campo->deriv[i][j][k].a33= (*jac)[i][j][k].a13*0.5*(campo->w[i+1][j][k]-campo->w[i-1][j][k])
																+(*jac)[i][j][k].a23*0.5*(campo->w[i][j+1][k]-campo->w[i][j-1][k])
																+((*jac)[i][j][k].a33)*(campo->w[i][j][k]-campo->w[i][j][k-1]);
		}
// faces bottom and top
	for(i=1;i<nx+1;i++)
		for(k=1;k<nz+1;k++)
		{
			j=0;
			// du/dx, du/dy, du/dz
			campo->deriv[i][j][k].a11= (*jac)[i][j][k].a11*0.5*(campo->u[i+1][j][k]-campo->u[i-1][j][k])
																+((*jac)[i][j][k].a21)*(campo->u[i][j+1][k]-campo->u[i][j][k])
																+(*jac)[i][j][k].a31*0.5*(campo->u[i][j][k+1]-campo->u[i][j][k-1]);
			campo->deriv[i][j][k].a12= (*jac)[i][j][k].a12*0.5*(campo->u[i+1][j][k]-campo->u[i-1][j][k])
																+((*jac)[i][j][k].a22)*(campo->u[i][j+1][k]-campo->u[i][j][k])
																+(*jac)[i][j][k].a32*0.5*(campo->u[i][j][k+1]-campo->u[i][j][k-1]);
			campo->deriv[i][j][k].a13= (*jac)[i][j][k].a13*0.5*(campo->u[i+1][j][k]-campo->u[i-1][j][k])
																+((*jac)[i][j][k].a23)*(campo->u[i][j+1][k]-campo->u[i][j][k])
																+(*jac)[i][j][k].a33*0.5*(campo->u[i][j][k+1]-campo->u[i][j][k-1]);
			// dv/dx, dv/dy, dv/dz
			campo->deriv[i][j][k].a21= (*jac)[i][j][k].a11*0.5*(campo->v[i+1][j][k]-campo->v[i-1][j][k])
																+((*jac)[i][j][k].a21)*(campo->v[i][j+1][k]-campo->v[i][j][k])
																+(*jac)[i][j][k].a31*0.5*(campo->v[i][j][k+1]-campo->v[i][j][k-1]);
			campo->deriv[i][j][k].a22= (*jac)[i][j][k].a12*0.5*(campo->v[i+1][j][k]-campo->v[i-1][j][k])
																+((*jac)[i][j][k].a22)*(campo->v[i][j+1][k]-campo->v[i][j][k])
																+(*jac)[i][j][k].a32*0.5*(campo->v[i][j][k+1]-campo->v[i][j][k-1]);
			campo->deriv[i][j][k].a23= (*jac)[i][j][k].a13*0.5*(campo->v[i+1][j][k]-campo->v[i-1][j][k])
																+((*jac)[i][j][k].a23)*(campo->v[i][j+1][k]-campo->v[i][j][k])
																+(*jac)[i][j][k].a33*0.5*(campo->v[i][j][k+1]-campo->v[i][j][k-1]);
			// dw/dx, dw/dy, dw/dz
			campo->deriv[i][j][k].a31= (*jac)[i][j][k].a11*0.5*(campo->w[i+1][j][k]-campo->w[i-1][j][k])
																+((*jac)[i][j][k].a21)*(campo->w[i][j+1][k]-campo->w[i][j][k])
																+(*jac)[i][j][k].a31*0.5*(campo->w[i][j][k+1]-campo->w[i][j][k-1]);
			campo->deriv[i][j][k].a32= (*jac)[i][j][k].a12*0.5*(campo->w[i+1][j][k]-campo->w[i-1][j][k])
																+((*jac)[i][j][k].a22)*(campo->w[i][j+1][k]-campo->w[i][j][k])
																+(*jac)[i][j][k].a32*0.5*(campo->w[i][j][k+1]-campo->w[i][j][k-1]);
			campo->deriv[i][j][k].a33= (*jac)[i][j][k].a13*0.5*(campo->w[i+1][j][k]-campo->w[i-1][j][k])
																+((*jac)[i][j][k].a23)*(campo->w[i][j+1][k]-campo->w[i][j][k])
																+(*jac)[i][j][k].a33*0.5*(campo->w[i][j][k+1]-campo->w[i][j][k-1]);
			j=ny+1;
			// du/dx, du/dy, du/dz
			campo->deriv[i][j][k].a11= (*jac)[i][j][k].a11*0.5*(campo->u[i+1][j][k]-campo->u[i-1][j][k])
																+((*jac)[i][j][k].a21)*(campo->u[i][j][k]-campo->u[i][j-1][k])
																+(*jac)[i][j][k].a31*0.5*(campo->u[i][j][k+1]-campo->u[i][j][k-1]);
			campo->deriv[i][j][k].a12= (*jac)[i][j][k].a12*0.5*(campo->u[i+1][j][k]-campo->u[i-1][j][k])
																+((*jac)[i][j][k].a22)*(campo->u[i][j][k]-campo->u[i][j-1][k])
																+(*jac)[i][j][k].a32*0.5*(campo->u[i][j][k+1]-campo->u[i][j][k-1]);
			campo->deriv[i][j][k].a13= (*jac)[i][j][k].a13*0.5*(campo->u[i+1][j][k]-campo->u[i-1][j][k])
																+((*jac)[i][j][k].a23)*(campo->u[i][j][k]-campo->u[i][j-1][k])
																+(*jac)[i][j][k].a33*0.5*(campo->u[i][j][k+1]-campo->u[i][j][k-1]);
			// dv/dx, dv/dy, dv/dz
			campo->deriv[i][j][k].a21= (*jac)[i][j][k].a11*0.5*(campo->v[i+1][j][k]-campo->v[i-1][j][k])
																+((*jac)[i][j][k].a21)*(campo->v[i][j][k]-campo->v[i][j-1][k])
																+(*jac)[i][j][k].a31*0.5*(campo->v[i][j][k+1]-campo->v[i][j][k-1]);
			campo->deriv[i][j][k].a22= (*jac)[i][j][k].a12*0.5*(campo->v[i+1][j][k]-campo->v[i-1][j][k])
																+((*jac)[i][j][k].a22)*(campo->v[i][j][k]-campo->v[i][j-1][k])
																+(*jac)[i][j][k].a32*0.5*(campo->v[i][j][k+1]-campo->v[i][j][k-1]);
			campo->deriv[i][j][k].a23= (*jac)[i][j][k].a13*0.5*(campo->v[i+1][j][k]-campo->v[i-1][j][k])
																+((*jac)[i][j][k].a23)*(campo->v[i][j][k]-campo->v[i][j-1][k])
																+(*jac)[i][j][k].a33*0.5*(campo->v[i][j][k+1]-campo->v[i][j][k-1]);
			// dw/dx, dw/dy, dw/dz
			campo->deriv[i][j][k].a31= (*jac)[i][j][k].a11*0.5*(campo->w[i+1][j][k]-campo->w[i-1][j][k])
																+((*jac)[i][j][k].a21)*(campo->w[i][j][k]-campo->w[i][j-1][k])
																+(*jac)[i][j][k].a31*0.5*(campo->w[i][j][k+1]-campo->w[i][j][k-1]);
			campo->deriv[i][j][k].a32= (*jac)[i][j][k].a12*0.5*(campo->w[i+1][j][k]-campo->w[i-1][j][k])
																+((*jac)[i][j][k].a22)*(campo->w[i][j][k]-campo->w[i][j-1][k])
																+(*jac)[i][j][k].a32*0.5*(campo->w[i][j][k+1]-campo->w[i][j][k-1]);
			campo->deriv[i][j][k].a33= (*jac)[i][j][k].a13*0.5*(campo->w[i+1][j][k]-campo->w[i-1][j][k])
																+((*jac)[i][j][k].a23)*(campo->w[i][j][k]-campo->w[i][j-1][k])
																+(*jac)[i][j][k].a33*0.5*(campo->w[i][j][k+1]-campo->w[i][j][k-1]);
		}
// the borders .....
	for(k=1;k<nz+1;++k)
	{
		i = 0;
		j = 0;
		// du/dx, du/dy, du/dz
		campo->deriv[i][j][k].a11= (*jac)[i][j][k].a11*(campo->u[i+1][j][k]-campo->u[i][j][k])
															+((*jac)[i][j][k].a21)*(campo->u[i][j+1][k]-campo->u[i][j][k])
															+(*jac)[i][j][k].a31*0.5*(campo->u[i][j][k+1]-campo->u[i][j][k-1]);
		campo->deriv[i][j][k].a12= (*jac)[i][j][k].a12*(campo->u[i+1][j][k]-campo->u[i][j][k])
															+((*jac)[i][j][k].a22)*(campo->u[i][j+1][k]-campo->u[i][j][k])
															+(*jac)[i][j][k].a32*0.5*(campo->u[i][j][k+1]-campo->u[i][j][k-1]);
		campo->deriv[i][j][k].a13= (*jac)[i][j][k].a13*(campo->u[i+1][j][k]-campo->u[i][j][k])
															+((*jac)[i][j][k].a23)*(campo->u[i][j+1][k]-campo->u[i][j][k])
															+(*jac)[i][j][k].a33*0.5*(campo->u[i][j][k+1]-campo->u[i][j][k-1]);
		// dv/dx, dv/dy, dv/dz
		campo->deriv[i][j][k].a21= (*jac)[i][j][k].a11*(campo->v[i+1][j][k]-campo->v[i][j][k])
															+((*jac)[i][j][k].a21)*(campo->v[i][j+1][k]-campo->v[i][j][k])
															+(*jac)[i][j][k].a31*0.5*(campo->v[i][j][k+1]-campo->v[i][j][k-1]);
		campo->deriv[i][j][k].a22= (*jac)[i][j][k].a12*(campo->v[i+1][j][k]-campo->v[i][j][k])
															+((*jac)[i][j][k].a22)*(campo->v[i][j+1][k]-campo->v[i][j][k])
															+(*jac)[i][j][k].a32*0.5*(campo->v[i][j][k+1]-campo->v[i][j][k-1]);
		campo->deriv[i][j][k].a23= (*jac)[i][j][k].a13*(campo->v[i+1][j][k]-campo->v[i][j][k])
															+((*jac)[i][j][k].a23)*(campo->v[i][j+1][k]-campo->v[i][j][k])
															+(*jac)[i][j][k].a33*0.5*(campo->v[i][j][k+1]-campo->v[i][j][k-1]);
		// dw/dx, dw/dy, dw/dz
		campo->deriv[i][j][k].a31= (*jac)[i][j][k].a11*(campo->w[i+1][j][k]-campo->w[i][j][k])
															+((*jac)[i][j][k].a21)*(campo->w[i][j+1][k]-campo->w[i][j][k])
															+(*jac)[i][j][k].a31*0.5*(campo->w[i][j][k+1]-campo->w[i][j][k-1]);
		campo->deriv[i][j][k].a32= (*jac)[i][j][k].a12*(campo->w[i+1][j][k]-campo->w[i][j][k])
															+((*jac)[i][j][k].a22)*(campo->w[i][j+1][k]-campo->w[i][j][k])
															+(*jac)[i][j][k].a32*0.5*(campo->w[i][j][k+1]-campo->w[i][j][k-1]);
		campo->deriv[i][j][k].a33= (*jac)[i][j][k].a13*(campo->w[i+1][j][k]-campo->w[i][j][k])
															+((*jac)[i][j][k].a23)*(campo->w[i][j+1][k]-campo->w[i][j][k])
															+(*jac)[i][j][k].a33*0.5*(campo->w[i][j][k+1]-campo->w[i][j][k-1]);

		i = 0;
		j = ny+1;
			// du/dx, du/dy, du/dz
			campo->deriv[i][j][k].a11= (*jac)[i][j][k].a11*(campo->u[i+1][j][k]-campo->u[i][j][k])
																+((*jac)[i][j][k].a21)*(campo->u[i][j][k]-campo->u[i][j-1][k])
																+(*jac)[i][j][k].a31*0.5*(campo->u[i][j][k+1]-campo->u[i][j][k-1]);
			campo->deriv[i][j][k].a12= (*jac)[i][j][k].a12*(campo->u[i+1][j][k]-campo->u[i][j][k])
																+((*jac)[i][j][k].a22)*(campo->u[i][j][k]-campo->u[i][j-1][k])
																+(*jac)[i][j][k].a32*0.5*(campo->u[i][j][k+1]-campo->u[i][j][k-1]);
			campo->deriv[i][j][k].a13= (*jac)[i][j][k].a13*(campo->u[i+1][j][k]-campo->u[i][j][k])
																+((*jac)[i][j][k].a23)*(campo->u[i][j][k]-campo->u[i][j-1][k])
																+(*jac)[i][j][k].a33*0.5*(campo->u[i][j][k+1]-campo->u[i][j][k-1]);
			// dv/dx, dv/dy, dv/dz
			campo->deriv[i][j][k].a21= (*jac)[i][j][k].a11*(campo->v[i+1][j][k]-campo->v[i][j][k])
																+((*jac)[i][j][k].a21)*(campo->v[i][j][k]-campo->v[i][j-1][k])
																+(*jac)[i][j][k].a31*0.5*(campo->v[i][j][k+1]-campo->v[i][j][k-1]);
			campo->deriv[i][j][k].a22= (*jac)[i][j][k].a12*(campo->v[i+1][j][k]-campo->v[i][j][k])
																+((*jac)[i][j][k].a22)*(campo->v[i][j][k]-campo->v[i][j-1][k])
																+(*jac)[i][j][k].a32*0.5*(campo->v[i][j][k+1]-campo->v[i][j][k-1]);
			campo->deriv[i][j][k].a23= (*jac)[i][j][k].a13*(campo->v[i+1][j][k]-campo->v[i][j][k])
																+((*jac)[i][j][k].a23)*(campo->v[i][j][k]-campo->v[i][j-1][k])
																+(*jac)[i][j][k].a33*0.5*(campo->v[i][j][k+1]-campo->v[i][j][k-1]);
			// dw/dx, dw/dy, dw/dz
			campo->deriv[i][j][k].a31= (*jac)[i][j][k].a11*(campo->w[i+1][j][k]-campo->w[i][j][k])
																+((*jac)[i][j][k].a21)*(campo->w[i][j][k]-campo->w[i][j-1][k])
																+(*jac)[i][j][k].a31*0.5*(campo->w[i][j][k+1]-campo->w[i][j][k-1]);
			campo->deriv[i][j][k].a32= (*jac)[i][j][k].a12*(campo->w[i+1][j][k]-campo->w[i][j][k])
																+((*jac)[i][j][k].a22)*(campo->w[i][j][k]-campo->w[i][j-1][k])
																+(*jac)[i][j][k].a32*0.5*(campo->w[i][j][k+1]-campo->w[i][j][k-1]);
			campo->deriv[i][j][k].a33= (*jac)[i][j][k].a13*(campo->w[i+1][j][k]-campo->w[i][j][k])
																+((*jac)[i][j][k].a23)*(campo->w[i][j][k]-campo->w[i][j-1][k])
																+(*jac)[i][j][k].a33*0.5*(campo->w[i][j][k+1]-campo->w[i][j][k-1]);
	
		i = nx+1;
		j = ny+1;
		
		// du/dx, du/dy, du/dz
		campo->deriv[i][j][k].a11= (*jac)[i][j][k].a11*(campo->u[i][j][k]-campo->u[i-1][j][k])
															+((*jac)[i][j][k].a21)*(campo->u[i][j][k]-campo->u[i][j-1][k])
															+(*jac)[i][j][k].a31*0.5*(campo->u[i][j][k+1]-campo->u[i][j][k-1]);
		campo->deriv[i][j][k].a12= (*jac)[i][j][k].a12*(campo->u[i][j][k]-campo->u[i-1][j][k])
															+((*jac)[i][j][k].a22)*(campo->u[i][j][k]-campo->u[i][j-1][k])
															+(*jac)[i][j][k].a32*0.5*(campo->u[i][j][k+1]-campo->u[i][j][k-1]);
		campo->deriv[i][j][k].a13= (*jac)[i][j][k].a13*(campo->u[i][j][k]-campo->u[i-1][j][k])
															+((*jac)[i][j][k].a23)*(campo->u[i][j][k]-campo->u[i][j-1][k])
															+(*jac)[i][j][k].a33*0.5*(campo->u[i][j][k+1]-campo->u[i][j][k-1]);
		// dv/dx, dv/dy, dv/dz
		campo->deriv[i][j][k].a21= (*jac)[i][j][k].a11*(campo->v[i][j][k]-campo->v[i-1][j][k])
															+((*jac)[i][j][k].a21)*(campo->v[i][j][k]-campo->v[i][j-1][k])
															+(*jac)[i][j][k].a31*0.5*(campo->v[i][j][k+1]-campo->v[i][j][k-1]);
		campo->deriv[i][j][k].a22= (*jac)[i][j][k].a12*(campo->v[i][j][k]-campo->v[i-1][j][k])
															+((*jac)[i][j][k].a22)*(campo->v[i][j][k]-campo->v[i][j-1][k])
															+(*jac)[i][j][k].a32*0.5*(campo->v[i][j][k+1]-campo->v[i][j][k-1]);
		campo->deriv[i][j][k].a23= (*jac)[i][j][k].a13*(campo->v[i][j][k]-campo->v[i-1][j][k])
															+((*jac)[i][j][k].a23)*(campo->v[i][j][k]-campo->v[i][j-1][k])
															+(*jac)[i][j][k].a33*0.5*(campo->v[i][j][k+1]-campo->v[i][j][k-1]);
		// dw/dx, dw/dy, dw/dz
		campo->deriv[i][j][k].a31= (*jac)[i][j][k].a11*(campo->w[i][j][k]-campo->w[i-1][j][k])
															+((*jac)[i][j][k].a21)*(campo->w[i][j][k]-campo->w[i][j-1][k])
															+(*jac)[i][j][k].a31*0.5*(campo->w[i][j][k+1]-campo->w[i][j][k-1]);
		campo->deriv[i][j][k].a32= (*jac)[i][j][k].a12*(campo->w[i][j][k]-campo->w[i-1][j][k])
															+((*jac)[i][j][k].a22)*(campo->w[i][j][k]-campo->w[i][j-1][k])
															+(*jac)[i][j][k].a32*0.5*(campo->w[i][j][k+1]-campo->w[i][j][k-1]);
		campo->deriv[i][j][k].a33= (*jac)[i][j][k].a13*(campo->w[i][j][k]-campo->w[i-1][j][k])
															+((*jac)[i][j][k].a23)*(campo->w[i][j][k]-campo->w[i][j-1][k])
															+(*jac)[i][j][k].a33*0.5*(campo->w[i][j][k+1]-campo->w[i][j][k-1]);
		i = nx+1;
		j = 0;
		
		// du/dx, du/dy, du/dz
		campo->deriv[i][j][k].a11= (*jac)[i][j][k].a11*(campo->u[i][j][k]-campo->u[i-1][j][k])
															+((*jac)[i][j][k].a21)*(campo->u[i][j+1][k]-campo->u[i][j][k])
															+(*jac)[i][j][k].a31*0.5*(campo->u[i][j][k+1]-campo->u[i][j][k-1]);
		campo->deriv[i][j][k].a12= (*jac)[i][j][k].a12*(campo->u[i][j][k]-campo->u[i-1][j][k])
															+((*jac)[i][j][k].a22)*(campo->u[i][j+1][k]-campo->u[i][j][k])
															+(*jac)[i][j][k].a32*0.5*(campo->u[i][j][k+1]-campo->u[i][j][k-1]);
		campo->deriv[i][j][k].a13= (*jac)[i][j][k].a13*(campo->u[i][j][k]-campo->u[i-1][j][k])
															+((*jac)[i][j][k].a23)*(campo->u[i][j+1][k]-campo->u[i][j][k])
															+(*jac)[i][j][k].a33*0.5*(campo->u[i][j][k+1]-campo->u[i][j][k-1]);
		// dv/dx, dv/dy, dv/dz
		campo->deriv[i][j][k].a21= (*jac)[i][j][k].a11*(campo->v[i][j][k]-campo->v[i-1][j][k])
															+((*jac)[i][j][k].a21)*(campo->v[i][j+1][k]-campo->v[i][j][k])
															+(*jac)[i][j][k].a31*0.5*(campo->v[i][j][k+1]-campo->v[i][j][k-1]);
		campo->deriv[i][j][k].a22= (*jac)[i][j][k].a12*(campo->v[i][j][k]-campo->v[i-1][j][k])
															+((*jac)[i][j][k].a22)*(campo->v[i][j+1][k]-campo->v[i][j][k])
															+(*jac)[i][j][k].a32*0.5*(campo->v[i][j][k+1]-campo->v[i][j][k-1]);
		campo->deriv[i][j][k].a23= (*jac)[i][j][k].a13*(campo->v[i][j][k]-campo->v[i-1][j][k])
															+((*jac)[i][j][k].a23)*(campo->v[i][j+1][k]-campo->v[i][j][k])
															+(*jac)[i][j][k].a33*0.5*(campo->v[i][j][k+1]-campo->v[i][j][k-1]);
		// dw/dx, dw/dy, dw/dz
		campo->deriv[i][j][k].a31= (*jac)[i][j][k].a11*(campo->w[i][j][k]-campo->w[i-1][j][k])
															+((*jac)[i][j][k].a21)*(campo->w[i][j+1][k]-campo->w[i][j][k])
															+(*jac)[i][j][k].a31*0.5*(campo->w[i][j][k+1]-campo->w[i][j][k-1]);
		campo->deriv[i][j][k].a32= (*jac)[i][j][k].a12*(campo->w[i][j][k]-campo->w[i-1][j][k])
															+((*jac)[i][j][k].a22)*(campo->w[i][j+1][k]-campo->w[i][j][k])
															+(*jac)[i][j][k].a32*0.5*(campo->w[i][j][k+1]-campo->w[i][j][k-1]);
		campo->deriv[i][j][k].a33= (*jac)[i][j][k].a13*(campo->w[i][j][k]-campo->w[i-1][j][k])
															+((*jac)[i][j][k].a23)*(campo->w[i][j+1][k]-campo->w[i][j][k])
															+(*jac)[i][j][k].a33*0.5*(campo->w[i][j][k+1]-campo->w[i][j][k-1]);
	}
	for(j=1;j<ny+1;++j)
	{
		i = 0;
		k = 0;

		campo->deriv[i][j][k].a11= (*jac)[i][j][k].a11*(campo->u[i+1][j][k]-campo->u[i][j][k])
															+(*jac)[i][j][k].a21*0.5*(campo->u[i][j+1][k]-campo->u[i][j-1][k])
															+(*jac)[i][j][k].a31*(campo->u[i][j][k+1]-campo->u[i][j][k]);
		campo->deriv[i][j][k].a12= (*jac)[i][j][k].a12*(campo->u[i+1][j][k]-campo->u[i][j][k])
															+(*jac)[i][j][k].a22*0.5*(campo->u[i][j+1][k]-campo->u[i][j-1][k])
															+(*jac)[i][j][k].a32*(campo->u[i][j][k+1]-campo->u[i][j][k]);
		campo->deriv[i][j][k].a13= (*jac)[i][j][k].a13*(campo->u[i+1][j][k]-campo->u[i][j][k])
															+(*jac)[i][j][k].a23*0.5*(campo->u[i][j+1][k]-campo->u[i][j-1][k])
															+(*jac)[i][j][k].a33*(campo->u[i][j][k+1]-campo->u[i][j][k]);
		// dv/dx, dv/dy, dv/dz
		campo->deriv[i][j][k].a21= (*jac)[i][j][k].a11*(campo->v[i+1][j][k]-campo->v[i][j][k])
															+(*jac)[i][j][k].a21*0.5*(campo->v[i][j+1][k]-campo->v[i][j-1][k])
															+(*jac)[i][j][k].a31*(campo->v[i][j][k+1]-campo->v[i][j][k]);
		campo->deriv[i][j][k].a22= (*jac)[i][j][k].a12*(campo->v[i+1][j][k]-campo->v[i][j][k])
															+(*jac)[i][j][k].a22*0.5*(campo->v[i][j+1][k]-campo->v[i][j-1][k])
															+(*jac)[i][j][k].a32*(campo->v[i][j][k+1]-campo->v[i][j][k]);
		campo->deriv[i][j][k].a23= (*jac)[i][j][k].a13*(campo->v[i+1][j][k]-campo->v[i][j][k])
															+(*jac)[i][j][k].a23*0.5*(campo->v[i][j+1][k]-campo->v[i][j-1][k])
															+(*jac)[i][j][k].a33*(campo->v[i][j][k+1]-campo->v[i][j][k]);
		// dw/dx, dw/dy, dw/dz
		campo->deriv[i][j][k].a31= (*jac)[i][j][k].a11*(campo->w[i+1][j][k]-campo->w[i][j][k])
															+(*jac)[i][j][k].a21*0.5*(campo->w[i][j+1][k]-campo->w[i][j-1][k])
															+(*jac)[i][j][k].a31*(campo->w[i][j][k+1]-campo->w[i][j][k]);
		campo->deriv[i][j][k].a32= (*jac)[i][j][k].a12*(campo->w[i+1][j][k]-campo->w[i][j][k])
															+(*jac)[i][j][k].a22*0.5*(campo->w[i][j+1][k]-campo->w[i][j-1][k])
															+(*jac)[i][j][k].a32*(campo->w[i][j][k+1]-campo->w[i][j][k]);
		campo->deriv[i][j][k].a33= (*jac)[i][j][k].a13*(campo->w[i+1][j][k]-campo->w[i][j][k])
															+(*jac)[i][j][k].a23*0.5*(campo->w[i][j+1][k]-campo->w[i][j-1][k])
															+(*jac)[i][j][k].a33*(campo->w[i][j][k+1]-campo->w[i][j][k]);
		i = nx+1;
		k = 0;
		// du/dx, du/dy, du/dz
		campo->deriv[i][j][k].a11= (*jac)[i][j][k].a11*(campo->u[i][j][k]-campo->u[i-1][j][k])
															+(*jac)[i][j][k].a21*0.5*(campo->u[i][j+1][k]-campo->u[i][j-1][k])
															+(*jac)[i][j][k].a31*(campo->u[i][j][k+1]-campo->u[i][j][k]);
		campo->deriv[i][j][k].a12= (*jac)[i][j][k].a12*(campo->u[i][j][k]-campo->u[i-1][j][k])
															+(*jac)[i][j][k].a22*0.5*(campo->u[i][j+1][k]-campo->u[i][j-1][k])
															+(*jac)[i][j][k].a32*(campo->u[i][j][k+1]-campo->u[i][j][k]);
		campo->deriv[i][j][k].a13= (*jac)[i][j][k].a13*(campo->u[i][j][k]-campo->u[i-1][j][k])
															+(*jac)[i][j][k].a23*0.5*(campo->u[i][j+1][k]-campo->u[i][j-1][k])
															+(*jac)[i][j][k].a33*(campo->u[i][j][k+1]-campo->u[i][j][k]);
		// dv/dx, dv/dy, dv/dz
		campo->deriv[i][j][k].a21= (*jac)[i][j][k].a11*(campo->v[i][j][k]-campo->v[i-1][j][k])
															+(*jac)[i][j][k].a21*0.5*(campo->v[i][j+1][k]-campo->v[i][j-1][k])
															+(*jac)[i][j][k].a31*(campo->v[i][j][k+1]-campo->v[i][j][k]);
		campo->deriv[i][j][k].a22= (*jac)[i][j][k].a12*(campo->v[i][j][k]-campo->v[i-1][j][k])
															+(*jac)[i][j][k].a22*0.5*(campo->v[i][j+1][k]-campo->v[i][j-1][k])
															+(*jac)[i][j][k].a32*(campo->v[i][j][k+1]-campo->v[i][j][k]);
		campo->deriv[i][j][k].a23= (*jac)[i][j][k].a13*(campo->v[i][j][k]-campo->v[i-1][j][k])
															+(*jac)[i][j][k].a23*0.5*(campo->v[i][j+1][k]-campo->v[i][j-1][k])
															+(*jac)[i][j][k].a33*(campo->v[i][j][k+1]-campo->v[i][j][k]);
		// dw/dx, dw/dy, dw/dz
		campo->deriv[i][j][k].a31= (*jac)[i][j][k].a11*(campo->w[i][j][k]-campo->w[i-1][j][k])
															+(*jac)[i][j][k].a21*0.5*(campo->w[i][j+1][k]-campo->w[i][j-1][k])
															+(*jac)[i][j][k].a31*(campo->w[i][j][k+1]-campo->w[i][j][k]);
		campo->deriv[i][j][k].a32= (*jac)[i][j][k].a12*(campo->w[i][j][k]-campo->w[i-1][j][k])
															+(*jac)[i][j][k].a22*0.5*(campo->w[i][j+1][k]-campo->w[i][j-1][k])
															+(*jac)[i][j][k].a32*(campo->w[i][j][k+1]-campo->w[i][j][k]);
		campo->deriv[i][j][k].a33= (*jac)[i][j][k].a13*(campo->w[i][j][k]-campo->w[i-1][j][k])
															+(*jac)[i][j][k].a23*0.5*(campo->w[i][j+1][k]-campo->w[i][j-1][k])
															+(*jac)[i][j][k].a33*(campo->w[i][j][k+1]-campo->w[i][j][k]);
		
		i = 0;
		k = nz +1;

		campo->deriv[i][j][k].a11= (*jac)[i][j][k].a11*(campo->u[i+1][j][k]-campo->u[i][j][k])
															+(*jac)[i][j][k].a21*0.5*(campo->u[i][j+1][k]-campo->u[i][j-1][k])
															+(*jac)[i][j][k].a31*(campo->u[i][j][k]-campo->u[i][j][k-1]);
		campo->deriv[i][j][k].a12= (*jac)[i][j][k].a12*(campo->u[i+1][j][k]-campo->u[i][j][k])
															+(*jac)[i][j][k].a22*0.5*(campo->u[i][j+1][k]-campo->u[i][j-1][k])
															+(*jac)[i][j][k].a32*(campo->u[i][j][k]-campo->u[i][j][k-1]);
		campo->deriv[i][j][k].a13= (*jac)[i][j][k].a13*(campo->u[i+1][j][k]-campo->u[i][j][k])
															+(*jac)[i][j][k].a23*0.5*(campo->u[i][j+1][k]-campo->u[i][j-1][k])
															+(*jac)[i][j][k].a33*(campo->u[i][j][k]-campo->u[i][j][k-1]);
		// dv/dx, dv/dy, dv/dz
		campo->deriv[i][j][k].a21= (*jac)[i][j][k].a11*(campo->v[i+1][j][k]-campo->v[i][j][k])
															+(*jac)[i][j][k].a21*0.5*(campo->v[i][j+1][k]-campo->v[i][j-1][k])
															+(*jac)[i][j][k].a31*(campo->v[i][j][k]-campo->v[i][j][k-1]);
		campo->deriv[i][j][k].a22= (*jac)[i][j][k].a12*(campo->v[i+1][j][k]-campo->v[i][j][k])
															+(*jac)[i][j][k].a22*0.5*(campo->v[i][j+1][k]-campo->v[i][j-1][k])
															+(*jac)[i][j][k].a32*(campo->v[i][j][k]-campo->v[i][j][k-1]);
		campo->deriv[i][j][k].a23= (*jac)[i][j][k].a13*(campo->v[i+1][j][k]-campo->v[i][j][k])
															+(*jac)[i][j][k].a23*0.5*(campo->v[i][j+1][k]-campo->v[i][j-1][k])
															+(*jac)[i][j][k].a33*(campo->v[i][j][k]-campo->v[i][j][k-1]);
		// dw/dx, dw/dy, dw/dz
		campo->deriv[i][j][k].a31= (*jac)[i][j][k].a11*(campo->w[i+1][j][k]-campo->w[i][j][k])
															+(*jac)[i][j][k].a21*0.5*(campo->w[i][j+1][k]-campo->w[i][j-1][k])
															+(*jac)[i][j][k].a31*(campo->w[i][j][k]-campo->w[i][j][k-1]);
		campo->deriv[i][j][k].a32= (*jac)[i][j][k].a12*(campo->w[i+1][j][k]-campo->w[i][j][k])
															+(*jac)[i][j][k].a22*0.5*(campo->w[i][j+1][k]-campo->w[i][j-1][k])
															+(*jac)[i][j][k].a32*(campo->w[i][j][k]-campo->w[i][j][k-1]);
		campo->deriv[i][j][k].a33= (*jac)[i][j][k].a13*(campo->w[i+1][j][k]-campo->w[i][j][k])
															+(*jac)[i][j][k].a23*0.5*(campo->w[i][j+1][k]-campo->w[i][j-1][k])
															+(*jac)[i][j][k].a33*(campo->w[i][j][k]-campo->w[i][j][k-1]);
		i = nx+1;
		k = nz+1;
			// du/dx, du/dy, du/dz
		campo->deriv[i][j][k].a11= (*jac)[i][j][k].a11*(campo->u[i][j][k]-campo->u[i-1][j][k])
															+(*jac)[i][j][k].a21*0.5*(campo->u[i][j+1][k]-campo->u[i][j-1][k])
															+(*jac)[i][j][k].a31*(campo->u[i][j][k]-campo->u[i][j][k-1]);
		campo->deriv[i][j][k].a12= (*jac)[i][j][k].a12*(campo->u[i][j][k]-campo->u[i-1][j][k])
															+(*jac)[i][j][k].a22*0.5*(campo->u[i][j+1][k]-campo->u[i][j-1][k])
															+(*jac)[i][j][k].a32*(campo->u[i][j][k]-campo->u[i][j][k-1]);
		campo->deriv[i][j][k].a13= (*jac)[i][j][k].a13*(campo->u[i][j][k]-campo->u[i-1][j][k])
															+(*jac)[i][j][k].a23*0.5*(campo->u[i][j+1][k]-campo->u[i][j-1][k])
															+(*jac)[i][j][k].a33*(campo->u[i][j][k]-campo->u[i][j][k-1]);
		// dv/dx, dv/dy, dv/dz
		campo->deriv[i][j][k].a21= (*jac)[i][j][k].a11*(campo->v[i][j][k]-campo->v[i-1][j][k])
															+(*jac)[i][j][k].a21*0.5*(campo->v[i][j+1][k]-campo->v[i][j-1][k])
															+(*jac)[i][j][k].a31*(campo->v[i][j][k]-campo->v[i][j][k-1]);
		campo->deriv[i][j][k].a22= (*jac)[i][j][k].a12*(campo->v[i][j][k]-campo->v[i-1][j][k])
															+(*jac)[i][j][k].a22*0.5*(campo->v[i][j+1][k]-campo->v[i][j-1][k])
															+(*jac)[i][j][k].a32*(campo->v[i][j][k]-campo->v[i][j][k-1]);
		campo->deriv[i][j][k].a23= (*jac)[i][j][k].a13*(campo->v[i][j][k]-campo->v[i-1][j][k])
															+(*jac)[i][j][k].a23*0.5*(campo->v[i][j+1][k]-campo->v[i][j-1][k])
															+(*jac)[i][j][k].a33*(campo->v[i][j][k]-campo->v[i][j][k-1]);
		// dw/dx, dw/dy, dw/dz
		campo->deriv[i][j][k].a31= (*jac)[i][j][k].a11*(campo->w[i][j][k]-campo->w[i-1][j][k])
															+(*jac)[i][j][k].a21*0.5*(campo->w[i][j+1][k]-campo->w[i][j-1][k])
															+(*jac)[i][j][k].a31*(campo->w[i][j][k]-campo->w[i][j][k-1]);
		campo->deriv[i][j][k].a32= (*jac)[i][j][k].a12*(campo->w[i][j][k]-campo->w[i-1][j][k])
															+(*jac)[i][j][k].a22*0.5*(campo->w[i][j+1][k]-campo->w[i][j-1][k])
															+(*jac)[i][j][k].a32*(campo->w[i][j][k]-campo->w[i][j][k-1]);
		campo->deriv[i][j][k].a33= (*jac)[i][j][k].a13*(campo->w[i][j][k]-campo->w[i-1][j][k])
															+(*jac)[i][j][k].a23*0.5*(campo->w[i][j+1][k]-campo->w[i][j-1][k])
															+(*jac)[i][j][k].a33*(campo->w[i][j][k]-campo->w[i][j][k-1]);
	}
	for(i=1;i<nx+1;++i)
	{
		j = 0;
		k = 0;

		// du/dx, du/dy, du/dz
		campo->deriv[i][j][k].a11= (*jac)[i][j][k].a11*0.5*(campo->u[i+1][j][k]-campo->u[i-1][j][k])
															+((*jac)[i][j][k].a21)*(campo->u[i][j+1][k]-campo->u[i][j][k])
															+((*jac)[i][j][k].a31)*(campo->u[i][j][k+1]-campo->u[i][j][k]);
		campo->deriv[i][j][k].a12= (*jac)[i][j][k].a12*0.5*(campo->u[i+1][j][k]-campo->u[i-1][j][k])
															+((*jac)[i][j][k].a22)*(campo->u[i][j+1][k]-campo->u[i][j][k])
															+(*jac)[i][j][k].a32*(campo->u[i][j][k+1]-campo->u[i][j][k]);
		campo->deriv[i][j][k].a13= (*jac)[i][j][k].a13*0.5*(campo->u[i+1][j][k]-campo->u[i-1][j][k])
															+((*jac)[i][j][k].a23)*(campo->u[i][j+1][k]-campo->u[i][j][k])
															+(*jac)[i][j][k].a33*(campo->u[i][j][k+1]-campo->u[i][j][k]);
		// dv/dx, dv/dy, dv/dz
		campo->deriv[i][j][k].a21= (*jac)[i][j][k].a11*0.5*(campo->v[i+1][j][k]-campo->v[i-1][j][k])
															+((*jac)[i][j][k].a21)*(campo->v[i][j+1][k]-campo->v[i][j][k])
															+(*jac)[i][j][k].a31*(campo->v[i][j][k+1]-campo->v[i][j][k]);
		campo->deriv[i][j][k].a22= (*jac)[i][j][k].a12*0.5*(campo->v[i+1][j][k]-campo->v[i-1][j][k])
															+((*jac)[i][j][k].a22)*(campo->v[i][j+1][k]-campo->v[i][j][k])
															+(*jac)[i][j][k].a32*(campo->v[i][j][k+1]-campo->v[i][j][k]);
		campo->deriv[i][j][k].a23= (*jac)[i][j][k].a13*0.5*(campo->v[i+1][j][k]-campo->v[i-1][j][k])
															+((*jac)[i][j][k].a23)*(campo->v[i][j+1][k]-campo->v[i][j][k])
															+(*jac)[i][j][k].a33*(campo->v[i][j][k+1]-campo->v[i][j][k]);
		// dw/dx, dw/dy, dw/dz
		campo->deriv[i][j][k].a31= (*jac)[i][j][k].a11*0.5*(campo->w[i+1][j][k]-campo->w[i-1][j][k])
															+((*jac)[i][j][k].a21)*(campo->w[i][j+1][k]-campo->w[i][j][k])
															+(*jac)[i][j][k].a31*(campo->w[i][j][k+1]-campo->w[i][j][k]);
		campo->deriv[i][j][k].a32= (*jac)[i][j][k].a12*0.5*(campo->w[i+1][j][k]-campo->w[i-1][j][k])
															+((*jac)[i][j][k].a22)*(campo->w[i][j+1][k]-campo->w[i][j][k])
															+(*jac)[i][j][k].a32*(campo->w[i][j][k+1]-campo->w[i][j][k]);
		campo->deriv[i][j][k].a33= (*jac)[i][j][k].a13*0.5*(campo->w[i+1][j][k]-campo->w[i-1][j][k])
															+((*jac)[i][j][k].a23)*(campo->w[i][j+1][k]-campo->w[i][j][k])
															+(*jac)[i][j][k].a33*(campo->w[i][j][k+1]-campo->w[i][j][k]);
		j = ny+1;
		k = 0;
		campo->deriv[i][j][k].a11= (*jac)[i][j][k].a11*0.5*(campo->u[i+1][j][k]-campo->u[i-1][j][k])
															+((*jac)[i][j][k].a21)*(campo->u[i][j][k]-campo->u[i][j-1][k])
															+(*jac)[i][j][k].a31*(campo->u[i][j][k+1]-campo->u[i][j][k]);
		campo->deriv[i][j][k].a12= (*jac)[i][j][k].a12*0.5*(campo->u[i+1][j][k]-campo->u[i-1][j][k])
															+((*jac)[i][j][k].a22)*(campo->u[i][j][k]-campo->u[i][j-1][k])
															+(*jac)[i][j][k].a32*(campo->u[i][j][k+1]-campo->u[i][j][k]);
		campo->deriv[i][j][k].a13= (*jac)[i][j][k].a13*0.5*(campo->u[i+1][j][k]-campo->u[i-1][j][k])
															+((*jac)[i][j][k].a23)*(campo->u[i][j][k]-campo->u[i][j-1][k])
															+(*jac)[i][j][k].a33*(campo->u[i][j][k+1]-campo->u[i][j][k]);
		// dv/dx, dv/dy, dv/dz
		campo->deriv[i][j][k].a21= (*jac)[i][j][k].a11*0.5*(campo->v[i+1][j][k]-campo->v[i-1][j][k])
															+((*jac)[i][j][k].a21)*(campo->v[i][j][k]-campo->v[i][j-1][k])
															+(*jac)[i][j][k].a31*(campo->v[i][j][k+1]-campo->v[i][j][k]);
		campo->deriv[i][j][k].a22= (*jac)[i][j][k].a12*0.5*(campo->v[i+1][j][k]-campo->v[i-1][j][k])
															+((*jac)[i][j][k].a22)*(campo->v[i][j][k]-campo->v[i][j-1][k])
															+(*jac)[i][j][k].a32*(campo->v[i][j][k+1]-campo->v[i][j][k]);
		campo->deriv[i][j][k].a23= (*jac)[i][j][k].a13*0.5*(campo->v[i+1][j][k]-campo->v[i-1][j][k])
															+((*jac)[i][j][k].a23)*(campo->v[i][j][k]-campo->v[i][j-1][k])
															+(*jac)[i][j][k].a33*(campo->v[i][j][k+1]-campo->v[i][j][k]);
		// dw/dx, dw/dy, dw/dz
		campo->deriv[i][j][k].a31= (*jac)[i][j][k].a11*0.5*(campo->w[i+1][j][k]-campo->w[i-1][j][k])
															+((*jac)[i][j][k].a21)*(campo->w[i][j][k]-campo->w[i][j-1][k])
															+(*jac)[i][j][k].a31*(campo->w[i][j][k+1]-campo->w[i][j][k]);
		campo->deriv[i][j][k].a32= (*jac)[i][j][k].a12*0.5*(campo->w[i+1][j][k]-campo->w[i-1][j][k])
															+((*jac)[i][j][k].a22)*(campo->w[i][j][k]-campo->w[i][j-1][k])
															+(*jac)[i][j][k].a32*(campo->w[i][j][k+1]-campo->w[i][j][k]);
		campo->deriv[i][j][k].a33= (*jac)[i][j][k].a13*0.5*(campo->w[i+1][j][k]-campo->w[i-1][j][k])
															+((*jac)[i][j][k].a23)*(campo->w[i][j][k]-campo->w[i][j-1][k])
															+(*jac)[i][j][k].a33*(campo->w[i][j][k+1]-campo->w[i][j][k]);

		j = ny+1;
		k = nz+1;
		campo->deriv[i][j][k].a11= (*jac)[i][j][k].a11*0.5*(campo->u[i+1][j][k]-campo->u[i-1][j][k])
															+((*jac)[i][j][k].a21)*(campo->u[i][j][k]-campo->u[i][j-1][k])
															+(*jac)[i][j][k].a31*(campo->u[i][j][k]-campo->u[i][j][k-1]);
		campo->deriv[i][j][k].a12= (*jac)[i][j][k].a12*0.5*(campo->u[i+1][j][k]-campo->u[i-1][j][k])
															+((*jac)[i][j][k].a22)*(campo->u[i][j][k]-campo->u[i][j-1][k])
															+(*jac)[i][j][k].a32*(campo->u[i][j][k]-campo->u[i][j][k-1]);
		campo->deriv[i][j][k].a13= (*jac)[i][j][k].a13*0.5*(campo->u[i+1][j][k]-campo->u[i-1][j][k])
															+((*jac)[i][j][k].a23)*(campo->u[i][j][k]-campo->u[i][j-1][k])
															+(*jac)[i][j][k].a33*(campo->u[i][j][k]-campo->u[i][j][k-1]);
		// dv/dx, dv/dy, dv/dz
		campo->deriv[i][j][k].a21= (*jac)[i][j][k].a11*0.5*(campo->v[i+1][j][k]-campo->v[i-1][j][k])
															+((*jac)[i][j][k].a21)*(campo->v[i][j][k]-campo->v[i][j-1][k])
															+(*jac)[i][j][k].a31*(campo->v[i][j][k]-campo->v[i][j][k-1]);
		campo->deriv[i][j][k].a22= (*jac)[i][j][k].a12*0.5*(campo->v[i+1][j][k]-campo->v[i-1][j][k])
															+((*jac)[i][j][k].a22)*(campo->v[i][j][k]-campo->v[i][j-1][k])
															+(*jac)[i][j][k].a32*(campo->v[i][j][k]-campo->v[i][j][k-1]);
		campo->deriv[i][j][k].a23= (*jac)[i][j][k].a13*0.5*(campo->v[i+1][j][k]-campo->v[i-1][j][k])
															+((*jac)[i][j][k].a23)*(campo->v[i][j][k]-campo->v[i][j-1][k])
															+(*jac)[i][j][k].a33*(campo->v[i][j][k]-campo->v[i][j][k-1]);
		// dw/dx, dw/dy, dw/dz
		campo->deriv[i][j][k].a31= (*jac)[i][j][k].a11*0.5*(campo->w[i+1][j][k]-campo->w[i-1][j][k])
															+((*jac)[i][j][k].a21)*(campo->w[i][j][k]-campo->w[i][j-1][k])
															+(*jac)[i][j][k].a31*(campo->w[i][j][k]-campo->w[i][j][k-1]);
		campo->deriv[i][j][k].a32= (*jac)[i][j][k].a12*0.5*(campo->w[i+1][j][k]-campo->w[i-1][j][k])
															+((*jac)[i][j][k].a22)*(campo->w[i][j][k]-campo->w[i][j-1][k])
															+(*jac)[i][j][k].a32*(campo->w[i][j][k]-campo->w[i][j][k-1]);
		campo->deriv[i][j][k].a33= (*jac)[i][j][k].a13*0.5*(campo->w[i+1][j][k]-campo->w[i-1][j][k])
															+((*jac)[i][j][k].a23)*(campo->w[i][j][k]-campo->w[i][j-1][k])
															+(*jac)[i][j][k].a33*(campo->w[i][j][k]-campo->w[i][j][k-1]);

		j = 0;
		k = nz+1;
			// du/dx, du/dy, du/dz
			campo->deriv[i][j][k].a11= (*jac)[i][j][k].a11*0.5*(campo->u[i+1][j][k]-campo->u[i-1][j][k])
																+((*jac)[i][j][k].a21)*(campo->u[i][j+1][k]-campo->u[i][j][k])
																+(*jac)[i][j][k].a31*(campo->u[i][j][k]-campo->u[i][j][k-1]);
			campo->deriv[i][j][k].a12= (*jac)[i][j][k].a12*0.5*(campo->u[i+1][j][k]-campo->u[i-1][j][k])
																+((*jac)[i][j][k].a22)*(campo->u[i][j+1][k]-campo->u[i][j][k])
																+(*jac)[i][j][k].a32*(campo->u[i][j][k]-campo->u[i][j][k-1]);
			campo->deriv[i][j][k].a13= (*jac)[i][j][k].a13*0.5*(campo->u[i+1][j][k]-campo->u[i-1][j][k])
																+((*jac)[i][j][k].a23)*(campo->u[i][j+1][k]-campo->u[i][j][k])
																+(*jac)[i][j][k].a33*(campo->u[i][j][k]-campo->u[i][j][k-1]);
			// dv/dx, dv/dy, dv/dz
			campo->deriv[i][j][k].a21= (*jac)[i][j][k].a11*0.5*(campo->v[i+1][j][k]-campo->v[i-1][j][k])
																+((*jac)[i][j][k].a21)*(campo->v[i][j+1][k]-campo->v[i][j][k])
																+(*jac)[i][j][k].a31*(campo->v[i][j][k]-campo->v[i][j][k-1]);
			campo->deriv[i][j][k].a22= (*jac)[i][j][k].a12*0.5*(campo->v[i+1][j][k]-campo->v[i-1][j][k])
																+((*jac)[i][j][k].a22)*(campo->v[i][j+1][k]-campo->v[i][j][k])
																+(*jac)[i][j][k].a32*(campo->v[i][j][k]-campo->v[i][j][k-1]);
			campo->deriv[i][j][k].a23= (*jac)[i][j][k].a13*0.5*(campo->v[i+1][j][k]-campo->v[i-1][j][k])
																+((*jac)[i][j][k].a23)*(campo->v[i][j+1][k]-campo->v[i][j][k])
																+(*jac)[i][j][k].a33*(campo->v[i][j][k]-campo->v[i][j][k-1]);
			// dw/dx, dw/dy, dw/dz
			campo->deriv[i][j][k].a31= (*jac)[i][j][k].a11*0.5*(campo->w[i+1][j][k]-campo->w[i-1][j][k])
																+((*jac)[i][j][k].a21)*(campo->w[i][j+1][k]-campo->w[i][j][k])
																+(*jac)[i][j][k].a31*(campo->w[i][j][k]-campo->w[i][j][k-1]);
			campo->deriv[i][j][k].a32= (*jac)[i][j][k].a12*0.5*(campo->w[i+1][j][k]-campo->w[i-1][j][k])
																+((*jac)[i][j][k].a22)*(campo->w[i][j+1][k]-campo->w[i][j][k])
																+(*jac)[i][j][k].a32*(campo->w[i][j][k]-campo->w[i][j][k-1]);
			campo->deriv[i][j][k].a33= (*jac)[i][j][k].a13*0.5*(campo->w[i+1][j][k]-campo->w[i-1][j][k])
																+((*jac)[i][j][k].a23)*(campo->w[i][j+1][k]-campo->w[i][j][k])
																+(*jac)[i][j][k].a33*(campo->w[i][j][k]-campo->w[i][j][k-1]);
	}
// the freaking 8 corners !!!!!
	i = 0;
	j = 0;
	k = 0;
	// du/dx, du/dy, du/dz
	campo->deriv[i][j][k].a11= (*jac)[i][j][k].a11*(campo->u[i+1][j][k]-campo->u[i][j][k])
														+((*jac)[i][j][k].a21)*(campo->u[i][j+1][k]-campo->u[i][j][k])
														+((*jac)[i][j][k].a31)*(campo->u[i][j][k+1]-campo->u[i][j][k]);
	campo->deriv[i][j][k].a12= (*jac)[i][j][k].a12*(campo->u[i+1][j][k]-campo->u[i][j][k])
														+((*jac)[i][j][k].a22)*(campo->u[i][j+1][k]-campo->u[i][j][k])
														+(*jac)[i][j][k].a32*(campo->u[i][j][k+1]-campo->u[i][j][k]);
	campo->deriv[i][j][k].a13= (*jac)[i][j][k].a13*(campo->u[i+1][j][k]-campo->u[i][j][k])
														+((*jac)[i][j][k].a23)*(campo->u[i][j+1][k]-campo->u[i][j][k])
														+(*jac)[i][j][k].a33*(campo->u[i][j][k+1]-campo->u[i][j][k]);
	// dv/dx, dv/dy, dv/dz
	campo->deriv[i][j][k].a21= (*jac)[i][j][k].a11*(campo->v[i+1][j][k]-campo->v[i][j][k])
														+((*jac)[i][j][k].a21)*(campo->v[i][j+1][k]-campo->v[i][j][k])
														+(*jac)[i][j][k].a31*(campo->v[i][j][k+1]-campo->v[i][j][k]);
	campo->deriv[i][j][k].a22= (*jac)[i][j][k].a12*(campo->v[i+1][j][k]-campo->v[i][j][k])
														+((*jac)[i][j][k].a22)*(campo->v[i][j+1][k]-campo->v[i][j][k])
														+(*jac)[i][j][k].a32*(campo->v[i][j][k+1]-campo->v[i][j][k]);
	campo->deriv[i][j][k].a23= (*jac)[i][j][k].a13*(campo->v[i+1][j][k]-campo->v[i][j][k])
														+((*jac)[i][j][k].a23)*(campo->v[i][j+1][k]-campo->v[i][j][k])
														+(*jac)[i][j][k].a33*(campo->v[i][j][k+1]-campo->v[i][j][k]);
	// dw/dx, dw/dy, dw/dz
	campo->deriv[i][j][k].a31= (*jac)[i][j][k].a11*(campo->w[i+1][j][k]-campo->w[i][j][k])
														+((*jac)[i][j][k].a21)*(campo->w[i][j+1][k]-campo->w[i][j][k])
														+(*jac)[i][j][k].a31*(campo->w[i][j][k+1]-campo->w[i][j][k]);
	campo->deriv[i][j][k].a32= (*jac)[i][j][k].a12*(campo->w[i+1][j][k]-campo->w[i][j][k])
														+((*jac)[i][j][k].a22)*(campo->w[i][j+1][k]-campo->w[i][j][k])
														+(*jac)[i][j][k].a32*(campo->w[i][j][k+1]-campo->w[i][j][k]);
	campo->deriv[i][j][k].a33= (*jac)[i][j][k].a13*(campo->w[i+1][j][k]-campo->w[i][j][k])
														+((*jac)[i][j][k].a23)*(campo->w[i][j+1][k]-campo->w[i][j][k])
														+(*jac)[i][j][k].a33*(campo->w[i][j][k+1]-campo->w[i][j][k]);
	i = 0;
	j = 0;
	k = nz+1;
	// du/dx, du/dy, du/dz
	campo->deriv[i][j][k].a11= (*jac)[i][j][k].a11*(campo->u[i+1][j][k]-campo->u[i][j][k])
														+((*jac)[i][j][k].a21)*(campo->u[i][j+1][k]-campo->u[i][j][k])
														+(*jac)[i][j][k].a31*(campo->u[i][j][k]-campo->u[i][j][k-1]);
	campo->deriv[i][j][k].a12= (*jac)[i][j][k].a12*(campo->u[i+1][j][k]-campo->u[i][j][k])
														+((*jac)[i][j][k].a22)*(campo->u[i][j+1][k]-campo->u[i][j][k])
														+(*jac)[i][j][k].a32*(campo->u[i][j][k]-campo->u[i][j][k-1]);
	campo->deriv[i][j][k].a13= (*jac)[i][j][k].a13*(campo->u[i+1][j][k]-campo->u[i][j][k])
														+((*jac)[i][j][k].a23)*(campo->u[i][j+1][k]-campo->u[i][j][k])
														+(*jac)[i][j][k].a33*(campo->u[i][j][k]-campo->u[i][j][k-1]);
	// dv/dx, dv/dy, dv/dz
	campo->deriv[i][j][k].a21= (*jac)[i][j][k].a11*(campo->v[i+1][j][k]-campo->v[i][j][k])
														+((*jac)[i][j][k].a21)*(campo->v[i][j+1][k]-campo->v[i][j][k])
														+(*jac)[i][j][k].a31*(campo->v[i][j][k]-campo->v[i][j][k-1]);
	campo->deriv[i][j][k].a22= (*jac)[i][j][k].a12*(campo->v[i+1][j][k]-campo->v[i][j][k])
														+((*jac)[i][j][k].a22)*(campo->v[i][j+1][k]-campo->v[i][j][k])
														+(*jac)[i][j][k].a32*(campo->v[i][j][k]-campo->v[i][j][k-1]);
	campo->deriv[i][j][k].a23= (*jac)[i][j][k].a13*(campo->v[i+1][j][k]-campo->v[i][j][k])
														+((*jac)[i][j][k].a23)*(campo->v[i][j+1][k]-campo->v[i][j][k])
														+(*jac)[i][j][k].a33*(campo->v[i][j][k]-campo->v[i][j][k-1]);
	// dw/dx, dw/dy, dw/dz
	campo->deriv[i][j][k].a31= (*jac)[i][j][k].a11*(campo->w[i+1][j][k]-campo->w[i][j][k])
														+((*jac)[i][j][k].a21)*(campo->w[i][j+1][k]-campo->w[i][j][k])
														+(*jac)[i][j][k].a31*(campo->w[i][j][k]-campo->w[i][j][k-1]);
	campo->deriv[i][j][k].a32= (*jac)[i][j][k].a12*(campo->w[i+1][j][k]-campo->w[i][j][k])
														+((*jac)[i][j][k].a22)*(campo->w[i][j+1][k]-campo->w[i][j][k])
														+(*jac)[i][j][k].a32*(campo->w[i][j][k]-campo->w[i][j][k-1]);
	campo->deriv[i][j][k].a33= (*jac)[i][j][k].a13*(campo->w[i+1][j][k]-campo->w[i][j][k])
														+((*jac)[i][j][k].a23)*(campo->w[i][j+1][k]-campo->w[i][j][k])
														+(*jac)[i][j][k].a33*(campo->w[i][j][k]-campo->w[i][j][k-1]);
	i = 0;
	j = ny+1;
	k = 0;
	campo->deriv[i][j][k].a11= (*jac)[i][j][k].a11*(campo->u[i+1][j][k]-campo->u[i][j][k])
														+(*jac)[i][j][k].a21*(campo->u[i][j][k]-campo->u[i][j-1][k])
														+(*jac)[i][j][k].a31*(campo->u[i][j][k+1]-campo->u[i][j][k]);
	campo->deriv[i][j][k].a12= (*jac)[i][j][k].a12*(campo->u[i+1][j][k]-campo->u[i][j][k])
														+(*jac)[i][j][k].a22*(campo->u[i][j][k]-campo->u[i][j-1][k])
														+(*jac)[i][j][k].a32*(campo->u[i][j][k+1]-campo->u[i][j][k]);
	campo->deriv[i][j][k].a13= (*jac)[i][j][k].a13*(campo->u[i+1][j][k]-campo->u[i][j][k])
														+(*jac)[i][j][k].a23*(campo->u[i][j][k]-campo->u[i][j-1][k])
														+(*jac)[i][j][k].a33*(campo->u[i][j][k+1]-campo->u[i][j][k]);
	// dv/dx, dv/dy, dv/dz
	campo->deriv[i][j][k].a21= (*jac)[i][j][k].a11*(campo->v[i+1][j][k]-campo->v[i][j][k])
														+(*jac)[i][j][k].a21*(campo->v[i][j][k]-campo->v[i][j-1][k])
														+(*jac)[i][j][k].a31*(campo->v[i][j][k+1]-campo->v[i][j][k]);
	campo->deriv[i][j][k].a22= (*jac)[i][j][k].a12*(campo->v[i+1][j][k]-campo->v[i][j][k])
														+(*jac)[i][j][k].a22*(campo->v[i][j][k]-campo->v[i][j-1][k])
														+(*jac)[i][j][k].a32*(campo->v[i][j][k+1]-campo->v[i][j][k]);
	campo->deriv[i][j][k].a23= (*jac)[i][j][k].a13*(campo->v[i+1][j][k]-campo->v[i][j][k])
														+(*jac)[i][j][k].a23*(campo->v[i][j][k]-campo->v[i][j-1][k])
														+(*jac)[i][j][k].a33*(campo->v[i][j][k+1]-campo->v[i][j][k]);
	// dw/dx, dw/dy, dw/dz
	campo->deriv[i][j][k].a31= (*jac)[i][j][k].a11*(campo->w[i+1][j][k]-campo->w[i][j][k])
														+(*jac)[i][j][k].a21*(campo->w[i][j][k]-campo->w[i][j-1][k])
														+(*jac)[i][j][k].a31*(campo->w[i][j][k+1]-campo->w[i][j][k]);
	campo->deriv[i][j][k].a32= (*jac)[i][j][k].a12*(campo->w[i+1][j][k]-campo->w[i][j][k])
														+(*jac)[i][j][k].a22*(campo->w[i][j][k]-campo->w[i][j-1][k])
														+(*jac)[i][j][k].a32*(campo->w[i][j][k+1]-campo->w[i][j][k]);
	campo->deriv[i][j][k].a33= (*jac)[i][j][k].a13*(campo->w[i+1][j][k]-campo->w[i][j][k])
														+(*jac)[i][j][k].a23*(campo->w[i][j][k]-campo->w[i][j-1][k])
														+(*jac)[i][j][k].a33*(campo->w[i][j][k+1]-campo->w[i][j][k]);

	i = nx+1;
	j = 0;
	k = 0;
	// du/dx, du/dy, du/dz
	campo->deriv[i][j][k].a11= (*jac)[i][j][k].a11*(campo->u[i][j][k]-campo->u[i-1][j][k])
														+((*jac)[i][j][k].a21)*(campo->u[i][j+1][k]-campo->u[i][j][k])
														+((*jac)[i][j][k].a31)*(campo->u[i][j][k+1]-campo->u[i][j][k]);
	campo->deriv[i][j][k].a12= (*jac)[i][j][k].a12*(campo->u[i][j][k]-campo->u[i-1][j][k])
														+((*jac)[i][j][k].a22)*(campo->u[i][j+1][k]-campo->u[i][j][k])
														+(*jac)[i][j][k].a32*(campo->u[i][j][k+1]-campo->u[i][j][k]);
	campo->deriv[i][j][k].a13= (*jac)[i][j][k].a13*(campo->u[i][j][k]-campo->u[i-1][j][k])
														+((*jac)[i][j][k].a23)*(campo->u[i][j+1][k]-campo->u[i][j][k])
														+(*jac)[i][j][k].a33*(campo->u[i][j][k+1]-campo->u[i][j][k]);
	// dv/dx, dv/dy, dv/dz
	campo->deriv[i][j][k].a21= (*jac)[i][j][k].a11*(campo->v[i][j][k]-campo->v[i-1][j][k])
														+((*jac)[i][j][k].a21)*(campo->v[i][j+1][k]-campo->v[i][j][k])
														+(*jac)[i][j][k].a31*(campo->v[i][j][k+1]-campo->v[i][j][k]);
	campo->deriv[i][j][k].a22= (*jac)[i][j][k].a12*(campo->v[i][j][k]-campo->v[i-1][j][k])
														+((*jac)[i][j][k].a22)*(campo->v[i][j+1][k]-campo->v[i][j][k])
														+(*jac)[i][j][k].a32*(campo->v[i][j][k+1]-campo->v[i][j][k]);
	campo->deriv[i][j][k].a23= (*jac)[i][j][k].a13*(campo->v[i][j][k]-campo->v[i-1][j][k])
														+((*jac)[i][j][k].a23)*(campo->v[i][j+1][k]-campo->v[i][j][k])
														+(*jac)[i][j][k].a33*(campo->v[i][j][k+1]-campo->v[i][j][k]);
	// dw/dx, dw/dy, dw/dz
	campo->deriv[i][j][k].a31= (*jac)[i][j][k].a11*(campo->w[i][j][k]-campo->w[i-1][j][k])
														+((*jac)[i][j][k].a21)*(campo->w[i][j+1][k]-campo->w[i][j][k])
														+(*jac)[i][j][k].a31*(campo->w[i][j][k+1]-campo->w[i][j][k]);
	campo->deriv[i][j][k].a32= (*jac)[i][j][k].a12*(campo->w[i][j][k]-campo->w[i-1][j][k])
														+((*jac)[i][j][k].a22)*(campo->w[i][j+1][k]-campo->w[i][j][k])
														+(*jac)[i][j][k].a32*(campo->w[i][j][k+1]-campo->w[i][j][k]);
	campo->deriv[i][j][k].a33= (*jac)[i][j][k].a13*(campo->w[i][j][k]-campo->w[i-1][j][k])
														+((*jac)[i][j][k].a23)*(campo->w[i][j+1][k]-campo->w[i][j][k])
														+(*jac)[i][j][k].a33*(campo->w[i][j][k+1]-campo->w[i][j][k]);

	i = nx+1;
	j = ny+1;
	k = 0;
		// du/dx, du/dy, du/dz
		campo->deriv[i][j][k].a11= (*jac)[i][j][k].a11*(campo->u[i][j][k]-campo->u[i-1][j][k])
															+((*jac)[i][j][k].a21)*(campo->u[i][j][k]-campo->u[i][j-1][k])
															+(*jac)[i][j][k].a31*(campo->u[i][j][k+1]-campo->u[i][j][k]);
		campo->deriv[i][j][k].a12= (*jac)[i][j][k].a12*(campo->u[i][j][k]-campo->u[i-1][j][k])
															+((*jac)[i][j][k].a22)*(campo->u[i][j][k]-campo->u[i][j-1][k])
															+(*jac)[i][j][k].a32*(campo->u[i][j][k+1]-campo->u[i][j][k]);
		campo->deriv[i][j][k].a13= (*jac)[i][j][k].a13*(campo->u[i][j][k]-campo->u[i-1][j][k])
															+((*jac)[i][j][k].a23)*(campo->u[i][j][k]-campo->u[i][j-1][k])
															+(*jac)[i][j][k].a33*(campo->u[i][j][k+1]-campo->u[i][j][k]);
		// dv/dx, dv/dy, dv/dz
		campo->deriv[i][j][k].a21= (*jac)[i][j][k].a11*(campo->v[i][j][k]-campo->v[i-1][j][k])
															+((*jac)[i][j][k].a21)*(campo->v[i][j][k]-campo->v[i][j-1][k])
															+(*jac)[i][j][k].a31*(campo->v[i][j][k+1]-campo->v[i][j][k]);
		campo->deriv[i][j][k].a22= (*jac)[i][j][k].a12*(campo->v[i][j][k]-campo->v[i-1][j][k])
															+((*jac)[i][j][k].a22)*(campo->v[i][j][k]-campo->v[i][j-1][k])
															+(*jac)[i][j][k].a32*(campo->v[i][j][k+1]-campo->v[i][j][k]);
		campo->deriv[i][j][k].a23= (*jac)[i][j][k].a13*(campo->v[i][j][k]-campo->v[i-1][j][k])
															+((*jac)[i][j][k].a23)*(campo->v[i][j][k]-campo->v[i][j-1][k])
															+(*jac)[i][j][k].a33*(campo->v[i][j][k+1]-campo->v[i][j][k]);
		// dw/dx, dw/dy, dw/dz
		campo->deriv[i][j][k].a31= (*jac)[i][j][k].a11*(campo->w[i][j][k]-campo->w[i-1][j][k])
															+((*jac)[i][j][k].a21)*(campo->w[i][j][k]-campo->w[i][j-1][k])
															+(*jac)[i][j][k].a31*(campo->w[i][j][k+1]-campo->w[i][j][k]);
		campo->deriv[i][j][k].a32= (*jac)[i][j][k].a12*(campo->w[i][j][k]-campo->w[i-1][j][k])
															+((*jac)[i][j][k].a22)*(campo->w[i][j][k]-campo->w[i][j-1][k])
															+(*jac)[i][j][k].a32*(campo->w[i][j][k+1]-campo->w[i][j][k]);
		campo->deriv[i][j][k].a33= (*jac)[i][j][k].a13*(campo->w[i][j][k]-campo->w[i-1][j][k])
															+((*jac)[i][j][k].a23)*(campo->w[i][j][k]-campo->w[i][j-1][k])
															+(*jac)[i][j][k].a33*(campo->w[i][j][k+1]-campo->w[i][j][k]);

	i = nx+1;
	j = 0;
	k = nz+1;
		// du/dx, du/dy, du/dz
	campo->deriv[i][j][k].a11= (*jac)[i][j][k].a11*(campo->u[i][j][k]-campo->u[i-1][j][k])
														+(*jac)[i][j][k].a21*(campo->u[i][j+1][k]-campo->u[i][j][k])
														+(*jac)[i][j][k].a31*(campo->u[i][j][k]-campo->u[i][j][k-1]);
	campo->deriv[i][j][k].a12= (*jac)[i][j][k].a12*(campo->u[i][j][k]-campo->u[i-1][j][k])
														+(*jac)[i][j][k].a22*(campo->u[i][j+1][k]-campo->u[i][j][k])
														+(*jac)[i][j][k].a32*(campo->u[i][j][k]-campo->u[i][j][k-1]);
	campo->deriv[i][j][k].a13= (*jac)[i][j][k].a13*(campo->u[i][j][k]-campo->u[i-1][j][k])
														+(*jac)[i][j][k].a23*(campo->u[i][j+1][k]-campo->u[i][j][k])
														+(*jac)[i][j][k].a33*(campo->u[i][j][k]-campo->u[i][j][k-1]);
	// dv/dx, dv/dy, dv/dz
	campo->deriv[i][j][k].a21= (*jac)[i][j][k].a11*(campo->v[i][j][k]-campo->v[i-1][j][k])
														+(*jac)[i][j][k].a21*(campo->v[i][j+1][k]-campo->v[i][j][k])
														+(*jac)[i][j][k].a31*(campo->v[i][j][k]-campo->v[i][j][k-1]);
	campo->deriv[i][j][k].a22= (*jac)[i][j][k].a12*(campo->v[i][j][k]-campo->v[i-1][j][k])
														+(*jac)[i][j][k].a22*(campo->v[i][j+1][k]-campo->v[i][j][k])
														+(*jac)[i][j][k].a32*(campo->v[i][j][k]-campo->v[i][j][k-1]);
	campo->deriv[i][j][k].a23= (*jac)[i][j][k].a13*(campo->v[i][j][k]-campo->v[i-1][j][k])
														+(*jac)[i][j][k].a23*(campo->v[i][j+1][k]-campo->v[i][j][k])
														+(*jac)[i][j][k].a33*(campo->v[i][j][k]-campo->v[i][j][k-1]);
	// dw/dx, dw/dy, dw/dz
	campo->deriv[i][j][k].a31= (*jac)[i][j][k].a11*(campo->w[i][j][k]-campo->w[i-1][j][k])
														+(*jac)[i][j][k].a21*(campo->w[i][j+1][k]-campo->w[i][j][k])
														+(*jac)[i][j][k].a31*(campo->w[i][j][k]-campo->w[i][j][k-1]);
	campo->deriv[i][j][k].a32= (*jac)[i][j][k].a12*(campo->w[i][j][k]-campo->w[i-1][j][k])
														+(*jac)[i][j][k].a22*(campo->w[i][j+1][k]-campo->w[i][j][k])
														+(*jac)[i][j][k].a32*(campo->w[i][j][k]-campo->w[i][j][k-1]);
	campo->deriv[i][j][k].a33= (*jac)[i][j][k].a13*(campo->w[i][j][k]-campo->w[i-1][j][k])
														+(*jac)[i][j][k].a23*(campo->w[i][j+1][k]-campo->w[i][j][k])
														+(*jac)[i][j][k].a33*(campo->w[i][j][k]-campo->w[i][j][k-1]);

	i = 0;
	j = ny+1;
	k = nz+1;
	campo->deriv[i][j][k].a11= (*jac)[i][j][k].a11*(campo->u[i+1][j][k]-campo->u[i][j][k])
														+((*jac)[i][j][k].a21)*(campo->u[i][j][k]-campo->u[i][j-1][k])
														+(*jac)[i][j][k].a31*(campo->u[i][j][k]-campo->u[i][j][k-1]);
	campo->deriv[i][j][k].a12= (*jac)[i][j][k].a12*(campo->u[i+1][j][k]-campo->u[i][j][k])
														+((*jac)[i][j][k].a22)*(campo->u[i][j][k]-campo->u[i][j-1][k])
														+(*jac)[i][j][k].a32*(campo->u[i][j][k]-campo->u[i][j][k-1]);
	campo->deriv[i][j][k].a13= (*jac)[i][j][k].a13*(campo->u[i+1][j][k]-campo->u[i][j][k])
														+((*jac)[i][j][k].a23)*(campo->u[i][j][k]-campo->u[i][j-1][k])
														+(*jac)[i][j][k].a33*(campo->u[i][j][k]-campo->u[i][j][k-1]);
	// dv/dx, dv/dy, dv/dz
	campo->deriv[i][j][k].a21= (*jac)[i][j][k].a11*(campo->v[i+1][j][k]-campo->v[i][j][k])
														+((*jac)[i][j][k].a21)*(campo->v[i][j][k]-campo->v[i][j-1][k])
														+(*jac)[i][j][k].a31*(campo->v[i][j][k]-campo->v[i][j][k-1]);
	campo->deriv[i][j][k].a22= (*jac)[i][j][k].a12*(campo->v[i+1][j][k]-campo->v[i][j][k])
														+((*jac)[i][j][k].a22)*(campo->v[i][j][k]-campo->v[i][j-1][k])
														+(*jac)[i][j][k].a32*(campo->v[i][j][k]-campo->v[i][j][k-1]);
	campo->deriv[i][j][k].a23= (*jac)[i][j][k].a13*(campo->v[i+1][j][k]-campo->v[i][j][k])
														+((*jac)[i][j][k].a23)*(campo->v[i][j][k]-campo->v[i][j-1][k])
														+(*jac)[i][j][k].a33*(campo->v[i][j][k]-campo->v[i][j][k-1]);
	// dw/dx, dw/dy, dw/dz
	campo->deriv[i][j][k].a31= (*jac)[i][j][k].a11*(campo->w[i+1][j][k]-campo->w[i][j][k])
														+((*jac)[i][j][k].a21)*(campo->w[i][j][k]-campo->w[i][j-1][k])
														+(*jac)[i][j][k].a31*(campo->w[i][j][k]-campo->w[i][j][k-1]);
	campo->deriv[i][j][k].a32= (*jac)[i][j][k].a12*(campo->w[i+1][j][k]-campo->w[i][j][k])
														+((*jac)[i][j][k].a22)*(campo->w[i][j][k]-campo->w[i][j-1][k])
														+(*jac)[i][j][k].a32*(campo->w[i][j][k]-campo->w[i][j][k-1]);
	campo->deriv[i][j][k].a33= (*jac)[i][j][k].a13*(campo->w[i+1][j][k]-campo->w[i][j][k])
														+((*jac)[i][j][k].a23)*(campo->w[i][j][k]-campo->w[i][j-1][k])
														+(*jac)[i][j][k].a33*(campo->w[i][j][k]-campo->w[i][j][k-1]);

	i = nx+1;
	j = ny+1;
	k = nz+1;

		campo->deriv[i][j][k].a11= (*jac)[i][j][k].a11*(campo->u[i][j][k]-campo->u[i-1][j][k])
															+((*jac)[i][j][k].a21)*(campo->u[i][j][k]-campo->u[i][j-1][k])
															+(*jac)[i][j][k].a31*(campo->u[i][j][k]-campo->u[i][j][k-1]);
		campo->deriv[i][j][k].a12= (*jac)[i][j][k].a12*(campo->u[i][j][k]-campo->u[i-1][j][k])
															+((*jac)[i][j][k].a22)*(campo->u[i][j][k]-campo->u[i][j-1][k])
															+(*jac)[i][j][k].a32*(campo->u[i][j][k]-campo->u[i][j][k-1]);
		campo->deriv[i][j][k].a13= (*jac)[i][j][k].a13*(campo->u[i][j][k]-campo->u[i-1][j][k])
															+((*jac)[i][j][k].a23)*(campo->u[i][j][k]-campo->u[i][j-1][k])
															+(*jac)[i][j][k].a33*(campo->u[i][j][k]-campo->u[i][j][k-1]);
		// dv/dx, dv/dy, dv/dz
		campo->deriv[i][j][k].a21= (*jac)[i][j][k].a11*(campo->v[i][j][k]-campo->v[i-1][j][k])
															+((*jac)[i][j][k].a21)*(campo->v[i][j][k]-campo->v[i][j-1][k])
															+(*jac)[i][j][k].a31*(campo->v[i][j][k]-campo->v[i][j][k-1]);
		campo->deriv[i][j][k].a22= (*jac)[i][j][k].a12*(campo->v[i][j][k]-campo->v[i-1][j][k])
															+((*jac)[i][j][k].a22)*(campo->v[i][j][k]-campo->v[i][j-1][k])
															+(*jac)[i][j][k].a32*(campo->v[i][j][k]-campo->v[i][j][k-1]);
		campo->deriv[i][j][k].a23= (*jac)[i][j][k].a13*(campo->v[i][j][k]-campo->v[i-1][j][k])
															+((*jac)[i][j][k].a23)*(campo->v[i][j][k]-campo->v[i][j-1][k])
															+(*jac)[i][j][k].a33*(campo->v[i][j][k]-campo->v[i][j][k-1]);
		// dw/dx, dw/dy, dw/dz
		campo->deriv[i][j][k].a31= (*jac)[i][j][k].a11*(campo->w[i][j][k]-campo->w[i-1][j][k])
															+((*jac)[i][j][k].a21)*(campo->w[i][j][k]-campo->w[i][j-1][k])
															+(*jac)[i][j][k].a31*(campo->w[i][j][k]-campo->w[i][j][k-1]);
		campo->deriv[i][j][k].a32= (*jac)[i][j][k].a12*(campo->w[i][j][k]-campo->w[i-1][j][k])
															+((*jac)[i][j][k].a22)*(campo->w[i][j][k]-campo->w[i][j-1][k])
															+(*jac)[i][j][k].a32*(campo->w[i][j][k]-campo->w[i][j][k-1]);
		campo->deriv[i][j][k].a33= (*jac)[i][j][k].a13*(campo->w[i][j][k]-campo->w[i-1][j][k])
															+((*jac)[i][j][k].a23)*(campo->w[i][j][k]-campo->w[i][j-1][k])
															+(*jac)[i][j][k].a33*(campo->w[i][j][k]-campo->w[i][j][k-1]);
	// This is an easter egg: if you find this comment, and you are absolutely confused about what I did
	// please add an X  here (I start): X
}

void calculateScalarDerivatives(int nx,
																int ny,
																int nz,
																enum derivdir d,
																struct matrix ***jac,
																double ***campo,
																double ****deriv)
{
	int i, j, k;
	
	if ((*deriv) == NULL)
	{
		(*deriv) = malloc ((nx+2) * sizeof(*(*deriv)));
		for (i = 0; i < nx+2; i++)
		{
			(*deriv)[i] = malloc((ny+2)*sizeof(**(*deriv)));
			for (j = 0; j < ny + 2; j++)
				(*deriv)[i][j] = malloc((nz+2)*sizeof(***(*deriv)));
		}
	}
// the interior field is calculated using centered differences
	for(i=1;i<nx+1;i++)
		for(j=1;j<ny+1;j++)
			for(k=1;k<nz+1;k++)
			{
				switch(d)
				{
					case dirX:
						(*deriv)[i][j][k] = jac[i][j][k].a11 * 0.5 * (campo[i+1][j][k]-campo[i-1][j][k]) +
														 jac[i][j][k].a12 * 0.5 * (campo[i][j+1][k]-campo[i][j-1][k]) +
														 jac[i][j][k].a13	* 0.5 * (campo[i][j][k+1]-campo[i][j][k-1]);	
					break;						
					case dirY:
						(*deriv)[i][j][k] = jac[i][j][k].a21 * 0.5 * (campo[i+1][j][k]-campo[i-1][j][k]) +
														 jac[i][j][k].a22 * 0.5 * (campo[i][j+1][k]-campo[i][j-1][k]) +
														 jac[i][j][k].a23	* 0.5 * (campo[i][j][k+1]-campo[i][j][k-1]);	
					break;
					case dirZ:
						(*deriv)[i][j][k] = jac[i][j][k].a31 * 0.5 * (campo[i+1][j][k]-campo[i-1][j][k]) +
														 jac[i][j][k].a32 * 0.5 * (campo[i][j+1][k]-campo[i][j-1][k]) +
														 jac[i][j][k].a33	* 0.5 * (campo[i][j][k+1]-campo[i][j][k-1]);	
					break;
					default:
						return;
				}
			}	
// faces 1 and 2	
	for	(j=1;j<ny+1;j++)
		for	(k=1;k<nz+1;k++)
		{
			i = 0;
				switch(d)
				{
					case dirX:
						(*deriv)[i][j][k] = jac[i][j][k].a11 * 1.0 * (campo[i+1][j][k]-campo[i][j][k]) +
														 jac[i][j][k].a12 * 0.5 * (campo[i][j+1][k]-campo[i][j-1][k]) +
														 jac[i][j][k].a13	* 0.5 * (campo[i][j][k+1]-campo[i][j][k-1]);	
					break;						
					case dirY:
						(*deriv)[i][j][k] = jac[i][j][k].a21 * 1.0 * (campo[i+1][j][k]-campo[i][j][k]) +
														 jac[i][j][k].a22 * 0.5 * (campo[i][j+1][k]-campo[i][j-1][k]) +
														 jac[i][j][k].a23	* 0.5 * (campo[i][j][k+1]-campo[i][j][k-1]);	
					break;
					case dirZ:
						(*deriv)[i][j][k] = jac[i][j][k].a31 * 1.0 * (campo[i+1][j][k]-campo[i][j][k]) +
														 jac[i][j][k].a32 * 0.5 * (campo[i][j+1][k]-campo[i][j-1][k]) +
														 jac[i][j][k].a33	* 0.5 * (campo[i][j][k+1]-campo[i][j][k-1]);	
					break;
					default:
						return;
				}
			i = nx + 1;
				switch(d)
				{
					case dirX:
						(*deriv)[i][j][k] = jac[i][j][k].a11 * 1.0 * (campo[i][j][k]-campo[i-1][j][k]) +
														 jac[i][j][k].a12 * 0.5 * (campo[i][j+1][k]-campo[i][j-1][k]) +
														 jac[i][j][k].a13	* 0.5 * (campo[i][j][k+1]-campo[i][j][k-1]);	
					break;						
					case dirY:
						(*deriv)[i][j][k] = jac[i][j][k].a21 * 1.0 * (campo[i][j][k]-campo[i-1][j][k]) +
														 jac[i][j][k].a22 * 0.5 * (campo[i][j+1][k]-campo[i][j-1][k]) +
														 jac[i][j][k].a23	* 0.5 * (campo[i][j][k+1]-campo[i][j][k-1]);	
					break;
					case dirZ:
						(*deriv)[i][j][k] = jac[i][j][k].a31 * 1.0 * (campo[i][j][k]-campo[i-1][j][k]) +
														 jac[i][j][k].a32 * 0.5 * (campo[i][j+1][k]-campo[i][j-1][k]) +
														 jac[i][j][k].a33	* 0.5 * (campo[i][j][k+1]-campo[i][j][k-1]);	
					break;
					default:
						return;
				}
		}
// faces right and left
	for(i=1;i<nx+1;i++)
		for(j=1;j<ny+1;j++)
		{
			k = 0;
			switch(d)
			{
				case dirX:
					(*deriv)[i][j][k] = jac[i][j][k].a11 * 0.5 * (campo[i+1][j][k]-campo[i-1][j][k]) +
													 jac[i][j][k].a12 * 0.5 * (campo[i][j+1][k]-campo[i][j-1][k]) +
													 jac[i][j][k].a13	* 1.0 * (campo[i][j][k+1]-campo[i][j][k]);	
				break;						
				case dirY:
					(*deriv)[i][j][k] = jac[i][j][k].a21 * 0.5 * (campo[i+1][j][k]-campo[i-1][j][k]) +
													 jac[i][j][k].a22 * 0.5 * (campo[i][j+1][k]-campo[i][j-1][k]) +
													 jac[i][j][k].a23	* 1.0 * (campo[i][j][k+1]-campo[i][j][k]);	
				break;
				case dirZ:
					(*deriv)[i][j][k] = jac[i][j][k].a31 * 0.5 * (campo[i+1][j][k]-campo[i-1][j][k]) +
													 jac[i][j][k].a32 * 0.5 * (campo[i][j+1][k]-campo[i][j-1][k]) +
													 jac[i][j][k].a33	* 1.0 * (campo[i][j][k+1]-campo[i][j][k]);	
				break;
				default:
					return;
			}
			k = nz+1;
			switch(d)
			{
				case dirX:
					(*deriv)[i][j][k] = jac[i][j][k].a11 * 0.5 * (campo[i+1][j][k]-campo[i-1][j][k]) +
													 jac[i][j][k].a12 * 0.5 * (campo[i][j+1][k]-campo[i][j-1][k]) +
													 jac[i][j][k].a13	* 1.0 * (campo[i][j][k]-campo[i][j][k-1]);	
				break;						
				case dirY:
					(*deriv)[i][j][k] = jac[i][j][k].a21 * 0.5 * (campo[i+1][j][k]-campo[i-1][j][k]) +
													 jac[i][j][k].a22 * 0.5 * (campo[i][j+1][k]-campo[i][j-1][k]) +
													 jac[i][j][k].a23	* 1.0 * (campo[i][j][k]-campo[i][j][k-1]);	
				break;
				case dirZ:
					(*deriv)[i][j][k] = jac[i][j][k].a31 * 0.5 * (campo[i+1][j][k]-campo[i-1][j][k]) +
													 jac[i][j][k].a32 * 0.5 * (campo[i][j+1][k]-campo[i][j-1][k]) +
													 jac[i][j][k].a33	* 1.0 * (campo[i][j][k]-campo[i][j][k-1]);	
				break;
				default:
					return;
			}
		}
// faces bottom and top
	for(i=1;i<nx+1;i++)
		for(k=1;k<nz+1;k++)
		{
			j=0;
			switch(d)
			{
				case dirX:
					(*deriv)[i][j][k] = jac[i][j][k].a11 * 0.5 * (campo[i+1][j][k]-campo[i-1][j][k]) +
													 jac[i][j][k].a12 * 1.0 * (campo[i][j+1][k]-campo[i][j][k]) +
													 jac[i][j][k].a13	* 0.5 * (campo[i][j][k+1]-campo[i][j][k-1]);	
				break;						
				case dirY:
					(*deriv)[i][j][k] = jac[i][j][k].a21 * 0.5 * (campo[i+1][j][k]-campo[i-1][j][k]) +
													 jac[i][j][k].a22 * 1.0 * (campo[i][j+1][k]-campo[i][j][k]) +
													 jac[i][j][k].a23	* 0.5 * (campo[i][j][k+1]-campo[i][j][k-1]);	
				break;
				case dirZ:
					(*deriv)[i][j][k] = jac[i][j][k].a31 * 0.5 * (campo[i+1][j][k]-campo[i-1][j][k]) +
													 jac[i][j][k].a32 * 1.0 * (campo[i][j+1][k]-campo[i][j][k]) +
													 jac[i][j][k].a33	* 0.5 * (campo[i][j][k+1]-campo[i][j][k-1]);	
				break;
				default:
					return;
			}
			j=ny+1;
			switch(d)
			{
				case dirX:
					(*deriv)[i][j][k] = jac[i][j][k].a11 * 0.5 * (campo[i+1][j][k]-campo[i-1][j][k]) +
													 jac[i][j][k].a12 * 1.0 * (campo[i][j][k]-campo[i][j-1][k]) +
													 jac[i][j][k].a13	* 0.5 * (campo[i][j][k+1]-campo[i][j][k-1]);	
				break;						
				case dirY:
					(*deriv)[i][j][k] = jac[i][j][k].a21 * 0.5 * (campo[i+1][j][k]-campo[i-1][j][k]) +
													 jac[i][j][k].a22 * 1.0 * (campo[i][j][k]-campo[i][j-1][k]) +
													 jac[i][j][k].a23	* 0.5 * (campo[i][j][k+1]-campo[i][j][k-1]);	
				break;
				case dirZ:
					(*deriv)[i][j][k] = jac[i][j][k].a31 * 0.5 * (campo[i+1][j][k]-campo[i-1][j][k]) +
													 jac[i][j][k].a32 * 1.0 * (campo[i][j][k]-campo[i][j-1][k]) +
													 jac[i][j][k].a33	* 0.5 * (campo[i][j][k+1]-campo[i][j][k-1]);	
				break;
				default:
					return;
			}
		}
// the 
	for(k=1;k<nz+1;++k)
	{
		i = 0;
		j = 0;
			switch(d)
			{
				case dirX:
					(*deriv)[i][j][k] = jac[i][j][k].a11 * 1.0 * (campo[i+1][j][k]-campo[i][j][k]) +
													 jac[i][j][k].a12 * 1.0 * (campo[i][j+1][k]-campo[i][j][k]) +
													 jac[i][j][k].a13	* 0.5 * (campo[i][j][k+1]-campo[i][j][k-1]);	
				break;						
				case dirY:
					(*deriv)[i][j][k] = jac[i][j][k].a21 * 1.0 * (campo[i+1][j][k]-campo[i][j][k]) +
													 jac[i][j][k].a22 * 1.0 * (campo[i][j+1][k]-campo[i][j][k]) +
													 jac[i][j][k].a23	* 0.5 * (campo[i][j][k+1]-campo[i][j][k-1]);	
				break;
				case dirZ:
					(*deriv)[i][j][k] = jac[i][j][k].a31 * 1.0 * (campo[i+1][j][k]-campo[i][j][k]) +
													 jac[i][j][k].a32 * 1.0 * (campo[i][j+1][k]-campo[i][j][k]) +
													 jac[i][j][k].a33	* 0.5 * (campo[i][j][k+1]-campo[i][j][k-1]);	
				break;
				default:
					return;
			}

		i = 0;
		j = ny+1;
			switch(d)
			{
				case dirX:
					(*deriv)[i][j][k] = jac[i][j][k].a11 * 1.0 * (campo[i+1][j][k]-campo[i][j][k]) +
													 jac[i][j][k].a12 * 1.0 * (campo[i][j][k]-campo[i][j-1][k]) +
													 jac[i][j][k].a13	* 0.5 * (campo[i][j][k+1]-campo[i][j][k-1]);	
				break;						
				case dirY:
					(*deriv)[i][j][k] = jac[i][j][k].a21 * 1.0 * (campo[i+1][j][k]-campo[i][j][k]) +
													 jac[i][j][k].a22 * 1.0 * (campo[i][j][k]-campo[i][j-1][k]) +
													 jac[i][j][k].a23	* 0.5 * (campo[i][j][k+1]-campo[i][j][k-1]);	
				break;
				case dirZ:
					(*deriv)[i][j][k] = jac[i][j][k].a31 * 1.0 * (campo[i+1][j][k]-campo[i][j][k]) +
													 jac[i][j][k].a32 * 1.0 * (campo[i][j][k]-campo[i][j-1][k]) +
													 jac[i][j][k].a33	* 0.5 * (campo[i][j][k+1]-campo[i][j][k-1]);	
				break;
				default:
					return;
			}

		i = nx+1;
		j = 0;
			switch(d)
			{
				case dirX:
					(*deriv)[i][j][k] = jac[i][j][k].a11 * 1.0 * (campo[i][j][k]-campo[i-1][j][k]) +
													 jac[i][j][k].a12 * 1.0 * (campo[i][j+1][k]-campo[i][j][k]) +
													 jac[i][j][k].a13	* 0.5 * (campo[i][j][k+1]-campo[i][j][k-1]);	
				break;						
				case dirY:
					(*deriv)[i][j][k] = jac[i][j][k].a21 * 1.0 * (campo[i][j][k]-campo[i-1][j][k]) +
													 jac[i][j][k].a22 * 1.0 * (campo[i][j+1][k]-campo[i][j][k]) +
													 jac[i][j][k].a23	* 0.5 * (campo[i][j][k+1]-campo[i][j][k-1]);	
				break;
				case dirZ:
					(*deriv)[i][j][k] = jac[i][j][k].a31 * 1.0 * (campo[i][j][k]-campo[i-1][j][k]) +
													 jac[i][j][k].a32 * 1.0 * (campo[i][j+1][k]-campo[i][j][k]) +
													 jac[i][j][k].a33	* 0.5 * (campo[i][j][k+1]-campo[i][j][k-1]);	
				break;
				default:
					return;
			}

		i = nx+1;
		j = ny+1;
			switch(d)
			{
				case dirX:
					(*deriv)[i][j][k] = jac[i][j][k].a11 * 1.0 * (campo[i][j][k]-campo[i-1][j][k]) +
													 jac[i][j][k].a12 * 1.0 * (campo[i][j][k]-campo[i][j-1][k]) +
													 jac[i][j][k].a13	* 0.5 * (campo[i][j][k+1]-campo[i][j][k-1]);	
				break;						
				case dirY:
					(*deriv)[i][j][k] = jac[i][j][k].a21 * 1.0 * (campo[i][j][k]-campo[i-1][j][k]) +
													 jac[i][j][k].a22 * 1.0 * (campo[i][j][k]-campo[i][j-1][k]) +
													 jac[i][j][k].a23	* 0.5 * (campo[i][j][k+1]-campo[i][j][k-1]);	
				break;
				case dirZ:
					(*deriv)[i][j][k] = jac[i][j][k].a31 * 1.0 * (campo[i][j][k]-campo[i-1][j][k]) +
													 jac[i][j][k].a32 * 1.0 * (campo[i][j][k]-campo[i][j-1][k]) +
													 jac[i][j][k].a33	* 0.5 * (campo[i][j][k+1]-campo[i][j][k-1]);	
				break;
				default:
					return;
			}
	}
	for(j=1;j<ny+1;++j)
	{
		i = 0;
		k = 0;
			switch(d)
			{
				case dirX:
					(*deriv)[i][j][k] = jac[i][j][k].a11 * 1.0 * (campo[i+1][j][k]-campo[i][j][k]) +
													 jac[i][j][k].a12 * 0.5 * (campo[i][j+1][k]-campo[i][j-1][k]) +
													 jac[i][j][k].a13	* 1.0 * (campo[i][j][k+1]-campo[i][j][k]);	
				break;						
				case dirY:
					(*deriv)[i][j][k] = jac[i][j][k].a21 * 1.0 * (campo[i+1][j][k]-campo[i][j][k]) +
													 jac[i][j][k].a22 * 0.5 * (campo[i][j+1][k]-campo[i][j-1][k]) +
													 jac[i][j][k].a23	* 1.0 * (campo[i][j][k+1]-campo[i][j][k]);	
				break;
				case dirZ:
					(*deriv)[i][j][k] = jac[i][j][k].a31 * 1.0 * (campo[i+1][j][k]-campo[i][j][k]) +
													 jac[i][j][k].a32 * 0.5 * (campo[i][j+1][k]-campo[i][j-1][k]) +
													 jac[i][j][k].a33	* 1.0 * (campo[i][j][k+1]-campo[i][j][k]);	
				break;
				default:
					return;
			}
		i = nx+1;
		k = 0;
			switch(d)
			{
				case dirX:
					(*deriv)[i][j][k] = jac[i][j][k].a11 * 1.0 * (campo[i][j][k]-campo[i-1][j][k]) +
													 jac[i][j][k].a12 * 0.5 * (campo[i][j+1][k]-campo[i][j-1][k]) +
													 jac[i][j][k].a13	* 1.0 * (campo[i][j][k+1]-campo[i][j][k]);	
				break;						
				case dirY:
					(*deriv)[i][j][k] = jac[i][j][k].a21 * 1.0 * (campo[i][j][k]-campo[i-1][j][k]) +
													 jac[i][j][k].a22 * 0.5 * (campo[i][j+1][k]-campo[i][j-1][k]) +
													 jac[i][j][k].a23	* 1.0 * (campo[i][j][k+1]-campo[i][j][k]);	
				break;
				case dirZ:
					(*deriv)[i][j][k] = jac[i][j][k].a31 * 1.0 * (campo[i][j][k]-campo[i-1][j][k]) +
													 jac[i][j][k].a32 * 0.5 * (campo[i][j+1][k]-campo[i][j-1][k]) +
													 jac[i][j][k].a33	* 1.0 * (campo[i][j][k+1]-campo[i][j][k]);	
				break;
				default:
					return;
			}
		i = nx+1;
		k = nz+1;
			switch(d)
			{
				case dirX:
					(*deriv)[i][j][k] = jac[i][j][k].a11 * 1.0 * (campo[i][j][k]-campo[i-1][j][k]) +
													 jac[i][j][k].a12 * 0.5 * (campo[i][j+1][k]-campo[i][j-1][k]) +
													 jac[i][j][k].a13	* 1.0 * (campo[i][j][k]-campo[i][j][k-1]);	
				break;						
				case dirY:
					(*deriv)[i][j][k] = jac[i][j][k].a21 * 1.0 * (campo[i][j][k]-campo[i-1][j][k]) +
													 jac[i][j][k].a22 * 0.5 * (campo[i][j+1][k]-campo[i][j-1][k]) +
													 jac[i][j][k].a23	* 1.0 * (campo[i][j][k]-campo[i][j][k-1]);	
				break;
				case dirZ:
					(*deriv)[i][j][k] = jac[i][j][k].a31 * 1.0 * (campo[i][j][k]-campo[i-1][j][k]) +
													 jac[i][j][k].a32 * 0.5 * (campo[i][j+1][k]-campo[i][j-1][k]) +
													 jac[i][j][k].a33	* 1.0 * (campo[i][j][k]-campo[i][j][k-1]);	
				break;
				default:
					return;
			}
		i = 0;
		k = nz+1;
			switch(d)
			{
				case dirX:
					(*deriv)[i][j][k] = jac[i][j][k].a11 * 1.0 * (campo[i+1][j][k]-campo[i][j][k]) +
													 jac[i][j][k].a12 * 0.5 * (campo[i][j+1][k]-campo[i][j-1][k]) +
													 jac[i][j][k].a13	* 1.0 * (campo[i][j][k]-campo[i][j][k-1]);	
				break;						
				case dirY:
					(*deriv)[i][j][k] = jac[i][j][k].a21 * 1.0 * (campo[i+1][j][k]-campo[i][j][k]) +
													 jac[i][j][k].a22 * 0.5 * (campo[i][j+1][k]-campo[i][j-1][k]) +
													 jac[i][j][k].a23	* 1.0 * (campo[i][j][k]-campo[i][j][k-1]);	
				break;
				case dirZ:
					(*deriv)[i][j][k] = jac[i][j][k].a31 * 1.0 * (campo[i+1][j][k]-campo[i][j][k]) +
													 jac[i][j][k].a32 * 0.5 * (campo[i][j+1][k]-campo[i][j-1][k]) +
													 jac[i][j][k].a33	* 1.0 * (campo[i][j][k]-campo[i][j][k-1]);	
				break;
				default:
					return;
			}
	}
	for(i=1;i<nx+1;++i)
	{
		j = 0;
		k = 0;
			switch(d)
			{
				case dirX:
					(*deriv)[i][j][k] = jac[i][j][k].a11 * 0.5 * (campo[i+1][j][k]-campo[i-1][j][k]) +
													 jac[i][j][k].a12 * 1.0 * (campo[i][j+1][k]-campo[i][j][k]) +
													 jac[i][j][k].a13	* 1.0 * (campo[i][j][k+1]-campo[i][j][k]);	
				break;						
				case dirY:
					(*deriv)[i][j][k] = jac[i][j][k].a21 * 0.5 * (campo[i+1][j][k]-campo[i-1][j][k]) +
													 jac[i][j][k].a22 * 1.0 * (campo[i][j+1][k]-campo[i][j][k]) +
													 jac[i][j][k].a23	* 1.0 * (campo[i][j][k+1]-campo[i][j][k]);	
				break;
				case dirZ:
					(*deriv)[i][j][k] = jac[i][j][k].a31 * 0.5 * (campo[i+1][j][k]-campo[i-1][j][k]) +
													 jac[i][j][k].a32 * 1.0 * (campo[i][j+1][k]-campo[i][j][k]) +
													 jac[i][j][k].a33	* 1.0 * (campo[i][j][k+1]-campo[i][j][k]);	
				break;
				default:
					return;
			}
		j = ny+1;
		k = 0;
			switch(d)
			{
				case dirX:
					(*deriv)[i][j][k] = jac[i][j][k].a11 * 0.5 * (campo[i+1][j][k]-campo[i-1][j][k]) +
													 jac[i][j][k].a12 * 1.0 * (campo[i][j][k]-campo[i][j-1][k]) +
													 jac[i][j][k].a13	* 1.0 * (campo[i][j][k+1]-campo[i][j][k]);	
				break;						
				case dirY:
					(*deriv)[i][j][k] = jac[i][j][k].a21 * 0.5 * (campo[i+1][j][k]-campo[i-1][j][k]) +
													 jac[i][j][k].a22 * 1.0 * (campo[i][j][k]-campo[i][j-1][k]) +
													 jac[i][j][k].a23	* 1.0 * (campo[i][j][k+1]-campo[i][j][k]);	
				break;
				case dirZ:
					(*deriv)[i][j][k] = jac[i][j][k].a31 * 0.5 * (campo[i+1][j][k]-campo[i-1][j][k]) +
													 jac[i][j][k].a32 * 1.0 * (campo[i][j][k]-campo[i][j-1][k]) +
													 jac[i][j][k].a33	* 1.0 * (campo[i][j][k+1]-campo[i][j][k]);	
				break;
				default:
					return;
			}

		j = ny+1;
		k = nz+1;
			switch(d)
			{
				case dirX:
					(*deriv)[i][j][k] = jac[i][j][k].a11 * 0.5 * (campo[i+1][j][k]-campo[i-1][j][k]) +
													 jac[i][j][k].a12 * 1.0 * (campo[i][j][k]-campo[i][j-1][k]) +
													 jac[i][j][k].a13	* 1.0 * (campo[i][j][k]-campo[i][j][k-1]);	
				break;						
				case dirY:
					(*deriv)[i][j][k] = jac[i][j][k].a21 * 0.5 * (campo[i+1][j][k]-campo[i-1][j][k]) +
													 jac[i][j][k].a22 * 1.0 * (campo[i][j][k]-campo[i][j-1][k]) +
													 jac[i][j][k].a23	* 1.0 * (campo[i][j][k]-campo[i][j][k-1]);	
				break;
				case dirZ:
					(*deriv)[i][j][k] = jac[i][j][k].a31 * 0.5 * (campo[i+1][j][k]-campo[i-1][j][k]) +
													 jac[i][j][k].a32 * 1.0 * (campo[i][j][k]-campo[i][j-1][k]) +
													 jac[i][j][k].a33	* 1.0 * (campo[i][j][k]-campo[i][j][k-1]);	
				break;
				default:
					return;
			}


		j = 0;
		k = nz+1;
			switch(d)
			{
				case dirX:
					(*deriv)[i][j][k] = jac[i][j][k].a11 * 0.5 * (campo[i+1][j][k]-campo[i-1][j][k]) +
													 jac[i][j][k].a12 * 1.0 * (campo[i][j+1][k]-campo[i][j][k]) +
													 jac[i][j][k].a13	* 1.0 * (campo[i][j][k]-campo[i][j][k-1]);	
				break;						
				case dirY:
					(*deriv)[i][j][k] = jac[i][j][k].a21 * 0.5 * (campo[i+1][j][k]-campo[i-1][j][k]) +
													 jac[i][j][k].a22 * 1.0 * (campo[i][j+1][k]-campo[i][j][k]) +
													 jac[i][j][k].a23	* 1.0 * (campo[i][j][k]-campo[i][j][k-1]);	
				break;
				case dirZ:
					(*deriv)[i][j][k] = jac[i][j][k].a31 * 0.5 * (campo[i+1][j][k]-campo[i-1][j][k]) +
													 jac[i][j][k].a32 * 1.0 * (campo[i][j+1][k]-campo[i][j][k]) +
													 jac[i][j][k].a33	* 1.0 * (campo[i][j][k]-campo[i][j][k-1]);	
				break;
				default:
					return;
			}

	}
	i = 0;
	j = 0;
	k = 0;
	switch(d)
	{
		case dirX:
			(*deriv)[i][j][k] = jac[i][j][k].a11 * 1.0 * (campo[i+1][j][k]-campo[i][j][k]) +
											 jac[i][j][k].a12 * 1.0 * (campo[i][j+1][k]-campo[i][j][k]) +
											 jac[i][j][k].a13	* 1.0 * (campo[i][j][k+1]-campo[i][j][k]);	
		break;						
		case dirY:
			(*deriv)[i][j][k] = jac[i][j][k].a21 * 1.0 * (campo[i+1][j][k]-campo[i][j][k]) +
											 jac[i][j][k].a22 * 1.0 * (campo[i][j+1][k]-campo[i][j][k]) +
											 jac[i][j][k].a23	* 1.0 * (campo[i][j][k+1]-campo[i][j][k]);	
		break;
		case dirZ:
			(*deriv)[i][j][k] = jac[i][j][k].a31 * 1.0 * (campo[i+1][j][k]-campo[i][j][k]) +
											 jac[i][j][k].a32 * 1.0 * (campo[i][j+1][k]-campo[i][j][k]) +
											 jac[i][j][k].a33	* 1.0 * (campo[i][j][k+1]-campo[i][j][k]);	
		break;
		default:
			return;
	}

	i = 0;
	j = 0;
	k = nz+1;
	switch(d)
	{
		case dirX:
			(*deriv)[i][j][k] = jac[i][j][k].a11 * 1.0 * (campo[i+1][j][k]-campo[i][j][k]) +
											 jac[i][j][k].a12 * 1.0 * (campo[i][j+1][k]-campo[i][j][k]) +
											 jac[i][j][k].a13	* 1.0 * (campo[i][j][k]-campo[i][j][k-1]);	
		break;						
		case dirY:
			(*deriv)[i][j][k] = jac[i][j][k].a21 * 1.0 * (campo[i+1][j][k]-campo[i][j][k]) +
											 jac[i][j][k].a22 * 1.0 * (campo[i][j+1][k]-campo[i][j][k]) +
											 jac[i][j][k].a23	* 1.0 * (campo[i][j][k]-campo[i][j][k-1]);	
		break;
		case dirZ:
			(*deriv)[i][j][k] = jac[i][j][k].a31 * 1.0 * (campo[i+1][j][k]-campo[i][j][k]) +
											 jac[i][j][k].a32 * 1.0 * (campo[i][j+1][k]-campo[i][j][k]) +
											 jac[i][j][k].a33	* 1.0 * (campo[i][j][k]-campo[i][j][k-1]);	
		break;
		default:
			return;
	}

	i = 0;
	j = ny+1;
	k = 0;
	switch(d)
	{
		case dirX:
			(*deriv)[i][j][k] = jac[i][j][k].a11 * 1.0 * (campo[i+1][j][k]-campo[i][j][k]) +
											 jac[i][j][k].a12 * 1.0 * (campo[i][j][k]-campo[i][j-1][k]) +
											 jac[i][j][k].a13	* 1.0 * (campo[i][j][k+1]-campo[i][j][k]);	
		break;						
		case dirY:
			(*deriv)[i][j][k] = jac[i][j][k].a21 * 1.0 * (campo[i+1][j][k]-campo[i][j][k]) +
											 jac[i][j][k].a22 * 1.0 * (campo[i][j][k]-campo[i][j-1][k]) +
											 jac[i][j][k].a23	* 1.0 * (campo[i][j][k+1]-campo[i][j][k]);	
		break;
		case dirZ:
			(*deriv)[i][j][k] = jac[i][j][k].a31 * 1.0 * (campo[i+1][j][k]-campo[i][j][k]) +
											 jac[i][j][k].a32 * 1.0 * (campo[i][j][k]-campo[i][j-1][k]) +
											 jac[i][j][k].a33	* 1.0 * (campo[i][j][k+1]-campo[i][j][k]);	
		break;
		default:
			return;
	}

	i = nx+1;
	j = 0;
	k = 0;

	switch(d)
	{
		case dirX:
			(*deriv)[i][j][k] = jac[i][j][k].a11 * 1.0 * (campo[i][j][k]-campo[i-1][j][k]) +
											 jac[i][j][k].a12 * 1.0 * (campo[i][j+1][k]-campo[i][j][k]) +
											 jac[i][j][k].a13	* 1.0 * (campo[i][j][k+1]-campo[i][j][k]);	
		break;						
		case dirY:
			(*deriv)[i][j][k] = jac[i][j][k].a21 * 1.0 * (campo[i][j][k]-campo[i-1][j][k]) +
											 jac[i][j][k].a22 * 1.0 * (campo[i][j+1][k]-campo[i][j][k]) +
											 jac[i][j][k].a23	* 1.0 * (campo[i][j][k+1]-campo[i][j][k]);	
		break;
		case dirZ:
			(*deriv)[i][j][k] = jac[i][j][k].a31 * 1.0 * (campo[i][j][k]-campo[i-1][j][k]) +
											 jac[i][j][k].a32 * 1.0 * (campo[i][j+1][k]-campo[i][j][k]) +
											 jac[i][j][k].a33	* 1.0 * (campo[i][j][k+1]-campo[i][j][k]);	
		break;
		default:
			return;
	}
	i = nx+1;
	j = ny+1;
	k = 0;
	switch(d)
	{
		case dirX:
			(*deriv)[i][j][k] = jac[i][j][k].a11 * 1.0 * (campo[i][j][k]-campo[i-1][j][k]) +
											 jac[i][j][k].a12 * 1.0 * (campo[i][j][k]-campo[i][j-1][k]) +
											 jac[i][j][k].a13	* 1.0 * (campo[i][j][k+1]-campo[i][j][k]);	
		break;						
		case dirY:
			(*deriv)[i][j][k] = jac[i][j][k].a21 * 1.0 * (campo[i][j][k]-campo[i-1][j][k]) +
											 jac[i][j][k].a22 * 1.0 * (campo[i][j][k]-campo[i][j-1][k]) +
											 jac[i][j][k].a23	* 1.0 * (campo[i][j][k+1]-campo[i][j][k]);	
		break;
		case dirZ:
			(*deriv)[i][j][k] = jac[i][j][k].a31 * 1.0 * (campo[i][j][k]-campo[i-1][j][k]) +
											 jac[i][j][k].a32 * 1.0 * (campo[i][j][k]-campo[i][j-1][k]) +
											 jac[i][j][k].a33	* 1.0 * (campo[i][j][k+1]-campo[i][j][k]);	
		break;
		default:
			return;
	}

	i = nx+1;
	j = 0;
	k = nz+1;
	switch(d)
	{
		case dirX:
			(*deriv)[i][j][k] = jac[i][j][k].a11 * 1.0 * (campo[i][j][k]-campo[i-1][j][k]) +
											 jac[i][j][k].a12 * 1.0 * (campo[i][j+1][k]-campo[i][j][k]) +
											 jac[i][j][k].a13	* 1.0 * (campo[i][j][k]-campo[i][j][k-1]);	
		break;						
		case dirY:
			(*deriv)[i][j][k] = jac[i][j][k].a21 * 1.0 * (campo[i][j][k]-campo[i-1][j][k]) +
											 jac[i][j][k].a22 * 1.0 * (campo[i][j+1][k]-campo[i][j][k]) +
											 jac[i][j][k].a23	* 1.0 * (campo[i][j][k]-campo[i][j][k-1]);	
		break;
		case dirZ:
			(*deriv)[i][j][k] = jac[i][j][k].a31 * 1.0 * (campo[i][j][k]-campo[i-1][j][k]) +
											 jac[i][j][k].a32 * 1.0 * (campo[i][j+1][k]-campo[i][j][k]) +
											 jac[i][j][k].a33	* 1.0 * (campo[i][j][k]-campo[i][j][k-1]);	
		break;
		default:
			return;
	}
	i = 0;
	j = ny+1;
	k = nz+1;
	switch(d)
	{
		case dirX:
			(*deriv)[i][j][k] = jac[i][j][k].a11 * 1.0 * (campo[i+1][j][k]-campo[i][j][k]) +
											 jac[i][j][k].a12 * 1.0 * (campo[i][j][k]-campo[i][j-1][k]) +
											 jac[i][j][k].a13	* 1.0 * (campo[i][j][k]-campo[i][j][k-1]);	
		break;						
		case dirY:
			(*deriv)[i][j][k] = jac[i][j][k].a21 * 1.0 * (campo[i+1][j][k]-campo[i][j][k]) +
											 jac[i][j][k].a22 * 1.0 * (campo[i][j][k]-campo[i][j-1][k]) +
											 jac[i][j][k].a23	* 1.0 * (campo[i][j][k]-campo[i][j][k-1]);	
		break;
		case dirZ:
			(*deriv)[i][j][k] = jac[i][j][k].a31 * 1.0 * (campo[i+1][j][k]-campo[i][j][k]) +
											 jac[i][j][k].a32 * 1.0 * (campo[i][j][k]-campo[i][j-1][k]) +
											 jac[i][j][k].a33	* 1.0 * (campo[i][j][k]-campo[i][j][k-1]);	
		break;
		default:
			return;
	}

	i = nx+1;
	j = ny+1;
	k = nz+1;
	switch(d)
	{
		case dirX:
			(*deriv)[i][j][k] = jac[i][j][k].a11 * 1.0 * (campo[i][j][k]-campo[i-1][j][k]) +
											 jac[i][j][k].a12 * 1.0 * (campo[i][j][k]-campo[i][j-1][k]) +
											 jac[i][j][k].a13	* 1.0 * (campo[i][j][k]-campo[i][j][k-1]);	
		break;						
		case dirY:
			(*deriv)[i][j][k] = jac[i][j][k].a21 * 1.0 * (campo[i][j][k]-campo[i-1][j][k]) +
											 jac[i][j][k].a22 * 1.0 * (campo[i][j][k]-campo[i][j-1][k]) +
											 jac[i][j][k].a23	* 1.0 * (campo[i][j][k]-campo[i][j][k-1]);	
		break;
		case dirZ:
			(*deriv)[i][j][k] = jac[i][j][k].a31 * 1.0 * (campo[i][j][k]-campo[i-1][j][k]) +
											 jac[i][j][k].a32 * 1.0 * (campo[i][j][k]-campo[i][j-1][k]) +
											 jac[i][j][k].a33	* 1.0 * (campo[i][j][k]-campo[i][j][k-1]);	
		break;
		default:
			return;
	}
}
