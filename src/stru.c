/* Copyright, 2015, Santiago Lopez Castano
 *=========================================================================
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.

 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */
#include "stru.h"
void init_3dMatrix(int nx, int ny, int nz, struct matrix **** t)
{
	int j, i;

	//if((*t) != NULL)
		//dealloc_3d(nx,ny,(void****)t);
	dealloc_3d(nx,ny,(void****)t);

	(*t) = (struct matrix***)malloc((nx+2)*sizeof(*(*t)));
	for(i = 0; i < nx + 2; i++)
	{
		(*t)[i] = (struct matrix**)malloc((ny+2)*sizeof(**(*t)));
		for(j = 0; j < ny +2; j++)
			(*t)[i][j] = (struct matrix*)malloc((nz+2)*sizeof(***(*t)));
  }
}
void init_3dArray(int nx, int ny, int nz, double **** t)
{
	int j, i;

//	if((*t) != NULL)
//		dealloc_3d(nx,ny,(void****)t);
	dealloc_3d(nx,ny,(void****)t);

	(*t) = (double***)calloc((nx+2),sizeof(*(*t)));
	for(i = 0; i < nx + 2; i++)
	{
		(*t)[i] = (double **)calloc((ny+2),sizeof(**(*t)));
		for(j = 0; j < ny +2; j++)
			(*t)[i][j] = (double *)calloc((nz+2),sizeof(***(*t)));
  }
}
void init_null_field(struct field *f)
{
	int i;
	f->u=NULL;
	f->v=NULL;
	f->w=NULL;
	f->fi=NULL;
	f->re=NULL;
/*
	f->uc=NULL;
	f->vc=NULL;
	f->wc=NULL;
	f->div=NULL;
*/
	f->rho=NULL;
	f->lambda2=NULL;
	f->q=NULL;
	f->secInv=NULL;
	f->thirdInv=NULL;
	f->wwsecInv=NULL;
	f->wwthirdInv=NULL;
	f->pcsecInv=NULL;
	f->pcthirdInv=NULL;
	for(i=0;i!=4;++i)
		f->quadrants[i]= NULL;
	//f->S=NULL;
	f->k=NULL;
	for(i=0;i<3;++i)
		f->turb_t_vec[i]=NULL;
	f->prod=NULL;
	f->press_diff=NULL;
	f->turb_transp=NULL;
	f->visc_diff=NULL;
	f->dissip=NULL;
	f->deriv=NULL;
	f->strain=NULL;
	f->skew_strain=NULL;
	f->vortX=NULL;
	f->vortY=NULL;
	f->vortZ=NULL;
	f->skewnessW=NULL;
	f->kurtosisW=NULL;
	f->skewnessV=NULL;
	f->kurtosisV=NULL;
	f->skewnessU=NULL;
	f->kurtosisU=NULL;
	f->uv=NULL;
	f->visc_diss=NULL;
	f->anisotropy=NULL;
	f->ww=NULL;
	f->wwS=NULL;
	f->SLambda1=NULL;
	f->SLambda2=NULL;
	f->SLambda3=NULL;
	f->anisotrww=NULL;
	f->anisotrpc=NULL;
	f->pressureStrain=NULL;
	f->VISA=NULL;
}
void free_memory_gc	(int nx, int ny, struct cent* f)
{
	int i, j;

	for(i=0;i<nx+2;i++)
	{
		for(j=0;j<ny+2;j++)
			free((void *)f->invjac[i][j]);
		free((void *)f->invjac[i]);
	}
	free((void *)f->invjac);
	for(i=0;i<nx+2;i++)
	{
		for(j=0;j<ny+2;j++)
			free((void *)f->jac[i][j]);
		free((void *)f->jac[i]);
	}
	free((void *)f->jac);
	for(i=0;i<nx+2;i++)
	{
		for(j=0;j<ny+2;j++)
			free((void *)f->zc[i][j]);
		free((void *)f->zc[i]);
	}
	free((void *)f->zc);
	for(i=0;i<nx+2;i++)
	{
		for(j=0;j<ny+2;j++)
			free((void *)f->yc[i][j]);
		free((void *)f->yc[i]);
	}
	free((void *)f->yc);
	for(i=0;i<nx+2;i++)
	{
		for(j=0;j<ny+2;j++)
			free((void *)f->xc[i][j]);
		free((void *)f->xc[i]);
	}
	free((void *)f->xc);
}
void dealloc_3d(int nx, int ny, void ****ptr)
{
	
	int i, j;
	if((*ptr) != NULL)
	{
		for(i=0;i<nx+2;i++)
		{
			for(j=0;j<ny+2;j++)
				free((void *)(*ptr)[i][j]);
			free((void *)(*ptr)[i]);
		}
		free((void *)(*ptr));
		*ptr = NULL;
	}
}
void free_memory_budget	(int nx, int ny, int nscal, struct field* f)
{
	int  k;
	
	dealloc_3d(nx,ny,(void*)&(f->turb_transp));
	dealloc_3d(nx,ny,(void*)&(f->visc_diss));
	for(k=0;k<3;++k)
		dealloc_3d(nx,ny,(void*)&(f->turb_t_vec[k]));

	dealloc_3d(nx,ny,(void*)&(f->visc_diff));
	dealloc_3d(nx,ny,(void*)&(f->prod));
}
void free_memory_f	(int nx, int ny, int nscal, struct field* f)
{
	int i, j, k;
  if(f->rho != NULL){  
    for(k=0;k<nscal;++k)
    {
      dealloc_3d(nx,ny,(void*)&f->rho[k]);
  /*
      for(i=0;i<nx+2;i++)
      {
        for(j=0;j<ny+2;j++)
          free((void *)f->rho[k][i][j]);
        free((void *)f->rho[k][i]);
      }
      free((void *)f->rho[k]);
  */
    }
    free((void *)f->rho);
  }
	dealloc_3d(nx,ny,(void*)&(f->anisotrww));
	dealloc_3d(nx,ny,(void*)&(f->anisotropy));
	dealloc_3d(nx,ny,(void*)&(f->uv));
	dealloc_3d(nx,ny,(void*)&(f->ww));
	dealloc_3d(nx,ny,(void*)&(f->wwsecInv));
	dealloc_3d(nx,ny,(void*)&(f->wwthirdInv));
	dealloc_3d(nx,ny,(void*)&(f->secInv));
	dealloc_3d(nx,ny,(void*)&(f->thirdInv));
	dealloc_3d(nx,ny,(void*)&(f->skew_strain));
	dealloc_3d(nx,ny,(void*)&(f->strain));
	dealloc_3d(nx,ny,(void*)&(f->deriv));
	dealloc_3d(nx,ny,(void*)&(f->lambda2));
	dealloc_3d(nx,ny,(void*)&(f->VISA));
	dealloc_3d(nx,ny,(void*)&(f->vortX));
	dealloc_3d(nx,ny,(void*)&(f->vortY));
	dealloc_3d(nx,ny,(void*)&(f->vortZ));

	//dealloc_3d(nx,ny,(void*)&(f->div));
	dealloc_3d(nx,ny,(void*)&(f->w));
	dealloc_3d(nx,ny,(void*)&(f->v));
	dealloc_3d(nx,ny,(void*)&(f->u));
	dealloc_3d(nx,ny,(void*)&(f->fi));
//	if(f->S != NULL)
//		free((void*) f->S);	
	for(k = 0; k < 4; k++)
		if(f->quadrants[k] != NULL)
			free((void*) f->quadrants[k]);
/*
  if(f->wc != NULL){  
    for(i=0;i<nx;i++)
    {
      for(j=0;j<ny;j++)
        free((void *)f->wc[i][j]);
      free((void *)f->wc[i]);
    }
    free((void *)f->wc);
  }
  if(f->vc != NULL){  
    for(i=0;i<nx;i++)
    {
      for(j=0;j<ny+1;j++)
        free((void *)f->vc[i][j]);
      free((void *)f->vc[i]);
    }
    free((void *)f->vc);
  }
  if(f->uc != NULL){  
    for(i=0;i<nx+1;i++)
    {
      for(j=0;j<ny;j++)
        free((void *)f->uc[i][j]);
      free((void *)f->uc[i]);
    }
    free((void *)f->uc);
  }
	for(i=0;i<nx+2;i++)
	{
		for(j=0;j<ny+2;j++)
			free((void *)f->fi[i][j]);
		free((void *)f->fi[i]);
	}
	free((void *)f->fi);
	for(i=0;i<nx+2;i++)
	{
		for(j=0;j<ny+2;j++)
			free((void *)f->w[i][j]);
		free((void *)f->w[i]);
	}
	free((void *)f->w);
	for(i=0;i<nx+2;i++)
	{
		for(j=0;j<ny+2;j++)
			free((void *)f->v[i][j]);
		free((void *)f->v[i]);
	}
	free((void *)f->v);
	for(i=0;i<nx+2;i++)
	{
		for(j=0;j<ny+2;j++)
			free((void *)f->u[i][j]);
		free((void *)f->u[i]);
	}
	free((void *)f->u);
	for(i=0;i<nx+2;i++)
	{
		for(j=0;j<ny+2;j++)
			free((void *)f->div[i][j]);
		free((void *)f->div[i]);
	}
	free((void *)f->div);
*/
}
double matrixTrace(struct matrix a)
{
	return a.a11+a.a22+a.a33;
}
struct matrix scalarMatrixMult(double a,
															 struct matrix r)
{
	struct matrix ret;

	ret.a11 = a*r.a11;
	ret.a12 = a*r.a12;
	ret.a13 = a*r.a13;

	ret.a21 = a*r.a21;
	ret.a22 = a*r.a22;
	ret.a23 = a*r.a23;

	ret.a31 = a*r.a31;
	ret.a32 = a*r.a32;
	ret.a33 = a*r.a33;
	return ret;
}
struct matrix matrixSum(const struct matrix l,
												const struct matrix r)
{
	struct matrix ret;

	ret.a11 = l.a11+r.a11;
	ret.a12 = l.a12+r.a12;
	ret.a13 = l.a13+r.a13;

	ret.a21 = l.a21+r.a21;
	ret.a22 = l.a22+r.a22;
	ret.a23 = l.a23+r.a23;
	
	ret.a31 = l.a31+r.a31;
	ret.a32 = l.a32+r.a32;
	ret.a33 = l.a33+r.a33;

	return ret;
}
struct matrix matrixTranspose(const struct matrix a)
{
	struct matrix ret;

	ret.a11 = a.a11;
	ret.a12 = a.a21;
	ret.a13 = a.a31;

	ret.a21 = a.a12;
	ret.a22 = a.a22;
	ret.a23 = a.a32;
	
	ret.a31 = a.a13;
	ret.a32 = a.a23;
	ret.a33 = a.a33;

	return ret;
}
struct matrix matrixRest(const struct matrix l,
												const struct matrix r)
{
	struct matrix ret;

	ret.a11 = l.a11-r.a11;
	ret.a12 = l.a12-r.a12;
	ret.a13 = l.a13-r.a13;

	ret.a21 = l.a21-r.a21;
	ret.a22 = l.a22-r.a22;
	ret.a23 = l.a23-r.a23;
	
	ret.a31 = l.a31-r.a31;
	ret.a32 = l.a32-r.a32;
	ret.a33 = l.a33-r.a33;

	return ret;
}
struct matrix matMul(const struct matrix l,
										 const struct matrix r)
{
	struct matrix ret;

	ret.a11 = l.a11*r.a11 + l.a12*r.a21 + l.a13*r.a31;
	ret.a21 = l.a21*r.a11 + l.a22*r.a21 + l.a23*r.a31;
	ret.a31 = l.a31*r.a11 + l.a32*r.a21 + l.a33*r.a31;

	ret.a12 = l.a11*r.a12 + l.a12*r.a22 + l.a13*r.a32;
	ret.a22 = l.a21*r.a12 + l.a22*r.a22 + l.a23*r.a32;
	ret.a32 = l.a31*r.a12 + l.a32*r.a22 + l.a33*r.a32;

	ret.a13 = l.a11*r.a13 + l.a12*r.a23 + l.a13*r.a33;
	ret.a23 = l.a21*r.a13 + l.a22*r.a23 + l.a23*r.a33;
	ret.a33 = l.a31*r.a13 + l.a32*r.a23 + l.a33*r.a33;

	return ret;
}
double matrixDeterminant(struct matrix a)
{
	return (a.a11)*(a.a22*a.a33 - a.a32*a.a23)
 				-(a.a12)*(a.a21*a.a33 - a.a31*a.a23)
 				+(a.a13)*(a.a21*a.a32 - a.a31*a.a22);
}
void passTensorElemTo3dArray(int nx,int ny,int nz, int elem,
														 struct matrix ****a, 
                             double ****b)
{
	int i, j, k;

	if(elem > 8 || elem < 0)
		return;

	init_3dArray(nx,ny,nz,b);

	switch(elem){
	case 0:
		for(i = 0; i < nx + 2; i++)	
		for(j = 0; j < ny + 2; j++)	
		for(k = 0; k < nz + 2; k++)
		(*b)[i][j][k] = (*a)[i][j][k].a11;	
	case 1:
		for(i = 0; i < nx + 2; i++)	
		for(j = 0; j < ny + 2; j++)	
		for(k = 0; k < nz + 2; k++)
		(*b)[i][j][k] = (*a)[i][j][k].a12;	
	case 2:
		for(i = 0; i < nx + 2; i++)	
		for(j = 0; j < ny + 2; j++)	
		for(k = 0; k < nz + 2; k++)
		(*b)[i][j][k] = (*a)[i][j][k].a13;	
	case 3:
		for(i = 0; i < nx + 2; i++)	
		for(j = 0; j < ny + 2; j++)	
		for(k = 0; k < nz + 2; k++)
		(*b)[i][j][k] = (*a)[i][j][k].a21;	
	case 4:
		for(i = 0; i < nx + 2; i++)	
		for(j = 0; j < ny + 2; j++)	
		for(k = 0; k < nz + 2; k++)
		(*b)[i][j][k] = (*a)[i][j][k].a22;	
	case 5:
		for(i = 0; i < nx + 2; i++)	
		for(j = 0; j < ny + 2; j++)	
		for(k = 0; k < nz + 2; k++)
		(*b)[i][j][k] = (*a)[i][j][k].a23;	
	case 6:
		for(i = 0; i < nx + 2; i++)	
		for(j = 0; j < ny + 2; j++)	
		for(k = 0; k < nz + 2; k++)
		(*b)[i][j][k] = (*a)[i][j][k].a31;	
	case 7:
		for(i = 0; i < nx + 2; i++)	
		for(j = 0; j < ny + 2; j++)	
		for(k = 0; k < nz + 2; k++)
		(*b)[i][j][k] = (*a)[i][j][k].a32;	
	case 8:
		for(i = 0; i < nx + 2; i++)	
		for(j = 0; j < ny + 2; j++)	
		for(k = 0; k < nz + 2; k++)
		(*b)[i][j][k] = (*a)[i][j][k].a33;	
	}
}
void sumFields(int nx, int ny, int nz, double ***a, double ***b, double ****c)
{
	int i, j, k;

	init_3dArray(nx,ny,nz,c);

	for(i = 0; i < nx + 2; i++)	
	for(j = 0; j < ny + 2; j++)	
	for(k = 0; k < nz + 2; k++)
		(*c)[i][j][k] = a[i][j][k] + b[i][j][k];
}
