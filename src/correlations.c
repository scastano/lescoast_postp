#include <stdlib.h>
#include <math.h>
#include "correlations.h"

void correlationX(int nx, int nz, int j,
									double ***const ui,
									double ***const uj,
									double **corrLine)
{
	int i, k;
	double val;
	double rms1, rms2;

  if (*corrLine != NULL)
		free((void*)*corrLine);	

  *corrLine = malloc((nx+2)*sizeof(double));	
	for (i = 0; i < nx+2; i++)
	{
		val = 0.0;
		for (k = 0; k < nz+2; k++)
			val += ui[0][j][k]*uj[i][j][k];
		(*corrLine)[i] = val/((double)(nz+2));
	}
// ui RMS
	val = 0.0;
	for (i = 0; i < nx+2; i++)
		for (k = 0; k < nz+2; k++)
			val += ui[i][j][k];
	val /= ((double)(nz+2))*((double)(nx+2));

	for (i = 0; i < nx+2; i++)
		for (k = 0; k < nz+2; k++)
			rms1 += (ui[i][j][k]-val)*(ui[i][j][k]-val);
	
	rms1 = sqrt(rms1/((double)(nz+2))/((double)(nx+2)));

// uj RMS
	val = 0.0;
	for (i = 0; i < nx+2; i++)
		for (k = 0; k < nz+2; k++)
			val += uj[i][j][k];
	val /= ((double)(nz+2))*((double)(nx+2));

	for (i = 0; i < nx+2; i++)
		for (k = 0; k < nz+2; k++)
			rms2 += (uj[i][j][k]-val)*(uj[i][j][k]-val);
	
	rms2 = sqrt(rms2/((double)(nz+2))/((double)(nx+2)));

	val = rms1*rms2;
	for (i = 0; i < nx+2; i++) 
		(*corrLine)[i] /= val;
}
void correlationTauVert(	int nx, int ny, int nz,int jj, int time,
													const struct field *pert,
													double **corrLine)
{
	int  j, t;
	double val, *partialCorr=NULL;

  if (*corrLine != NULL)
		free((void*)*corrLine);

  *corrLine = calloc((ny+2-jj),sizeof(double));	
	for (t = 0; t < time; t++)
	{
		correlationYStrain(nx,ny,nz, jj,pert[t].strain,&partialCorr);
				
		for (j = 0; j < ny+2-jj; j++)
			(*corrLine)[j] += partialCorr[j];
	}
	for (j = 0; j < ny+2-jj; j++)
		(*corrLine)[j] /= time;
	free((void*)partialCorr);	
}
void correlationXMean(int nx, int ny, int nz, int j, int time,
											const struct field *pert,
											double **corrLine)
{
	int i, ii;
	double *partial=NULL;

  if (*corrLine != NULL)
		free((void*)*corrLine);
	
  *corrLine = calloc((nx+2),sizeof(double));
	for(ii=0; ii<time;ii++)
	{
		correlationX(nx,nz,j,pert[ii].u, pert[ii].u, &partial);
		for (i = 0; i < nx+2; i++)
			(*corrLine)[i] += partial[i];
	}	
	for (i = 0; i < nx+2; i++)
		(*corrLine)[i] /= (double)time;
}
void correlationYStrain(int nx, int ny, int nz,int jj,
												struct matrix *** const ui,
												double **corrLine)
{
	int i, j, k;
	double val;


  if (*corrLine != NULL)
		free((void*)*corrLine);	

  *corrLine = malloc((ny+2-jj)*sizeof(double));	

	for (j = jj; j < ny+2; j++) 
	{
		val = 0.0;
		for (k = 0; k < nz+2; k++)
			for (i = 0; i < nx+2; i++)
				val += ui[i][jj][k].a12*ui[i][j][k].a12;
		(*corrLine)[j-jj] = val;///pow((double)(nx+2)*(nz+2),2.0);
	}
	val = 0.0;
	for (k = 0; k < nz+2; k++)
		for (i = 0; i < nx+2; i++)
			val += ui[i][jj][k].a12*ui[i][jj][k].a12;
	//val /= pow((double)(nx+2)*(nz+2),2.0);
	for (j = 0; j < ny+2-jj; j++) 
		(*corrLine)[j] /= val;
}

void correlationY(int nx, int ny, int nz,
									const double ***ui,
									const double ***uj,
									double **corrLine)
{
	int i, j, k;
	double val;


  if (*corrLine != NULL)
		free((void*)*corrLine);	

  *corrLine = malloc((ny+2)*sizeof(double));	

	for (j = 0; j < ny+2; j++) 
	{
		val = 0.0;
		for (k = 0; k < nz+2; k++)
			for (i = 0; i < nx+2; i++)
				val += ui[i][0][k]*uj[i][j][k];
		(*corrLine)[j] = val/((double)(nx+2)*(nz+2));
	}
	val = 0.0;
	for (k = 0; k < nz+2; k++)
		for (i = 0; i < nx+2; i++)
			val += ui[i][0][k]*uj[i][0][k];
	val /= ((double)(nx+2)*(nz+2));
	for (j = 0; j < ny+2; j++) 
		(*corrLine)[j] /= val;
}

void correlationYX(	int nx, int ny, int nz, int k, int time,
										const struct field *pert,
										double **corrLine)
{
	int i, j, t, jinit = 1;
	double val;

  if (*corrLine != NULL)
		free((void*)*corrLine);	

  *corrLine = malloc((ny+2)*sizeof(double));	
	for (j = jinit; j < ny+2; j++)
	{
		val = 0.0;
		for (t = 0; t < time; t++)
			for (i = 0; i < nx+2; i++)
				val += pert[t].u[i][jinit][k]*pert[t].u[i][j][k];
		(*corrLine)[j] = val/((double)(nx+2)*t);
	}
	val = 0.0;
	for (t = 0; t < time; t++)
		for (i = 0; i < nx+2; i++)
			val += pert[t].u[i][jinit][k]*pert[t].u[i][jinit][k];

	val /= ((double)(nx+2)*t);

	for (j = jinit; j < ny+2; j++) 
		(*corrLine)[j] /= val;
	for (j = 0; j < jinit; j++) 
		(*corrLine)[j] = 1.0;
}
void correlationYY(	int nx, int ny, int nz, int k, int time,
										const struct field *pert,
										double **corrLine)
{
	int i, j, t, jinit = 1;
	double val;

  if (*corrLine != NULL)
		free((void*)*corrLine);	

  *corrLine = malloc((ny+2)*sizeof(double));	
	for (j = jinit; j < ny+2; j++)
	{
		val = 0.0;
		for (t = 0; t < time; t++)
			for (i = 0; i < nx+2; i++)
				val += pert[t].v[i][jinit][k]*pert[t].v[i][j][k];
		(*corrLine)[j] = val/((double)(nx+2)*t);
	}
	val = 0.0;
	for (t = 0; t < time; t++)
		for (i = 0; i < nx+2; i++)
			val += pert[t].v[i][jinit][k]*pert[t].v[i][jinit][k];

	val /= ((double)(nx+2)*t);

	for (j = jinit; j < ny+2; j++) 
		(*corrLine)[j] /= val;
	for (j = 0; j < jinit; j++) 
		(*corrLine)[j] = 1.0;
}
void correlationXX(	int nx, int ny, int nz, int j, int time,
										const struct field *pert,
										double **corrLine)
{
	int i, k, t, xinit = 0;
	double val;

  if (*corrLine != NULL)
		free((void*)*corrLine);	

	t = 1;
  *corrLine = malloc((nx+2)*sizeof(double));	
	for (i = xinit; i < nx+2; i++)
	{
		val = 0.0;
		//for (t = 0; t < time; t++)
			for (k = 0; k < nz+2; k++)
				val += pert[t].u[xinit][j][k]*pert[t].u[i][j][k];
		(*corrLine)[i] = val/((double)(nz+2)*t);
	}
	val = 0.0;
	//for (t = 0; t < time; t++)
		for (k = 0; k < nz+2; k++)
			val += pert[t].u[xinit][j][k]*pert[t].u[xinit][j][k];

	val /= ((double)(nz+2)*t);

	for (i = 0; i < xinit; i++) 
		(*corrLine)[i] = 1;
	for (i = xinit; i < nx+2; i++) 
		(*corrLine)[i] /= val;
}
