/* Copyright, 2015, Santiago Lopez Castano
 *=========================================================================
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.

 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */
#include <stdlib.h>
#include <math.h>
#include "stru.h"
#include "PDF.h"

int comp (const void* a, const void* b)
{
	double aa = *((double*)a);
	double bb = *((double*)b);
	if (aa > bb) return  1;
	if (aa < bb) return -1;
	return 0;
}

double VarMatr (
								int nx, int  j, int nz,
								struct matrix **** const matr 
							 )
{
	int i, k;
	double ret;

	for(i = 0; i < nx + 2; i++)
		for(k = 0; k < nz + 2; k++)
			ret = (*matr)[i][j][k].a12*(*matr)[i][j][k].a12/((nx+2)*(nz+2));	
	return ret;
}
void PDFMatr (
							int nx, int  j, int nz,
							int bins,
							struct matrix **** const matr, 
							double ** list,
							double ** l
						 )
{
	int i, k, mm;
  double m[(nz+2)*(nx+2)],dx,min;
	
	
//	*m = malloc((nz+2)*(nx+2)*sizeof(double));
	mm = 0;
	for(i = 0; i < nx + 2; i++)
		for(k = 0; k < nz + 2; k++)
			m[mm++] = (*matr)[i][j][k].a12;

	mm = (nz+2)*(nx+2);
	qsort (m, mm, sizeof(*m), comp);
	*list = calloc(bins+2,sizeof(double));
	*l = calloc(bins,sizeof(double));
	
	dx  =(abs(m[0])-abs(m[mm-1]))/bins;
	min = m[0];
	mm = 0;
	for(j = 0; j < (nz+2)*(nx+2) + 1; j++)
		if((m[j] >= (min + dx*mm))&&(m[j] < (min + dx*(mm+1))))
			(*list)[mm]+=1.0/(nz+2)*(nx+2);
		else
			++mm;
	(*list)[bins+1] = m[bins-1];
	(*list)[bins] = m[0];
	for(j = 0; j < bins; j++)
		(*l)[j] = 0.5*((min + dx*j) + (min + dx*(j+1)));
}
void PDFList (
							int nx, int  j, int nz,
							struct matrix **** const matr, 
							double ** m
						 )
{
	int i, k, mm;
  double dx;
	
	
	*m = malloc((nz+2)*(nx+2)*sizeof(**m));
	mm = 0;
	for(i = 0; i < nx + 2; i++)
		for(k = 0; k < nz + 2; k++)
			(*m)[mm++] = (*matr)[i][j][k].a12;

	mm = (nz+2)*(nx+2);
	qsort (*m, mm, sizeof(*m), comp);
}
